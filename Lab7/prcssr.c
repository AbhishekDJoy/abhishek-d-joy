#include<stdio.h>
#include<stdlib.h>
#include<signal.h>
#include<unistd.h>
#include<time.h>
#include<sys/stat.h>
#include<sys/wait.h>
#include<sys/types.h>
#include<fcntl.h>


#define end_point 50





 int fd_ch1[2],fd_ch2[2],fd_ch3[2],fd_ch4[2],fd_ch5[2],fd_ch6[2],fd_ch7[2],fd_ch8[2];


void turtlein()
{
int trt_posn=0;
        while(1)
        {
            read(fd_ch3[0],&trt_posn,sizeof(int));
            trt_posn=trt_posn+2;
            write(fd_ch6[1],&trt_posn,sizeof(int));
            write(fd_ch2[1],&trt_posn,sizeof(int));
        }
        exit(0);
}
void harein(){
 int hre_posn=0;
        int trt_posn=0;
       
        int min = 5, sleep_cnt=0;
       
        int sleep_TIME = rand()%10;
       
        while(1)
        {
            if(sleep_cnt!=0)
            {
                sleep_cnt = sleep_cnt+1;
                read(fd_ch5[0],&hre_posn,sizeof(int));
                read(fd_ch6[0],&trt_posn,sizeof(int));
                write(fd_ch4[1],&hre_posn,sizeof(int));

                if(sleep_cnt>=sleep_TIME)
                    sleep_cnt = 0;
               
                continue;
            }

            read(fd_ch5[0],&hre_posn,sizeof(int));
            read(fd_ch6[0],&trt_posn,sizeof(int));
            hre_posn=hre_posn+4;
            write(fd_ch4[1],&hre_posn,sizeof(int));
            if(hre_posn-trt_posn>=8)
            {
                sleep_cnt=1;
            }
        }
        exit(0);       
}
int main()
{
   

    pipe(fd_ch1);
    pipe(fd_ch2);
    pipe(fd_ch3);
    pipe(fd_ch4);
    pipe(fd_ch5);
    pipe(fd_ch6);
    pipe(fd_ch7);
    pipe(fd_ch8);

    int hre_posn=0;
    int trt_posn=0;
    pid_t gd,tut,hre,ref;

    tut=fork();
    if(tut==0)
    {
        turtlein();
}
    write(fd_ch3[1],&trt_posn,sizeof(int));

    hre=fork();
    if(hre==0)
    {
        harein();
    }
    write(fd_ch5[1],&hre_posn,sizeof(int));
   
    ref=fork();
    if(ref==0)
    {
        int trt_posn, hre_posn;
        int val=0;
        while(1)
        {
            read(fd_ch7[0],&trt_posn,sizeof(int));
            read(fd_ch7[0],&hre_posn,sizeof(int));
            printf("position of the Turtle: %d\n",trt_posn);
            printf("position of the HARE: %d\n",hre_posn);
            write(fd_ch8[1],&val,sizeof(int));
            if(trt_posn>=50 || hre_posn>=50)
            {
                break;
            }
        }
        exit(0);
    }
    int val=0;
    int win_val=0;
    while(1)
    {
        read(fd_ch2[0],&trt_posn,sizeof(int));
        read(fd_ch4[0],&hre_posn,sizeof(int));
        srand(time(0));
        int chance=rand()%2;
        if(chance==1)
        {
            gd=fork();
            if(gd==0)
            {
                int trt_posn, hre_posn;
                printf("give the new turtle position : ");
                scanf("%d",&trt_posn);
                printf("give the new hare position : ");
                scanf("%d",&hre_posn);
                write(fd_ch1[1],&trt_posn,sizeof(int));
                write(fd_ch1[1],&hre_posn,sizeof(int));
                exit(0);
            }
            read(fd_ch1[0],&trt_posn,sizeof(int));
            read(fd_ch1[0],&hre_posn,sizeof(int));
            wait(0);
        }
        if(hre_posn>=end_point)
        {
            win_val=1;
            break;
        }
        else if(trt_posn>=end_point)
        {
            break;
        }
        write(fd_ch7[1],&trt_posn,sizeof(int));
        write(fd_ch7[1],&hre_posn,sizeof(int));
        write(fd_ch3[1],&trt_posn,sizeof(int));
        write(fd_ch5[1],&hre_posn,sizeof(int));
        read(fd_ch8[0],&val,sizeof(int));
    }

    write(fd_ch7[1],&trt_posn,sizeof(int));
    write(fd_ch7[1],&hre_posn,sizeof(int));
    write(fd_ch3[1],&trt_posn,sizeof(int));
    write(fd_ch5[1],&hre_posn,sizeof(int));
    read(fd_ch8[0],&val,sizeof(int));

    if(win_val==0){
        printf("\nTURTLE WINS THE RACE\n");
    }
    else
    {
        printf("\nHARE WINS THE RACE\n");
    }
    kill(hre,SIGTERM);
    kill(tut,SIGTERM);
    kill(ref,SIGTERM);
    return 0;
}
