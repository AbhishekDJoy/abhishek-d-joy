#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<pthread.h>
#include<time.h>
#include<signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/stat.h>

#define raceend 2000

int harepos =0, turtpos =0,hareti=0, turtleti=0;



pthread_mutex_t tmut= PTHREAD_MUTEX_INITIALIZER; 
pthread_mutex_t hmut= PTHREAD_MUTEX_INITIALIZER; 
pthread_mutex_t cmut= PTHREAD_MUTEX_INITIALIZER;

void *turtle(void *args){

    while(turtpos < raceend) 
    {

        pthread_mutex_lock(&tmut);

        turtpos++;

        turtleti++;

        pthread_mutex_unlock(&tmut);
    }

	return NULL;
}

void *hare(void *args){

	while(harepos < raceend )
	{

		if(harepos - turtpos >= 80) 
		{
			srand(time(0));

			int sleep =rand()%100;
 			hareti= hareti+sleep;

			srand(time(0));

			int rand_sleep=rand()%60;
			usleep(rand_sleep);
		}
		else
		{

			pthread_mutex_lock(&hmut);
	 		harepos=harepos+6;
			hareti++;
			pthread_mutex_unlock(&hmut);
		}
	}
	return NULL;
}



void *reporter (void *args){

	while(turtpos < raceend || harepos < raceend)
	{
	pthread_mutex_lock(&cmut);

	printf("\nPosition of turtle is : %d ,Iteration : %d\n", turtpos, turtleti);
 	printf("Position of hare is : %d, Iteration : %d\n" ,harepos, hareti);

	pthread_mutex_unlock(&cmut);
 	usleep(50);
	}

	return NULL;
}


void *god(void *args)
{

	while(turtpos <raceend || harepos < raceend)
	{

		pthread_mutex_lock(&cmut); 
		pthread_mutex_lock(&tmut);

		pthread_mutex_lock(&hmut);

	double c = (double) rand() / (RAND_MAX);
	if(c>=0.8)
	{

		if(turtpos < raceend)
		{

			printf(" The position of turtle changes to :");
 			scanf("%d",&turtpos);
		}


		if(harepos< raceend)
		{

			printf("The position of hare changes to : ");
	
			scanf("%d",&harepos);

		}
	}

	else 
	{

    		pthread_mutex_unlock (&cmut);

 	   	pthread_mutex_unlock (&tmut); 
    		pthread_mutex_unlock (&hmut);

    		usleep(60);

    		continue;
	}

	pthread_mutex_unlock (&cmut);

	pthread_mutex_unlock (&tmut);
 	pthread_mutex_unlock (&hmut);
	}

return NULL;
}
int main()
{

        srand(time(0));

        pthread_t tid_t,tid_h,tid_r,tid_g;



        pthread_create(&tid_t,NULL, turtle, NULL);
        pthread_create(&tid_h, NULL, hare,NULL);
        pthread_create(&tid_r,NULL, reporter,NULL);
        pthread_create(&tid_g, NULL,god, NULL);

        pthread_join(tid_t,NULL);

        pthread_join(tid_h,NULL);

        pthread_join(tid_r,NULL);
        pthread_join(tid_g,NULL);

        printf("\n The time take by hare: %d\n",hareti);
        printf("\nThe time take by turtle : %d\n", turtleti);

        if(turtleti < hareti)
        {
        	printf("\n Winner is the Turtle\n");
        }

        else if(turtleti > hareti)
        {
		printf("\n Winner is the Hare\n");
        }

        else
        {
		printf("\n Race is draw\n");
        }

        return 0;
}
