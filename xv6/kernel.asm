
kernel:     file format elf32-i386


Disassembly of section .text:

80100000 <multiboot_header>:
80100000:	02 b0 ad 1b 00 00    	add    0x1bad(%eax),%dh
80100006:	00 00                	add    %al,(%eax)
80100008:	fe 4f 52             	decb   0x52(%edi)
8010000b:	e4                   	.byte 0xe4

8010000c <entry>:

# Entering xv6 on boot processor, with paging off.
.globl entry
entry:
  # Turn on page size extension for 4Mbyte pages
  movl    %cr4, %eax
8010000c:	0f 20 e0             	mov    %cr4,%eax
  orl     $(CR4_PSE), %eax
8010000f:	83 c8 10             	or     $0x10,%eax
  movl    %eax, %cr4
80100012:	0f 22 e0             	mov    %eax,%cr4
  # Set page directory
  movl    $(V2P_WO(entrypgdir)), %eax
80100015:	b8 00 90 10 00       	mov    $0x109000,%eax
  movl    %eax, %cr3
8010001a:	0f 22 d8             	mov    %eax,%cr3
  # Turn on paging.
  movl    %cr0, %eax
8010001d:	0f 20 c0             	mov    %cr0,%eax
  orl     $(CR0_PG|CR0_WP), %eax
80100020:	0d 00 00 01 80       	or     $0x80010000,%eax
  movl    %eax, %cr0
80100025:	0f 22 c0             	mov    %eax,%cr0

  # Set up the stack pointer.
  movl $(stack + KSTACKSIZE), %esp
80100028:	bc c0 b5 10 80       	mov    $0x8010b5c0,%esp

  # Jump to main(), and switch to executing at
  # high addresses. The indirect call is needed because
  # the assembler produces a PC-relative instruction
  # for a direct jump.
  mov $main, %eax
8010002d:	b8 60 30 10 80       	mov    $0x80103060,%eax
  jmp *%eax
80100032:	ff e0                	jmp    *%eax
80100034:	66 90                	xchg   %ax,%ax
80100036:	66 90                	xchg   %ax,%ax
80100038:	66 90                	xchg   %ax,%ax
8010003a:	66 90                	xchg   %ax,%ax
8010003c:	66 90                	xchg   %ax,%ax
8010003e:	66 90                	xchg   %ax,%ax

80100040 <binit>:
  struct buf head;
} bcache;

void
binit(void)
{
80100040:	f3 0f 1e fb          	endbr32 
80100044:	55                   	push   %ebp
80100045:	89 e5                	mov    %esp,%ebp
80100047:	53                   	push   %ebx

//PAGEBREAK!
  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
  bcache.head.next = &bcache.head;
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
80100048:	bb f4 b5 10 80       	mov    $0x8010b5f4,%ebx
{
8010004d:	83 ec 0c             	sub    $0xc,%esp
  initlock(&bcache.lock, "bcache");
80100050:	68 00 72 10 80       	push   $0x80107200
80100055:	68 c0 b5 10 80       	push   $0x8010b5c0
8010005a:	e8 91 44 00 00       	call   801044f0 <initlock>
  bcache.head.next = &bcache.head;
8010005f:	83 c4 10             	add    $0x10,%esp
80100062:	b8 bc fc 10 80       	mov    $0x8010fcbc,%eax
  bcache.head.prev = &bcache.head;
80100067:	c7 05 0c fd 10 80 bc 	movl   $0x8010fcbc,0x8010fd0c
8010006e:	fc 10 80 
  bcache.head.next = &bcache.head;
80100071:	c7 05 10 fd 10 80 bc 	movl   $0x8010fcbc,0x8010fd10
80100078:	fc 10 80 
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
8010007b:	eb 05                	jmp    80100082 <binit+0x42>
8010007d:	8d 76 00             	lea    0x0(%esi),%esi
80100080:	89 d3                	mov    %edx,%ebx
    b->next = bcache.head.next;
80100082:	89 43 54             	mov    %eax,0x54(%ebx)
    b->prev = &bcache.head;
    initsleeplock(&b->lock, "buffer");
80100085:	83 ec 08             	sub    $0x8,%esp
80100088:	8d 43 0c             	lea    0xc(%ebx),%eax
    b->prev = &bcache.head;
8010008b:	c7 43 50 bc fc 10 80 	movl   $0x8010fcbc,0x50(%ebx)
    initsleeplock(&b->lock, "buffer");
80100092:	68 07 72 10 80       	push   $0x80107207
80100097:	50                   	push   %eax
80100098:	e8 13 43 00 00       	call   801043b0 <initsleeplock>
    bcache.head.next->prev = b;
8010009d:	a1 10 fd 10 80       	mov    0x8010fd10,%eax
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
801000a2:	8d 93 5c 02 00 00    	lea    0x25c(%ebx),%edx
801000a8:	83 c4 10             	add    $0x10,%esp
    bcache.head.next->prev = b;
801000ab:	89 58 50             	mov    %ebx,0x50(%eax)
    bcache.head.next = b;
801000ae:	89 d8                	mov    %ebx,%eax
801000b0:	89 1d 10 fd 10 80    	mov    %ebx,0x8010fd10
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
801000b6:	81 fb 60 fa 10 80    	cmp    $0x8010fa60,%ebx
801000bc:	75 c2                	jne    80100080 <binit+0x40>
  }
}
801000be:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801000c1:	c9                   	leave  
801000c2:	c3                   	ret    
801000c3:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801000ca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801000d0 <bread>:
}

// Return a locked buf with the contents of the indicated block.
struct buf*
bread(uint dev, uint blockno)
{
801000d0:	f3 0f 1e fb          	endbr32 
801000d4:	55                   	push   %ebp
801000d5:	89 e5                	mov    %esp,%ebp
801000d7:	57                   	push   %edi
801000d8:	56                   	push   %esi
801000d9:	53                   	push   %ebx
801000da:	83 ec 18             	sub    $0x18,%esp
801000dd:	8b 7d 08             	mov    0x8(%ebp),%edi
801000e0:	8b 75 0c             	mov    0xc(%ebp),%esi
  acquire(&bcache.lock);
801000e3:	68 c0 b5 10 80       	push   $0x8010b5c0
801000e8:	e8 83 45 00 00       	call   80104670 <acquire>
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
801000ed:	8b 1d 10 fd 10 80    	mov    0x8010fd10,%ebx
801000f3:	83 c4 10             	add    $0x10,%esp
801000f6:	81 fb bc fc 10 80    	cmp    $0x8010fcbc,%ebx
801000fc:	75 0d                	jne    8010010b <bread+0x3b>
801000fe:	eb 20                	jmp    80100120 <bread+0x50>
80100100:	8b 5b 54             	mov    0x54(%ebx),%ebx
80100103:	81 fb bc fc 10 80    	cmp    $0x8010fcbc,%ebx
80100109:	74 15                	je     80100120 <bread+0x50>
    if(b->dev == dev && b->blockno == blockno){
8010010b:	3b 7b 04             	cmp    0x4(%ebx),%edi
8010010e:	75 f0                	jne    80100100 <bread+0x30>
80100110:	3b 73 08             	cmp    0x8(%ebx),%esi
80100113:	75 eb                	jne    80100100 <bread+0x30>
      b->refcnt++;
80100115:	83 43 4c 01          	addl   $0x1,0x4c(%ebx)
      release(&bcache.lock);
80100119:	eb 3f                	jmp    8010015a <bread+0x8a>
8010011b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010011f:	90                   	nop
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100120:	8b 1d 0c fd 10 80    	mov    0x8010fd0c,%ebx
80100126:	81 fb bc fc 10 80    	cmp    $0x8010fcbc,%ebx
8010012c:	75 0d                	jne    8010013b <bread+0x6b>
8010012e:	eb 70                	jmp    801001a0 <bread+0xd0>
80100130:	8b 5b 50             	mov    0x50(%ebx),%ebx
80100133:	81 fb bc fc 10 80    	cmp    $0x8010fcbc,%ebx
80100139:	74 65                	je     801001a0 <bread+0xd0>
    if(b->refcnt == 0 && (b->flags & B_DIRTY) == 0) {
8010013b:	8b 43 4c             	mov    0x4c(%ebx),%eax
8010013e:	85 c0                	test   %eax,%eax
80100140:	75 ee                	jne    80100130 <bread+0x60>
80100142:	f6 03 04             	testb  $0x4,(%ebx)
80100145:	75 e9                	jne    80100130 <bread+0x60>
      b->dev = dev;
80100147:	89 7b 04             	mov    %edi,0x4(%ebx)
      b->blockno = blockno;
8010014a:	89 73 08             	mov    %esi,0x8(%ebx)
      b->flags = 0;
8010014d:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
      b->refcnt = 1;
80100153:	c7 43 4c 01 00 00 00 	movl   $0x1,0x4c(%ebx)
      release(&bcache.lock);
8010015a:	83 ec 0c             	sub    $0xc,%esp
8010015d:	68 c0 b5 10 80       	push   $0x8010b5c0
80100162:	e8 c9 45 00 00       	call   80104730 <release>
      acquiresleep(&b->lock);
80100167:	8d 43 0c             	lea    0xc(%ebx),%eax
8010016a:	89 04 24             	mov    %eax,(%esp)
8010016d:	e8 7e 42 00 00       	call   801043f0 <acquiresleep>
      return b;
80100172:	83 c4 10             	add    $0x10,%esp
  struct buf *b;

  b = bget(dev, blockno);
  if((b->flags & B_VALID) == 0) {
80100175:	f6 03 02             	testb  $0x2,(%ebx)
80100178:	74 0e                	je     80100188 <bread+0xb8>
    iderw(b);
  }
  return b;
}
8010017a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010017d:	89 d8                	mov    %ebx,%eax
8010017f:	5b                   	pop    %ebx
80100180:	5e                   	pop    %esi
80100181:	5f                   	pop    %edi
80100182:	5d                   	pop    %ebp
80100183:	c3                   	ret    
80100184:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    iderw(b);
80100188:	83 ec 0c             	sub    $0xc,%esp
8010018b:	53                   	push   %ebx
8010018c:	e8 0f 21 00 00       	call   801022a0 <iderw>
80100191:	83 c4 10             	add    $0x10,%esp
}
80100194:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100197:	89 d8                	mov    %ebx,%eax
80100199:	5b                   	pop    %ebx
8010019a:	5e                   	pop    %esi
8010019b:	5f                   	pop    %edi
8010019c:	5d                   	pop    %ebp
8010019d:	c3                   	ret    
8010019e:	66 90                	xchg   %ax,%ax
  panic("bget: no buffers");
801001a0:	83 ec 0c             	sub    $0xc,%esp
801001a3:	68 0e 72 10 80       	push   $0x8010720e
801001a8:	e8 e3 01 00 00       	call   80100390 <panic>
801001ad:	8d 76 00             	lea    0x0(%esi),%esi

801001b0 <bwrite>:

// Write b's contents to disk.  Must be locked.
void
bwrite(struct buf *b)
{
801001b0:	f3 0f 1e fb          	endbr32 
801001b4:	55                   	push   %ebp
801001b5:	89 e5                	mov    %esp,%ebp
801001b7:	53                   	push   %ebx
801001b8:	83 ec 10             	sub    $0x10,%esp
801001bb:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holdingsleep(&b->lock))
801001be:	8d 43 0c             	lea    0xc(%ebx),%eax
801001c1:	50                   	push   %eax
801001c2:	e8 c9 42 00 00       	call   80104490 <holdingsleep>
801001c7:	83 c4 10             	add    $0x10,%esp
801001ca:	85 c0                	test   %eax,%eax
801001cc:	74 0f                	je     801001dd <bwrite+0x2d>
    panic("bwrite");
  b->flags |= B_DIRTY;
801001ce:	83 0b 04             	orl    $0x4,(%ebx)
  iderw(b);
801001d1:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
801001d4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801001d7:	c9                   	leave  
  iderw(b);
801001d8:	e9 c3 20 00 00       	jmp    801022a0 <iderw>
    panic("bwrite");
801001dd:	83 ec 0c             	sub    $0xc,%esp
801001e0:	68 1f 72 10 80       	push   $0x8010721f
801001e5:	e8 a6 01 00 00       	call   80100390 <panic>
801001ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801001f0 <brelse>:

// Release a locked buffer.
// Move to the head of the MRU list.
void
brelse(struct buf *b)
{
801001f0:	f3 0f 1e fb          	endbr32 
801001f4:	55                   	push   %ebp
801001f5:	89 e5                	mov    %esp,%ebp
801001f7:	56                   	push   %esi
801001f8:	53                   	push   %ebx
801001f9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holdingsleep(&b->lock))
801001fc:	8d 73 0c             	lea    0xc(%ebx),%esi
801001ff:	83 ec 0c             	sub    $0xc,%esp
80100202:	56                   	push   %esi
80100203:	e8 88 42 00 00       	call   80104490 <holdingsleep>
80100208:	83 c4 10             	add    $0x10,%esp
8010020b:	85 c0                	test   %eax,%eax
8010020d:	74 66                	je     80100275 <brelse+0x85>
    panic("brelse");

  releasesleep(&b->lock);
8010020f:	83 ec 0c             	sub    $0xc,%esp
80100212:	56                   	push   %esi
80100213:	e8 38 42 00 00       	call   80104450 <releasesleep>

  acquire(&bcache.lock);
80100218:	c7 04 24 c0 b5 10 80 	movl   $0x8010b5c0,(%esp)
8010021f:	e8 4c 44 00 00       	call   80104670 <acquire>
  b->refcnt--;
80100224:	8b 43 4c             	mov    0x4c(%ebx),%eax
  if (b->refcnt == 0) {
80100227:	83 c4 10             	add    $0x10,%esp
  b->refcnt--;
8010022a:	83 e8 01             	sub    $0x1,%eax
8010022d:	89 43 4c             	mov    %eax,0x4c(%ebx)
  if (b->refcnt == 0) {
80100230:	85 c0                	test   %eax,%eax
80100232:	75 2f                	jne    80100263 <brelse+0x73>
    // no one is waiting for it.
    b->next->prev = b->prev;
80100234:	8b 43 54             	mov    0x54(%ebx),%eax
80100237:	8b 53 50             	mov    0x50(%ebx),%edx
8010023a:	89 50 50             	mov    %edx,0x50(%eax)
    b->prev->next = b->next;
8010023d:	8b 43 50             	mov    0x50(%ebx),%eax
80100240:	8b 53 54             	mov    0x54(%ebx),%edx
80100243:	89 50 54             	mov    %edx,0x54(%eax)
    b->next = bcache.head.next;
80100246:	a1 10 fd 10 80       	mov    0x8010fd10,%eax
    b->prev = &bcache.head;
8010024b:	c7 43 50 bc fc 10 80 	movl   $0x8010fcbc,0x50(%ebx)
    b->next = bcache.head.next;
80100252:	89 43 54             	mov    %eax,0x54(%ebx)
    bcache.head.next->prev = b;
80100255:	a1 10 fd 10 80       	mov    0x8010fd10,%eax
8010025a:	89 58 50             	mov    %ebx,0x50(%eax)
    bcache.head.next = b;
8010025d:	89 1d 10 fd 10 80    	mov    %ebx,0x8010fd10
  }
  
  release(&bcache.lock);
80100263:	c7 45 08 c0 b5 10 80 	movl   $0x8010b5c0,0x8(%ebp)
}
8010026a:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010026d:	5b                   	pop    %ebx
8010026e:	5e                   	pop    %esi
8010026f:	5d                   	pop    %ebp
  release(&bcache.lock);
80100270:	e9 bb 44 00 00       	jmp    80104730 <release>
    panic("brelse");
80100275:	83 ec 0c             	sub    $0xc,%esp
80100278:	68 26 72 10 80       	push   $0x80107226
8010027d:	e8 0e 01 00 00       	call   80100390 <panic>
80100282:	66 90                	xchg   %ax,%ax
80100284:	66 90                	xchg   %ax,%ax
80100286:	66 90                	xchg   %ax,%ax
80100288:	66 90                	xchg   %ax,%ax
8010028a:	66 90                	xchg   %ax,%ax
8010028c:	66 90                	xchg   %ax,%ax
8010028e:	66 90                	xchg   %ax,%ax

80100290 <consoleread>:
  }
}

int
consoleread(struct inode *ip, char *dst, int n)
{
80100290:	f3 0f 1e fb          	endbr32 
80100294:	55                   	push   %ebp
80100295:	89 e5                	mov    %esp,%ebp
80100297:	57                   	push   %edi
80100298:	56                   	push   %esi
80100299:	53                   	push   %ebx
8010029a:	83 ec 18             	sub    $0x18,%esp
  uint target;
  int c;

  iunlock(ip);
8010029d:	ff 75 08             	pushl  0x8(%ebp)
{
801002a0:	8b 5d 10             	mov    0x10(%ebp),%ebx
  target = n;
801002a3:	89 de                	mov    %ebx,%esi
  iunlock(ip);
801002a5:	e8 b6 15 00 00       	call   80101860 <iunlock>
  acquire(&cons.lock);
801002aa:	c7 04 24 20 a5 10 80 	movl   $0x8010a520,(%esp)
801002b1:	e8 ba 43 00 00       	call   80104670 <acquire>
        // caller gets a 0-byte result.
        input.r--;
      }
      break;
    }
    *dst++ = c;
801002b6:	8b 7d 0c             	mov    0xc(%ebp),%edi
  while(n > 0){
801002b9:	83 c4 10             	add    $0x10,%esp
    *dst++ = c;
801002bc:	01 df                	add    %ebx,%edi
  while(n > 0){
801002be:	85 db                	test   %ebx,%ebx
801002c0:	0f 8e 97 00 00 00    	jle    8010035d <consoleread+0xcd>
    while(input.r == input.w){
801002c6:	a1 a0 ff 10 80       	mov    0x8010ffa0,%eax
801002cb:	3b 05 a4 ff 10 80    	cmp    0x8010ffa4,%eax
801002d1:	74 27                	je     801002fa <consoleread+0x6a>
801002d3:	eb 5b                	jmp    80100330 <consoleread+0xa0>
801002d5:	8d 76 00             	lea    0x0(%esi),%esi
      sleep(&input.r, &cons.lock);
801002d8:	83 ec 08             	sub    $0x8,%esp
801002db:	68 20 a5 10 80       	push   $0x8010a520
801002e0:	68 a0 ff 10 80       	push   $0x8010ffa0
801002e5:	e8 16 3d 00 00       	call   80104000 <sleep>
    while(input.r == input.w){
801002ea:	a1 a0 ff 10 80       	mov    0x8010ffa0,%eax
801002ef:	83 c4 10             	add    $0x10,%esp
801002f2:	3b 05 a4 ff 10 80    	cmp    0x8010ffa4,%eax
801002f8:	75 36                	jne    80100330 <consoleread+0xa0>
      if(myproc()->killed){
801002fa:	e8 01 37 00 00       	call   80103a00 <myproc>
801002ff:	8b 48 24             	mov    0x24(%eax),%ecx
80100302:	85 c9                	test   %ecx,%ecx
80100304:	74 d2                	je     801002d8 <consoleread+0x48>
        release(&cons.lock);
80100306:	83 ec 0c             	sub    $0xc,%esp
80100309:	68 20 a5 10 80       	push   $0x8010a520
8010030e:	e8 1d 44 00 00       	call   80104730 <release>
        ilock(ip);
80100313:	5a                   	pop    %edx
80100314:	ff 75 08             	pushl  0x8(%ebp)
80100317:	e8 64 14 00 00       	call   80101780 <ilock>
        return -1;
8010031c:	83 c4 10             	add    $0x10,%esp
  }
  release(&cons.lock);
  ilock(ip);

  return target - n;
}
8010031f:	8d 65 f4             	lea    -0xc(%ebp),%esp
        return -1;
80100322:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80100327:	5b                   	pop    %ebx
80100328:	5e                   	pop    %esi
80100329:	5f                   	pop    %edi
8010032a:	5d                   	pop    %ebp
8010032b:	c3                   	ret    
8010032c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    c = input.buf[input.r++ % INPUT_BUF];
80100330:	8d 50 01             	lea    0x1(%eax),%edx
80100333:	89 15 a0 ff 10 80    	mov    %edx,0x8010ffa0
80100339:	89 c2                	mov    %eax,%edx
8010033b:	83 e2 7f             	and    $0x7f,%edx
8010033e:	0f be 8a 20 ff 10 80 	movsbl -0x7fef00e0(%edx),%ecx
    if(c == C('D')){  // EOF
80100345:	80 f9 04             	cmp    $0x4,%cl
80100348:	74 38                	je     80100382 <consoleread+0xf2>
    *dst++ = c;
8010034a:	89 d8                	mov    %ebx,%eax
    --n;
8010034c:	83 eb 01             	sub    $0x1,%ebx
    *dst++ = c;
8010034f:	f7 d8                	neg    %eax
80100351:	88 0c 07             	mov    %cl,(%edi,%eax,1)
    if(c == '\n')
80100354:	83 f9 0a             	cmp    $0xa,%ecx
80100357:	0f 85 61 ff ff ff    	jne    801002be <consoleread+0x2e>
  release(&cons.lock);
8010035d:	83 ec 0c             	sub    $0xc,%esp
80100360:	68 20 a5 10 80       	push   $0x8010a520
80100365:	e8 c6 43 00 00       	call   80104730 <release>
  ilock(ip);
8010036a:	58                   	pop    %eax
8010036b:	ff 75 08             	pushl  0x8(%ebp)
8010036e:	e8 0d 14 00 00       	call   80101780 <ilock>
  return target - n;
80100373:	89 f0                	mov    %esi,%eax
80100375:	83 c4 10             	add    $0x10,%esp
}
80100378:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return target - n;
8010037b:	29 d8                	sub    %ebx,%eax
}
8010037d:	5b                   	pop    %ebx
8010037e:	5e                   	pop    %esi
8010037f:	5f                   	pop    %edi
80100380:	5d                   	pop    %ebp
80100381:	c3                   	ret    
      if(n < target){
80100382:	39 f3                	cmp    %esi,%ebx
80100384:	73 d7                	jae    8010035d <consoleread+0xcd>
        input.r--;
80100386:	a3 a0 ff 10 80       	mov    %eax,0x8010ffa0
8010038b:	eb d0                	jmp    8010035d <consoleread+0xcd>
8010038d:	8d 76 00             	lea    0x0(%esi),%esi

80100390 <panic>:
{
80100390:	f3 0f 1e fb          	endbr32 
80100394:	55                   	push   %ebp
80100395:	89 e5                	mov    %esp,%ebp
80100397:	56                   	push   %esi
80100398:	53                   	push   %ebx
80100399:	83 ec 30             	sub    $0x30,%esp
}

static inline void
cli(void)
{
  asm volatile("cli");
8010039c:	fa                   	cli    
  cons.locking = 0;
8010039d:	c7 05 54 a5 10 80 00 	movl   $0x0,0x8010a554
801003a4:	00 00 00 
  getcallerpcs(&s, pcs);
801003a7:	8d 5d d0             	lea    -0x30(%ebp),%ebx
801003aa:	8d 75 f8             	lea    -0x8(%ebp),%esi
  cprintf("lapicid %d: panic: ", lapicid());
801003ad:	e8 0e 25 00 00       	call   801028c0 <lapicid>
801003b2:	83 ec 08             	sub    $0x8,%esp
801003b5:	50                   	push   %eax
801003b6:	68 2d 72 10 80       	push   $0x8010722d
801003bb:	e8 f0 02 00 00       	call   801006b0 <cprintf>
  cprintf(s);
801003c0:	58                   	pop    %eax
801003c1:	ff 75 08             	pushl  0x8(%ebp)
801003c4:	e8 e7 02 00 00       	call   801006b0 <cprintf>
  cprintf("\n");
801003c9:	c7 04 24 b7 7d 10 80 	movl   $0x80107db7,(%esp)
801003d0:	e8 db 02 00 00       	call   801006b0 <cprintf>
  getcallerpcs(&s, pcs);
801003d5:	8d 45 08             	lea    0x8(%ebp),%eax
801003d8:	5a                   	pop    %edx
801003d9:	59                   	pop    %ecx
801003da:	53                   	push   %ebx
801003db:	50                   	push   %eax
801003dc:	e8 2f 41 00 00       	call   80104510 <getcallerpcs>
  for(i=0; i<10; i++)
801003e1:	83 c4 10             	add    $0x10,%esp
    cprintf(" %p", pcs[i]);
801003e4:	83 ec 08             	sub    $0x8,%esp
801003e7:	ff 33                	pushl  (%ebx)
801003e9:	83 c3 04             	add    $0x4,%ebx
801003ec:	68 41 72 10 80       	push   $0x80107241
801003f1:	e8 ba 02 00 00       	call   801006b0 <cprintf>
  for(i=0; i<10; i++)
801003f6:	83 c4 10             	add    $0x10,%esp
801003f9:	39 f3                	cmp    %esi,%ebx
801003fb:	75 e7                	jne    801003e4 <panic+0x54>
  panicked = 1; // freeze other CPU
801003fd:	c7 05 58 a5 10 80 01 	movl   $0x1,0x8010a558
80100404:	00 00 00 
  for(;;)
80100407:	eb fe                	jmp    80100407 <panic+0x77>
80100409:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80100410 <consputc.part.0>:
consputc(int c)
80100410:	55                   	push   %ebp
80100411:	89 e5                	mov    %esp,%ebp
80100413:	57                   	push   %edi
80100414:	56                   	push   %esi
80100415:	53                   	push   %ebx
80100416:	89 c3                	mov    %eax,%ebx
80100418:	83 ec 1c             	sub    $0x1c,%esp
  if(c == BACKSPACE){
8010041b:	3d 00 01 00 00       	cmp    $0x100,%eax
80100420:	0f 84 ea 00 00 00    	je     80100510 <consputc.part.0+0x100>
    uartputc(c);
80100426:	83 ec 0c             	sub    $0xc,%esp
80100429:	50                   	push   %eax
8010042a:	e8 c1 59 00 00       	call   80105df0 <uartputc>
8010042f:	83 c4 10             	add    $0x10,%esp
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100432:	bf d4 03 00 00       	mov    $0x3d4,%edi
80100437:	b8 0e 00 00 00       	mov    $0xe,%eax
8010043c:	89 fa                	mov    %edi,%edx
8010043e:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010043f:	b9 d5 03 00 00       	mov    $0x3d5,%ecx
80100444:	89 ca                	mov    %ecx,%edx
80100446:	ec                   	in     (%dx),%al
  pos = inb(CRTPORT+1) << 8;
80100447:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010044a:	89 fa                	mov    %edi,%edx
8010044c:	c1 e0 08             	shl    $0x8,%eax
8010044f:	89 c6                	mov    %eax,%esi
80100451:	b8 0f 00 00 00       	mov    $0xf,%eax
80100456:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80100457:	89 ca                	mov    %ecx,%edx
80100459:	ec                   	in     (%dx),%al
  pos |= inb(CRTPORT+1);
8010045a:	0f b6 c0             	movzbl %al,%eax
8010045d:	09 f0                	or     %esi,%eax
  if(c == '\n')
8010045f:	83 fb 0a             	cmp    $0xa,%ebx
80100462:	0f 84 90 00 00 00    	je     801004f8 <consputc.part.0+0xe8>
  else if(c == BACKSPACE){
80100468:	81 fb 00 01 00 00    	cmp    $0x100,%ebx
8010046e:	74 70                	je     801004e0 <consputc.part.0+0xd0>
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
80100470:	0f b6 db             	movzbl %bl,%ebx
80100473:	8d 70 01             	lea    0x1(%eax),%esi
80100476:	80 cf 07             	or     $0x7,%bh
80100479:	66 89 9c 00 00 80 0b 	mov    %bx,-0x7ff48000(%eax,%eax,1)
80100480:	80 
  if(pos < 0 || pos > 25*80)
80100481:	81 fe d0 07 00 00    	cmp    $0x7d0,%esi
80100487:	0f 8f f9 00 00 00    	jg     80100586 <consputc.part.0+0x176>
  if((pos/80) >= 24){  // Scroll up.
8010048d:	81 fe 7f 07 00 00    	cmp    $0x77f,%esi
80100493:	0f 8f a7 00 00 00    	jg     80100540 <consputc.part.0+0x130>
80100499:	89 f0                	mov    %esi,%eax
8010049b:	8d b4 36 00 80 0b 80 	lea    -0x7ff48000(%esi,%esi,1),%esi
801004a2:	88 45 e7             	mov    %al,-0x19(%ebp)
801004a5:	0f b6 fc             	movzbl %ah,%edi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801004a8:	bb d4 03 00 00       	mov    $0x3d4,%ebx
801004ad:	b8 0e 00 00 00       	mov    $0xe,%eax
801004b2:	89 da                	mov    %ebx,%edx
801004b4:	ee                   	out    %al,(%dx)
801004b5:	b9 d5 03 00 00       	mov    $0x3d5,%ecx
801004ba:	89 f8                	mov    %edi,%eax
801004bc:	89 ca                	mov    %ecx,%edx
801004be:	ee                   	out    %al,(%dx)
801004bf:	b8 0f 00 00 00       	mov    $0xf,%eax
801004c4:	89 da                	mov    %ebx,%edx
801004c6:	ee                   	out    %al,(%dx)
801004c7:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
801004cb:	89 ca                	mov    %ecx,%edx
801004cd:	ee                   	out    %al,(%dx)
  crt[pos] = ' ' | 0x0700;
801004ce:	b8 20 07 00 00       	mov    $0x720,%eax
801004d3:	66 89 06             	mov    %ax,(%esi)
}
801004d6:	8d 65 f4             	lea    -0xc(%ebp),%esp
801004d9:	5b                   	pop    %ebx
801004da:	5e                   	pop    %esi
801004db:	5f                   	pop    %edi
801004dc:	5d                   	pop    %ebp
801004dd:	c3                   	ret    
801004de:	66 90                	xchg   %ax,%ax
    if(pos > 0) --pos;
801004e0:	8d 70 ff             	lea    -0x1(%eax),%esi
801004e3:	85 c0                	test   %eax,%eax
801004e5:	75 9a                	jne    80100481 <consputc.part.0+0x71>
801004e7:	c6 45 e7 00          	movb   $0x0,-0x19(%ebp)
801004eb:	be 00 80 0b 80       	mov    $0x800b8000,%esi
801004f0:	31 ff                	xor    %edi,%edi
801004f2:	eb b4                	jmp    801004a8 <consputc.part.0+0x98>
801004f4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    pos += 80 - pos%80;
801004f8:	ba cd cc cc cc       	mov    $0xcccccccd,%edx
801004fd:	f7 e2                	mul    %edx
801004ff:	c1 ea 06             	shr    $0x6,%edx
80100502:	8d 04 92             	lea    (%edx,%edx,4),%eax
80100505:	c1 e0 04             	shl    $0x4,%eax
80100508:	8d 70 50             	lea    0x50(%eax),%esi
8010050b:	e9 71 ff ff ff       	jmp    80100481 <consputc.part.0+0x71>
    uartputc('\b'); uartputc(' '); uartputc('\b');
80100510:	83 ec 0c             	sub    $0xc,%esp
80100513:	6a 08                	push   $0x8
80100515:	e8 d6 58 00 00       	call   80105df0 <uartputc>
8010051a:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80100521:	e8 ca 58 00 00       	call   80105df0 <uartputc>
80100526:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
8010052d:	e8 be 58 00 00       	call   80105df0 <uartputc>
80100532:	83 c4 10             	add    $0x10,%esp
80100535:	e9 f8 fe ff ff       	jmp    80100432 <consputc.part.0+0x22>
8010053a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
80100540:	83 ec 04             	sub    $0x4,%esp
    pos -= 80;
80100543:	8d 5e b0             	lea    -0x50(%esi),%ebx
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
80100546:	8d b4 36 60 7f 0b 80 	lea    -0x7ff480a0(%esi,%esi,1),%esi
8010054d:	bf 07 00 00 00       	mov    $0x7,%edi
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
80100552:	68 60 0e 00 00       	push   $0xe60
80100557:	68 a0 80 0b 80       	push   $0x800b80a0
8010055c:	68 00 80 0b 80       	push   $0x800b8000
80100561:	e8 ba 42 00 00       	call   80104820 <memmove>
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
80100566:	b8 80 07 00 00       	mov    $0x780,%eax
8010056b:	83 c4 0c             	add    $0xc,%esp
8010056e:	29 d8                	sub    %ebx,%eax
80100570:	01 c0                	add    %eax,%eax
80100572:	50                   	push   %eax
80100573:	6a 00                	push   $0x0
80100575:	56                   	push   %esi
80100576:	e8 05 42 00 00       	call   80104780 <memset>
8010057b:	88 5d e7             	mov    %bl,-0x19(%ebp)
8010057e:	83 c4 10             	add    $0x10,%esp
80100581:	e9 22 ff ff ff       	jmp    801004a8 <consputc.part.0+0x98>
    panic("pos under/overflow");
80100586:	83 ec 0c             	sub    $0xc,%esp
80100589:	68 45 72 10 80       	push   $0x80107245
8010058e:	e8 fd fd ff ff       	call   80100390 <panic>
80100593:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010059a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801005a0 <printint>:
{
801005a0:	55                   	push   %ebp
801005a1:	89 e5                	mov    %esp,%ebp
801005a3:	57                   	push   %edi
801005a4:	56                   	push   %esi
801005a5:	53                   	push   %ebx
801005a6:	83 ec 2c             	sub    $0x2c,%esp
801005a9:	89 55 d4             	mov    %edx,-0x2c(%ebp)
  if(sign && (sign = xx < 0))
801005ac:	85 c9                	test   %ecx,%ecx
801005ae:	74 04                	je     801005b4 <printint+0x14>
801005b0:	85 c0                	test   %eax,%eax
801005b2:	78 6d                	js     80100621 <printint+0x81>
    x = xx;
801005b4:	89 c1                	mov    %eax,%ecx
801005b6:	31 f6                	xor    %esi,%esi
  i = 0;
801005b8:	89 75 cc             	mov    %esi,-0x34(%ebp)
801005bb:	31 db                	xor    %ebx,%ebx
801005bd:	8d 7d d7             	lea    -0x29(%ebp),%edi
    buf[i++] = digits[x % base];
801005c0:	89 c8                	mov    %ecx,%eax
801005c2:	31 d2                	xor    %edx,%edx
801005c4:	89 ce                	mov    %ecx,%esi
801005c6:	f7 75 d4             	divl   -0x2c(%ebp)
801005c9:	0f b6 92 70 72 10 80 	movzbl -0x7fef8d90(%edx),%edx
801005d0:	89 45 d0             	mov    %eax,-0x30(%ebp)
801005d3:	89 d8                	mov    %ebx,%eax
801005d5:	8d 5b 01             	lea    0x1(%ebx),%ebx
  }while((x /= base) != 0);
801005d8:	8b 4d d0             	mov    -0x30(%ebp),%ecx
801005db:	89 75 d0             	mov    %esi,-0x30(%ebp)
    buf[i++] = digits[x % base];
801005de:	88 14 1f             	mov    %dl,(%edi,%ebx,1)
  }while((x /= base) != 0);
801005e1:	8b 75 d4             	mov    -0x2c(%ebp),%esi
801005e4:	39 75 d0             	cmp    %esi,-0x30(%ebp)
801005e7:	73 d7                	jae    801005c0 <printint+0x20>
801005e9:	8b 75 cc             	mov    -0x34(%ebp),%esi
  if(sign)
801005ec:	85 f6                	test   %esi,%esi
801005ee:	74 0c                	je     801005fc <printint+0x5c>
    buf[i++] = '-';
801005f0:	c6 44 1d d8 2d       	movb   $0x2d,-0x28(%ebp,%ebx,1)
    buf[i++] = digits[x % base];
801005f5:	89 d8                	mov    %ebx,%eax
    buf[i++] = '-';
801005f7:	ba 2d 00 00 00       	mov    $0x2d,%edx
  while(--i >= 0)
801005fc:	8d 5c 05 d7          	lea    -0x29(%ebp,%eax,1),%ebx
80100600:	0f be c2             	movsbl %dl,%eax
  if(panicked){
80100603:	8b 15 58 a5 10 80    	mov    0x8010a558,%edx
80100609:	85 d2                	test   %edx,%edx
8010060b:	74 03                	je     80100610 <printint+0x70>
  asm volatile("cli");
8010060d:	fa                   	cli    
    for(;;)
8010060e:	eb fe                	jmp    8010060e <printint+0x6e>
80100610:	e8 fb fd ff ff       	call   80100410 <consputc.part.0>
  while(--i >= 0)
80100615:	39 fb                	cmp    %edi,%ebx
80100617:	74 10                	je     80100629 <printint+0x89>
80100619:	0f be 03             	movsbl (%ebx),%eax
8010061c:	83 eb 01             	sub    $0x1,%ebx
8010061f:	eb e2                	jmp    80100603 <printint+0x63>
    x = -xx;
80100621:	f7 d8                	neg    %eax
80100623:	89 ce                	mov    %ecx,%esi
80100625:	89 c1                	mov    %eax,%ecx
80100627:	eb 8f                	jmp    801005b8 <printint+0x18>
}
80100629:	83 c4 2c             	add    $0x2c,%esp
8010062c:	5b                   	pop    %ebx
8010062d:	5e                   	pop    %esi
8010062e:	5f                   	pop    %edi
8010062f:	5d                   	pop    %ebp
80100630:	c3                   	ret    
80100631:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100638:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010063f:	90                   	nop

80100640 <consolewrite>:

int
consolewrite(struct inode *ip, char *buf, int n)
{
80100640:	f3 0f 1e fb          	endbr32 
80100644:	55                   	push   %ebp
80100645:	89 e5                	mov    %esp,%ebp
80100647:	57                   	push   %edi
80100648:	56                   	push   %esi
80100649:	53                   	push   %ebx
8010064a:	83 ec 18             	sub    $0x18,%esp
  int i;

  iunlock(ip);
8010064d:	ff 75 08             	pushl  0x8(%ebp)
{
80100650:	8b 5d 10             	mov    0x10(%ebp),%ebx
  iunlock(ip);
80100653:	e8 08 12 00 00       	call   80101860 <iunlock>
  acquire(&cons.lock);
80100658:	c7 04 24 20 a5 10 80 	movl   $0x8010a520,(%esp)
8010065f:	e8 0c 40 00 00       	call   80104670 <acquire>
  for(i = 0; i < n; i++)
80100664:	83 c4 10             	add    $0x10,%esp
80100667:	85 db                	test   %ebx,%ebx
80100669:	7e 24                	jle    8010068f <consolewrite+0x4f>
8010066b:	8b 7d 0c             	mov    0xc(%ebp),%edi
8010066e:	8d 34 1f             	lea    (%edi,%ebx,1),%esi
  if(panicked){
80100671:	8b 15 58 a5 10 80    	mov    0x8010a558,%edx
80100677:	85 d2                	test   %edx,%edx
80100679:	74 05                	je     80100680 <consolewrite+0x40>
8010067b:	fa                   	cli    
    for(;;)
8010067c:	eb fe                	jmp    8010067c <consolewrite+0x3c>
8010067e:	66 90                	xchg   %ax,%ax
    consputc(buf[i] & 0xff);
80100680:	0f b6 07             	movzbl (%edi),%eax
80100683:	83 c7 01             	add    $0x1,%edi
80100686:	e8 85 fd ff ff       	call   80100410 <consputc.part.0>
  for(i = 0; i < n; i++)
8010068b:	39 fe                	cmp    %edi,%esi
8010068d:	75 e2                	jne    80100671 <consolewrite+0x31>
  release(&cons.lock);
8010068f:	83 ec 0c             	sub    $0xc,%esp
80100692:	68 20 a5 10 80       	push   $0x8010a520
80100697:	e8 94 40 00 00       	call   80104730 <release>
  ilock(ip);
8010069c:	58                   	pop    %eax
8010069d:	ff 75 08             	pushl  0x8(%ebp)
801006a0:	e8 db 10 00 00       	call   80101780 <ilock>

  return n;
}
801006a5:	8d 65 f4             	lea    -0xc(%ebp),%esp
801006a8:	89 d8                	mov    %ebx,%eax
801006aa:	5b                   	pop    %ebx
801006ab:	5e                   	pop    %esi
801006ac:	5f                   	pop    %edi
801006ad:	5d                   	pop    %ebp
801006ae:	c3                   	ret    
801006af:	90                   	nop

801006b0 <cprintf>:
{
801006b0:	f3 0f 1e fb          	endbr32 
801006b4:	55                   	push   %ebp
801006b5:	89 e5                	mov    %esp,%ebp
801006b7:	57                   	push   %edi
801006b8:	56                   	push   %esi
801006b9:	53                   	push   %ebx
801006ba:	83 ec 1c             	sub    $0x1c,%esp
  locking = cons.locking;
801006bd:	a1 54 a5 10 80       	mov    0x8010a554,%eax
801006c2:	89 45 e0             	mov    %eax,-0x20(%ebp)
  if(locking)
801006c5:	85 c0                	test   %eax,%eax
801006c7:	0f 85 e8 00 00 00    	jne    801007b5 <cprintf+0x105>
  if (fmt == 0)
801006cd:	8b 45 08             	mov    0x8(%ebp),%eax
801006d0:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801006d3:	85 c0                	test   %eax,%eax
801006d5:	0f 84 5a 01 00 00    	je     80100835 <cprintf+0x185>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
801006db:	0f b6 00             	movzbl (%eax),%eax
801006de:	85 c0                	test   %eax,%eax
801006e0:	74 36                	je     80100718 <cprintf+0x68>
  argp = (uint*)(void*)(&fmt + 1);
801006e2:	8d 5d 0c             	lea    0xc(%ebp),%ebx
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
801006e5:	31 f6                	xor    %esi,%esi
    if(c != '%'){
801006e7:	83 f8 25             	cmp    $0x25,%eax
801006ea:	74 44                	je     80100730 <cprintf+0x80>
  if(panicked){
801006ec:	8b 0d 58 a5 10 80    	mov    0x8010a558,%ecx
801006f2:	85 c9                	test   %ecx,%ecx
801006f4:	74 0f                	je     80100705 <cprintf+0x55>
801006f6:	fa                   	cli    
    for(;;)
801006f7:	eb fe                	jmp    801006f7 <cprintf+0x47>
801006f9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100700:	b8 25 00 00 00       	mov    $0x25,%eax
80100705:	e8 06 fd ff ff       	call   80100410 <consputc.part.0>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
8010070a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010070d:	83 c6 01             	add    $0x1,%esi
80100710:	0f b6 04 30          	movzbl (%eax,%esi,1),%eax
80100714:	85 c0                	test   %eax,%eax
80100716:	75 cf                	jne    801006e7 <cprintf+0x37>
  if(locking)
80100718:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010071b:	85 c0                	test   %eax,%eax
8010071d:	0f 85 fd 00 00 00    	jne    80100820 <cprintf+0x170>
}
80100723:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100726:	5b                   	pop    %ebx
80100727:	5e                   	pop    %esi
80100728:	5f                   	pop    %edi
80100729:	5d                   	pop    %ebp
8010072a:	c3                   	ret    
8010072b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010072f:	90                   	nop
    c = fmt[++i] & 0xff;
80100730:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100733:	83 c6 01             	add    $0x1,%esi
80100736:	0f b6 3c 30          	movzbl (%eax,%esi,1),%edi
    if(c == 0)
8010073a:	85 ff                	test   %edi,%edi
8010073c:	74 da                	je     80100718 <cprintf+0x68>
    switch(c){
8010073e:	83 ff 70             	cmp    $0x70,%edi
80100741:	74 5a                	je     8010079d <cprintf+0xed>
80100743:	7f 2a                	jg     8010076f <cprintf+0xbf>
80100745:	83 ff 25             	cmp    $0x25,%edi
80100748:	0f 84 92 00 00 00    	je     801007e0 <cprintf+0x130>
8010074e:	83 ff 64             	cmp    $0x64,%edi
80100751:	0f 85 a1 00 00 00    	jne    801007f8 <cprintf+0x148>
      printint(*argp++, 10, 1);
80100757:	8b 03                	mov    (%ebx),%eax
80100759:	8d 7b 04             	lea    0x4(%ebx),%edi
8010075c:	b9 01 00 00 00       	mov    $0x1,%ecx
80100761:	ba 0a 00 00 00       	mov    $0xa,%edx
80100766:	89 fb                	mov    %edi,%ebx
80100768:	e8 33 fe ff ff       	call   801005a0 <printint>
      break;
8010076d:	eb 9b                	jmp    8010070a <cprintf+0x5a>
    switch(c){
8010076f:	83 ff 73             	cmp    $0x73,%edi
80100772:	75 24                	jne    80100798 <cprintf+0xe8>
      if((s = (char*)*argp++) == 0)
80100774:	8d 7b 04             	lea    0x4(%ebx),%edi
80100777:	8b 1b                	mov    (%ebx),%ebx
80100779:	85 db                	test   %ebx,%ebx
8010077b:	75 55                	jne    801007d2 <cprintf+0x122>
        s = "(null)";
8010077d:	bb 58 72 10 80       	mov    $0x80107258,%ebx
      for(; *s; s++)
80100782:	b8 28 00 00 00       	mov    $0x28,%eax
  if(panicked){
80100787:	8b 15 58 a5 10 80    	mov    0x8010a558,%edx
8010078d:	85 d2                	test   %edx,%edx
8010078f:	74 39                	je     801007ca <cprintf+0x11a>
80100791:	fa                   	cli    
    for(;;)
80100792:	eb fe                	jmp    80100792 <cprintf+0xe2>
80100794:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    switch(c){
80100798:	83 ff 78             	cmp    $0x78,%edi
8010079b:	75 5b                	jne    801007f8 <cprintf+0x148>
      printint(*argp++, 16, 0);
8010079d:	8b 03                	mov    (%ebx),%eax
8010079f:	8d 7b 04             	lea    0x4(%ebx),%edi
801007a2:	31 c9                	xor    %ecx,%ecx
801007a4:	ba 10 00 00 00       	mov    $0x10,%edx
801007a9:	89 fb                	mov    %edi,%ebx
801007ab:	e8 f0 fd ff ff       	call   801005a0 <printint>
      break;
801007b0:	e9 55 ff ff ff       	jmp    8010070a <cprintf+0x5a>
    acquire(&cons.lock);
801007b5:	83 ec 0c             	sub    $0xc,%esp
801007b8:	68 20 a5 10 80       	push   $0x8010a520
801007bd:	e8 ae 3e 00 00       	call   80104670 <acquire>
801007c2:	83 c4 10             	add    $0x10,%esp
801007c5:	e9 03 ff ff ff       	jmp    801006cd <cprintf+0x1d>
801007ca:	e8 41 fc ff ff       	call   80100410 <consputc.part.0>
      for(; *s; s++)
801007cf:	83 c3 01             	add    $0x1,%ebx
801007d2:	0f be 03             	movsbl (%ebx),%eax
801007d5:	84 c0                	test   %al,%al
801007d7:	75 ae                	jne    80100787 <cprintf+0xd7>
      if((s = (char*)*argp++) == 0)
801007d9:	89 fb                	mov    %edi,%ebx
801007db:	e9 2a ff ff ff       	jmp    8010070a <cprintf+0x5a>
  if(panicked){
801007e0:	8b 3d 58 a5 10 80    	mov    0x8010a558,%edi
801007e6:	85 ff                	test   %edi,%edi
801007e8:	0f 84 12 ff ff ff    	je     80100700 <cprintf+0x50>
801007ee:	fa                   	cli    
    for(;;)
801007ef:	eb fe                	jmp    801007ef <cprintf+0x13f>
801007f1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  if(panicked){
801007f8:	8b 0d 58 a5 10 80    	mov    0x8010a558,%ecx
801007fe:	85 c9                	test   %ecx,%ecx
80100800:	74 06                	je     80100808 <cprintf+0x158>
80100802:	fa                   	cli    
    for(;;)
80100803:	eb fe                	jmp    80100803 <cprintf+0x153>
80100805:	8d 76 00             	lea    0x0(%esi),%esi
80100808:	b8 25 00 00 00       	mov    $0x25,%eax
8010080d:	e8 fe fb ff ff       	call   80100410 <consputc.part.0>
  if(panicked){
80100812:	8b 15 58 a5 10 80    	mov    0x8010a558,%edx
80100818:	85 d2                	test   %edx,%edx
8010081a:	74 2c                	je     80100848 <cprintf+0x198>
8010081c:	fa                   	cli    
    for(;;)
8010081d:	eb fe                	jmp    8010081d <cprintf+0x16d>
8010081f:	90                   	nop
    release(&cons.lock);
80100820:	83 ec 0c             	sub    $0xc,%esp
80100823:	68 20 a5 10 80       	push   $0x8010a520
80100828:	e8 03 3f 00 00       	call   80104730 <release>
8010082d:	83 c4 10             	add    $0x10,%esp
}
80100830:	e9 ee fe ff ff       	jmp    80100723 <cprintf+0x73>
    panic("null fmt");
80100835:	83 ec 0c             	sub    $0xc,%esp
80100838:	68 5f 72 10 80       	push   $0x8010725f
8010083d:	e8 4e fb ff ff       	call   80100390 <panic>
80100842:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80100848:	89 f8                	mov    %edi,%eax
8010084a:	e8 c1 fb ff ff       	call   80100410 <consputc.part.0>
8010084f:	e9 b6 fe ff ff       	jmp    8010070a <cprintf+0x5a>
80100854:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010085b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010085f:	90                   	nop

80100860 <consoleintr>:
{
80100860:	f3 0f 1e fb          	endbr32 
80100864:	55                   	push   %ebp
80100865:	89 e5                	mov    %esp,%ebp
80100867:	57                   	push   %edi
80100868:	56                   	push   %esi
  int c, doprocdump = 0;
80100869:	31 f6                	xor    %esi,%esi
{
8010086b:	53                   	push   %ebx
8010086c:	83 ec 18             	sub    $0x18,%esp
8010086f:	8b 7d 08             	mov    0x8(%ebp),%edi
  acquire(&cons.lock);
80100872:	68 20 a5 10 80       	push   $0x8010a520
80100877:	e8 f4 3d 00 00       	call   80104670 <acquire>
  while((c = getc()) >= 0){
8010087c:	83 c4 10             	add    $0x10,%esp
8010087f:	eb 17                	jmp    80100898 <consoleintr+0x38>
    switch(c){
80100881:	83 fb 08             	cmp    $0x8,%ebx
80100884:	0f 84 f6 00 00 00    	je     80100980 <consoleintr+0x120>
8010088a:	83 fb 10             	cmp    $0x10,%ebx
8010088d:	0f 85 15 01 00 00    	jne    801009a8 <consoleintr+0x148>
80100893:	be 01 00 00 00       	mov    $0x1,%esi
  while((c = getc()) >= 0){
80100898:	ff d7                	call   *%edi
8010089a:	89 c3                	mov    %eax,%ebx
8010089c:	85 c0                	test   %eax,%eax
8010089e:	0f 88 23 01 00 00    	js     801009c7 <consoleintr+0x167>
    switch(c){
801008a4:	83 fb 15             	cmp    $0x15,%ebx
801008a7:	74 77                	je     80100920 <consoleintr+0xc0>
801008a9:	7e d6                	jle    80100881 <consoleintr+0x21>
801008ab:	83 fb 7f             	cmp    $0x7f,%ebx
801008ae:	0f 84 cc 00 00 00    	je     80100980 <consoleintr+0x120>
      if(c != 0 && input.e-input.r < INPUT_BUF){
801008b4:	a1 a8 ff 10 80       	mov    0x8010ffa8,%eax
801008b9:	89 c2                	mov    %eax,%edx
801008bb:	2b 15 a0 ff 10 80    	sub    0x8010ffa0,%edx
801008c1:	83 fa 7f             	cmp    $0x7f,%edx
801008c4:	77 d2                	ja     80100898 <consoleintr+0x38>
        c = (c == '\r') ? '\n' : c;
801008c6:	8d 48 01             	lea    0x1(%eax),%ecx
801008c9:	8b 15 58 a5 10 80    	mov    0x8010a558,%edx
801008cf:	83 e0 7f             	and    $0x7f,%eax
        input.buf[input.e++ % INPUT_BUF] = c;
801008d2:	89 0d a8 ff 10 80    	mov    %ecx,0x8010ffa8
        c = (c == '\r') ? '\n' : c;
801008d8:	83 fb 0d             	cmp    $0xd,%ebx
801008db:	0f 84 02 01 00 00    	je     801009e3 <consoleintr+0x183>
        input.buf[input.e++ % INPUT_BUF] = c;
801008e1:	88 98 20 ff 10 80    	mov    %bl,-0x7fef00e0(%eax)
  if(panicked){
801008e7:	85 d2                	test   %edx,%edx
801008e9:	0f 85 ff 00 00 00    	jne    801009ee <consoleintr+0x18e>
801008ef:	89 d8                	mov    %ebx,%eax
801008f1:	e8 1a fb ff ff       	call   80100410 <consputc.part.0>
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
801008f6:	83 fb 0a             	cmp    $0xa,%ebx
801008f9:	0f 84 0f 01 00 00    	je     80100a0e <consoleintr+0x1ae>
801008ff:	83 fb 04             	cmp    $0x4,%ebx
80100902:	0f 84 06 01 00 00    	je     80100a0e <consoleintr+0x1ae>
80100908:	a1 a0 ff 10 80       	mov    0x8010ffa0,%eax
8010090d:	83 e8 80             	sub    $0xffffff80,%eax
80100910:	39 05 a8 ff 10 80    	cmp    %eax,0x8010ffa8
80100916:	75 80                	jne    80100898 <consoleintr+0x38>
80100918:	e9 f6 00 00 00       	jmp    80100a13 <consoleintr+0x1b3>
8010091d:	8d 76 00             	lea    0x0(%esi),%esi
      while(input.e != input.w &&
80100920:	a1 a8 ff 10 80       	mov    0x8010ffa8,%eax
80100925:	39 05 a4 ff 10 80    	cmp    %eax,0x8010ffa4
8010092b:	0f 84 67 ff ff ff    	je     80100898 <consoleintr+0x38>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
80100931:	83 e8 01             	sub    $0x1,%eax
80100934:	89 c2                	mov    %eax,%edx
80100936:	83 e2 7f             	and    $0x7f,%edx
      while(input.e != input.w &&
80100939:	80 ba 20 ff 10 80 0a 	cmpb   $0xa,-0x7fef00e0(%edx)
80100940:	0f 84 52 ff ff ff    	je     80100898 <consoleintr+0x38>
  if(panicked){
80100946:	8b 15 58 a5 10 80    	mov    0x8010a558,%edx
        input.e--;
8010094c:	a3 a8 ff 10 80       	mov    %eax,0x8010ffa8
  if(panicked){
80100951:	85 d2                	test   %edx,%edx
80100953:	74 0b                	je     80100960 <consoleintr+0x100>
80100955:	fa                   	cli    
    for(;;)
80100956:	eb fe                	jmp    80100956 <consoleintr+0xf6>
80100958:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010095f:	90                   	nop
80100960:	b8 00 01 00 00       	mov    $0x100,%eax
80100965:	e8 a6 fa ff ff       	call   80100410 <consputc.part.0>
      while(input.e != input.w &&
8010096a:	a1 a8 ff 10 80       	mov    0x8010ffa8,%eax
8010096f:	3b 05 a4 ff 10 80    	cmp    0x8010ffa4,%eax
80100975:	75 ba                	jne    80100931 <consoleintr+0xd1>
80100977:	e9 1c ff ff ff       	jmp    80100898 <consoleintr+0x38>
8010097c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      if(input.e != input.w){
80100980:	a1 a8 ff 10 80       	mov    0x8010ffa8,%eax
80100985:	3b 05 a4 ff 10 80    	cmp    0x8010ffa4,%eax
8010098b:	0f 84 07 ff ff ff    	je     80100898 <consoleintr+0x38>
        input.e--;
80100991:	83 e8 01             	sub    $0x1,%eax
80100994:	a3 a8 ff 10 80       	mov    %eax,0x8010ffa8
  if(panicked){
80100999:	a1 58 a5 10 80       	mov    0x8010a558,%eax
8010099e:	85 c0                	test   %eax,%eax
801009a0:	74 16                	je     801009b8 <consoleintr+0x158>
801009a2:	fa                   	cli    
    for(;;)
801009a3:	eb fe                	jmp    801009a3 <consoleintr+0x143>
801009a5:	8d 76 00             	lea    0x0(%esi),%esi
      if(c != 0 && input.e-input.r < INPUT_BUF){
801009a8:	85 db                	test   %ebx,%ebx
801009aa:	0f 84 e8 fe ff ff    	je     80100898 <consoleintr+0x38>
801009b0:	e9 ff fe ff ff       	jmp    801008b4 <consoleintr+0x54>
801009b5:	8d 76 00             	lea    0x0(%esi),%esi
801009b8:	b8 00 01 00 00       	mov    $0x100,%eax
801009bd:	e8 4e fa ff ff       	call   80100410 <consputc.part.0>
801009c2:	e9 d1 fe ff ff       	jmp    80100898 <consoleintr+0x38>
  release(&cons.lock);
801009c7:	83 ec 0c             	sub    $0xc,%esp
801009ca:	68 20 a5 10 80       	push   $0x8010a520
801009cf:	e8 5c 3d 00 00       	call   80104730 <release>
  if(doprocdump) {
801009d4:	83 c4 10             	add    $0x10,%esp
801009d7:	85 f6                	test   %esi,%esi
801009d9:	75 1d                	jne    801009f8 <consoleintr+0x198>
}
801009db:	8d 65 f4             	lea    -0xc(%ebp),%esp
801009de:	5b                   	pop    %ebx
801009df:	5e                   	pop    %esi
801009e0:	5f                   	pop    %edi
801009e1:	5d                   	pop    %ebp
801009e2:	c3                   	ret    
        input.buf[input.e++ % INPUT_BUF] = c;
801009e3:	c6 80 20 ff 10 80 0a 	movb   $0xa,-0x7fef00e0(%eax)
  if(panicked){
801009ea:	85 d2                	test   %edx,%edx
801009ec:	74 16                	je     80100a04 <consoleintr+0x1a4>
801009ee:	fa                   	cli    
    for(;;)
801009ef:	eb fe                	jmp    801009ef <consoleintr+0x18f>
801009f1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
}
801009f8:	8d 65 f4             	lea    -0xc(%ebp),%esp
801009fb:	5b                   	pop    %ebx
801009fc:	5e                   	pop    %esi
801009fd:	5f                   	pop    %edi
801009fe:	5d                   	pop    %ebp
    procdump();  // now call procdump() wo. cons.lock held
801009ff:	e9 dc 38 00 00       	jmp    801042e0 <procdump>
80100a04:	b8 0a 00 00 00       	mov    $0xa,%eax
80100a09:	e8 02 fa ff ff       	call   80100410 <consputc.part.0>
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
80100a0e:	a1 a8 ff 10 80       	mov    0x8010ffa8,%eax
          release(&cons.lock);
80100a13:	83 ec 0c             	sub    $0xc,%esp
          input.w = input.e;
80100a16:	a3 a4 ff 10 80       	mov    %eax,0x8010ffa4
          release(&cons.lock);
80100a1b:	68 20 a5 10 80       	push   $0x8010a520
80100a20:	e8 0b 3d 00 00       	call   80104730 <release>
          wakeup(&input.r);
80100a25:	c7 04 24 a0 ff 10 80 	movl   $0x8010ffa0,(%esp)
80100a2c:	e8 cf 37 00 00       	call   80104200 <wakeup>
          acquire(&cons.lock);
80100a31:	c7 04 24 20 a5 10 80 	movl   $0x8010a520,(%esp)
80100a38:	e8 33 3c 00 00       	call   80104670 <acquire>
80100a3d:	83 c4 10             	add    $0x10,%esp
80100a40:	e9 53 fe ff ff       	jmp    80100898 <consoleintr+0x38>
80100a45:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100a4c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80100a50 <consoleinit>:

void
consoleinit(void)
{
80100a50:	f3 0f 1e fb          	endbr32 
80100a54:	55                   	push   %ebp
80100a55:	89 e5                	mov    %esp,%ebp
80100a57:	83 ec 10             	sub    $0x10,%esp
  initlock(&cons.lock, "console");
80100a5a:	68 68 72 10 80       	push   $0x80107268
80100a5f:	68 20 a5 10 80       	push   $0x8010a520
80100a64:	e8 87 3a 00 00       	call   801044f0 <initlock>

  devsw[CONSOLE].write = consolewrite;
  devsw[CONSOLE].read = consoleread;
  cons.locking = 1;

  ioapicenable(IRQ_KBD, 0);
80100a69:	58                   	pop    %eax
80100a6a:	5a                   	pop    %edx
80100a6b:	6a 00                	push   $0x0
80100a6d:	6a 01                	push   $0x1
  devsw[CONSOLE].write = consolewrite;
80100a6f:	c7 05 6c 09 11 80 40 	movl   $0x80100640,0x8011096c
80100a76:	06 10 80 
  devsw[CONSOLE].read = consoleread;
80100a79:	c7 05 68 09 11 80 90 	movl   $0x80100290,0x80110968
80100a80:	02 10 80 
  cons.locking = 1;
80100a83:	c7 05 54 a5 10 80 01 	movl   $0x1,0x8010a554
80100a8a:	00 00 00 
  ioapicenable(IRQ_KBD, 0);
80100a8d:	e8 be 19 00 00       	call   80102450 <ioapicenable>
}
80100a92:	83 c4 10             	add    $0x10,%esp
80100a95:	c9                   	leave  
80100a96:	c3                   	ret    
80100a97:	66 90                	xchg   %ax,%ax
80100a99:	66 90                	xchg   %ax,%ax
80100a9b:	66 90                	xchg   %ax,%ax
80100a9d:	66 90                	xchg   %ax,%ax
80100a9f:	90                   	nop

80100aa0 <exec>:
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
{
80100aa0:	f3 0f 1e fb          	endbr32 
80100aa4:	55                   	push   %ebp
80100aa5:	89 e5                	mov    %esp,%ebp
80100aa7:	57                   	push   %edi
80100aa8:	56                   	push   %esi
80100aa9:	53                   	push   %ebx
80100aaa:	81 ec 0c 01 00 00    	sub    $0x10c,%esp
  uint argc, sz, sp, ustack[3+MAXARG+1];
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir, *oldpgdir;
  struct proc *curproc = myproc();
80100ab0:	e8 4b 2f 00 00       	call   80103a00 <myproc>
80100ab5:	89 85 ec fe ff ff    	mov    %eax,-0x114(%ebp)

  begin_op();
80100abb:	e8 90 22 00 00       	call   80102d50 <begin_op>

  if((ip = namei(path)) == 0){
80100ac0:	83 ec 0c             	sub    $0xc,%esp
80100ac3:	ff 75 08             	pushl  0x8(%ebp)
80100ac6:	e8 85 15 00 00       	call   80102050 <namei>
80100acb:	83 c4 10             	add    $0x10,%esp
80100ace:	85 c0                	test   %eax,%eax
80100ad0:	0f 84 fe 02 00 00    	je     80100dd4 <exec+0x334>
    end_op();
    cprintf("exec: fail\n");
    return -1;
  }
  ilock(ip);
80100ad6:	83 ec 0c             	sub    $0xc,%esp
80100ad9:	89 c3                	mov    %eax,%ebx
80100adb:	50                   	push   %eax
80100adc:	e8 9f 0c 00 00       	call   80101780 <ilock>
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) != sizeof(elf))
80100ae1:	8d 85 24 ff ff ff    	lea    -0xdc(%ebp),%eax
80100ae7:	6a 34                	push   $0x34
80100ae9:	6a 00                	push   $0x0
80100aeb:	50                   	push   %eax
80100aec:	53                   	push   %ebx
80100aed:	e8 8e 0f 00 00       	call   80101a80 <readi>
80100af2:	83 c4 20             	add    $0x20,%esp
80100af5:	83 f8 34             	cmp    $0x34,%eax
80100af8:	74 26                	je     80100b20 <exec+0x80>

 bad:
  if(pgdir)
    freevm(pgdir);
  if(ip){
    iunlockput(ip);
80100afa:	83 ec 0c             	sub    $0xc,%esp
80100afd:	53                   	push   %ebx
80100afe:	e8 1d 0f 00 00       	call   80101a20 <iunlockput>
    end_op();
80100b03:	e8 b8 22 00 00       	call   80102dc0 <end_op>
80100b08:	83 c4 10             	add    $0x10,%esp
  }
  return -1;
80100b0b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80100b10:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100b13:	5b                   	pop    %ebx
80100b14:	5e                   	pop    %esi
80100b15:	5f                   	pop    %edi
80100b16:	5d                   	pop    %ebp
80100b17:	c3                   	ret    
80100b18:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100b1f:	90                   	nop
  if(elf.magic != ELF_MAGIC)
80100b20:	81 bd 24 ff ff ff 7f 	cmpl   $0x464c457f,-0xdc(%ebp)
80100b27:	45 4c 46 
80100b2a:	75 ce                	jne    80100afa <exec+0x5a>
  if((pgdir = setupkvm()) == 0)
80100b2c:	e8 2f 64 00 00       	call   80106f60 <setupkvm>
80100b31:	89 85 f4 fe ff ff    	mov    %eax,-0x10c(%ebp)
80100b37:	85 c0                	test   %eax,%eax
80100b39:	74 bf                	je     80100afa <exec+0x5a>
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100b3b:	66 83 bd 50 ff ff ff 	cmpw   $0x0,-0xb0(%ebp)
80100b42:	00 
80100b43:	8b b5 40 ff ff ff    	mov    -0xc0(%ebp),%esi
80100b49:	0f 84 a4 02 00 00    	je     80100df3 <exec+0x353>
  sz = 0;
80100b4f:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
80100b56:	00 00 00 
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100b59:	31 ff                	xor    %edi,%edi
80100b5b:	e9 86 00 00 00       	jmp    80100be6 <exec+0x146>
    if(ph.type != ELF_PROG_LOAD)
80100b60:	83 bd 04 ff ff ff 01 	cmpl   $0x1,-0xfc(%ebp)
80100b67:	75 6c                	jne    80100bd5 <exec+0x135>
    if(ph.memsz < ph.filesz)
80100b69:	8b 85 18 ff ff ff    	mov    -0xe8(%ebp),%eax
80100b6f:	3b 85 14 ff ff ff    	cmp    -0xec(%ebp),%eax
80100b75:	0f 82 87 00 00 00    	jb     80100c02 <exec+0x162>
    if(ph.vaddr + ph.memsz < ph.vaddr)
80100b7b:	03 85 0c ff ff ff    	add    -0xf4(%ebp),%eax
80100b81:	72 7f                	jb     80100c02 <exec+0x162>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
80100b83:	83 ec 04             	sub    $0x4,%esp
80100b86:	50                   	push   %eax
80100b87:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100b8d:	ff b5 f4 fe ff ff    	pushl  -0x10c(%ebp)
80100b93:	e8 e8 61 00 00       	call   80106d80 <allocuvm>
80100b98:	83 c4 10             	add    $0x10,%esp
80100b9b:	89 85 f0 fe ff ff    	mov    %eax,-0x110(%ebp)
80100ba1:	85 c0                	test   %eax,%eax
80100ba3:	74 5d                	je     80100c02 <exec+0x162>
    if(ph.vaddr % PGSIZE != 0)
80100ba5:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
80100bab:	a9 ff 0f 00 00       	test   $0xfff,%eax
80100bb0:	75 50                	jne    80100c02 <exec+0x162>
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
80100bb2:	83 ec 0c             	sub    $0xc,%esp
80100bb5:	ff b5 14 ff ff ff    	pushl  -0xec(%ebp)
80100bbb:	ff b5 08 ff ff ff    	pushl  -0xf8(%ebp)
80100bc1:	53                   	push   %ebx
80100bc2:	50                   	push   %eax
80100bc3:	ff b5 f4 fe ff ff    	pushl  -0x10c(%ebp)
80100bc9:	e8 e2 60 00 00       	call   80106cb0 <loaduvm>
80100bce:	83 c4 20             	add    $0x20,%esp
80100bd1:	85 c0                	test   %eax,%eax
80100bd3:	78 2d                	js     80100c02 <exec+0x162>
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100bd5:	0f b7 85 50 ff ff ff 	movzwl -0xb0(%ebp),%eax
80100bdc:	83 c7 01             	add    $0x1,%edi
80100bdf:	83 c6 20             	add    $0x20,%esi
80100be2:	39 f8                	cmp    %edi,%eax
80100be4:	7e 3a                	jle    80100c20 <exec+0x180>
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
80100be6:	8d 85 04 ff ff ff    	lea    -0xfc(%ebp),%eax
80100bec:	6a 20                	push   $0x20
80100bee:	56                   	push   %esi
80100bef:	50                   	push   %eax
80100bf0:	53                   	push   %ebx
80100bf1:	e8 8a 0e 00 00       	call   80101a80 <readi>
80100bf6:	83 c4 10             	add    $0x10,%esp
80100bf9:	83 f8 20             	cmp    $0x20,%eax
80100bfc:	0f 84 5e ff ff ff    	je     80100b60 <exec+0xc0>
    freevm(pgdir);
80100c02:	83 ec 0c             	sub    $0xc,%esp
80100c05:	ff b5 f4 fe ff ff    	pushl  -0x10c(%ebp)
80100c0b:	e8 d0 62 00 00       	call   80106ee0 <freevm>
  if(ip){
80100c10:	83 c4 10             	add    $0x10,%esp
80100c13:	e9 e2 fe ff ff       	jmp    80100afa <exec+0x5a>
80100c18:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100c1f:	90                   	nop
80100c20:	8b bd f0 fe ff ff    	mov    -0x110(%ebp),%edi
80100c26:	81 c7 ff 0f 00 00    	add    $0xfff,%edi
80100c2c:	81 e7 00 f0 ff ff    	and    $0xfffff000,%edi
80100c32:	8d b7 00 20 00 00    	lea    0x2000(%edi),%esi
  iunlockput(ip);
80100c38:	83 ec 0c             	sub    $0xc,%esp
80100c3b:	53                   	push   %ebx
80100c3c:	e8 df 0d 00 00       	call   80101a20 <iunlockput>
  end_op();
80100c41:	e8 7a 21 00 00       	call   80102dc0 <end_op>
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
80100c46:	83 c4 0c             	add    $0xc,%esp
80100c49:	56                   	push   %esi
80100c4a:	57                   	push   %edi
80100c4b:	8b bd f4 fe ff ff    	mov    -0x10c(%ebp),%edi
80100c51:	57                   	push   %edi
80100c52:	e8 29 61 00 00       	call   80106d80 <allocuvm>
80100c57:	83 c4 10             	add    $0x10,%esp
80100c5a:	89 c6                	mov    %eax,%esi
80100c5c:	85 c0                	test   %eax,%eax
80100c5e:	0f 84 94 00 00 00    	je     80100cf8 <exec+0x258>
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80100c64:	83 ec 08             	sub    $0x8,%esp
80100c67:	8d 80 00 e0 ff ff    	lea    -0x2000(%eax),%eax
  for(argc = 0; argv[argc]; argc++) {
80100c6d:	89 f3                	mov    %esi,%ebx
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80100c6f:	50                   	push   %eax
80100c70:	57                   	push   %edi
  for(argc = 0; argv[argc]; argc++) {
80100c71:	31 ff                	xor    %edi,%edi
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80100c73:	e8 88 63 00 00       	call   80107000 <clearpteu>
  for(argc = 0; argv[argc]; argc++) {
80100c78:	8b 45 0c             	mov    0xc(%ebp),%eax
80100c7b:	83 c4 10             	add    $0x10,%esp
80100c7e:	8d 95 58 ff ff ff    	lea    -0xa8(%ebp),%edx
80100c84:	8b 00                	mov    (%eax),%eax
80100c86:	85 c0                	test   %eax,%eax
80100c88:	0f 84 8b 00 00 00    	je     80100d19 <exec+0x279>
80100c8e:	89 b5 f0 fe ff ff    	mov    %esi,-0x110(%ebp)
80100c94:	8b b5 f4 fe ff ff    	mov    -0x10c(%ebp),%esi
80100c9a:	eb 23                	jmp    80100cbf <exec+0x21f>
80100c9c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100ca0:	8b 45 0c             	mov    0xc(%ebp),%eax
    ustack[3+argc] = sp;
80100ca3:	89 9c bd 64 ff ff ff 	mov    %ebx,-0x9c(%ebp,%edi,4)
  for(argc = 0; argv[argc]; argc++) {
80100caa:	83 c7 01             	add    $0x1,%edi
    ustack[3+argc] = sp;
80100cad:	8d 95 58 ff ff ff    	lea    -0xa8(%ebp),%edx
  for(argc = 0; argv[argc]; argc++) {
80100cb3:	8b 04 b8             	mov    (%eax,%edi,4),%eax
80100cb6:	85 c0                	test   %eax,%eax
80100cb8:	74 59                	je     80100d13 <exec+0x273>
    if(argc >= MAXARG)
80100cba:	83 ff 20             	cmp    $0x20,%edi
80100cbd:	74 39                	je     80100cf8 <exec+0x258>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80100cbf:	83 ec 0c             	sub    $0xc,%esp
80100cc2:	50                   	push   %eax
80100cc3:	e8 b8 3c 00 00       	call   80104980 <strlen>
80100cc8:	f7 d0                	not    %eax
80100cca:	01 c3                	add    %eax,%ebx
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
80100ccc:	58                   	pop    %eax
80100ccd:	8b 45 0c             	mov    0xc(%ebp),%eax
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80100cd0:	83 e3 fc             	and    $0xfffffffc,%ebx
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
80100cd3:	ff 34 b8             	pushl  (%eax,%edi,4)
80100cd6:	e8 a5 3c 00 00       	call   80104980 <strlen>
80100cdb:	83 c0 01             	add    $0x1,%eax
80100cde:	50                   	push   %eax
80100cdf:	8b 45 0c             	mov    0xc(%ebp),%eax
80100ce2:	ff 34 b8             	pushl  (%eax,%edi,4)
80100ce5:	53                   	push   %ebx
80100ce6:	56                   	push   %esi
80100ce7:	e8 74 64 00 00       	call   80107160 <copyout>
80100cec:	83 c4 20             	add    $0x20,%esp
80100cef:	85 c0                	test   %eax,%eax
80100cf1:	79 ad                	jns    80100ca0 <exec+0x200>
80100cf3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100cf7:	90                   	nop
    freevm(pgdir);
80100cf8:	83 ec 0c             	sub    $0xc,%esp
80100cfb:	ff b5 f4 fe ff ff    	pushl  -0x10c(%ebp)
80100d01:	e8 da 61 00 00       	call   80106ee0 <freevm>
80100d06:	83 c4 10             	add    $0x10,%esp
  return -1;
80100d09:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100d0e:	e9 fd fd ff ff       	jmp    80100b10 <exec+0x70>
80100d13:	8b b5 f0 fe ff ff    	mov    -0x110(%ebp),%esi
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100d19:	8d 04 bd 04 00 00 00 	lea    0x4(,%edi,4),%eax
80100d20:	89 d9                	mov    %ebx,%ecx
  ustack[3+argc] = 0;
80100d22:	c7 84 bd 64 ff ff ff 	movl   $0x0,-0x9c(%ebp,%edi,4)
80100d29:	00 00 00 00 
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100d2d:	29 c1                	sub    %eax,%ecx
  sp -= (3+argc+1) * 4;
80100d2f:	83 c0 0c             	add    $0xc,%eax
  ustack[1] = argc;
80100d32:	89 bd 5c ff ff ff    	mov    %edi,-0xa4(%ebp)
  sp -= (3+argc+1) * 4;
80100d38:	29 c3                	sub    %eax,%ebx
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80100d3a:	50                   	push   %eax
80100d3b:	52                   	push   %edx
80100d3c:	53                   	push   %ebx
80100d3d:	ff b5 f4 fe ff ff    	pushl  -0x10c(%ebp)
  ustack[0] = 0xffffffff;  // fake return PC
80100d43:	c7 85 58 ff ff ff ff 	movl   $0xffffffff,-0xa8(%ebp)
80100d4a:	ff ff ff 
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100d4d:	89 8d 60 ff ff ff    	mov    %ecx,-0xa0(%ebp)
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80100d53:	e8 08 64 00 00       	call   80107160 <copyout>
80100d58:	83 c4 10             	add    $0x10,%esp
80100d5b:	85 c0                	test   %eax,%eax
80100d5d:	78 99                	js     80100cf8 <exec+0x258>
  for(last=s=path; *s; s++)
80100d5f:	8b 45 08             	mov    0x8(%ebp),%eax
80100d62:	8b 55 08             	mov    0x8(%ebp),%edx
80100d65:	0f b6 00             	movzbl (%eax),%eax
80100d68:	84 c0                	test   %al,%al
80100d6a:	74 13                	je     80100d7f <exec+0x2df>
80100d6c:	89 d1                	mov    %edx,%ecx
80100d6e:	66 90                	xchg   %ax,%ax
    if(*s == '/')
80100d70:	83 c1 01             	add    $0x1,%ecx
80100d73:	3c 2f                	cmp    $0x2f,%al
  for(last=s=path; *s; s++)
80100d75:	0f b6 01             	movzbl (%ecx),%eax
    if(*s == '/')
80100d78:	0f 44 d1             	cmove  %ecx,%edx
  for(last=s=path; *s; s++)
80100d7b:	84 c0                	test   %al,%al
80100d7d:	75 f1                	jne    80100d70 <exec+0x2d0>
  safestrcpy(curproc->name, last, sizeof(curproc->name));
80100d7f:	8b bd ec fe ff ff    	mov    -0x114(%ebp),%edi
80100d85:	83 ec 04             	sub    $0x4,%esp
80100d88:	6a 10                	push   $0x10
80100d8a:	89 f8                	mov    %edi,%eax
80100d8c:	52                   	push   %edx
80100d8d:	83 c0 6c             	add    $0x6c,%eax
80100d90:	50                   	push   %eax
80100d91:	e8 aa 3b 00 00       	call   80104940 <safestrcpy>
  curproc->pgdir = pgdir;
80100d96:	8b 8d f4 fe ff ff    	mov    -0x10c(%ebp),%ecx
  oldpgdir = curproc->pgdir;
80100d9c:	89 f8                	mov    %edi,%eax
80100d9e:	8b 7f 04             	mov    0x4(%edi),%edi
  curproc->sz = sz;
80100da1:	89 30                	mov    %esi,(%eax)
  curproc->pgdir = pgdir;
80100da3:	89 48 04             	mov    %ecx,0x4(%eax)
  curproc->tf->eip = elf.entry;  // main
80100da6:	89 c1                	mov    %eax,%ecx
80100da8:	8b 95 3c ff ff ff    	mov    -0xc4(%ebp),%edx
80100dae:	8b 40 18             	mov    0x18(%eax),%eax
80100db1:	89 50 38             	mov    %edx,0x38(%eax)
  curproc->tf->esp = sp;
80100db4:	8b 41 18             	mov    0x18(%ecx),%eax
80100db7:	89 58 44             	mov    %ebx,0x44(%eax)
  switchuvm(curproc);
80100dba:	89 0c 24             	mov    %ecx,(%esp)
80100dbd:	e8 5e 5d 00 00       	call   80106b20 <switchuvm>
  freevm(oldpgdir);
80100dc2:	89 3c 24             	mov    %edi,(%esp)
80100dc5:	e8 16 61 00 00       	call   80106ee0 <freevm>
  return 0;
80100dca:	83 c4 10             	add    $0x10,%esp
80100dcd:	31 c0                	xor    %eax,%eax
80100dcf:	e9 3c fd ff ff       	jmp    80100b10 <exec+0x70>
    end_op();
80100dd4:	e8 e7 1f 00 00       	call   80102dc0 <end_op>
    cprintf("exec: fail\n");
80100dd9:	83 ec 0c             	sub    $0xc,%esp
80100ddc:	68 81 72 10 80       	push   $0x80107281
80100de1:	e8 ca f8 ff ff       	call   801006b0 <cprintf>
    return -1;
80100de6:	83 c4 10             	add    $0x10,%esp
80100de9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100dee:	e9 1d fd ff ff       	jmp    80100b10 <exec+0x70>
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100df3:	31 ff                	xor    %edi,%edi
80100df5:	be 00 20 00 00       	mov    $0x2000,%esi
80100dfa:	e9 39 fe ff ff       	jmp    80100c38 <exec+0x198>
80100dff:	90                   	nop

80100e00 <fileinit>:
  struct file file[NFILE];
} ftable;

void
fileinit(void)
{
80100e00:	f3 0f 1e fb          	endbr32 
80100e04:	55                   	push   %ebp
80100e05:	89 e5                	mov    %esp,%ebp
80100e07:	83 ec 10             	sub    $0x10,%esp
  initlock(&ftable.lock, "ftable");
80100e0a:	68 8d 72 10 80       	push   $0x8010728d
80100e0f:	68 c0 ff 10 80       	push   $0x8010ffc0
80100e14:	e8 d7 36 00 00       	call   801044f0 <initlock>
}
80100e19:	83 c4 10             	add    $0x10,%esp
80100e1c:	c9                   	leave  
80100e1d:	c3                   	ret    
80100e1e:	66 90                	xchg   %ax,%ax

80100e20 <filealloc>:

// Allocate a file structure.
struct file*
filealloc(void)
{
80100e20:	f3 0f 1e fb          	endbr32 
80100e24:	55                   	push   %ebp
80100e25:	89 e5                	mov    %esp,%ebp
80100e27:	53                   	push   %ebx
  struct file *f;

  acquire(&ftable.lock);
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80100e28:	bb f4 ff 10 80       	mov    $0x8010fff4,%ebx
{
80100e2d:	83 ec 10             	sub    $0x10,%esp
  acquire(&ftable.lock);
80100e30:	68 c0 ff 10 80       	push   $0x8010ffc0
80100e35:	e8 36 38 00 00       	call   80104670 <acquire>
80100e3a:	83 c4 10             	add    $0x10,%esp
80100e3d:	eb 0c                	jmp    80100e4b <filealloc+0x2b>
80100e3f:	90                   	nop
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80100e40:	83 c3 18             	add    $0x18,%ebx
80100e43:	81 fb 54 09 11 80    	cmp    $0x80110954,%ebx
80100e49:	74 25                	je     80100e70 <filealloc+0x50>
    if(f->ref == 0){
80100e4b:	8b 43 04             	mov    0x4(%ebx),%eax
80100e4e:	85 c0                	test   %eax,%eax
80100e50:	75 ee                	jne    80100e40 <filealloc+0x20>
      f->ref = 1;
      release(&ftable.lock);
80100e52:	83 ec 0c             	sub    $0xc,%esp
      f->ref = 1;
80100e55:	c7 43 04 01 00 00 00 	movl   $0x1,0x4(%ebx)
      release(&ftable.lock);
80100e5c:	68 c0 ff 10 80       	push   $0x8010ffc0
80100e61:	e8 ca 38 00 00       	call   80104730 <release>
      return f;
    }
  }
  release(&ftable.lock);
  return 0;
}
80100e66:	89 d8                	mov    %ebx,%eax
      return f;
80100e68:	83 c4 10             	add    $0x10,%esp
}
80100e6b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100e6e:	c9                   	leave  
80100e6f:	c3                   	ret    
  release(&ftable.lock);
80100e70:	83 ec 0c             	sub    $0xc,%esp
  return 0;
80100e73:	31 db                	xor    %ebx,%ebx
  release(&ftable.lock);
80100e75:	68 c0 ff 10 80       	push   $0x8010ffc0
80100e7a:	e8 b1 38 00 00       	call   80104730 <release>
}
80100e7f:	89 d8                	mov    %ebx,%eax
  return 0;
80100e81:	83 c4 10             	add    $0x10,%esp
}
80100e84:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100e87:	c9                   	leave  
80100e88:	c3                   	ret    
80100e89:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80100e90 <filedup>:

// Increment ref count for file f.
struct file*
filedup(struct file *f)
{
80100e90:	f3 0f 1e fb          	endbr32 
80100e94:	55                   	push   %ebp
80100e95:	89 e5                	mov    %esp,%ebp
80100e97:	53                   	push   %ebx
80100e98:	83 ec 10             	sub    $0x10,%esp
80100e9b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&ftable.lock);
80100e9e:	68 c0 ff 10 80       	push   $0x8010ffc0
80100ea3:	e8 c8 37 00 00       	call   80104670 <acquire>
  if(f->ref < 1)
80100ea8:	8b 43 04             	mov    0x4(%ebx),%eax
80100eab:	83 c4 10             	add    $0x10,%esp
80100eae:	85 c0                	test   %eax,%eax
80100eb0:	7e 1a                	jle    80100ecc <filedup+0x3c>
    panic("filedup");
  f->ref++;
80100eb2:	83 c0 01             	add    $0x1,%eax
  release(&ftable.lock);
80100eb5:	83 ec 0c             	sub    $0xc,%esp
  f->ref++;
80100eb8:	89 43 04             	mov    %eax,0x4(%ebx)
  release(&ftable.lock);
80100ebb:	68 c0 ff 10 80       	push   $0x8010ffc0
80100ec0:	e8 6b 38 00 00       	call   80104730 <release>
  return f;
}
80100ec5:	89 d8                	mov    %ebx,%eax
80100ec7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100eca:	c9                   	leave  
80100ecb:	c3                   	ret    
    panic("filedup");
80100ecc:	83 ec 0c             	sub    $0xc,%esp
80100ecf:	68 94 72 10 80       	push   $0x80107294
80100ed4:	e8 b7 f4 ff ff       	call   80100390 <panic>
80100ed9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80100ee0 <fileclose>:

// Close file f.  (Decrement ref count, close when reaches 0.)
void
fileclose(struct file *f)
{
80100ee0:	f3 0f 1e fb          	endbr32 
80100ee4:	55                   	push   %ebp
80100ee5:	89 e5                	mov    %esp,%ebp
80100ee7:	57                   	push   %edi
80100ee8:	56                   	push   %esi
80100ee9:	53                   	push   %ebx
80100eea:	83 ec 28             	sub    $0x28,%esp
80100eed:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct file ff;

  acquire(&ftable.lock);
80100ef0:	68 c0 ff 10 80       	push   $0x8010ffc0
80100ef5:	e8 76 37 00 00       	call   80104670 <acquire>
  if(f->ref < 1)
80100efa:	8b 53 04             	mov    0x4(%ebx),%edx
80100efd:	83 c4 10             	add    $0x10,%esp
80100f00:	85 d2                	test   %edx,%edx
80100f02:	0f 8e a1 00 00 00    	jle    80100fa9 <fileclose+0xc9>
    panic("fileclose");
  if(--f->ref > 0){
80100f08:	83 ea 01             	sub    $0x1,%edx
80100f0b:	89 53 04             	mov    %edx,0x4(%ebx)
80100f0e:	75 40                	jne    80100f50 <fileclose+0x70>
    release(&ftable.lock);
    return;
  }
  ff = *f;
80100f10:	0f b6 43 09          	movzbl 0x9(%ebx),%eax
  f->ref = 0;
  f->type = FD_NONE;
  release(&ftable.lock);
80100f14:	83 ec 0c             	sub    $0xc,%esp
  ff = *f;
80100f17:	8b 3b                	mov    (%ebx),%edi
  f->type = FD_NONE;
80100f19:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  ff = *f;
80100f1f:	8b 73 0c             	mov    0xc(%ebx),%esi
80100f22:	88 45 e7             	mov    %al,-0x19(%ebp)
80100f25:	8b 43 10             	mov    0x10(%ebx),%eax
  release(&ftable.lock);
80100f28:	68 c0 ff 10 80       	push   $0x8010ffc0
  ff = *f;
80100f2d:	89 45 e0             	mov    %eax,-0x20(%ebp)
  release(&ftable.lock);
80100f30:	e8 fb 37 00 00       	call   80104730 <release>

  if(ff.type == FD_PIPE)
80100f35:	83 c4 10             	add    $0x10,%esp
80100f38:	83 ff 01             	cmp    $0x1,%edi
80100f3b:	74 53                	je     80100f90 <fileclose+0xb0>
    pipeclose(ff.pipe, ff.writable);
  else if(ff.type == FD_INODE){
80100f3d:	83 ff 02             	cmp    $0x2,%edi
80100f40:	74 26                	je     80100f68 <fileclose+0x88>
    begin_op();
    iput(ff.ip);
    end_op();
  }
}
80100f42:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100f45:	5b                   	pop    %ebx
80100f46:	5e                   	pop    %esi
80100f47:	5f                   	pop    %edi
80100f48:	5d                   	pop    %ebp
80100f49:	c3                   	ret    
80100f4a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    release(&ftable.lock);
80100f50:	c7 45 08 c0 ff 10 80 	movl   $0x8010ffc0,0x8(%ebp)
}
80100f57:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100f5a:	5b                   	pop    %ebx
80100f5b:	5e                   	pop    %esi
80100f5c:	5f                   	pop    %edi
80100f5d:	5d                   	pop    %ebp
    release(&ftable.lock);
80100f5e:	e9 cd 37 00 00       	jmp    80104730 <release>
80100f63:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100f67:	90                   	nop
    begin_op();
80100f68:	e8 e3 1d 00 00       	call   80102d50 <begin_op>
    iput(ff.ip);
80100f6d:	83 ec 0c             	sub    $0xc,%esp
80100f70:	ff 75 e0             	pushl  -0x20(%ebp)
80100f73:	e8 38 09 00 00       	call   801018b0 <iput>
    end_op();
80100f78:	83 c4 10             	add    $0x10,%esp
}
80100f7b:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100f7e:	5b                   	pop    %ebx
80100f7f:	5e                   	pop    %esi
80100f80:	5f                   	pop    %edi
80100f81:	5d                   	pop    %ebp
    end_op();
80100f82:	e9 39 1e 00 00       	jmp    80102dc0 <end_op>
80100f87:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100f8e:	66 90                	xchg   %ax,%ax
    pipeclose(ff.pipe, ff.writable);
80100f90:	0f be 5d e7          	movsbl -0x19(%ebp),%ebx
80100f94:	83 ec 08             	sub    $0x8,%esp
80100f97:	53                   	push   %ebx
80100f98:	56                   	push   %esi
80100f99:	e8 82 25 00 00       	call   80103520 <pipeclose>
80100f9e:	83 c4 10             	add    $0x10,%esp
}
80100fa1:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100fa4:	5b                   	pop    %ebx
80100fa5:	5e                   	pop    %esi
80100fa6:	5f                   	pop    %edi
80100fa7:	5d                   	pop    %ebp
80100fa8:	c3                   	ret    
    panic("fileclose");
80100fa9:	83 ec 0c             	sub    $0xc,%esp
80100fac:	68 9c 72 10 80       	push   $0x8010729c
80100fb1:	e8 da f3 ff ff       	call   80100390 <panic>
80100fb6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100fbd:	8d 76 00             	lea    0x0(%esi),%esi

80100fc0 <filestat>:

// Get metadata about file f.
int
filestat(struct file *f, struct stat *st)
{
80100fc0:	f3 0f 1e fb          	endbr32 
80100fc4:	55                   	push   %ebp
80100fc5:	89 e5                	mov    %esp,%ebp
80100fc7:	53                   	push   %ebx
80100fc8:	83 ec 04             	sub    $0x4,%esp
80100fcb:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(f->type == FD_INODE){
80100fce:	83 3b 02             	cmpl   $0x2,(%ebx)
80100fd1:	75 2d                	jne    80101000 <filestat+0x40>
    ilock(f->ip);
80100fd3:	83 ec 0c             	sub    $0xc,%esp
80100fd6:	ff 73 10             	pushl  0x10(%ebx)
80100fd9:	e8 a2 07 00 00       	call   80101780 <ilock>
    stati(f->ip, st);
80100fde:	58                   	pop    %eax
80100fdf:	5a                   	pop    %edx
80100fe0:	ff 75 0c             	pushl  0xc(%ebp)
80100fe3:	ff 73 10             	pushl  0x10(%ebx)
80100fe6:	e8 65 0a 00 00       	call   80101a50 <stati>
    iunlock(f->ip);
80100feb:	59                   	pop    %ecx
80100fec:	ff 73 10             	pushl  0x10(%ebx)
80100fef:	e8 6c 08 00 00       	call   80101860 <iunlock>
    return 0;
  }
  return -1;
}
80100ff4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
    return 0;
80100ff7:	83 c4 10             	add    $0x10,%esp
80100ffa:	31 c0                	xor    %eax,%eax
}
80100ffc:	c9                   	leave  
80100ffd:	c3                   	ret    
80100ffe:	66 90                	xchg   %ax,%ax
80101000:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  return -1;
80101003:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80101008:	c9                   	leave  
80101009:	c3                   	ret    
8010100a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80101010 <fileread>:

// Read from file f.
int
fileread(struct file *f, char *addr, int n)
{
80101010:	f3 0f 1e fb          	endbr32 
80101014:	55                   	push   %ebp
80101015:	89 e5                	mov    %esp,%ebp
80101017:	57                   	push   %edi
80101018:	56                   	push   %esi
80101019:	53                   	push   %ebx
8010101a:	83 ec 0c             	sub    $0xc,%esp
8010101d:	8b 5d 08             	mov    0x8(%ebp),%ebx
80101020:	8b 75 0c             	mov    0xc(%ebp),%esi
80101023:	8b 7d 10             	mov    0x10(%ebp),%edi
  int r;

  if(f->readable == 0)
80101026:	80 7b 08 00          	cmpb   $0x0,0x8(%ebx)
8010102a:	74 64                	je     80101090 <fileread+0x80>
    return -1;
  if(f->type == FD_PIPE)
8010102c:	8b 03                	mov    (%ebx),%eax
8010102e:	83 f8 01             	cmp    $0x1,%eax
80101031:	74 45                	je     80101078 <fileread+0x68>
    return piperead(f->pipe, addr, n);
  if(f->type == FD_INODE){
80101033:	83 f8 02             	cmp    $0x2,%eax
80101036:	75 5f                	jne    80101097 <fileread+0x87>
    ilock(f->ip);
80101038:	83 ec 0c             	sub    $0xc,%esp
8010103b:	ff 73 10             	pushl  0x10(%ebx)
8010103e:	e8 3d 07 00 00       	call   80101780 <ilock>
    if((r = readi(f->ip, addr, f->off, n)) > 0)
80101043:	57                   	push   %edi
80101044:	ff 73 14             	pushl  0x14(%ebx)
80101047:	56                   	push   %esi
80101048:	ff 73 10             	pushl  0x10(%ebx)
8010104b:	e8 30 0a 00 00       	call   80101a80 <readi>
80101050:	83 c4 20             	add    $0x20,%esp
80101053:	89 c6                	mov    %eax,%esi
80101055:	85 c0                	test   %eax,%eax
80101057:	7e 03                	jle    8010105c <fileread+0x4c>
      f->off += r;
80101059:	01 43 14             	add    %eax,0x14(%ebx)
    iunlock(f->ip);
8010105c:	83 ec 0c             	sub    $0xc,%esp
8010105f:	ff 73 10             	pushl  0x10(%ebx)
80101062:	e8 f9 07 00 00       	call   80101860 <iunlock>
    return r;
80101067:	83 c4 10             	add    $0x10,%esp
  }
  panic("fileread");
}
8010106a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010106d:	89 f0                	mov    %esi,%eax
8010106f:	5b                   	pop    %ebx
80101070:	5e                   	pop    %esi
80101071:	5f                   	pop    %edi
80101072:	5d                   	pop    %ebp
80101073:	c3                   	ret    
80101074:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return piperead(f->pipe, addr, n);
80101078:	8b 43 0c             	mov    0xc(%ebx),%eax
8010107b:	89 45 08             	mov    %eax,0x8(%ebp)
}
8010107e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101081:	5b                   	pop    %ebx
80101082:	5e                   	pop    %esi
80101083:	5f                   	pop    %edi
80101084:	5d                   	pop    %ebp
    return piperead(f->pipe, addr, n);
80101085:	e9 36 26 00 00       	jmp    801036c0 <piperead>
8010108a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    return -1;
80101090:	be ff ff ff ff       	mov    $0xffffffff,%esi
80101095:	eb d3                	jmp    8010106a <fileread+0x5a>
  panic("fileread");
80101097:	83 ec 0c             	sub    $0xc,%esp
8010109a:	68 a6 72 10 80       	push   $0x801072a6
8010109f:	e8 ec f2 ff ff       	call   80100390 <panic>
801010a4:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801010ab:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801010af:	90                   	nop

801010b0 <filewrite>:

//PAGEBREAK!
// Write to file f.
int
filewrite(struct file *f, char *addr, int n)
{
801010b0:	f3 0f 1e fb          	endbr32 
801010b4:	55                   	push   %ebp
801010b5:	89 e5                	mov    %esp,%ebp
801010b7:	57                   	push   %edi
801010b8:	56                   	push   %esi
801010b9:	53                   	push   %ebx
801010ba:	83 ec 1c             	sub    $0x1c,%esp
801010bd:	8b 45 0c             	mov    0xc(%ebp),%eax
801010c0:	8b 75 08             	mov    0x8(%ebp),%esi
801010c3:	89 45 dc             	mov    %eax,-0x24(%ebp)
801010c6:	8b 45 10             	mov    0x10(%ebp),%eax
  int r;

  if(f->writable == 0)
801010c9:	80 7e 09 00          	cmpb   $0x0,0x9(%esi)
{
801010cd:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if(f->writable == 0)
801010d0:	0f 84 c1 00 00 00    	je     80101197 <filewrite+0xe7>
    return -1;
  if(f->type == FD_PIPE)
801010d6:	8b 06                	mov    (%esi),%eax
801010d8:	83 f8 01             	cmp    $0x1,%eax
801010db:	0f 84 c3 00 00 00    	je     801011a4 <filewrite+0xf4>
    return pipewrite(f->pipe, addr, n);
  if(f->type == FD_INODE){
801010e1:	83 f8 02             	cmp    $0x2,%eax
801010e4:	0f 85 cc 00 00 00    	jne    801011b6 <filewrite+0x106>
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((MAXOPBLOCKS-1-1-2) / 2) * 512;
    int i = 0;
    while(i < n){
801010ea:	8b 45 e4             	mov    -0x1c(%ebp),%eax
    int i = 0;
801010ed:	31 ff                	xor    %edi,%edi
    while(i < n){
801010ef:	85 c0                	test   %eax,%eax
801010f1:	7f 34                	jg     80101127 <filewrite+0x77>
801010f3:	e9 98 00 00 00       	jmp    80101190 <filewrite+0xe0>
801010f8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801010ff:	90                   	nop
        n1 = max;

      begin_op();
      ilock(f->ip);
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
        f->off += r;
80101100:	01 46 14             	add    %eax,0x14(%esi)
      iunlock(f->ip);
80101103:	83 ec 0c             	sub    $0xc,%esp
80101106:	ff 76 10             	pushl  0x10(%esi)
        f->off += r;
80101109:	89 45 e0             	mov    %eax,-0x20(%ebp)
      iunlock(f->ip);
8010110c:	e8 4f 07 00 00       	call   80101860 <iunlock>
      end_op();
80101111:	e8 aa 1c 00 00       	call   80102dc0 <end_op>

      if(r < 0)
        break;
      if(r != n1)
80101116:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101119:	83 c4 10             	add    $0x10,%esp
8010111c:	39 c3                	cmp    %eax,%ebx
8010111e:	75 60                	jne    80101180 <filewrite+0xd0>
        panic("short filewrite");
      i += r;
80101120:	01 df                	add    %ebx,%edi
    while(i < n){
80101122:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
80101125:	7e 69                	jle    80101190 <filewrite+0xe0>
      int n1 = n - i;
80101127:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
8010112a:	b8 00 06 00 00       	mov    $0x600,%eax
8010112f:	29 fb                	sub    %edi,%ebx
      if(n1 > max)
80101131:	81 fb 00 06 00 00    	cmp    $0x600,%ebx
80101137:	0f 4f d8             	cmovg  %eax,%ebx
      begin_op();
8010113a:	e8 11 1c 00 00       	call   80102d50 <begin_op>
      ilock(f->ip);
8010113f:	83 ec 0c             	sub    $0xc,%esp
80101142:	ff 76 10             	pushl  0x10(%esi)
80101145:	e8 36 06 00 00       	call   80101780 <ilock>
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
8010114a:	8b 45 dc             	mov    -0x24(%ebp),%eax
8010114d:	53                   	push   %ebx
8010114e:	ff 76 14             	pushl  0x14(%esi)
80101151:	01 f8                	add    %edi,%eax
80101153:	50                   	push   %eax
80101154:	ff 76 10             	pushl  0x10(%esi)
80101157:	e8 24 0a 00 00       	call   80101b80 <writei>
8010115c:	83 c4 20             	add    $0x20,%esp
8010115f:	85 c0                	test   %eax,%eax
80101161:	7f 9d                	jg     80101100 <filewrite+0x50>
      iunlock(f->ip);
80101163:	83 ec 0c             	sub    $0xc,%esp
80101166:	ff 76 10             	pushl  0x10(%esi)
80101169:	89 45 e4             	mov    %eax,-0x1c(%ebp)
8010116c:	e8 ef 06 00 00       	call   80101860 <iunlock>
      end_op();
80101171:	e8 4a 1c 00 00       	call   80102dc0 <end_op>
      if(r < 0)
80101176:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101179:	83 c4 10             	add    $0x10,%esp
8010117c:	85 c0                	test   %eax,%eax
8010117e:	75 17                	jne    80101197 <filewrite+0xe7>
        panic("short filewrite");
80101180:	83 ec 0c             	sub    $0xc,%esp
80101183:	68 af 72 10 80       	push   $0x801072af
80101188:	e8 03 f2 ff ff       	call   80100390 <panic>
8010118d:	8d 76 00             	lea    0x0(%esi),%esi
    }
    return i == n ? n : -1;
80101190:	89 f8                	mov    %edi,%eax
80101192:	3b 7d e4             	cmp    -0x1c(%ebp),%edi
80101195:	74 05                	je     8010119c <filewrite+0xec>
80101197:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  }
  panic("filewrite");
}
8010119c:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010119f:	5b                   	pop    %ebx
801011a0:	5e                   	pop    %esi
801011a1:	5f                   	pop    %edi
801011a2:	5d                   	pop    %ebp
801011a3:	c3                   	ret    
    return pipewrite(f->pipe, addr, n);
801011a4:	8b 46 0c             	mov    0xc(%esi),%eax
801011a7:	89 45 08             	mov    %eax,0x8(%ebp)
}
801011aa:	8d 65 f4             	lea    -0xc(%ebp),%esp
801011ad:	5b                   	pop    %ebx
801011ae:	5e                   	pop    %esi
801011af:	5f                   	pop    %edi
801011b0:	5d                   	pop    %ebp
    return pipewrite(f->pipe, addr, n);
801011b1:	e9 0a 24 00 00       	jmp    801035c0 <pipewrite>
  panic("filewrite");
801011b6:	83 ec 0c             	sub    $0xc,%esp
801011b9:	68 b5 72 10 80       	push   $0x801072b5
801011be:	e8 cd f1 ff ff       	call   80100390 <panic>
801011c3:	66 90                	xchg   %ax,%ax
801011c5:	66 90                	xchg   %ax,%ax
801011c7:	66 90                	xchg   %ax,%ax
801011c9:	66 90                	xchg   %ax,%ax
801011cb:	66 90                	xchg   %ax,%ax
801011cd:	66 90                	xchg   %ax,%ax
801011cf:	90                   	nop

801011d0 <bfree>:
}

// Free a disk block.
static void
bfree(int dev, uint b)
{
801011d0:	55                   	push   %ebp
801011d1:	89 c1                	mov    %eax,%ecx
  struct buf *bp;
  int bi, m;

  bp = bread(dev, BBLOCK(b, sb));
801011d3:	89 d0                	mov    %edx,%eax
801011d5:	c1 e8 0c             	shr    $0xc,%eax
801011d8:	03 05 d8 09 11 80    	add    0x801109d8,%eax
{
801011de:	89 e5                	mov    %esp,%ebp
801011e0:	56                   	push   %esi
801011e1:	53                   	push   %ebx
801011e2:	89 d3                	mov    %edx,%ebx
  bp = bread(dev, BBLOCK(b, sb));
801011e4:	83 ec 08             	sub    $0x8,%esp
801011e7:	50                   	push   %eax
801011e8:	51                   	push   %ecx
801011e9:	e8 e2 ee ff ff       	call   801000d0 <bread>
  bi = b % BPB;
  m = 1 << (bi % 8);
801011ee:	89 d9                	mov    %ebx,%ecx
  if((bp->data[bi/8] & m) == 0)
801011f0:	c1 fb 03             	sar    $0x3,%ebx
  m = 1 << (bi % 8);
801011f3:	ba 01 00 00 00       	mov    $0x1,%edx
801011f8:	83 e1 07             	and    $0x7,%ecx
  if((bp->data[bi/8] & m) == 0)
801011fb:	81 e3 ff 01 00 00    	and    $0x1ff,%ebx
80101201:	83 c4 10             	add    $0x10,%esp
  m = 1 << (bi % 8);
80101204:	d3 e2                	shl    %cl,%edx
  if((bp->data[bi/8] & m) == 0)
80101206:	0f b6 4c 18 5c       	movzbl 0x5c(%eax,%ebx,1),%ecx
8010120b:	85 d1                	test   %edx,%ecx
8010120d:	74 25                	je     80101234 <bfree+0x64>
    panic("freeing free block");
  bp->data[bi/8] &= ~m;
8010120f:	f7 d2                	not    %edx
  log_write(bp);
80101211:	83 ec 0c             	sub    $0xc,%esp
80101214:	89 c6                	mov    %eax,%esi
  bp->data[bi/8] &= ~m;
80101216:	21 ca                	and    %ecx,%edx
80101218:	88 54 18 5c          	mov    %dl,0x5c(%eax,%ebx,1)
  log_write(bp);
8010121c:	50                   	push   %eax
8010121d:	e8 0e 1d 00 00       	call   80102f30 <log_write>
  brelse(bp);
80101222:	89 34 24             	mov    %esi,(%esp)
80101225:	e8 c6 ef ff ff       	call   801001f0 <brelse>
}
8010122a:	83 c4 10             	add    $0x10,%esp
8010122d:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101230:	5b                   	pop    %ebx
80101231:	5e                   	pop    %esi
80101232:	5d                   	pop    %ebp
80101233:	c3                   	ret    
    panic("freeing free block");
80101234:	83 ec 0c             	sub    $0xc,%esp
80101237:	68 bf 72 10 80       	push   $0x801072bf
8010123c:	e8 4f f1 ff ff       	call   80100390 <panic>
80101241:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101248:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010124f:	90                   	nop

80101250 <balloc>:
{
80101250:	55                   	push   %ebp
80101251:	89 e5                	mov    %esp,%ebp
80101253:	57                   	push   %edi
80101254:	56                   	push   %esi
80101255:	53                   	push   %ebx
80101256:	83 ec 1c             	sub    $0x1c,%esp
  for(b = 0; b < sb.size; b += BPB){
80101259:	8b 0d c0 09 11 80    	mov    0x801109c0,%ecx
{
8010125f:	89 45 d8             	mov    %eax,-0x28(%ebp)
  for(b = 0; b < sb.size; b += BPB){
80101262:	85 c9                	test   %ecx,%ecx
80101264:	0f 84 87 00 00 00    	je     801012f1 <balloc+0xa1>
8010126a:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
    bp = bread(dev, BBLOCK(b, sb));
80101271:	8b 75 dc             	mov    -0x24(%ebp),%esi
80101274:	83 ec 08             	sub    $0x8,%esp
80101277:	89 f0                	mov    %esi,%eax
80101279:	c1 f8 0c             	sar    $0xc,%eax
8010127c:	03 05 d8 09 11 80    	add    0x801109d8,%eax
80101282:	50                   	push   %eax
80101283:	ff 75 d8             	pushl  -0x28(%ebp)
80101286:	e8 45 ee ff ff       	call   801000d0 <bread>
8010128b:	83 c4 10             	add    $0x10,%esp
8010128e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
80101291:	a1 c0 09 11 80       	mov    0x801109c0,%eax
80101296:	89 45 e0             	mov    %eax,-0x20(%ebp)
80101299:	31 c0                	xor    %eax,%eax
8010129b:	eb 2f                	jmp    801012cc <balloc+0x7c>
8010129d:	8d 76 00             	lea    0x0(%esi),%esi
      m = 1 << (bi % 8);
801012a0:	89 c1                	mov    %eax,%ecx
801012a2:	bb 01 00 00 00       	mov    $0x1,%ebx
      if((bp->data[bi/8] & m) == 0){  // Is block free?
801012a7:	8b 55 e4             	mov    -0x1c(%ebp),%edx
      m = 1 << (bi % 8);
801012aa:	83 e1 07             	and    $0x7,%ecx
801012ad:	d3 e3                	shl    %cl,%ebx
      if((bp->data[bi/8] & m) == 0){  // Is block free?
801012af:	89 c1                	mov    %eax,%ecx
801012b1:	c1 f9 03             	sar    $0x3,%ecx
801012b4:	0f b6 7c 0a 5c       	movzbl 0x5c(%edx,%ecx,1),%edi
801012b9:	89 fa                	mov    %edi,%edx
801012bb:	85 df                	test   %ebx,%edi
801012bd:	74 41                	je     80101300 <balloc+0xb0>
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
801012bf:	83 c0 01             	add    $0x1,%eax
801012c2:	83 c6 01             	add    $0x1,%esi
801012c5:	3d 00 10 00 00       	cmp    $0x1000,%eax
801012ca:	74 05                	je     801012d1 <balloc+0x81>
801012cc:	39 75 e0             	cmp    %esi,-0x20(%ebp)
801012cf:	77 cf                	ja     801012a0 <balloc+0x50>
    brelse(bp);
801012d1:	83 ec 0c             	sub    $0xc,%esp
801012d4:	ff 75 e4             	pushl  -0x1c(%ebp)
801012d7:	e8 14 ef ff ff       	call   801001f0 <brelse>
  for(b = 0; b < sb.size; b += BPB){
801012dc:	81 45 dc 00 10 00 00 	addl   $0x1000,-0x24(%ebp)
801012e3:	83 c4 10             	add    $0x10,%esp
801012e6:	8b 45 dc             	mov    -0x24(%ebp),%eax
801012e9:	39 05 c0 09 11 80    	cmp    %eax,0x801109c0
801012ef:	77 80                	ja     80101271 <balloc+0x21>
  panic("balloc: out of blocks");
801012f1:	83 ec 0c             	sub    $0xc,%esp
801012f4:	68 d2 72 10 80       	push   $0x801072d2
801012f9:	e8 92 f0 ff ff       	call   80100390 <panic>
801012fe:	66 90                	xchg   %ax,%ax
        bp->data[bi/8] |= m;  // Mark block in use.
80101300:	8b 7d e4             	mov    -0x1c(%ebp),%edi
        log_write(bp);
80101303:	83 ec 0c             	sub    $0xc,%esp
        bp->data[bi/8] |= m;  // Mark block in use.
80101306:	09 da                	or     %ebx,%edx
80101308:	88 54 0f 5c          	mov    %dl,0x5c(%edi,%ecx,1)
        log_write(bp);
8010130c:	57                   	push   %edi
8010130d:	e8 1e 1c 00 00       	call   80102f30 <log_write>
        brelse(bp);
80101312:	89 3c 24             	mov    %edi,(%esp)
80101315:	e8 d6 ee ff ff       	call   801001f0 <brelse>
  bp = bread(dev, bno);
8010131a:	58                   	pop    %eax
8010131b:	5a                   	pop    %edx
8010131c:	56                   	push   %esi
8010131d:	ff 75 d8             	pushl  -0x28(%ebp)
80101320:	e8 ab ed ff ff       	call   801000d0 <bread>
  memset(bp->data, 0, BSIZE);
80101325:	83 c4 0c             	add    $0xc,%esp
  bp = bread(dev, bno);
80101328:	89 c3                	mov    %eax,%ebx
  memset(bp->data, 0, BSIZE);
8010132a:	8d 40 5c             	lea    0x5c(%eax),%eax
8010132d:	68 00 02 00 00       	push   $0x200
80101332:	6a 00                	push   $0x0
80101334:	50                   	push   %eax
80101335:	e8 46 34 00 00       	call   80104780 <memset>
  log_write(bp);
8010133a:	89 1c 24             	mov    %ebx,(%esp)
8010133d:	e8 ee 1b 00 00       	call   80102f30 <log_write>
  brelse(bp);
80101342:	89 1c 24             	mov    %ebx,(%esp)
80101345:	e8 a6 ee ff ff       	call   801001f0 <brelse>
}
8010134a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010134d:	89 f0                	mov    %esi,%eax
8010134f:	5b                   	pop    %ebx
80101350:	5e                   	pop    %esi
80101351:	5f                   	pop    %edi
80101352:	5d                   	pop    %ebp
80101353:	c3                   	ret    
80101354:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010135b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010135f:	90                   	nop

80101360 <iget>:
// Find the inode with number inum on device dev
// and return the in-memory copy. Does not lock
// the inode and does not read it from disk.
static struct inode*
iget(uint dev, uint inum)
{
80101360:	55                   	push   %ebp
80101361:	89 e5                	mov    %esp,%ebp
80101363:	57                   	push   %edi
80101364:	89 c7                	mov    %eax,%edi
80101366:	56                   	push   %esi
  struct inode *ip, *empty;

  acquire(&icache.lock);

  // Is the inode already cached?
  empty = 0;
80101367:	31 f6                	xor    %esi,%esi
{
80101369:	53                   	push   %ebx
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
8010136a:	bb 14 0a 11 80       	mov    $0x80110a14,%ebx
{
8010136f:	83 ec 28             	sub    $0x28,%esp
80101372:	89 55 e4             	mov    %edx,-0x1c(%ebp)
  acquire(&icache.lock);
80101375:	68 e0 09 11 80       	push   $0x801109e0
8010137a:	e8 f1 32 00 00       	call   80104670 <acquire>
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
8010137f:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  acquire(&icache.lock);
80101382:	83 c4 10             	add    $0x10,%esp
80101385:	eb 1b                	jmp    801013a2 <iget+0x42>
80101387:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010138e:	66 90                	xchg   %ax,%ax
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101390:	39 3b                	cmp    %edi,(%ebx)
80101392:	74 6c                	je     80101400 <iget+0xa0>
80101394:	81 c3 90 00 00 00    	add    $0x90,%ebx
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
8010139a:	81 fb 34 26 11 80    	cmp    $0x80112634,%ebx
801013a0:	73 26                	jae    801013c8 <iget+0x68>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
801013a2:	8b 4b 08             	mov    0x8(%ebx),%ecx
801013a5:	85 c9                	test   %ecx,%ecx
801013a7:	7f e7                	jg     80101390 <iget+0x30>
      ip->ref++;
      release(&icache.lock);
      return ip;
    }
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
801013a9:	85 f6                	test   %esi,%esi
801013ab:	75 e7                	jne    80101394 <iget+0x34>
801013ad:	89 d8                	mov    %ebx,%eax
801013af:	81 c3 90 00 00 00    	add    $0x90,%ebx
801013b5:	85 c9                	test   %ecx,%ecx
801013b7:	75 6e                	jne    80101427 <iget+0xc7>
801013b9:	89 c6                	mov    %eax,%esi
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
801013bb:	81 fb 34 26 11 80    	cmp    $0x80112634,%ebx
801013c1:	72 df                	jb     801013a2 <iget+0x42>
801013c3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801013c7:	90                   	nop
      empty = ip;
  }

  // Recycle an inode cache entry.
  if(empty == 0)
801013c8:	85 f6                	test   %esi,%esi
801013ca:	74 73                	je     8010143f <iget+0xdf>
  ip = empty;
  ip->dev = dev;
  ip->inum = inum;
  ip->ref = 1;
  ip->valid = 0;
  release(&icache.lock);
801013cc:	83 ec 0c             	sub    $0xc,%esp
  ip->dev = dev;
801013cf:	89 3e                	mov    %edi,(%esi)
  ip->inum = inum;
801013d1:	89 56 04             	mov    %edx,0x4(%esi)
  ip->ref = 1;
801013d4:	c7 46 08 01 00 00 00 	movl   $0x1,0x8(%esi)
  ip->valid = 0;
801013db:	c7 46 4c 00 00 00 00 	movl   $0x0,0x4c(%esi)
  release(&icache.lock);
801013e2:	68 e0 09 11 80       	push   $0x801109e0
801013e7:	e8 44 33 00 00       	call   80104730 <release>

  return ip;
801013ec:	83 c4 10             	add    $0x10,%esp
}
801013ef:	8d 65 f4             	lea    -0xc(%ebp),%esp
801013f2:	89 f0                	mov    %esi,%eax
801013f4:	5b                   	pop    %ebx
801013f5:	5e                   	pop    %esi
801013f6:	5f                   	pop    %edi
801013f7:	5d                   	pop    %ebp
801013f8:	c3                   	ret    
801013f9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101400:	39 53 04             	cmp    %edx,0x4(%ebx)
80101403:	75 8f                	jne    80101394 <iget+0x34>
      release(&icache.lock);
80101405:	83 ec 0c             	sub    $0xc,%esp
      ip->ref++;
80101408:	83 c1 01             	add    $0x1,%ecx
      return ip;
8010140b:	89 de                	mov    %ebx,%esi
      release(&icache.lock);
8010140d:	68 e0 09 11 80       	push   $0x801109e0
      ip->ref++;
80101412:	89 4b 08             	mov    %ecx,0x8(%ebx)
      release(&icache.lock);
80101415:	e8 16 33 00 00       	call   80104730 <release>
      return ip;
8010141a:	83 c4 10             	add    $0x10,%esp
}
8010141d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101420:	89 f0                	mov    %esi,%eax
80101422:	5b                   	pop    %ebx
80101423:	5e                   	pop    %esi
80101424:	5f                   	pop    %edi
80101425:	5d                   	pop    %ebp
80101426:	c3                   	ret    
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101427:	81 fb 34 26 11 80    	cmp    $0x80112634,%ebx
8010142d:	73 10                	jae    8010143f <iget+0xdf>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
8010142f:	8b 4b 08             	mov    0x8(%ebx),%ecx
80101432:	85 c9                	test   %ecx,%ecx
80101434:	0f 8f 56 ff ff ff    	jg     80101390 <iget+0x30>
8010143a:	e9 6e ff ff ff       	jmp    801013ad <iget+0x4d>
    panic("iget: no inodes");
8010143f:	83 ec 0c             	sub    $0xc,%esp
80101442:	68 e8 72 10 80       	push   $0x801072e8
80101447:	e8 44 ef ff ff       	call   80100390 <panic>
8010144c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101450 <bmap>:

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint
bmap(struct inode *ip, uint bn)
{
80101450:	55                   	push   %ebp
80101451:	89 e5                	mov    %esp,%ebp
80101453:	57                   	push   %edi
80101454:	56                   	push   %esi
80101455:	89 c6                	mov    %eax,%esi
80101457:	53                   	push   %ebx
80101458:	83 ec 1c             	sub    $0x1c,%esp
  uint addr, *a;
  struct buf *bp;

  if(bn < NDIRECT){
8010145b:	83 fa 0b             	cmp    $0xb,%edx
8010145e:	0f 86 84 00 00 00    	jbe    801014e8 <bmap+0x98>
    if((addr = ip->addrs[bn]) == 0)
      ip->addrs[bn] = addr = balloc(ip->dev);
    return addr;
  }
  bn -= NDIRECT;
80101464:	8d 5a f4             	lea    -0xc(%edx),%ebx

  if(bn < NINDIRECT){
80101467:	83 fb 7f             	cmp    $0x7f,%ebx
8010146a:	0f 87 98 00 00 00    	ja     80101508 <bmap+0xb8>
    // Load indirect block, allocating if necessary.
    if((addr = ip->addrs[NDIRECT]) == 0)
80101470:	8b 80 8c 00 00 00    	mov    0x8c(%eax),%eax
80101476:	8b 16                	mov    (%esi),%edx
80101478:	85 c0                	test   %eax,%eax
8010147a:	74 54                	je     801014d0 <bmap+0x80>
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
    bp = bread(ip->dev, addr);
8010147c:	83 ec 08             	sub    $0x8,%esp
8010147f:	50                   	push   %eax
80101480:	52                   	push   %edx
80101481:	e8 4a ec ff ff       	call   801000d0 <bread>
    a = (uint*)bp->data;
    if((addr = a[bn]) == 0){
80101486:	83 c4 10             	add    $0x10,%esp
80101489:	8d 54 98 5c          	lea    0x5c(%eax,%ebx,4),%edx
    bp = bread(ip->dev, addr);
8010148d:	89 c7                	mov    %eax,%edi
    if((addr = a[bn]) == 0){
8010148f:	8b 1a                	mov    (%edx),%ebx
80101491:	85 db                	test   %ebx,%ebx
80101493:	74 1b                	je     801014b0 <bmap+0x60>
      a[bn] = addr = balloc(ip->dev);
      log_write(bp);
    }
    brelse(bp);
80101495:	83 ec 0c             	sub    $0xc,%esp
80101498:	57                   	push   %edi
80101499:	e8 52 ed ff ff       	call   801001f0 <brelse>
    return addr;
8010149e:	83 c4 10             	add    $0x10,%esp
  }

  panic("bmap: out of range");
}
801014a1:	8d 65 f4             	lea    -0xc(%ebp),%esp
801014a4:	89 d8                	mov    %ebx,%eax
801014a6:	5b                   	pop    %ebx
801014a7:	5e                   	pop    %esi
801014a8:	5f                   	pop    %edi
801014a9:	5d                   	pop    %ebp
801014aa:	c3                   	ret    
801014ab:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801014af:	90                   	nop
      a[bn] = addr = balloc(ip->dev);
801014b0:	8b 06                	mov    (%esi),%eax
801014b2:	89 55 e4             	mov    %edx,-0x1c(%ebp)
801014b5:	e8 96 fd ff ff       	call   80101250 <balloc>
801014ba:	8b 55 e4             	mov    -0x1c(%ebp),%edx
      log_write(bp);
801014bd:	83 ec 0c             	sub    $0xc,%esp
      a[bn] = addr = balloc(ip->dev);
801014c0:	89 c3                	mov    %eax,%ebx
801014c2:	89 02                	mov    %eax,(%edx)
      log_write(bp);
801014c4:	57                   	push   %edi
801014c5:	e8 66 1a 00 00       	call   80102f30 <log_write>
801014ca:	83 c4 10             	add    $0x10,%esp
801014cd:	eb c6                	jmp    80101495 <bmap+0x45>
801014cf:	90                   	nop
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
801014d0:	89 d0                	mov    %edx,%eax
801014d2:	e8 79 fd ff ff       	call   80101250 <balloc>
801014d7:	8b 16                	mov    (%esi),%edx
801014d9:	89 86 8c 00 00 00    	mov    %eax,0x8c(%esi)
801014df:	eb 9b                	jmp    8010147c <bmap+0x2c>
801014e1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if((addr = ip->addrs[bn]) == 0)
801014e8:	8d 3c 90             	lea    (%eax,%edx,4),%edi
801014eb:	8b 5f 5c             	mov    0x5c(%edi),%ebx
801014ee:	85 db                	test   %ebx,%ebx
801014f0:	75 af                	jne    801014a1 <bmap+0x51>
      ip->addrs[bn] = addr = balloc(ip->dev);
801014f2:	8b 00                	mov    (%eax),%eax
801014f4:	e8 57 fd ff ff       	call   80101250 <balloc>
801014f9:	89 47 5c             	mov    %eax,0x5c(%edi)
801014fc:	89 c3                	mov    %eax,%ebx
}
801014fe:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101501:	89 d8                	mov    %ebx,%eax
80101503:	5b                   	pop    %ebx
80101504:	5e                   	pop    %esi
80101505:	5f                   	pop    %edi
80101506:	5d                   	pop    %ebp
80101507:	c3                   	ret    
  panic("bmap: out of range");
80101508:	83 ec 0c             	sub    $0xc,%esp
8010150b:	68 f8 72 10 80       	push   $0x801072f8
80101510:	e8 7b ee ff ff       	call   80100390 <panic>
80101515:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010151c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101520 <readsb>:
{
80101520:	f3 0f 1e fb          	endbr32 
80101524:	55                   	push   %ebp
80101525:	89 e5                	mov    %esp,%ebp
80101527:	56                   	push   %esi
80101528:	53                   	push   %ebx
80101529:	8b 75 0c             	mov    0xc(%ebp),%esi
  bp = bread(dev, 1);
8010152c:	83 ec 08             	sub    $0x8,%esp
8010152f:	6a 01                	push   $0x1
80101531:	ff 75 08             	pushl  0x8(%ebp)
80101534:	e8 97 eb ff ff       	call   801000d0 <bread>
  memmove(sb, bp->data, sizeof(*sb));
80101539:	83 c4 0c             	add    $0xc,%esp
  bp = bread(dev, 1);
8010153c:	89 c3                	mov    %eax,%ebx
  memmove(sb, bp->data, sizeof(*sb));
8010153e:	8d 40 5c             	lea    0x5c(%eax),%eax
80101541:	6a 1c                	push   $0x1c
80101543:	50                   	push   %eax
80101544:	56                   	push   %esi
80101545:	e8 d6 32 00 00       	call   80104820 <memmove>
  brelse(bp);
8010154a:	89 5d 08             	mov    %ebx,0x8(%ebp)
8010154d:	83 c4 10             	add    $0x10,%esp
}
80101550:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101553:	5b                   	pop    %ebx
80101554:	5e                   	pop    %esi
80101555:	5d                   	pop    %ebp
  brelse(bp);
80101556:	e9 95 ec ff ff       	jmp    801001f0 <brelse>
8010155b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010155f:	90                   	nop

80101560 <iinit>:
{
80101560:	f3 0f 1e fb          	endbr32 
80101564:	55                   	push   %ebp
80101565:	89 e5                	mov    %esp,%ebp
80101567:	53                   	push   %ebx
80101568:	bb 20 0a 11 80       	mov    $0x80110a20,%ebx
8010156d:	83 ec 0c             	sub    $0xc,%esp
  initlock(&icache.lock, "icache");
80101570:	68 0b 73 10 80       	push   $0x8010730b
80101575:	68 e0 09 11 80       	push   $0x801109e0
8010157a:	e8 71 2f 00 00       	call   801044f0 <initlock>
  for(i = 0; i < NINODE; i++) {
8010157f:	83 c4 10             	add    $0x10,%esp
80101582:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    initsleeplock(&icache.inode[i].lock, "inode");
80101588:	83 ec 08             	sub    $0x8,%esp
8010158b:	68 12 73 10 80       	push   $0x80107312
80101590:	53                   	push   %ebx
80101591:	81 c3 90 00 00 00    	add    $0x90,%ebx
80101597:	e8 14 2e 00 00       	call   801043b0 <initsleeplock>
  for(i = 0; i < NINODE; i++) {
8010159c:	83 c4 10             	add    $0x10,%esp
8010159f:	81 fb 40 26 11 80    	cmp    $0x80112640,%ebx
801015a5:	75 e1                	jne    80101588 <iinit+0x28>
  readsb(dev, &sb);
801015a7:	83 ec 08             	sub    $0x8,%esp
801015aa:	68 c0 09 11 80       	push   $0x801109c0
801015af:	ff 75 08             	pushl  0x8(%ebp)
801015b2:	e8 69 ff ff ff       	call   80101520 <readsb>
  cprintf("sb: size %d nblocks %d ninodes %d nlog %d logstart %d\
801015b7:	ff 35 d8 09 11 80    	pushl  0x801109d8
801015bd:	ff 35 d4 09 11 80    	pushl  0x801109d4
801015c3:	ff 35 d0 09 11 80    	pushl  0x801109d0
801015c9:	ff 35 cc 09 11 80    	pushl  0x801109cc
801015cf:	ff 35 c8 09 11 80    	pushl  0x801109c8
801015d5:	ff 35 c4 09 11 80    	pushl  0x801109c4
801015db:	ff 35 c0 09 11 80    	pushl  0x801109c0
801015e1:	68 78 73 10 80       	push   $0x80107378
801015e6:	e8 c5 f0 ff ff       	call   801006b0 <cprintf>
}
801015eb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801015ee:	83 c4 30             	add    $0x30,%esp
801015f1:	c9                   	leave  
801015f2:	c3                   	ret    
801015f3:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801015fa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80101600 <ialloc>:
{
80101600:	f3 0f 1e fb          	endbr32 
80101604:	55                   	push   %ebp
80101605:	89 e5                	mov    %esp,%ebp
80101607:	57                   	push   %edi
80101608:	56                   	push   %esi
80101609:	53                   	push   %ebx
8010160a:	83 ec 1c             	sub    $0x1c,%esp
8010160d:	8b 45 0c             	mov    0xc(%ebp),%eax
  for(inum = 1; inum < sb.ninodes; inum++){
80101610:	83 3d c8 09 11 80 01 	cmpl   $0x1,0x801109c8
{
80101617:	8b 75 08             	mov    0x8(%ebp),%esi
8010161a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  for(inum = 1; inum < sb.ninodes; inum++){
8010161d:	0f 86 8d 00 00 00    	jbe    801016b0 <ialloc+0xb0>
80101623:	bf 01 00 00 00       	mov    $0x1,%edi
80101628:	eb 1d                	jmp    80101647 <ialloc+0x47>
8010162a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    brelse(bp);
80101630:	83 ec 0c             	sub    $0xc,%esp
  for(inum = 1; inum < sb.ninodes; inum++){
80101633:	83 c7 01             	add    $0x1,%edi
    brelse(bp);
80101636:	53                   	push   %ebx
80101637:	e8 b4 eb ff ff       	call   801001f0 <brelse>
  for(inum = 1; inum < sb.ninodes; inum++){
8010163c:	83 c4 10             	add    $0x10,%esp
8010163f:	3b 3d c8 09 11 80    	cmp    0x801109c8,%edi
80101645:	73 69                	jae    801016b0 <ialloc+0xb0>
    bp = bread(dev, IBLOCK(inum, sb));
80101647:	89 f8                	mov    %edi,%eax
80101649:	83 ec 08             	sub    $0x8,%esp
8010164c:	c1 e8 03             	shr    $0x3,%eax
8010164f:	03 05 d4 09 11 80    	add    0x801109d4,%eax
80101655:	50                   	push   %eax
80101656:	56                   	push   %esi
80101657:	e8 74 ea ff ff       	call   801000d0 <bread>
    if(dip->type == 0){  // a free inode
8010165c:	83 c4 10             	add    $0x10,%esp
    bp = bread(dev, IBLOCK(inum, sb));
8010165f:	89 c3                	mov    %eax,%ebx
    dip = (struct dinode*)bp->data + inum%IPB;
80101661:	89 f8                	mov    %edi,%eax
80101663:	83 e0 07             	and    $0x7,%eax
80101666:	c1 e0 06             	shl    $0x6,%eax
80101669:	8d 4c 03 5c          	lea    0x5c(%ebx,%eax,1),%ecx
    if(dip->type == 0){  // a free inode
8010166d:	66 83 39 00          	cmpw   $0x0,(%ecx)
80101671:	75 bd                	jne    80101630 <ialloc+0x30>
      memset(dip, 0, sizeof(*dip));
80101673:	83 ec 04             	sub    $0x4,%esp
80101676:	89 4d e0             	mov    %ecx,-0x20(%ebp)
80101679:	6a 40                	push   $0x40
8010167b:	6a 00                	push   $0x0
8010167d:	51                   	push   %ecx
8010167e:	e8 fd 30 00 00       	call   80104780 <memset>
      dip->type = type;
80101683:	0f b7 45 e4          	movzwl -0x1c(%ebp),%eax
80101687:	8b 4d e0             	mov    -0x20(%ebp),%ecx
8010168a:	66 89 01             	mov    %ax,(%ecx)
      log_write(bp);   // mark it allocated on the disk
8010168d:	89 1c 24             	mov    %ebx,(%esp)
80101690:	e8 9b 18 00 00       	call   80102f30 <log_write>
      brelse(bp);
80101695:	89 1c 24             	mov    %ebx,(%esp)
80101698:	e8 53 eb ff ff       	call   801001f0 <brelse>
      return iget(dev, inum);
8010169d:	83 c4 10             	add    $0x10,%esp
}
801016a0:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return iget(dev, inum);
801016a3:	89 fa                	mov    %edi,%edx
}
801016a5:	5b                   	pop    %ebx
      return iget(dev, inum);
801016a6:	89 f0                	mov    %esi,%eax
}
801016a8:	5e                   	pop    %esi
801016a9:	5f                   	pop    %edi
801016aa:	5d                   	pop    %ebp
      return iget(dev, inum);
801016ab:	e9 b0 fc ff ff       	jmp    80101360 <iget>
  panic("ialloc: no inodes");
801016b0:	83 ec 0c             	sub    $0xc,%esp
801016b3:	68 18 73 10 80       	push   $0x80107318
801016b8:	e8 d3 ec ff ff       	call   80100390 <panic>
801016bd:	8d 76 00             	lea    0x0(%esi),%esi

801016c0 <iupdate>:
{
801016c0:	f3 0f 1e fb          	endbr32 
801016c4:	55                   	push   %ebp
801016c5:	89 e5                	mov    %esp,%ebp
801016c7:	56                   	push   %esi
801016c8:	53                   	push   %ebx
801016c9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801016cc:	8b 43 04             	mov    0x4(%ebx),%eax
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
801016cf:	83 c3 5c             	add    $0x5c,%ebx
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801016d2:	83 ec 08             	sub    $0x8,%esp
801016d5:	c1 e8 03             	shr    $0x3,%eax
801016d8:	03 05 d4 09 11 80    	add    0x801109d4,%eax
801016de:	50                   	push   %eax
801016df:	ff 73 a4             	pushl  -0x5c(%ebx)
801016e2:	e8 e9 e9 ff ff       	call   801000d0 <bread>
  dip->type = ip->type;
801016e7:	0f b7 53 f4          	movzwl -0xc(%ebx),%edx
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
801016eb:	83 c4 0c             	add    $0xc,%esp
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801016ee:	89 c6                	mov    %eax,%esi
  dip = (struct dinode*)bp->data + ip->inum%IPB;
801016f0:	8b 43 a8             	mov    -0x58(%ebx),%eax
801016f3:	83 e0 07             	and    $0x7,%eax
801016f6:	c1 e0 06             	shl    $0x6,%eax
801016f9:	8d 44 06 5c          	lea    0x5c(%esi,%eax,1),%eax
  dip->type = ip->type;
801016fd:	66 89 10             	mov    %dx,(%eax)
  dip->major = ip->major;
80101700:	0f b7 53 f6          	movzwl -0xa(%ebx),%edx
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
80101704:	83 c0 0c             	add    $0xc,%eax
  dip->major = ip->major;
80101707:	66 89 50 f6          	mov    %dx,-0xa(%eax)
  dip->minor = ip->minor;
8010170b:	0f b7 53 f8          	movzwl -0x8(%ebx),%edx
8010170f:	66 89 50 f8          	mov    %dx,-0x8(%eax)
  dip->nlink = ip->nlink;
80101713:	0f b7 53 fa          	movzwl -0x6(%ebx),%edx
80101717:	66 89 50 fa          	mov    %dx,-0x6(%eax)
  dip->size = ip->size;
8010171b:	8b 53 fc             	mov    -0x4(%ebx),%edx
8010171e:	89 50 fc             	mov    %edx,-0x4(%eax)
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
80101721:	6a 34                	push   $0x34
80101723:	53                   	push   %ebx
80101724:	50                   	push   %eax
80101725:	e8 f6 30 00 00       	call   80104820 <memmove>
  log_write(bp);
8010172a:	89 34 24             	mov    %esi,(%esp)
8010172d:	e8 fe 17 00 00       	call   80102f30 <log_write>
  brelse(bp);
80101732:	89 75 08             	mov    %esi,0x8(%ebp)
80101735:	83 c4 10             	add    $0x10,%esp
}
80101738:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010173b:	5b                   	pop    %ebx
8010173c:	5e                   	pop    %esi
8010173d:	5d                   	pop    %ebp
  brelse(bp);
8010173e:	e9 ad ea ff ff       	jmp    801001f0 <brelse>
80101743:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010174a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80101750 <idup>:
{
80101750:	f3 0f 1e fb          	endbr32 
80101754:	55                   	push   %ebp
80101755:	89 e5                	mov    %esp,%ebp
80101757:	53                   	push   %ebx
80101758:	83 ec 10             	sub    $0x10,%esp
8010175b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&icache.lock);
8010175e:	68 e0 09 11 80       	push   $0x801109e0
80101763:	e8 08 2f 00 00       	call   80104670 <acquire>
  ip->ref++;
80101768:	83 43 08 01          	addl   $0x1,0x8(%ebx)
  release(&icache.lock);
8010176c:	c7 04 24 e0 09 11 80 	movl   $0x801109e0,(%esp)
80101773:	e8 b8 2f 00 00       	call   80104730 <release>
}
80101778:	89 d8                	mov    %ebx,%eax
8010177a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010177d:	c9                   	leave  
8010177e:	c3                   	ret    
8010177f:	90                   	nop

80101780 <ilock>:
{
80101780:	f3 0f 1e fb          	endbr32 
80101784:	55                   	push   %ebp
80101785:	89 e5                	mov    %esp,%ebp
80101787:	56                   	push   %esi
80101788:	53                   	push   %ebx
80101789:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(ip == 0 || ip->ref < 1)
8010178c:	85 db                	test   %ebx,%ebx
8010178e:	0f 84 b3 00 00 00    	je     80101847 <ilock+0xc7>
80101794:	8b 53 08             	mov    0x8(%ebx),%edx
80101797:	85 d2                	test   %edx,%edx
80101799:	0f 8e a8 00 00 00    	jle    80101847 <ilock+0xc7>
  acquiresleep(&ip->lock);
8010179f:	83 ec 0c             	sub    $0xc,%esp
801017a2:	8d 43 0c             	lea    0xc(%ebx),%eax
801017a5:	50                   	push   %eax
801017a6:	e8 45 2c 00 00       	call   801043f0 <acquiresleep>
  if(ip->valid == 0){
801017ab:	8b 43 4c             	mov    0x4c(%ebx),%eax
801017ae:	83 c4 10             	add    $0x10,%esp
801017b1:	85 c0                	test   %eax,%eax
801017b3:	74 0b                	je     801017c0 <ilock+0x40>
}
801017b5:	8d 65 f8             	lea    -0x8(%ebp),%esp
801017b8:	5b                   	pop    %ebx
801017b9:	5e                   	pop    %esi
801017ba:	5d                   	pop    %ebp
801017bb:	c3                   	ret    
801017bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801017c0:	8b 43 04             	mov    0x4(%ebx),%eax
801017c3:	83 ec 08             	sub    $0x8,%esp
801017c6:	c1 e8 03             	shr    $0x3,%eax
801017c9:	03 05 d4 09 11 80    	add    0x801109d4,%eax
801017cf:	50                   	push   %eax
801017d0:	ff 33                	pushl  (%ebx)
801017d2:	e8 f9 e8 ff ff       	call   801000d0 <bread>
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
801017d7:	83 c4 0c             	add    $0xc,%esp
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801017da:	89 c6                	mov    %eax,%esi
    dip = (struct dinode*)bp->data + ip->inum%IPB;
801017dc:	8b 43 04             	mov    0x4(%ebx),%eax
801017df:	83 e0 07             	and    $0x7,%eax
801017e2:	c1 e0 06             	shl    $0x6,%eax
801017e5:	8d 44 06 5c          	lea    0x5c(%esi,%eax,1),%eax
    ip->type = dip->type;
801017e9:	0f b7 10             	movzwl (%eax),%edx
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
801017ec:	83 c0 0c             	add    $0xc,%eax
    ip->type = dip->type;
801017ef:	66 89 53 50          	mov    %dx,0x50(%ebx)
    ip->major = dip->major;
801017f3:	0f b7 50 f6          	movzwl -0xa(%eax),%edx
801017f7:	66 89 53 52          	mov    %dx,0x52(%ebx)
    ip->minor = dip->minor;
801017fb:	0f b7 50 f8          	movzwl -0x8(%eax),%edx
801017ff:	66 89 53 54          	mov    %dx,0x54(%ebx)
    ip->nlink = dip->nlink;
80101803:	0f b7 50 fa          	movzwl -0x6(%eax),%edx
80101807:	66 89 53 56          	mov    %dx,0x56(%ebx)
    ip->size = dip->size;
8010180b:	8b 50 fc             	mov    -0x4(%eax),%edx
8010180e:	89 53 58             	mov    %edx,0x58(%ebx)
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
80101811:	6a 34                	push   $0x34
80101813:	50                   	push   %eax
80101814:	8d 43 5c             	lea    0x5c(%ebx),%eax
80101817:	50                   	push   %eax
80101818:	e8 03 30 00 00       	call   80104820 <memmove>
    brelse(bp);
8010181d:	89 34 24             	mov    %esi,(%esp)
80101820:	e8 cb e9 ff ff       	call   801001f0 <brelse>
    if(ip->type == 0)
80101825:	83 c4 10             	add    $0x10,%esp
80101828:	66 83 7b 50 00       	cmpw   $0x0,0x50(%ebx)
    ip->valid = 1;
8010182d:	c7 43 4c 01 00 00 00 	movl   $0x1,0x4c(%ebx)
    if(ip->type == 0)
80101834:	0f 85 7b ff ff ff    	jne    801017b5 <ilock+0x35>
      panic("ilock: no type");
8010183a:	83 ec 0c             	sub    $0xc,%esp
8010183d:	68 30 73 10 80       	push   $0x80107330
80101842:	e8 49 eb ff ff       	call   80100390 <panic>
    panic("ilock");
80101847:	83 ec 0c             	sub    $0xc,%esp
8010184a:	68 2a 73 10 80       	push   $0x8010732a
8010184f:	e8 3c eb ff ff       	call   80100390 <panic>
80101854:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010185b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010185f:	90                   	nop

80101860 <iunlock>:
{
80101860:	f3 0f 1e fb          	endbr32 
80101864:	55                   	push   %ebp
80101865:	89 e5                	mov    %esp,%ebp
80101867:	56                   	push   %esi
80101868:	53                   	push   %ebx
80101869:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(ip == 0 || !holdingsleep(&ip->lock) || ip->ref < 1)
8010186c:	85 db                	test   %ebx,%ebx
8010186e:	74 28                	je     80101898 <iunlock+0x38>
80101870:	83 ec 0c             	sub    $0xc,%esp
80101873:	8d 73 0c             	lea    0xc(%ebx),%esi
80101876:	56                   	push   %esi
80101877:	e8 14 2c 00 00       	call   80104490 <holdingsleep>
8010187c:	83 c4 10             	add    $0x10,%esp
8010187f:	85 c0                	test   %eax,%eax
80101881:	74 15                	je     80101898 <iunlock+0x38>
80101883:	8b 43 08             	mov    0x8(%ebx),%eax
80101886:	85 c0                	test   %eax,%eax
80101888:	7e 0e                	jle    80101898 <iunlock+0x38>
  releasesleep(&ip->lock);
8010188a:	89 75 08             	mov    %esi,0x8(%ebp)
}
8010188d:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101890:	5b                   	pop    %ebx
80101891:	5e                   	pop    %esi
80101892:	5d                   	pop    %ebp
  releasesleep(&ip->lock);
80101893:	e9 b8 2b 00 00       	jmp    80104450 <releasesleep>
    panic("iunlock");
80101898:	83 ec 0c             	sub    $0xc,%esp
8010189b:	68 3f 73 10 80       	push   $0x8010733f
801018a0:	e8 eb ea ff ff       	call   80100390 <panic>
801018a5:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801018ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801018b0 <iput>:
{
801018b0:	f3 0f 1e fb          	endbr32 
801018b4:	55                   	push   %ebp
801018b5:	89 e5                	mov    %esp,%ebp
801018b7:	57                   	push   %edi
801018b8:	56                   	push   %esi
801018b9:	53                   	push   %ebx
801018ba:	83 ec 28             	sub    $0x28,%esp
801018bd:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquiresleep(&ip->lock);
801018c0:	8d 7b 0c             	lea    0xc(%ebx),%edi
801018c3:	57                   	push   %edi
801018c4:	e8 27 2b 00 00       	call   801043f0 <acquiresleep>
  if(ip->valid && ip->nlink == 0){
801018c9:	8b 53 4c             	mov    0x4c(%ebx),%edx
801018cc:	83 c4 10             	add    $0x10,%esp
801018cf:	85 d2                	test   %edx,%edx
801018d1:	74 07                	je     801018da <iput+0x2a>
801018d3:	66 83 7b 56 00       	cmpw   $0x0,0x56(%ebx)
801018d8:	74 36                	je     80101910 <iput+0x60>
  releasesleep(&ip->lock);
801018da:	83 ec 0c             	sub    $0xc,%esp
801018dd:	57                   	push   %edi
801018de:	e8 6d 2b 00 00       	call   80104450 <releasesleep>
  acquire(&icache.lock);
801018e3:	c7 04 24 e0 09 11 80 	movl   $0x801109e0,(%esp)
801018ea:	e8 81 2d 00 00       	call   80104670 <acquire>
  ip->ref--;
801018ef:	83 6b 08 01          	subl   $0x1,0x8(%ebx)
  release(&icache.lock);
801018f3:	83 c4 10             	add    $0x10,%esp
801018f6:	c7 45 08 e0 09 11 80 	movl   $0x801109e0,0x8(%ebp)
}
801018fd:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101900:	5b                   	pop    %ebx
80101901:	5e                   	pop    %esi
80101902:	5f                   	pop    %edi
80101903:	5d                   	pop    %ebp
  release(&icache.lock);
80101904:	e9 27 2e 00 00       	jmp    80104730 <release>
80101909:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    acquire(&icache.lock);
80101910:	83 ec 0c             	sub    $0xc,%esp
80101913:	68 e0 09 11 80       	push   $0x801109e0
80101918:	e8 53 2d 00 00       	call   80104670 <acquire>
    int r = ip->ref;
8010191d:	8b 73 08             	mov    0x8(%ebx),%esi
    release(&icache.lock);
80101920:	c7 04 24 e0 09 11 80 	movl   $0x801109e0,(%esp)
80101927:	e8 04 2e 00 00       	call   80104730 <release>
    if(r == 1){
8010192c:	83 c4 10             	add    $0x10,%esp
8010192f:	83 fe 01             	cmp    $0x1,%esi
80101932:	75 a6                	jne    801018da <iput+0x2a>
80101934:	8d 8b 8c 00 00 00    	lea    0x8c(%ebx),%ecx
8010193a:	89 7d e4             	mov    %edi,-0x1c(%ebp)
8010193d:	8d 73 5c             	lea    0x5c(%ebx),%esi
80101940:	89 cf                	mov    %ecx,%edi
80101942:	eb 0b                	jmp    8010194f <iput+0x9f>
80101944:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
{
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
80101948:	83 c6 04             	add    $0x4,%esi
8010194b:	39 fe                	cmp    %edi,%esi
8010194d:	74 19                	je     80101968 <iput+0xb8>
    if(ip->addrs[i]){
8010194f:	8b 16                	mov    (%esi),%edx
80101951:	85 d2                	test   %edx,%edx
80101953:	74 f3                	je     80101948 <iput+0x98>
      bfree(ip->dev, ip->addrs[i]);
80101955:	8b 03                	mov    (%ebx),%eax
80101957:	e8 74 f8 ff ff       	call   801011d0 <bfree>
      ip->addrs[i] = 0;
8010195c:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
80101962:	eb e4                	jmp    80101948 <iput+0x98>
80101964:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    }
  }

  if(ip->addrs[NDIRECT]){
80101968:	8b 83 8c 00 00 00    	mov    0x8c(%ebx),%eax
8010196e:	8b 7d e4             	mov    -0x1c(%ebp),%edi
80101971:	85 c0                	test   %eax,%eax
80101973:	75 33                	jne    801019a8 <iput+0xf8>
    bfree(ip->dev, ip->addrs[NDIRECT]);
    ip->addrs[NDIRECT] = 0;
  }

  ip->size = 0;
  iupdate(ip);
80101975:	83 ec 0c             	sub    $0xc,%esp
  ip->size = 0;
80101978:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)
  iupdate(ip);
8010197f:	53                   	push   %ebx
80101980:	e8 3b fd ff ff       	call   801016c0 <iupdate>
      ip->type = 0;
80101985:	31 c0                	xor    %eax,%eax
80101987:	66 89 43 50          	mov    %ax,0x50(%ebx)
      iupdate(ip);
8010198b:	89 1c 24             	mov    %ebx,(%esp)
8010198e:	e8 2d fd ff ff       	call   801016c0 <iupdate>
      ip->valid = 0;
80101993:	c7 43 4c 00 00 00 00 	movl   $0x0,0x4c(%ebx)
8010199a:	83 c4 10             	add    $0x10,%esp
8010199d:	e9 38 ff ff ff       	jmp    801018da <iput+0x2a>
801019a2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
801019a8:	83 ec 08             	sub    $0x8,%esp
801019ab:	50                   	push   %eax
801019ac:	ff 33                	pushl  (%ebx)
801019ae:	e8 1d e7 ff ff       	call   801000d0 <bread>
801019b3:	89 7d e0             	mov    %edi,-0x20(%ebp)
801019b6:	83 c4 10             	add    $0x10,%esp
801019b9:	8d 88 5c 02 00 00    	lea    0x25c(%eax),%ecx
801019bf:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    for(j = 0; j < NINDIRECT; j++){
801019c2:	8d 70 5c             	lea    0x5c(%eax),%esi
801019c5:	89 cf                	mov    %ecx,%edi
801019c7:	eb 0e                	jmp    801019d7 <iput+0x127>
801019c9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801019d0:	83 c6 04             	add    $0x4,%esi
801019d3:	39 f7                	cmp    %esi,%edi
801019d5:	74 19                	je     801019f0 <iput+0x140>
      if(a[j])
801019d7:	8b 16                	mov    (%esi),%edx
801019d9:	85 d2                	test   %edx,%edx
801019db:	74 f3                	je     801019d0 <iput+0x120>
        bfree(ip->dev, a[j]);
801019dd:	8b 03                	mov    (%ebx),%eax
801019df:	e8 ec f7 ff ff       	call   801011d0 <bfree>
801019e4:	eb ea                	jmp    801019d0 <iput+0x120>
801019e6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801019ed:	8d 76 00             	lea    0x0(%esi),%esi
    brelse(bp);
801019f0:	83 ec 0c             	sub    $0xc,%esp
801019f3:	ff 75 e4             	pushl  -0x1c(%ebp)
801019f6:	8b 7d e0             	mov    -0x20(%ebp),%edi
801019f9:	e8 f2 e7 ff ff       	call   801001f0 <brelse>
    bfree(ip->dev, ip->addrs[NDIRECT]);
801019fe:	8b 93 8c 00 00 00    	mov    0x8c(%ebx),%edx
80101a04:	8b 03                	mov    (%ebx),%eax
80101a06:	e8 c5 f7 ff ff       	call   801011d0 <bfree>
    ip->addrs[NDIRECT] = 0;
80101a0b:	83 c4 10             	add    $0x10,%esp
80101a0e:	c7 83 8c 00 00 00 00 	movl   $0x0,0x8c(%ebx)
80101a15:	00 00 00 
80101a18:	e9 58 ff ff ff       	jmp    80101975 <iput+0xc5>
80101a1d:	8d 76 00             	lea    0x0(%esi),%esi

80101a20 <iunlockput>:
{
80101a20:	f3 0f 1e fb          	endbr32 
80101a24:	55                   	push   %ebp
80101a25:	89 e5                	mov    %esp,%ebp
80101a27:	53                   	push   %ebx
80101a28:	83 ec 10             	sub    $0x10,%esp
80101a2b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  iunlock(ip);
80101a2e:	53                   	push   %ebx
80101a2f:	e8 2c fe ff ff       	call   80101860 <iunlock>
  iput(ip);
80101a34:	89 5d 08             	mov    %ebx,0x8(%ebp)
80101a37:	83 c4 10             	add    $0x10,%esp
}
80101a3a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101a3d:	c9                   	leave  
  iput(ip);
80101a3e:	e9 6d fe ff ff       	jmp    801018b0 <iput>
80101a43:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101a4a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80101a50 <stati>:

// Copy stat information from inode.
// Caller must hold ip->lock.
void
stati(struct inode *ip, struct stat *st)
{
80101a50:	f3 0f 1e fb          	endbr32 
80101a54:	55                   	push   %ebp
80101a55:	89 e5                	mov    %esp,%ebp
80101a57:	8b 55 08             	mov    0x8(%ebp),%edx
80101a5a:	8b 45 0c             	mov    0xc(%ebp),%eax
  st->dev = ip->dev;
80101a5d:	8b 0a                	mov    (%edx),%ecx
80101a5f:	89 48 04             	mov    %ecx,0x4(%eax)
  st->ino = ip->inum;
80101a62:	8b 4a 04             	mov    0x4(%edx),%ecx
80101a65:	89 48 08             	mov    %ecx,0x8(%eax)
  st->type = ip->type;
80101a68:	0f b7 4a 50          	movzwl 0x50(%edx),%ecx
80101a6c:	66 89 08             	mov    %cx,(%eax)
  st->nlink = ip->nlink;
80101a6f:	0f b7 4a 56          	movzwl 0x56(%edx),%ecx
80101a73:	66 89 48 0c          	mov    %cx,0xc(%eax)
  st->size = ip->size;
80101a77:	8b 52 58             	mov    0x58(%edx),%edx
80101a7a:	89 50 10             	mov    %edx,0x10(%eax)
}
80101a7d:	5d                   	pop    %ebp
80101a7e:	c3                   	ret    
80101a7f:	90                   	nop

80101a80 <readi>:
//PAGEBREAK!
// Read data from inode.
// Caller must hold ip->lock.
int
readi(struct inode *ip, char *dst, uint off, uint n)
{
80101a80:	f3 0f 1e fb          	endbr32 
80101a84:	55                   	push   %ebp
80101a85:	89 e5                	mov    %esp,%ebp
80101a87:	57                   	push   %edi
80101a88:	56                   	push   %esi
80101a89:	53                   	push   %ebx
80101a8a:	83 ec 1c             	sub    $0x1c,%esp
80101a8d:	8b 7d 0c             	mov    0xc(%ebp),%edi
80101a90:	8b 45 08             	mov    0x8(%ebp),%eax
80101a93:	8b 75 10             	mov    0x10(%ebp),%esi
80101a96:	89 7d e0             	mov    %edi,-0x20(%ebp)
80101a99:	8b 7d 14             	mov    0x14(%ebp),%edi
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80101a9c:	66 83 78 50 03       	cmpw   $0x3,0x50(%eax)
{
80101aa1:	89 45 d8             	mov    %eax,-0x28(%ebp)
80101aa4:	89 7d e4             	mov    %edi,-0x1c(%ebp)
  if(ip->type == T_DEV){
80101aa7:	0f 84 a3 00 00 00    	je     80101b50 <readi+0xd0>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
      return -1;
    return devsw[ip->major].read(ip, dst, n);
  }

  if(off > ip->size || off + n < off)
80101aad:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101ab0:	8b 40 58             	mov    0x58(%eax),%eax
80101ab3:	39 c6                	cmp    %eax,%esi
80101ab5:	0f 87 b6 00 00 00    	ja     80101b71 <readi+0xf1>
80101abb:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
80101abe:	31 c9                	xor    %ecx,%ecx
80101ac0:	89 da                	mov    %ebx,%edx
80101ac2:	01 f2                	add    %esi,%edx
80101ac4:	0f 92 c1             	setb   %cl
80101ac7:	89 cf                	mov    %ecx,%edi
80101ac9:	0f 82 a2 00 00 00    	jb     80101b71 <readi+0xf1>
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;
80101acf:	89 c1                	mov    %eax,%ecx
80101ad1:	29 f1                	sub    %esi,%ecx
80101ad3:	39 d0                	cmp    %edx,%eax
80101ad5:	0f 43 cb             	cmovae %ebx,%ecx
80101ad8:	89 4d e4             	mov    %ecx,-0x1c(%ebp)

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101adb:	85 c9                	test   %ecx,%ecx
80101add:	74 63                	je     80101b42 <readi+0xc2>
80101adf:	90                   	nop
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101ae0:	8b 5d d8             	mov    -0x28(%ebp),%ebx
80101ae3:	89 f2                	mov    %esi,%edx
80101ae5:	c1 ea 09             	shr    $0x9,%edx
80101ae8:	89 d8                	mov    %ebx,%eax
80101aea:	e8 61 f9 ff ff       	call   80101450 <bmap>
80101aef:	83 ec 08             	sub    $0x8,%esp
80101af2:	50                   	push   %eax
80101af3:	ff 33                	pushl  (%ebx)
80101af5:	e8 d6 e5 ff ff       	call   801000d0 <bread>
    m = min(n - tot, BSIZE - off%BSIZE);
80101afa:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
80101afd:	b9 00 02 00 00       	mov    $0x200,%ecx
80101b02:	83 c4 0c             	add    $0xc,%esp
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101b05:	89 c2                	mov    %eax,%edx
    m = min(n - tot, BSIZE - off%BSIZE);
80101b07:	89 f0                	mov    %esi,%eax
80101b09:	25 ff 01 00 00       	and    $0x1ff,%eax
80101b0e:	29 fb                	sub    %edi,%ebx
    memmove(dst, bp->data + off%BSIZE, m);
80101b10:	89 55 dc             	mov    %edx,-0x24(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
80101b13:	29 c1                	sub    %eax,%ecx
    memmove(dst, bp->data + off%BSIZE, m);
80101b15:	8d 44 02 5c          	lea    0x5c(%edx,%eax,1),%eax
    m = min(n - tot, BSIZE - off%BSIZE);
80101b19:	39 d9                	cmp    %ebx,%ecx
80101b1b:	0f 46 d9             	cmovbe %ecx,%ebx
    memmove(dst, bp->data + off%BSIZE, m);
80101b1e:	53                   	push   %ebx
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101b1f:	01 df                	add    %ebx,%edi
80101b21:	01 de                	add    %ebx,%esi
    memmove(dst, bp->data + off%BSIZE, m);
80101b23:	50                   	push   %eax
80101b24:	ff 75 e0             	pushl  -0x20(%ebp)
80101b27:	e8 f4 2c 00 00       	call   80104820 <memmove>
    brelse(bp);
80101b2c:	8b 55 dc             	mov    -0x24(%ebp),%edx
80101b2f:	89 14 24             	mov    %edx,(%esp)
80101b32:	e8 b9 e6 ff ff       	call   801001f0 <brelse>
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101b37:	01 5d e0             	add    %ebx,-0x20(%ebp)
80101b3a:	83 c4 10             	add    $0x10,%esp
80101b3d:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
80101b40:	77 9e                	ja     80101ae0 <readi+0x60>
  }
  return n;
80101b42:	8b 45 e4             	mov    -0x1c(%ebp),%eax
}
80101b45:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101b48:	5b                   	pop    %ebx
80101b49:	5e                   	pop    %esi
80101b4a:	5f                   	pop    %edi
80101b4b:	5d                   	pop    %ebp
80101b4c:	c3                   	ret    
80101b4d:	8d 76 00             	lea    0x0(%esi),%esi
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
80101b50:	0f bf 40 52          	movswl 0x52(%eax),%eax
80101b54:	66 83 f8 09          	cmp    $0x9,%ax
80101b58:	77 17                	ja     80101b71 <readi+0xf1>
80101b5a:	8b 04 c5 60 09 11 80 	mov    -0x7feef6a0(,%eax,8),%eax
80101b61:	85 c0                	test   %eax,%eax
80101b63:	74 0c                	je     80101b71 <readi+0xf1>
    return devsw[ip->major].read(ip, dst, n);
80101b65:	89 7d 10             	mov    %edi,0x10(%ebp)
}
80101b68:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101b6b:	5b                   	pop    %ebx
80101b6c:	5e                   	pop    %esi
80101b6d:	5f                   	pop    %edi
80101b6e:	5d                   	pop    %ebp
    return devsw[ip->major].read(ip, dst, n);
80101b6f:	ff e0                	jmp    *%eax
      return -1;
80101b71:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101b76:	eb cd                	jmp    80101b45 <readi+0xc5>
80101b78:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101b7f:	90                   	nop

80101b80 <writei>:
// PAGEBREAK!
// Write data to inode.
// Caller must hold ip->lock.
int
writei(struct inode *ip, char *src, uint off, uint n)
{
80101b80:	f3 0f 1e fb          	endbr32 
80101b84:	55                   	push   %ebp
80101b85:	89 e5                	mov    %esp,%ebp
80101b87:	57                   	push   %edi
80101b88:	56                   	push   %esi
80101b89:	53                   	push   %ebx
80101b8a:	83 ec 1c             	sub    $0x1c,%esp
80101b8d:	8b 45 08             	mov    0x8(%ebp),%eax
80101b90:	8b 75 0c             	mov    0xc(%ebp),%esi
80101b93:	8b 7d 14             	mov    0x14(%ebp),%edi
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80101b96:	66 83 78 50 03       	cmpw   $0x3,0x50(%eax)
{
80101b9b:	89 75 dc             	mov    %esi,-0x24(%ebp)
80101b9e:	89 45 d8             	mov    %eax,-0x28(%ebp)
80101ba1:	8b 75 10             	mov    0x10(%ebp),%esi
80101ba4:	89 7d e0             	mov    %edi,-0x20(%ebp)
  if(ip->type == T_DEV){
80101ba7:	0f 84 b3 00 00 00    	je     80101c60 <writei+0xe0>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
      return -1;
    return devsw[ip->major].write(ip, src, n);
  }

  if(off > ip->size || off + n < off)
80101bad:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101bb0:	39 70 58             	cmp    %esi,0x58(%eax)
80101bb3:	0f 82 e3 00 00 00    	jb     80101c9c <writei+0x11c>
    return -1;
  if(off + n > MAXFILE*BSIZE)
80101bb9:	8b 7d e0             	mov    -0x20(%ebp),%edi
80101bbc:	89 f8                	mov    %edi,%eax
80101bbe:	01 f0                	add    %esi,%eax
80101bc0:	0f 82 d6 00 00 00    	jb     80101c9c <writei+0x11c>
80101bc6:	3d 00 18 01 00       	cmp    $0x11800,%eax
80101bcb:	0f 87 cb 00 00 00    	ja     80101c9c <writei+0x11c>
    return -1;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101bd1:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
80101bd8:	85 ff                	test   %edi,%edi
80101bda:	74 75                	je     80101c51 <writei+0xd1>
80101bdc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101be0:	8b 7d d8             	mov    -0x28(%ebp),%edi
80101be3:	89 f2                	mov    %esi,%edx
80101be5:	c1 ea 09             	shr    $0x9,%edx
80101be8:	89 f8                	mov    %edi,%eax
80101bea:	e8 61 f8 ff ff       	call   80101450 <bmap>
80101bef:	83 ec 08             	sub    $0x8,%esp
80101bf2:	50                   	push   %eax
80101bf3:	ff 37                	pushl  (%edi)
80101bf5:	e8 d6 e4 ff ff       	call   801000d0 <bread>
    m = min(n - tot, BSIZE - off%BSIZE);
80101bfa:	b9 00 02 00 00       	mov    $0x200,%ecx
80101bff:	8b 5d e0             	mov    -0x20(%ebp),%ebx
80101c02:	2b 5d e4             	sub    -0x1c(%ebp),%ebx
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101c05:	89 c7                	mov    %eax,%edi
    m = min(n - tot, BSIZE - off%BSIZE);
80101c07:	89 f0                	mov    %esi,%eax
80101c09:	83 c4 0c             	add    $0xc,%esp
80101c0c:	25 ff 01 00 00       	and    $0x1ff,%eax
80101c11:	29 c1                	sub    %eax,%ecx
    memmove(bp->data + off%BSIZE, src, m);
80101c13:	8d 44 07 5c          	lea    0x5c(%edi,%eax,1),%eax
    m = min(n - tot, BSIZE - off%BSIZE);
80101c17:	39 d9                	cmp    %ebx,%ecx
80101c19:	0f 46 d9             	cmovbe %ecx,%ebx
    memmove(bp->data + off%BSIZE, src, m);
80101c1c:	53                   	push   %ebx
  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101c1d:	01 de                	add    %ebx,%esi
    memmove(bp->data + off%BSIZE, src, m);
80101c1f:	ff 75 dc             	pushl  -0x24(%ebp)
80101c22:	50                   	push   %eax
80101c23:	e8 f8 2b 00 00       	call   80104820 <memmove>
    log_write(bp);
80101c28:	89 3c 24             	mov    %edi,(%esp)
80101c2b:	e8 00 13 00 00       	call   80102f30 <log_write>
    brelse(bp);
80101c30:	89 3c 24             	mov    %edi,(%esp)
80101c33:	e8 b8 e5 ff ff       	call   801001f0 <brelse>
  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101c38:	01 5d e4             	add    %ebx,-0x1c(%ebp)
80101c3b:	83 c4 10             	add    $0x10,%esp
80101c3e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101c41:	01 5d dc             	add    %ebx,-0x24(%ebp)
80101c44:	39 45 e0             	cmp    %eax,-0x20(%ebp)
80101c47:	77 97                	ja     80101be0 <writei+0x60>
  }

  if(n > 0 && off > ip->size){
80101c49:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101c4c:	3b 70 58             	cmp    0x58(%eax),%esi
80101c4f:	77 37                	ja     80101c88 <writei+0x108>
    ip->size = off;
    iupdate(ip);
  }
  return n;
80101c51:	8b 45 e0             	mov    -0x20(%ebp),%eax
}
80101c54:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101c57:	5b                   	pop    %ebx
80101c58:	5e                   	pop    %esi
80101c59:	5f                   	pop    %edi
80101c5a:	5d                   	pop    %ebp
80101c5b:	c3                   	ret    
80101c5c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
80101c60:	0f bf 40 52          	movswl 0x52(%eax),%eax
80101c64:	66 83 f8 09          	cmp    $0x9,%ax
80101c68:	77 32                	ja     80101c9c <writei+0x11c>
80101c6a:	8b 04 c5 64 09 11 80 	mov    -0x7feef69c(,%eax,8),%eax
80101c71:	85 c0                	test   %eax,%eax
80101c73:	74 27                	je     80101c9c <writei+0x11c>
    return devsw[ip->major].write(ip, src, n);
80101c75:	89 7d 10             	mov    %edi,0x10(%ebp)
}
80101c78:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101c7b:	5b                   	pop    %ebx
80101c7c:	5e                   	pop    %esi
80101c7d:	5f                   	pop    %edi
80101c7e:	5d                   	pop    %ebp
    return devsw[ip->major].write(ip, src, n);
80101c7f:	ff e0                	jmp    *%eax
80101c81:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    ip->size = off;
80101c88:	8b 45 d8             	mov    -0x28(%ebp),%eax
    iupdate(ip);
80101c8b:	83 ec 0c             	sub    $0xc,%esp
    ip->size = off;
80101c8e:	89 70 58             	mov    %esi,0x58(%eax)
    iupdate(ip);
80101c91:	50                   	push   %eax
80101c92:	e8 29 fa ff ff       	call   801016c0 <iupdate>
80101c97:	83 c4 10             	add    $0x10,%esp
80101c9a:	eb b5                	jmp    80101c51 <writei+0xd1>
      return -1;
80101c9c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101ca1:	eb b1                	jmp    80101c54 <writei+0xd4>
80101ca3:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101caa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80101cb0 <namecmp>:
//PAGEBREAK!
// Directories

int
namecmp(const char *s, const char *t)
{
80101cb0:	f3 0f 1e fb          	endbr32 
80101cb4:	55                   	push   %ebp
80101cb5:	89 e5                	mov    %esp,%ebp
80101cb7:	83 ec 0c             	sub    $0xc,%esp
  return strncmp(s, t, DIRSIZ);
80101cba:	6a 0e                	push   $0xe
80101cbc:	ff 75 0c             	pushl  0xc(%ebp)
80101cbf:	ff 75 08             	pushl  0x8(%ebp)
80101cc2:	e8 c9 2b 00 00       	call   80104890 <strncmp>
}
80101cc7:	c9                   	leave  
80101cc8:	c3                   	ret    
80101cc9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80101cd0 <dirlookup>:

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
struct inode*
dirlookup(struct inode *dp, char *name, uint *poff)
{
80101cd0:	f3 0f 1e fb          	endbr32 
80101cd4:	55                   	push   %ebp
80101cd5:	89 e5                	mov    %esp,%ebp
80101cd7:	57                   	push   %edi
80101cd8:	56                   	push   %esi
80101cd9:	53                   	push   %ebx
80101cda:	83 ec 1c             	sub    $0x1c,%esp
80101cdd:	8b 5d 08             	mov    0x8(%ebp),%ebx
  uint off, inum;
  struct dirent de;

  if(dp->type != T_DIR)
80101ce0:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80101ce5:	0f 85 89 00 00 00    	jne    80101d74 <dirlookup+0xa4>
    panic("dirlookup not DIR");

  for(off = 0; off < dp->size; off += sizeof(de)){
80101ceb:	8b 53 58             	mov    0x58(%ebx),%edx
80101cee:	31 ff                	xor    %edi,%edi
80101cf0:	8d 75 d8             	lea    -0x28(%ebp),%esi
80101cf3:	85 d2                	test   %edx,%edx
80101cf5:	74 42                	je     80101d39 <dirlookup+0x69>
80101cf7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101cfe:	66 90                	xchg   %ax,%ax
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101d00:	6a 10                	push   $0x10
80101d02:	57                   	push   %edi
80101d03:	56                   	push   %esi
80101d04:	53                   	push   %ebx
80101d05:	e8 76 fd ff ff       	call   80101a80 <readi>
80101d0a:	83 c4 10             	add    $0x10,%esp
80101d0d:	83 f8 10             	cmp    $0x10,%eax
80101d10:	75 55                	jne    80101d67 <dirlookup+0x97>
      panic("dirlookup read");
    if(de.inum == 0)
80101d12:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
80101d17:	74 18                	je     80101d31 <dirlookup+0x61>
  return strncmp(s, t, DIRSIZ);
80101d19:	83 ec 04             	sub    $0x4,%esp
80101d1c:	8d 45 da             	lea    -0x26(%ebp),%eax
80101d1f:	6a 0e                	push   $0xe
80101d21:	50                   	push   %eax
80101d22:	ff 75 0c             	pushl  0xc(%ebp)
80101d25:	e8 66 2b 00 00       	call   80104890 <strncmp>
      continue;
    if(namecmp(name, de.name) == 0){
80101d2a:	83 c4 10             	add    $0x10,%esp
80101d2d:	85 c0                	test   %eax,%eax
80101d2f:	74 17                	je     80101d48 <dirlookup+0x78>
  for(off = 0; off < dp->size; off += sizeof(de)){
80101d31:	83 c7 10             	add    $0x10,%edi
80101d34:	3b 7b 58             	cmp    0x58(%ebx),%edi
80101d37:	72 c7                	jb     80101d00 <dirlookup+0x30>
      return iget(dp->dev, inum);
    }
  }

  return 0;
}
80101d39:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
80101d3c:	31 c0                	xor    %eax,%eax
}
80101d3e:	5b                   	pop    %ebx
80101d3f:	5e                   	pop    %esi
80101d40:	5f                   	pop    %edi
80101d41:	5d                   	pop    %ebp
80101d42:	c3                   	ret    
80101d43:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101d47:	90                   	nop
      if(poff)
80101d48:	8b 45 10             	mov    0x10(%ebp),%eax
80101d4b:	85 c0                	test   %eax,%eax
80101d4d:	74 05                	je     80101d54 <dirlookup+0x84>
        *poff = off;
80101d4f:	8b 45 10             	mov    0x10(%ebp),%eax
80101d52:	89 38                	mov    %edi,(%eax)
      inum = de.inum;
80101d54:	0f b7 55 d8          	movzwl -0x28(%ebp),%edx
      return iget(dp->dev, inum);
80101d58:	8b 03                	mov    (%ebx),%eax
80101d5a:	e8 01 f6 ff ff       	call   80101360 <iget>
}
80101d5f:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101d62:	5b                   	pop    %ebx
80101d63:	5e                   	pop    %esi
80101d64:	5f                   	pop    %edi
80101d65:	5d                   	pop    %ebp
80101d66:	c3                   	ret    
      panic("dirlookup read");
80101d67:	83 ec 0c             	sub    $0xc,%esp
80101d6a:	68 59 73 10 80       	push   $0x80107359
80101d6f:	e8 1c e6 ff ff       	call   80100390 <panic>
    panic("dirlookup not DIR");
80101d74:	83 ec 0c             	sub    $0xc,%esp
80101d77:	68 47 73 10 80       	push   $0x80107347
80101d7c:	e8 0f e6 ff ff       	call   80100390 <panic>
80101d81:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101d88:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101d8f:	90                   	nop

80101d90 <namex>:
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
// Must be called inside a transaction since it calls iput().
static struct inode*
namex(char *path, int nameiparent, char *name)
{
80101d90:	55                   	push   %ebp
80101d91:	89 e5                	mov    %esp,%ebp
80101d93:	57                   	push   %edi
80101d94:	56                   	push   %esi
80101d95:	53                   	push   %ebx
80101d96:	89 c3                	mov    %eax,%ebx
80101d98:	83 ec 1c             	sub    $0x1c,%esp
  struct inode *ip, *next;

  if(*path == '/')
80101d9b:	80 38 2f             	cmpb   $0x2f,(%eax)
{
80101d9e:	89 55 e0             	mov    %edx,-0x20(%ebp)
80101da1:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  if(*path == '/')
80101da4:	0f 84 86 01 00 00    	je     80101f30 <namex+0x1a0>
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(myproc()->cwd);
80101daa:	e8 51 1c 00 00       	call   80103a00 <myproc>
  acquire(&icache.lock);
80101daf:	83 ec 0c             	sub    $0xc,%esp
80101db2:	89 df                	mov    %ebx,%edi
    ip = idup(myproc()->cwd);
80101db4:	8b 70 68             	mov    0x68(%eax),%esi
  acquire(&icache.lock);
80101db7:	68 e0 09 11 80       	push   $0x801109e0
80101dbc:	e8 af 28 00 00       	call   80104670 <acquire>
  ip->ref++;
80101dc1:	83 46 08 01          	addl   $0x1,0x8(%esi)
  release(&icache.lock);
80101dc5:	c7 04 24 e0 09 11 80 	movl   $0x801109e0,(%esp)
80101dcc:	e8 5f 29 00 00       	call   80104730 <release>
80101dd1:	83 c4 10             	add    $0x10,%esp
80101dd4:	eb 0d                	jmp    80101de3 <namex+0x53>
80101dd6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101ddd:	8d 76 00             	lea    0x0(%esi),%esi
    path++;
80101de0:	83 c7 01             	add    $0x1,%edi
  while(*path == '/')
80101de3:	0f b6 07             	movzbl (%edi),%eax
80101de6:	3c 2f                	cmp    $0x2f,%al
80101de8:	74 f6                	je     80101de0 <namex+0x50>
  if(*path == 0)
80101dea:	84 c0                	test   %al,%al
80101dec:	0f 84 ee 00 00 00    	je     80101ee0 <namex+0x150>
  while(*path != '/' && *path != 0)
80101df2:	0f b6 07             	movzbl (%edi),%eax
80101df5:	84 c0                	test   %al,%al
80101df7:	0f 84 fb 00 00 00    	je     80101ef8 <namex+0x168>
80101dfd:	89 fb                	mov    %edi,%ebx
80101dff:	3c 2f                	cmp    $0x2f,%al
80101e01:	0f 84 f1 00 00 00    	je     80101ef8 <namex+0x168>
80101e07:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101e0e:	66 90                	xchg   %ax,%ax
80101e10:	0f b6 43 01          	movzbl 0x1(%ebx),%eax
    path++;
80101e14:	83 c3 01             	add    $0x1,%ebx
  while(*path != '/' && *path != 0)
80101e17:	3c 2f                	cmp    $0x2f,%al
80101e19:	74 04                	je     80101e1f <namex+0x8f>
80101e1b:	84 c0                	test   %al,%al
80101e1d:	75 f1                	jne    80101e10 <namex+0x80>
  len = path - s;
80101e1f:	89 d8                	mov    %ebx,%eax
80101e21:	29 f8                	sub    %edi,%eax
  if(len >= DIRSIZ)
80101e23:	83 f8 0d             	cmp    $0xd,%eax
80101e26:	0f 8e 84 00 00 00    	jle    80101eb0 <namex+0x120>
    memmove(name, s, DIRSIZ);
80101e2c:	83 ec 04             	sub    $0x4,%esp
80101e2f:	6a 0e                	push   $0xe
80101e31:	57                   	push   %edi
    path++;
80101e32:	89 df                	mov    %ebx,%edi
    memmove(name, s, DIRSIZ);
80101e34:	ff 75 e4             	pushl  -0x1c(%ebp)
80101e37:	e8 e4 29 00 00       	call   80104820 <memmove>
80101e3c:	83 c4 10             	add    $0x10,%esp
  while(*path == '/')
80101e3f:	80 3b 2f             	cmpb   $0x2f,(%ebx)
80101e42:	75 0c                	jne    80101e50 <namex+0xc0>
80101e44:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    path++;
80101e48:	83 c7 01             	add    $0x1,%edi
  while(*path == '/')
80101e4b:	80 3f 2f             	cmpb   $0x2f,(%edi)
80101e4e:	74 f8                	je     80101e48 <namex+0xb8>

  while((path = skipelem(path, name)) != 0){
    ilock(ip);
80101e50:	83 ec 0c             	sub    $0xc,%esp
80101e53:	56                   	push   %esi
80101e54:	e8 27 f9 ff ff       	call   80101780 <ilock>
    if(ip->type != T_DIR){
80101e59:	83 c4 10             	add    $0x10,%esp
80101e5c:	66 83 7e 50 01       	cmpw   $0x1,0x50(%esi)
80101e61:	0f 85 a1 00 00 00    	jne    80101f08 <namex+0x178>
      iunlockput(ip);
      return 0;
    }
    if(nameiparent && *path == '\0'){
80101e67:	8b 55 e0             	mov    -0x20(%ebp),%edx
80101e6a:	85 d2                	test   %edx,%edx
80101e6c:	74 09                	je     80101e77 <namex+0xe7>
80101e6e:	80 3f 00             	cmpb   $0x0,(%edi)
80101e71:	0f 84 d9 00 00 00    	je     80101f50 <namex+0x1c0>
      // Stop one level early.
      iunlock(ip);
      return ip;
    }
    if((next = dirlookup(ip, name, 0)) == 0){
80101e77:	83 ec 04             	sub    $0x4,%esp
80101e7a:	6a 00                	push   $0x0
80101e7c:	ff 75 e4             	pushl  -0x1c(%ebp)
80101e7f:	56                   	push   %esi
80101e80:	e8 4b fe ff ff       	call   80101cd0 <dirlookup>
80101e85:	83 c4 10             	add    $0x10,%esp
80101e88:	89 c3                	mov    %eax,%ebx
80101e8a:	85 c0                	test   %eax,%eax
80101e8c:	74 7a                	je     80101f08 <namex+0x178>
  iunlock(ip);
80101e8e:	83 ec 0c             	sub    $0xc,%esp
80101e91:	56                   	push   %esi
80101e92:	e8 c9 f9 ff ff       	call   80101860 <iunlock>
  iput(ip);
80101e97:	89 34 24             	mov    %esi,(%esp)
80101e9a:	89 de                	mov    %ebx,%esi
80101e9c:	e8 0f fa ff ff       	call   801018b0 <iput>
80101ea1:	83 c4 10             	add    $0x10,%esp
80101ea4:	e9 3a ff ff ff       	jmp    80101de3 <namex+0x53>
80101ea9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101eb0:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80101eb3:	8d 0c 02             	lea    (%edx,%eax,1),%ecx
80101eb6:	89 4d dc             	mov    %ecx,-0x24(%ebp)
    memmove(name, s, len);
80101eb9:	83 ec 04             	sub    $0x4,%esp
80101ebc:	50                   	push   %eax
80101ebd:	57                   	push   %edi
    name[len] = 0;
80101ebe:	89 df                	mov    %ebx,%edi
    memmove(name, s, len);
80101ec0:	ff 75 e4             	pushl  -0x1c(%ebp)
80101ec3:	e8 58 29 00 00       	call   80104820 <memmove>
    name[len] = 0;
80101ec8:	8b 45 dc             	mov    -0x24(%ebp),%eax
80101ecb:	83 c4 10             	add    $0x10,%esp
80101ece:	c6 00 00             	movb   $0x0,(%eax)
80101ed1:	e9 69 ff ff ff       	jmp    80101e3f <namex+0xaf>
80101ed6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101edd:	8d 76 00             	lea    0x0(%esi),%esi
      return 0;
    }
    iunlockput(ip);
    ip = next;
  }
  if(nameiparent){
80101ee0:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101ee3:	85 c0                	test   %eax,%eax
80101ee5:	0f 85 85 00 00 00    	jne    80101f70 <namex+0x1e0>
    iput(ip);
    return 0;
  }
  return ip;
}
80101eeb:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101eee:	89 f0                	mov    %esi,%eax
80101ef0:	5b                   	pop    %ebx
80101ef1:	5e                   	pop    %esi
80101ef2:	5f                   	pop    %edi
80101ef3:	5d                   	pop    %ebp
80101ef4:	c3                   	ret    
80101ef5:	8d 76 00             	lea    0x0(%esi),%esi
  while(*path != '/' && *path != 0)
80101ef8:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101efb:	89 fb                	mov    %edi,%ebx
80101efd:	89 45 dc             	mov    %eax,-0x24(%ebp)
80101f00:	31 c0                	xor    %eax,%eax
80101f02:	eb b5                	jmp    80101eb9 <namex+0x129>
80101f04:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  iunlock(ip);
80101f08:	83 ec 0c             	sub    $0xc,%esp
80101f0b:	56                   	push   %esi
80101f0c:	e8 4f f9 ff ff       	call   80101860 <iunlock>
  iput(ip);
80101f11:	89 34 24             	mov    %esi,(%esp)
      return 0;
80101f14:	31 f6                	xor    %esi,%esi
  iput(ip);
80101f16:	e8 95 f9 ff ff       	call   801018b0 <iput>
      return 0;
80101f1b:	83 c4 10             	add    $0x10,%esp
}
80101f1e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101f21:	89 f0                	mov    %esi,%eax
80101f23:	5b                   	pop    %ebx
80101f24:	5e                   	pop    %esi
80101f25:	5f                   	pop    %edi
80101f26:	5d                   	pop    %ebp
80101f27:	c3                   	ret    
80101f28:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101f2f:	90                   	nop
    ip = iget(ROOTDEV, ROOTINO);
80101f30:	ba 01 00 00 00       	mov    $0x1,%edx
80101f35:	b8 01 00 00 00       	mov    $0x1,%eax
80101f3a:	89 df                	mov    %ebx,%edi
80101f3c:	e8 1f f4 ff ff       	call   80101360 <iget>
80101f41:	89 c6                	mov    %eax,%esi
80101f43:	e9 9b fe ff ff       	jmp    80101de3 <namex+0x53>
80101f48:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101f4f:	90                   	nop
      iunlock(ip);
80101f50:	83 ec 0c             	sub    $0xc,%esp
80101f53:	56                   	push   %esi
80101f54:	e8 07 f9 ff ff       	call   80101860 <iunlock>
      return ip;
80101f59:	83 c4 10             	add    $0x10,%esp
}
80101f5c:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101f5f:	89 f0                	mov    %esi,%eax
80101f61:	5b                   	pop    %ebx
80101f62:	5e                   	pop    %esi
80101f63:	5f                   	pop    %edi
80101f64:	5d                   	pop    %ebp
80101f65:	c3                   	ret    
80101f66:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101f6d:	8d 76 00             	lea    0x0(%esi),%esi
    iput(ip);
80101f70:	83 ec 0c             	sub    $0xc,%esp
80101f73:	56                   	push   %esi
    return 0;
80101f74:	31 f6                	xor    %esi,%esi
    iput(ip);
80101f76:	e8 35 f9 ff ff       	call   801018b0 <iput>
    return 0;
80101f7b:	83 c4 10             	add    $0x10,%esp
80101f7e:	e9 68 ff ff ff       	jmp    80101eeb <namex+0x15b>
80101f83:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101f8a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80101f90 <dirlink>:
{
80101f90:	f3 0f 1e fb          	endbr32 
80101f94:	55                   	push   %ebp
80101f95:	89 e5                	mov    %esp,%ebp
80101f97:	57                   	push   %edi
80101f98:	56                   	push   %esi
80101f99:	53                   	push   %ebx
80101f9a:	83 ec 20             	sub    $0x20,%esp
80101f9d:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if((ip = dirlookup(dp, name, 0)) != 0){
80101fa0:	6a 00                	push   $0x0
80101fa2:	ff 75 0c             	pushl  0xc(%ebp)
80101fa5:	53                   	push   %ebx
80101fa6:	e8 25 fd ff ff       	call   80101cd0 <dirlookup>
80101fab:	83 c4 10             	add    $0x10,%esp
80101fae:	85 c0                	test   %eax,%eax
80101fb0:	75 6b                	jne    8010201d <dirlink+0x8d>
  for(off = 0; off < dp->size; off += sizeof(de)){
80101fb2:	8b 7b 58             	mov    0x58(%ebx),%edi
80101fb5:	8d 75 d8             	lea    -0x28(%ebp),%esi
80101fb8:	85 ff                	test   %edi,%edi
80101fba:	74 2d                	je     80101fe9 <dirlink+0x59>
80101fbc:	31 ff                	xor    %edi,%edi
80101fbe:	8d 75 d8             	lea    -0x28(%ebp),%esi
80101fc1:	eb 0d                	jmp    80101fd0 <dirlink+0x40>
80101fc3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101fc7:	90                   	nop
80101fc8:	83 c7 10             	add    $0x10,%edi
80101fcb:	3b 7b 58             	cmp    0x58(%ebx),%edi
80101fce:	73 19                	jae    80101fe9 <dirlink+0x59>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101fd0:	6a 10                	push   $0x10
80101fd2:	57                   	push   %edi
80101fd3:	56                   	push   %esi
80101fd4:	53                   	push   %ebx
80101fd5:	e8 a6 fa ff ff       	call   80101a80 <readi>
80101fda:	83 c4 10             	add    $0x10,%esp
80101fdd:	83 f8 10             	cmp    $0x10,%eax
80101fe0:	75 4e                	jne    80102030 <dirlink+0xa0>
    if(de.inum == 0)
80101fe2:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
80101fe7:	75 df                	jne    80101fc8 <dirlink+0x38>
  strncpy(de.name, name, DIRSIZ);
80101fe9:	83 ec 04             	sub    $0x4,%esp
80101fec:	8d 45 da             	lea    -0x26(%ebp),%eax
80101fef:	6a 0e                	push   $0xe
80101ff1:	ff 75 0c             	pushl  0xc(%ebp)
80101ff4:	50                   	push   %eax
80101ff5:	e8 e6 28 00 00       	call   801048e0 <strncpy>
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101ffa:	6a 10                	push   $0x10
  de.inum = inum;
80101ffc:	8b 45 10             	mov    0x10(%ebp),%eax
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101fff:	57                   	push   %edi
80102000:	56                   	push   %esi
80102001:	53                   	push   %ebx
  de.inum = inum;
80102002:	66 89 45 d8          	mov    %ax,-0x28(%ebp)
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80102006:	e8 75 fb ff ff       	call   80101b80 <writei>
8010200b:	83 c4 20             	add    $0x20,%esp
8010200e:	83 f8 10             	cmp    $0x10,%eax
80102011:	75 2a                	jne    8010203d <dirlink+0xad>
  return 0;
80102013:	31 c0                	xor    %eax,%eax
}
80102015:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102018:	5b                   	pop    %ebx
80102019:	5e                   	pop    %esi
8010201a:	5f                   	pop    %edi
8010201b:	5d                   	pop    %ebp
8010201c:	c3                   	ret    
    iput(ip);
8010201d:	83 ec 0c             	sub    $0xc,%esp
80102020:	50                   	push   %eax
80102021:	e8 8a f8 ff ff       	call   801018b0 <iput>
    return -1;
80102026:	83 c4 10             	add    $0x10,%esp
80102029:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010202e:	eb e5                	jmp    80102015 <dirlink+0x85>
      panic("dirlink read");
80102030:	83 ec 0c             	sub    $0xc,%esp
80102033:	68 68 73 10 80       	push   $0x80107368
80102038:	e8 53 e3 ff ff       	call   80100390 <panic>
    panic("dirlink");
8010203d:	83 ec 0c             	sub    $0xc,%esp
80102040:	68 9e 7b 10 80       	push   $0x80107b9e
80102045:	e8 46 e3 ff ff       	call   80100390 <panic>
8010204a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80102050 <namei>:

struct inode*
namei(char *path)
{
80102050:	f3 0f 1e fb          	endbr32 
80102054:	55                   	push   %ebp
  char name[DIRSIZ];
  return namex(path, 0, name);
80102055:	31 d2                	xor    %edx,%edx
{
80102057:	89 e5                	mov    %esp,%ebp
80102059:	83 ec 18             	sub    $0x18,%esp
  return namex(path, 0, name);
8010205c:	8b 45 08             	mov    0x8(%ebp),%eax
8010205f:	8d 4d ea             	lea    -0x16(%ebp),%ecx
80102062:	e8 29 fd ff ff       	call   80101d90 <namex>
}
80102067:	c9                   	leave  
80102068:	c3                   	ret    
80102069:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80102070 <nameiparent>:

struct inode*
nameiparent(char *path, char *name)
{
80102070:	f3 0f 1e fb          	endbr32 
80102074:	55                   	push   %ebp
  return namex(path, 1, name);
80102075:	ba 01 00 00 00       	mov    $0x1,%edx
{
8010207a:	89 e5                	mov    %esp,%ebp
  return namex(path, 1, name);
8010207c:	8b 4d 0c             	mov    0xc(%ebp),%ecx
8010207f:	8b 45 08             	mov    0x8(%ebp),%eax
}
80102082:	5d                   	pop    %ebp
  return namex(path, 1, name);
80102083:	e9 08 fd ff ff       	jmp    80101d90 <namex>
80102088:	66 90                	xchg   %ax,%ax
8010208a:	66 90                	xchg   %ax,%ax
8010208c:	66 90                	xchg   %ax,%ax
8010208e:	66 90                	xchg   %ax,%ax

80102090 <idestart>:
}

// Start the request for b.  Caller must hold idelock.
static void
idestart(struct buf *b)
{
80102090:	55                   	push   %ebp
80102091:	89 e5                	mov    %esp,%ebp
80102093:	57                   	push   %edi
80102094:	56                   	push   %esi
80102095:	53                   	push   %ebx
80102096:	83 ec 0c             	sub    $0xc,%esp
  if(b == 0)
80102099:	85 c0                	test   %eax,%eax
8010209b:	0f 84 b4 00 00 00    	je     80102155 <idestart+0xc5>
    panic("idestart");
  if(b->blockno >= FSSIZE)
801020a1:	8b 70 08             	mov    0x8(%eax),%esi
801020a4:	89 c3                	mov    %eax,%ebx
801020a6:	81 fe e7 03 00 00    	cmp    $0x3e7,%esi
801020ac:	0f 87 96 00 00 00    	ja     80102148 <idestart+0xb8>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801020b2:	b9 f7 01 00 00       	mov    $0x1f7,%ecx
801020b7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801020be:	66 90                	xchg   %ax,%ax
801020c0:	89 ca                	mov    %ecx,%edx
801020c2:	ec                   	in     (%dx),%al
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
801020c3:	83 e0 c0             	and    $0xffffffc0,%eax
801020c6:	3c 40                	cmp    $0x40,%al
801020c8:	75 f6                	jne    801020c0 <idestart+0x30>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801020ca:	31 ff                	xor    %edi,%edi
801020cc:	ba f6 03 00 00       	mov    $0x3f6,%edx
801020d1:	89 f8                	mov    %edi,%eax
801020d3:	ee                   	out    %al,(%dx)
801020d4:	b8 01 00 00 00       	mov    $0x1,%eax
801020d9:	ba f2 01 00 00       	mov    $0x1f2,%edx
801020de:	ee                   	out    %al,(%dx)
801020df:	ba f3 01 00 00       	mov    $0x1f3,%edx
801020e4:	89 f0                	mov    %esi,%eax
801020e6:	ee                   	out    %al,(%dx)

  idewait(0);
  outb(0x3f6, 0);  // generate interrupt
  outb(0x1f2, sector_per_block);  // number of sectors
  outb(0x1f3, sector & 0xff);
  outb(0x1f4, (sector >> 8) & 0xff);
801020e7:	89 f0                	mov    %esi,%eax
801020e9:	ba f4 01 00 00       	mov    $0x1f4,%edx
801020ee:	c1 f8 08             	sar    $0x8,%eax
801020f1:	ee                   	out    %al,(%dx)
801020f2:	ba f5 01 00 00       	mov    $0x1f5,%edx
801020f7:	89 f8                	mov    %edi,%eax
801020f9:	ee                   	out    %al,(%dx)
  outb(0x1f5, (sector >> 16) & 0xff);
  outb(0x1f6, 0xe0 | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
801020fa:	0f b6 43 04          	movzbl 0x4(%ebx),%eax
801020fe:	ba f6 01 00 00       	mov    $0x1f6,%edx
80102103:	c1 e0 04             	shl    $0x4,%eax
80102106:	83 e0 10             	and    $0x10,%eax
80102109:	83 c8 e0             	or     $0xffffffe0,%eax
8010210c:	ee                   	out    %al,(%dx)
  if(b->flags & B_DIRTY){
8010210d:	f6 03 04             	testb  $0x4,(%ebx)
80102110:	75 16                	jne    80102128 <idestart+0x98>
80102112:	b8 20 00 00 00       	mov    $0x20,%eax
80102117:	89 ca                	mov    %ecx,%edx
80102119:	ee                   	out    %al,(%dx)
    outb(0x1f7, write_cmd);
    outsl(0x1f0, b->data, BSIZE/4);
  } else {
    outb(0x1f7, read_cmd);
  }
}
8010211a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010211d:	5b                   	pop    %ebx
8010211e:	5e                   	pop    %esi
8010211f:	5f                   	pop    %edi
80102120:	5d                   	pop    %ebp
80102121:	c3                   	ret    
80102122:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80102128:	b8 30 00 00 00       	mov    $0x30,%eax
8010212d:	89 ca                	mov    %ecx,%edx
8010212f:	ee                   	out    %al,(%dx)
  asm volatile("cld; rep outsl" :
80102130:	b9 80 00 00 00       	mov    $0x80,%ecx
    outsl(0x1f0, b->data, BSIZE/4);
80102135:	8d 73 5c             	lea    0x5c(%ebx),%esi
80102138:	ba f0 01 00 00       	mov    $0x1f0,%edx
8010213d:	fc                   	cld    
8010213e:	f3 6f                	rep outsl %ds:(%esi),(%dx)
}
80102140:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102143:	5b                   	pop    %ebx
80102144:	5e                   	pop    %esi
80102145:	5f                   	pop    %edi
80102146:	5d                   	pop    %ebp
80102147:	c3                   	ret    
    panic("incorrect blockno");
80102148:	83 ec 0c             	sub    $0xc,%esp
8010214b:	68 d4 73 10 80       	push   $0x801073d4
80102150:	e8 3b e2 ff ff       	call   80100390 <panic>
    panic("idestart");
80102155:	83 ec 0c             	sub    $0xc,%esp
80102158:	68 cb 73 10 80       	push   $0x801073cb
8010215d:	e8 2e e2 ff ff       	call   80100390 <panic>
80102162:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102169:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80102170 <ideinit>:
{
80102170:	f3 0f 1e fb          	endbr32 
80102174:	55                   	push   %ebp
80102175:	89 e5                	mov    %esp,%ebp
80102177:	83 ec 10             	sub    $0x10,%esp
  initlock(&idelock, "ide");
8010217a:	68 e6 73 10 80       	push   $0x801073e6
8010217f:	68 80 a5 10 80       	push   $0x8010a580
80102184:	e8 67 23 00 00       	call   801044f0 <initlock>
  ioapicenable(IRQ_IDE, ncpu - 1);
80102189:	58                   	pop    %eax
8010218a:	a1 00 2d 11 80       	mov    0x80112d00,%eax
8010218f:	5a                   	pop    %edx
80102190:	83 e8 01             	sub    $0x1,%eax
80102193:	50                   	push   %eax
80102194:	6a 0e                	push   $0xe
80102196:	e8 b5 02 00 00       	call   80102450 <ioapicenable>
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
8010219b:	83 c4 10             	add    $0x10,%esp
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010219e:	ba f7 01 00 00       	mov    $0x1f7,%edx
801021a3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801021a7:	90                   	nop
801021a8:	ec                   	in     (%dx),%al
801021a9:	83 e0 c0             	and    $0xffffffc0,%eax
801021ac:	3c 40                	cmp    $0x40,%al
801021ae:	75 f8                	jne    801021a8 <ideinit+0x38>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801021b0:	b8 f0 ff ff ff       	mov    $0xfffffff0,%eax
801021b5:	ba f6 01 00 00       	mov    $0x1f6,%edx
801021ba:	ee                   	out    %al,(%dx)
801021bb:	b9 e8 03 00 00       	mov    $0x3e8,%ecx
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801021c0:	ba f7 01 00 00       	mov    $0x1f7,%edx
801021c5:	eb 0e                	jmp    801021d5 <ideinit+0x65>
801021c7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801021ce:	66 90                	xchg   %ax,%ax
  for(i=0; i<1000; i++){
801021d0:	83 e9 01             	sub    $0x1,%ecx
801021d3:	74 0f                	je     801021e4 <ideinit+0x74>
801021d5:	ec                   	in     (%dx),%al
    if(inb(0x1f7) != 0){
801021d6:	84 c0                	test   %al,%al
801021d8:	74 f6                	je     801021d0 <ideinit+0x60>
      havedisk1 = 1;
801021da:	c7 05 60 a5 10 80 01 	movl   $0x1,0x8010a560
801021e1:	00 00 00 
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801021e4:	b8 e0 ff ff ff       	mov    $0xffffffe0,%eax
801021e9:	ba f6 01 00 00       	mov    $0x1f6,%edx
801021ee:	ee                   	out    %al,(%dx)
}
801021ef:	c9                   	leave  
801021f0:	c3                   	ret    
801021f1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801021f8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801021ff:	90                   	nop

80102200 <ideintr>:

// Interrupt handler.
void
ideintr(void)
{
80102200:	f3 0f 1e fb          	endbr32 
80102204:	55                   	push   %ebp
80102205:	89 e5                	mov    %esp,%ebp
80102207:	57                   	push   %edi
80102208:	56                   	push   %esi
80102209:	53                   	push   %ebx
8010220a:	83 ec 18             	sub    $0x18,%esp
  struct buf *b;

  // First queued buffer is the active request.
  acquire(&idelock);
8010220d:	68 80 a5 10 80       	push   $0x8010a580
80102212:	e8 59 24 00 00       	call   80104670 <acquire>

  if((b = idequeue) == 0){
80102217:	8b 1d 64 a5 10 80    	mov    0x8010a564,%ebx
8010221d:	83 c4 10             	add    $0x10,%esp
80102220:	85 db                	test   %ebx,%ebx
80102222:	74 5f                	je     80102283 <ideintr+0x83>
    release(&idelock);
    return;
  }
  idequeue = b->qnext;
80102224:	8b 43 58             	mov    0x58(%ebx),%eax
80102227:	a3 64 a5 10 80       	mov    %eax,0x8010a564

  // Read data if needed.
  if(!(b->flags & B_DIRTY) && idewait(1) >= 0)
8010222c:	8b 33                	mov    (%ebx),%esi
8010222e:	f7 c6 04 00 00 00    	test   $0x4,%esi
80102234:	75 2b                	jne    80102261 <ideintr+0x61>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102236:	ba f7 01 00 00       	mov    $0x1f7,%edx
8010223b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010223f:	90                   	nop
80102240:	ec                   	in     (%dx),%al
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
80102241:	89 c1                	mov    %eax,%ecx
80102243:	83 e1 c0             	and    $0xffffffc0,%ecx
80102246:	80 f9 40             	cmp    $0x40,%cl
80102249:	75 f5                	jne    80102240 <ideintr+0x40>
  if(checkerr && (r & (IDE_DF|IDE_ERR)) != 0)
8010224b:	a8 21                	test   $0x21,%al
8010224d:	75 12                	jne    80102261 <ideintr+0x61>
    insl(0x1f0, b->data, BSIZE/4);
8010224f:	8d 7b 5c             	lea    0x5c(%ebx),%edi
  asm volatile("cld; rep insl" :
80102252:	b9 80 00 00 00       	mov    $0x80,%ecx
80102257:	ba f0 01 00 00       	mov    $0x1f0,%edx
8010225c:	fc                   	cld    
8010225d:	f3 6d                	rep insl (%dx),%es:(%edi)
8010225f:	8b 33                	mov    (%ebx),%esi

  // Wake process waiting for this buf.
  b->flags |= B_VALID;
  b->flags &= ~B_DIRTY;
80102261:	83 e6 fb             	and    $0xfffffffb,%esi
  wakeup(b);
80102264:	83 ec 0c             	sub    $0xc,%esp
  b->flags &= ~B_DIRTY;
80102267:	83 ce 02             	or     $0x2,%esi
8010226a:	89 33                	mov    %esi,(%ebx)
  wakeup(b);
8010226c:	53                   	push   %ebx
8010226d:	e8 8e 1f 00 00       	call   80104200 <wakeup>

  // Start disk on next buf in queue.
  if(idequeue != 0)
80102272:	a1 64 a5 10 80       	mov    0x8010a564,%eax
80102277:	83 c4 10             	add    $0x10,%esp
8010227a:	85 c0                	test   %eax,%eax
8010227c:	74 05                	je     80102283 <ideintr+0x83>
    idestart(idequeue);
8010227e:	e8 0d fe ff ff       	call   80102090 <idestart>
    release(&idelock);
80102283:	83 ec 0c             	sub    $0xc,%esp
80102286:	68 80 a5 10 80       	push   $0x8010a580
8010228b:	e8 a0 24 00 00       	call   80104730 <release>

  release(&idelock);
}
80102290:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102293:	5b                   	pop    %ebx
80102294:	5e                   	pop    %esi
80102295:	5f                   	pop    %edi
80102296:	5d                   	pop    %ebp
80102297:	c3                   	ret    
80102298:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010229f:	90                   	nop

801022a0 <iderw>:
// Sync buf with disk.
// If B_DIRTY is set, write buf to disk, clear B_DIRTY, set B_VALID.
// Else if B_VALID is not set, read buf from disk, set B_VALID.
void
iderw(struct buf *b)
{
801022a0:	f3 0f 1e fb          	endbr32 
801022a4:	55                   	push   %ebp
801022a5:	89 e5                	mov    %esp,%ebp
801022a7:	53                   	push   %ebx
801022a8:	83 ec 10             	sub    $0x10,%esp
801022ab:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct buf **pp;

  if(!holdingsleep(&b->lock))
801022ae:	8d 43 0c             	lea    0xc(%ebx),%eax
801022b1:	50                   	push   %eax
801022b2:	e8 d9 21 00 00       	call   80104490 <holdingsleep>
801022b7:	83 c4 10             	add    $0x10,%esp
801022ba:	85 c0                	test   %eax,%eax
801022bc:	0f 84 cf 00 00 00    	je     80102391 <iderw+0xf1>
    panic("iderw: buf not locked");
  if((b->flags & (B_VALID|B_DIRTY)) == B_VALID)
801022c2:	8b 03                	mov    (%ebx),%eax
801022c4:	83 e0 06             	and    $0x6,%eax
801022c7:	83 f8 02             	cmp    $0x2,%eax
801022ca:	0f 84 b4 00 00 00    	je     80102384 <iderw+0xe4>
    panic("iderw: nothing to do");
  if(b->dev != 0 && !havedisk1)
801022d0:	8b 53 04             	mov    0x4(%ebx),%edx
801022d3:	85 d2                	test   %edx,%edx
801022d5:	74 0d                	je     801022e4 <iderw+0x44>
801022d7:	a1 60 a5 10 80       	mov    0x8010a560,%eax
801022dc:	85 c0                	test   %eax,%eax
801022de:	0f 84 93 00 00 00    	je     80102377 <iderw+0xd7>
    panic("iderw: ide disk 1 not present");

  acquire(&idelock);  //DOC:acquire-lock
801022e4:	83 ec 0c             	sub    $0xc,%esp
801022e7:	68 80 a5 10 80       	push   $0x8010a580
801022ec:	e8 7f 23 00 00       	call   80104670 <acquire>

  // Append b to idequeue.
  b->qnext = 0;
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
801022f1:	a1 64 a5 10 80       	mov    0x8010a564,%eax
  b->qnext = 0;
801022f6:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
801022fd:	83 c4 10             	add    $0x10,%esp
80102300:	85 c0                	test   %eax,%eax
80102302:	74 6c                	je     80102370 <iderw+0xd0>
80102304:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102308:	89 c2                	mov    %eax,%edx
8010230a:	8b 40 58             	mov    0x58(%eax),%eax
8010230d:	85 c0                	test   %eax,%eax
8010230f:	75 f7                	jne    80102308 <iderw+0x68>
80102311:	83 c2 58             	add    $0x58,%edx
    ;
  *pp = b;
80102314:	89 1a                	mov    %ebx,(%edx)

  // Start disk if necessary.
  if(idequeue == b)
80102316:	39 1d 64 a5 10 80    	cmp    %ebx,0x8010a564
8010231c:	74 42                	je     80102360 <iderw+0xc0>
    idestart(b);

  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
8010231e:	8b 03                	mov    (%ebx),%eax
80102320:	83 e0 06             	and    $0x6,%eax
80102323:	83 f8 02             	cmp    $0x2,%eax
80102326:	74 23                	je     8010234b <iderw+0xab>
80102328:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010232f:	90                   	nop
    sleep(b, &idelock);
80102330:	83 ec 08             	sub    $0x8,%esp
80102333:	68 80 a5 10 80       	push   $0x8010a580
80102338:	53                   	push   %ebx
80102339:	e8 c2 1c 00 00       	call   80104000 <sleep>
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
8010233e:	8b 03                	mov    (%ebx),%eax
80102340:	83 c4 10             	add    $0x10,%esp
80102343:	83 e0 06             	and    $0x6,%eax
80102346:	83 f8 02             	cmp    $0x2,%eax
80102349:	75 e5                	jne    80102330 <iderw+0x90>
  }


  release(&idelock);
8010234b:	c7 45 08 80 a5 10 80 	movl   $0x8010a580,0x8(%ebp)
}
80102352:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102355:	c9                   	leave  
  release(&idelock);
80102356:	e9 d5 23 00 00       	jmp    80104730 <release>
8010235b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010235f:	90                   	nop
    idestart(b);
80102360:	89 d8                	mov    %ebx,%eax
80102362:	e8 29 fd ff ff       	call   80102090 <idestart>
80102367:	eb b5                	jmp    8010231e <iderw+0x7e>
80102369:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
80102370:	ba 64 a5 10 80       	mov    $0x8010a564,%edx
80102375:	eb 9d                	jmp    80102314 <iderw+0x74>
    panic("iderw: ide disk 1 not present");
80102377:	83 ec 0c             	sub    $0xc,%esp
8010237a:	68 15 74 10 80       	push   $0x80107415
8010237f:	e8 0c e0 ff ff       	call   80100390 <panic>
    panic("iderw: nothing to do");
80102384:	83 ec 0c             	sub    $0xc,%esp
80102387:	68 00 74 10 80       	push   $0x80107400
8010238c:	e8 ff df ff ff       	call   80100390 <panic>
    panic("iderw: buf not locked");
80102391:	83 ec 0c             	sub    $0xc,%esp
80102394:	68 ea 73 10 80       	push   $0x801073ea
80102399:	e8 f2 df ff ff       	call   80100390 <panic>
8010239e:	66 90                	xchg   %ax,%ax

801023a0 <ioapicinit>:
  ioapic->data = data;
}

void
ioapicinit(void)
{
801023a0:	f3 0f 1e fb          	endbr32 
801023a4:	55                   	push   %ebp
  int i, id, maxintr;

  ioapic = (volatile struct ioapic*)IOAPIC;
801023a5:	c7 05 34 26 11 80 00 	movl   $0xfec00000,0x80112634
801023ac:	00 c0 fe 
{
801023af:	89 e5                	mov    %esp,%ebp
801023b1:	56                   	push   %esi
801023b2:	53                   	push   %ebx
  ioapic->reg = reg;
801023b3:	c7 05 00 00 c0 fe 01 	movl   $0x1,0xfec00000
801023ba:	00 00 00 
  return ioapic->data;
801023bd:	8b 15 34 26 11 80    	mov    0x80112634,%edx
801023c3:	8b 72 10             	mov    0x10(%edx),%esi
  ioapic->reg = reg;
801023c6:	c7 02 00 00 00 00    	movl   $0x0,(%edx)
  return ioapic->data;
801023cc:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
  id = ioapicread(REG_ID) >> 24;
  if(id != ioapicid)
801023d2:	0f b6 15 60 27 11 80 	movzbl 0x80112760,%edx
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
801023d9:	c1 ee 10             	shr    $0x10,%esi
801023dc:	89 f0                	mov    %esi,%eax
801023de:	0f b6 f0             	movzbl %al,%esi
  return ioapic->data;
801023e1:	8b 41 10             	mov    0x10(%ecx),%eax
  id = ioapicread(REG_ID) >> 24;
801023e4:	c1 e8 18             	shr    $0x18,%eax
  if(id != ioapicid)
801023e7:	39 c2                	cmp    %eax,%edx
801023e9:	74 16                	je     80102401 <ioapicinit+0x61>
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");
801023eb:	83 ec 0c             	sub    $0xc,%esp
801023ee:	68 34 74 10 80       	push   $0x80107434
801023f3:	e8 b8 e2 ff ff       	call   801006b0 <cprintf>
801023f8:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
801023fe:	83 c4 10             	add    $0x10,%esp
80102401:	83 c6 21             	add    $0x21,%esi
{
80102404:	ba 10 00 00 00       	mov    $0x10,%edx
80102409:	b8 20 00 00 00       	mov    $0x20,%eax
8010240e:	66 90                	xchg   %ax,%ax
  ioapic->reg = reg;
80102410:	89 11                	mov    %edx,(%ecx)

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
80102412:	89 c3                	mov    %eax,%ebx
  ioapic->data = data;
80102414:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
8010241a:	83 c0 01             	add    $0x1,%eax
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
8010241d:	81 cb 00 00 01 00    	or     $0x10000,%ebx
  ioapic->data = data;
80102423:	89 59 10             	mov    %ebx,0x10(%ecx)
  ioapic->reg = reg;
80102426:	8d 5a 01             	lea    0x1(%edx),%ebx
80102429:	83 c2 02             	add    $0x2,%edx
8010242c:	89 19                	mov    %ebx,(%ecx)
  ioapic->data = data;
8010242e:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
80102434:	c7 41 10 00 00 00 00 	movl   $0x0,0x10(%ecx)
  for(i = 0; i <= maxintr; i++){
8010243b:	39 f0                	cmp    %esi,%eax
8010243d:	75 d1                	jne    80102410 <ioapicinit+0x70>
    ioapicwrite(REG_TABLE+2*i+1, 0);
  }
}
8010243f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102442:	5b                   	pop    %ebx
80102443:	5e                   	pop    %esi
80102444:	5d                   	pop    %ebp
80102445:	c3                   	ret    
80102446:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010244d:	8d 76 00             	lea    0x0(%esi),%esi

80102450 <ioapicenable>:

void
ioapicenable(int irq, int cpunum)
{
80102450:	f3 0f 1e fb          	endbr32 
80102454:	55                   	push   %ebp
  ioapic->reg = reg;
80102455:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
{
8010245b:	89 e5                	mov    %esp,%ebp
8010245d:	8b 45 08             	mov    0x8(%ebp),%eax
  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
80102460:	8d 50 20             	lea    0x20(%eax),%edx
80102463:	8d 44 00 10          	lea    0x10(%eax,%eax,1),%eax
  ioapic->reg = reg;
80102467:	89 01                	mov    %eax,(%ecx)
  ioapic->data = data;
80102469:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
8010246f:	83 c0 01             	add    $0x1,%eax
  ioapic->data = data;
80102472:	89 51 10             	mov    %edx,0x10(%ecx)
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
80102475:	8b 55 0c             	mov    0xc(%ebp),%edx
  ioapic->reg = reg;
80102478:	89 01                	mov    %eax,(%ecx)
  ioapic->data = data;
8010247a:	a1 34 26 11 80       	mov    0x80112634,%eax
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
8010247f:	c1 e2 18             	shl    $0x18,%edx
  ioapic->data = data;
80102482:	89 50 10             	mov    %edx,0x10(%eax)
}
80102485:	5d                   	pop    %ebp
80102486:	c3                   	ret    
80102487:	66 90                	xchg   %ax,%ax
80102489:	66 90                	xchg   %ax,%ax
8010248b:	66 90                	xchg   %ax,%ax
8010248d:	66 90                	xchg   %ax,%ax
8010248f:	90                   	nop

80102490 <kfree>:
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(char *v)
{
80102490:	f3 0f 1e fb          	endbr32 
80102494:	55                   	push   %ebp
80102495:	89 e5                	mov    %esp,%ebp
80102497:	53                   	push   %ebx
80102498:	83 ec 04             	sub    $0x4,%esp
8010249b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct run *r;

  if((uint)v % PGSIZE || v < end || V2P(v) >= PHYSTOP)
8010249e:	f7 c3 ff 0f 00 00    	test   $0xfff,%ebx
801024a4:	75 7a                	jne    80102520 <kfree+0x90>
801024a6:	81 fb a8 54 11 80    	cmp    $0x801154a8,%ebx
801024ac:	72 72                	jb     80102520 <kfree+0x90>
801024ae:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
801024b4:	3d ff ff ff 0d       	cmp    $0xdffffff,%eax
801024b9:	77 65                	ja     80102520 <kfree+0x90>
    panic("kfree");

  // Fill with junk to catch dangling refs.
  memset(v, 1, PGSIZE);
801024bb:	83 ec 04             	sub    $0x4,%esp
801024be:	68 00 10 00 00       	push   $0x1000
801024c3:	6a 01                	push   $0x1
801024c5:	53                   	push   %ebx
801024c6:	e8 b5 22 00 00       	call   80104780 <memset>

  if(kmem.use_lock)
801024cb:	8b 15 74 26 11 80    	mov    0x80112674,%edx
801024d1:	83 c4 10             	add    $0x10,%esp
801024d4:	85 d2                	test   %edx,%edx
801024d6:	75 20                	jne    801024f8 <kfree+0x68>
    acquire(&kmem.lock);
  r = (struct run*)v;
  r->next = kmem.freelist;
801024d8:	a1 78 26 11 80       	mov    0x80112678,%eax
801024dd:	89 03                	mov    %eax,(%ebx)
  kmem.freelist = r;
  if(kmem.use_lock)
801024df:	a1 74 26 11 80       	mov    0x80112674,%eax
  kmem.freelist = r;
801024e4:	89 1d 78 26 11 80    	mov    %ebx,0x80112678
  if(kmem.use_lock)
801024ea:	85 c0                	test   %eax,%eax
801024ec:	75 22                	jne    80102510 <kfree+0x80>
    release(&kmem.lock);
}
801024ee:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801024f1:	c9                   	leave  
801024f2:	c3                   	ret    
801024f3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801024f7:	90                   	nop
    acquire(&kmem.lock);
801024f8:	83 ec 0c             	sub    $0xc,%esp
801024fb:	68 40 26 11 80       	push   $0x80112640
80102500:	e8 6b 21 00 00       	call   80104670 <acquire>
80102505:	83 c4 10             	add    $0x10,%esp
80102508:	eb ce                	jmp    801024d8 <kfree+0x48>
8010250a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    release(&kmem.lock);
80102510:	c7 45 08 40 26 11 80 	movl   $0x80112640,0x8(%ebp)
}
80102517:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010251a:	c9                   	leave  
    release(&kmem.lock);
8010251b:	e9 10 22 00 00       	jmp    80104730 <release>
    panic("kfree");
80102520:	83 ec 0c             	sub    $0xc,%esp
80102523:	68 66 74 10 80       	push   $0x80107466
80102528:	e8 63 de ff ff       	call   80100390 <panic>
8010252d:	8d 76 00             	lea    0x0(%esi),%esi

80102530 <freerange>:
{
80102530:	f3 0f 1e fb          	endbr32 
80102534:	55                   	push   %ebp
80102535:	89 e5                	mov    %esp,%ebp
80102537:	56                   	push   %esi
  p = (char*)PGROUNDUP((uint)vstart);
80102538:	8b 45 08             	mov    0x8(%ebp),%eax
{
8010253b:	8b 75 0c             	mov    0xc(%ebp),%esi
8010253e:	53                   	push   %ebx
  p = (char*)PGROUNDUP((uint)vstart);
8010253f:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80102545:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
8010254b:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80102551:	39 de                	cmp    %ebx,%esi
80102553:	72 1f                	jb     80102574 <freerange+0x44>
80102555:	8d 76 00             	lea    0x0(%esi),%esi
    kfree(p);
80102558:	83 ec 0c             	sub    $0xc,%esp
8010255b:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102561:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
80102567:	50                   	push   %eax
80102568:	e8 23 ff ff ff       	call   80102490 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
8010256d:	83 c4 10             	add    $0x10,%esp
80102570:	39 f3                	cmp    %esi,%ebx
80102572:	76 e4                	jbe    80102558 <freerange+0x28>
}
80102574:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102577:	5b                   	pop    %ebx
80102578:	5e                   	pop    %esi
80102579:	5d                   	pop    %ebp
8010257a:	c3                   	ret    
8010257b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010257f:	90                   	nop

80102580 <kinit1>:
{
80102580:	f3 0f 1e fb          	endbr32 
80102584:	55                   	push   %ebp
80102585:	89 e5                	mov    %esp,%ebp
80102587:	56                   	push   %esi
80102588:	53                   	push   %ebx
80102589:	8b 75 0c             	mov    0xc(%ebp),%esi
  initlock(&kmem.lock, "kmem");
8010258c:	83 ec 08             	sub    $0x8,%esp
8010258f:	68 6c 74 10 80       	push   $0x8010746c
80102594:	68 40 26 11 80       	push   $0x80112640
80102599:	e8 52 1f 00 00       	call   801044f0 <initlock>
  p = (char*)PGROUNDUP((uint)vstart);
8010259e:	8b 45 08             	mov    0x8(%ebp),%eax
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801025a1:	83 c4 10             	add    $0x10,%esp
  kmem.use_lock = 0;
801025a4:	c7 05 74 26 11 80 00 	movl   $0x0,0x80112674
801025ab:	00 00 00 
  p = (char*)PGROUNDUP((uint)vstart);
801025ae:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
801025b4:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801025ba:	81 c3 00 10 00 00    	add    $0x1000,%ebx
801025c0:	39 de                	cmp    %ebx,%esi
801025c2:	72 20                	jb     801025e4 <kinit1+0x64>
801025c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    kfree(p);
801025c8:	83 ec 0c             	sub    $0xc,%esp
801025cb:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801025d1:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
801025d7:	50                   	push   %eax
801025d8:	e8 b3 fe ff ff       	call   80102490 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801025dd:	83 c4 10             	add    $0x10,%esp
801025e0:	39 de                	cmp    %ebx,%esi
801025e2:	73 e4                	jae    801025c8 <kinit1+0x48>
}
801025e4:	8d 65 f8             	lea    -0x8(%ebp),%esp
801025e7:	5b                   	pop    %ebx
801025e8:	5e                   	pop    %esi
801025e9:	5d                   	pop    %ebp
801025ea:	c3                   	ret    
801025eb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801025ef:	90                   	nop

801025f0 <kinit2>:
{
801025f0:	f3 0f 1e fb          	endbr32 
801025f4:	55                   	push   %ebp
801025f5:	89 e5                	mov    %esp,%ebp
801025f7:	56                   	push   %esi
  p = (char*)PGROUNDUP((uint)vstart);
801025f8:	8b 45 08             	mov    0x8(%ebp),%eax
{
801025fb:	8b 75 0c             	mov    0xc(%ebp),%esi
801025fe:	53                   	push   %ebx
  p = (char*)PGROUNDUP((uint)vstart);
801025ff:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80102605:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
8010260b:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80102611:	39 de                	cmp    %ebx,%esi
80102613:	72 1f                	jb     80102634 <kinit2+0x44>
80102615:	8d 76 00             	lea    0x0(%esi),%esi
    kfree(p);
80102618:	83 ec 0c             	sub    $0xc,%esp
8010261b:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102621:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
80102627:	50                   	push   %eax
80102628:	e8 63 fe ff ff       	call   80102490 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
8010262d:	83 c4 10             	add    $0x10,%esp
80102630:	39 de                	cmp    %ebx,%esi
80102632:	73 e4                	jae    80102618 <kinit2+0x28>
  kmem.use_lock = 1;
80102634:	c7 05 74 26 11 80 01 	movl   $0x1,0x80112674
8010263b:	00 00 00 
}
8010263e:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102641:	5b                   	pop    %ebx
80102642:	5e                   	pop    %esi
80102643:	5d                   	pop    %ebp
80102644:	c3                   	ret    
80102645:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010264c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80102650 <kalloc>:
// Allocate one 4096-byte page of physical memory.
// Returns a pointer that the kernel can use.
// Returns 0 if the memory cannot be allocated.
char*
kalloc(void)
{
80102650:	f3 0f 1e fb          	endbr32 
  struct run *r;

  if(kmem.use_lock)
80102654:	a1 74 26 11 80       	mov    0x80112674,%eax
80102659:	85 c0                	test   %eax,%eax
8010265b:	75 1b                	jne    80102678 <kalloc+0x28>
    acquire(&kmem.lock);
  r = kmem.freelist;
8010265d:	a1 78 26 11 80       	mov    0x80112678,%eax
  if(r)
80102662:	85 c0                	test   %eax,%eax
80102664:	74 0a                	je     80102670 <kalloc+0x20>
    kmem.freelist = r->next;
80102666:	8b 10                	mov    (%eax),%edx
80102668:	89 15 78 26 11 80    	mov    %edx,0x80112678
  if(kmem.use_lock)
8010266e:	c3                   	ret    
8010266f:	90                   	nop
    release(&kmem.lock);
  return (char*)r;
}
80102670:	c3                   	ret    
80102671:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
{
80102678:	55                   	push   %ebp
80102679:	89 e5                	mov    %esp,%ebp
8010267b:	83 ec 24             	sub    $0x24,%esp
    acquire(&kmem.lock);
8010267e:	68 40 26 11 80       	push   $0x80112640
80102683:	e8 e8 1f 00 00       	call   80104670 <acquire>
  r = kmem.freelist;
80102688:	a1 78 26 11 80       	mov    0x80112678,%eax
  if(r)
8010268d:	8b 15 74 26 11 80    	mov    0x80112674,%edx
80102693:	83 c4 10             	add    $0x10,%esp
80102696:	85 c0                	test   %eax,%eax
80102698:	74 08                	je     801026a2 <kalloc+0x52>
    kmem.freelist = r->next;
8010269a:	8b 08                	mov    (%eax),%ecx
8010269c:	89 0d 78 26 11 80    	mov    %ecx,0x80112678
  if(kmem.use_lock)
801026a2:	85 d2                	test   %edx,%edx
801026a4:	74 16                	je     801026bc <kalloc+0x6c>
    release(&kmem.lock);
801026a6:	83 ec 0c             	sub    $0xc,%esp
801026a9:	89 45 f4             	mov    %eax,-0xc(%ebp)
801026ac:	68 40 26 11 80       	push   $0x80112640
801026b1:	e8 7a 20 00 00       	call   80104730 <release>
  return (char*)r;
801026b6:	8b 45 f4             	mov    -0xc(%ebp),%eax
    release(&kmem.lock);
801026b9:	83 c4 10             	add    $0x10,%esp
}
801026bc:	c9                   	leave  
801026bd:	c3                   	ret    
801026be:	66 90                	xchg   %ax,%ax

801026c0 <kbdgetc>:
#include "defs.h"
#include "kbd.h"

int
kbdgetc(void)
{
801026c0:	f3 0f 1e fb          	endbr32 
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801026c4:	ba 64 00 00 00       	mov    $0x64,%edx
801026c9:	ec                   	in     (%dx),%al
    normalmap, shiftmap, ctlmap, ctlmap
  };
  uint st, data, c;

  st = inb(KBSTATP);
  if((st & KBS_DIB) == 0)
801026ca:	a8 01                	test   $0x1,%al
801026cc:	0f 84 be 00 00 00    	je     80102790 <kbdgetc+0xd0>
{
801026d2:	55                   	push   %ebp
801026d3:	ba 60 00 00 00       	mov    $0x60,%edx
801026d8:	89 e5                	mov    %esp,%ebp
801026da:	53                   	push   %ebx
801026db:	ec                   	in     (%dx),%al
  return data;
801026dc:	8b 1d b4 a5 10 80    	mov    0x8010a5b4,%ebx
    return -1;
  data = inb(KBDATAP);
801026e2:	0f b6 d0             	movzbl %al,%edx

  if(data == 0xE0){
801026e5:	3c e0                	cmp    $0xe0,%al
801026e7:	74 57                	je     80102740 <kbdgetc+0x80>
    shift |= E0ESC;
    return 0;
  } else if(data & 0x80){
801026e9:	89 d9                	mov    %ebx,%ecx
801026eb:	83 e1 40             	and    $0x40,%ecx
801026ee:	84 c0                	test   %al,%al
801026f0:	78 5e                	js     80102750 <kbdgetc+0x90>
    // Key released
    data = (shift & E0ESC ? data : data & 0x7F);
    shift &= ~(shiftcode[data] | E0ESC);
    return 0;
  } else if(shift & E0ESC){
801026f2:	85 c9                	test   %ecx,%ecx
801026f4:	74 09                	je     801026ff <kbdgetc+0x3f>
    // Last character was an E0 escape; or with 0x80
    data |= 0x80;
801026f6:	83 c8 80             	or     $0xffffff80,%eax
    shift &= ~E0ESC;
801026f9:	83 e3 bf             	and    $0xffffffbf,%ebx
    data |= 0x80;
801026fc:	0f b6 d0             	movzbl %al,%edx
  }

  shift |= shiftcode[data];
801026ff:	0f b6 8a a0 75 10 80 	movzbl -0x7fef8a60(%edx),%ecx
  shift ^= togglecode[data];
80102706:	0f b6 82 a0 74 10 80 	movzbl -0x7fef8b60(%edx),%eax
  shift |= shiftcode[data];
8010270d:	09 d9                	or     %ebx,%ecx
  shift ^= togglecode[data];
8010270f:	31 c1                	xor    %eax,%ecx
  c = charcode[shift & (CTL | SHIFT)][data];
80102711:	89 c8                	mov    %ecx,%eax
  shift ^= togglecode[data];
80102713:	89 0d b4 a5 10 80    	mov    %ecx,0x8010a5b4
  c = charcode[shift & (CTL | SHIFT)][data];
80102719:	83 e0 03             	and    $0x3,%eax
  if(shift & CAPSLOCK){
8010271c:	83 e1 08             	and    $0x8,%ecx
  c = charcode[shift & (CTL | SHIFT)][data];
8010271f:	8b 04 85 80 74 10 80 	mov    -0x7fef8b80(,%eax,4),%eax
80102726:	0f b6 04 10          	movzbl (%eax,%edx,1),%eax
  if(shift & CAPSLOCK){
8010272a:	74 0b                	je     80102737 <kbdgetc+0x77>
    if('a' <= c && c <= 'z')
8010272c:	8d 50 9f             	lea    -0x61(%eax),%edx
8010272f:	83 fa 19             	cmp    $0x19,%edx
80102732:	77 44                	ja     80102778 <kbdgetc+0xb8>
      c += 'A' - 'a';
80102734:	83 e8 20             	sub    $0x20,%eax
    else if('A' <= c && c <= 'Z')
      c += 'a' - 'A';
  }
  return c;
}
80102737:	5b                   	pop    %ebx
80102738:	5d                   	pop    %ebp
80102739:	c3                   	ret    
8010273a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    shift |= E0ESC;
80102740:	83 cb 40             	or     $0x40,%ebx
    return 0;
80102743:	31 c0                	xor    %eax,%eax
    shift |= E0ESC;
80102745:	89 1d b4 a5 10 80    	mov    %ebx,0x8010a5b4
}
8010274b:	5b                   	pop    %ebx
8010274c:	5d                   	pop    %ebp
8010274d:	c3                   	ret    
8010274e:	66 90                	xchg   %ax,%ax
    data = (shift & E0ESC ? data : data & 0x7F);
80102750:	83 e0 7f             	and    $0x7f,%eax
80102753:	85 c9                	test   %ecx,%ecx
80102755:	0f 44 d0             	cmove  %eax,%edx
    return 0;
80102758:	31 c0                	xor    %eax,%eax
    shift &= ~(shiftcode[data] | E0ESC);
8010275a:	0f b6 8a a0 75 10 80 	movzbl -0x7fef8a60(%edx),%ecx
80102761:	83 c9 40             	or     $0x40,%ecx
80102764:	0f b6 c9             	movzbl %cl,%ecx
80102767:	f7 d1                	not    %ecx
80102769:	21 d9                	and    %ebx,%ecx
}
8010276b:	5b                   	pop    %ebx
8010276c:	5d                   	pop    %ebp
    shift &= ~(shiftcode[data] | E0ESC);
8010276d:	89 0d b4 a5 10 80    	mov    %ecx,0x8010a5b4
}
80102773:	c3                   	ret    
80102774:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    else if('A' <= c && c <= 'Z')
80102778:	8d 48 bf             	lea    -0x41(%eax),%ecx
      c += 'a' - 'A';
8010277b:	8d 50 20             	lea    0x20(%eax),%edx
}
8010277e:	5b                   	pop    %ebx
8010277f:	5d                   	pop    %ebp
      c += 'a' - 'A';
80102780:	83 f9 1a             	cmp    $0x1a,%ecx
80102783:	0f 42 c2             	cmovb  %edx,%eax
}
80102786:	c3                   	ret    
80102787:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010278e:	66 90                	xchg   %ax,%ax
    return -1;
80102790:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80102795:	c3                   	ret    
80102796:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010279d:	8d 76 00             	lea    0x0(%esi),%esi

801027a0 <kbdintr>:

void
kbdintr(void)
{
801027a0:	f3 0f 1e fb          	endbr32 
801027a4:	55                   	push   %ebp
801027a5:	89 e5                	mov    %esp,%ebp
801027a7:	83 ec 14             	sub    $0x14,%esp
  consoleintr(kbdgetc);
801027aa:	68 c0 26 10 80       	push   $0x801026c0
801027af:	e8 ac e0 ff ff       	call   80100860 <consoleintr>
}
801027b4:	83 c4 10             	add    $0x10,%esp
801027b7:	c9                   	leave  
801027b8:	c3                   	ret    
801027b9:	66 90                	xchg   %ax,%ax
801027bb:	66 90                	xchg   %ax,%ax
801027bd:	66 90                	xchg   %ax,%ax
801027bf:	90                   	nop

801027c0 <lapicinit>:
  lapic[ID];  // wait for write to finish, by reading
}

void
lapicinit(void)
{
801027c0:	f3 0f 1e fb          	endbr32 
  if(!lapic)
801027c4:	a1 7c 26 11 80       	mov    0x8011267c,%eax
801027c9:	85 c0                	test   %eax,%eax
801027cb:	0f 84 c7 00 00 00    	je     80102898 <lapicinit+0xd8>
  lapic[index] = value;
801027d1:	c7 80 f0 00 00 00 3f 	movl   $0x13f,0xf0(%eax)
801027d8:	01 00 00 
  lapic[ID];  // wait for write to finish, by reading
801027db:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
801027de:	c7 80 e0 03 00 00 0b 	movl   $0xb,0x3e0(%eax)
801027e5:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
801027e8:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
801027eb:	c7 80 20 03 00 00 20 	movl   $0x20020,0x320(%eax)
801027f2:	00 02 00 
  lapic[ID];  // wait for write to finish, by reading
801027f5:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
801027f8:	c7 80 80 03 00 00 80 	movl   $0x989680,0x380(%eax)
801027ff:	96 98 00 
  lapic[ID];  // wait for write to finish, by reading
80102802:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102805:	c7 80 50 03 00 00 00 	movl   $0x10000,0x350(%eax)
8010280c:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
8010280f:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102812:	c7 80 60 03 00 00 00 	movl   $0x10000,0x360(%eax)
80102819:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
8010281c:	8b 50 20             	mov    0x20(%eax),%edx
  lapicw(LINT0, MASKED);
  lapicw(LINT1, MASKED);

  // Disable performance counter overflow interrupts
  // on machines that provide that interrupt entry.
  if(((lapic[VER]>>16) & 0xFF) >= 4)
8010281f:	8b 50 30             	mov    0x30(%eax),%edx
80102822:	c1 ea 10             	shr    $0x10,%edx
80102825:	81 e2 fc 00 00 00    	and    $0xfc,%edx
8010282b:	75 73                	jne    801028a0 <lapicinit+0xe0>
  lapic[index] = value;
8010282d:	c7 80 70 03 00 00 33 	movl   $0x33,0x370(%eax)
80102834:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102837:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
8010283a:	c7 80 80 02 00 00 00 	movl   $0x0,0x280(%eax)
80102841:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102844:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102847:	c7 80 80 02 00 00 00 	movl   $0x0,0x280(%eax)
8010284e:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102851:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102854:	c7 80 b0 00 00 00 00 	movl   $0x0,0xb0(%eax)
8010285b:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
8010285e:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102861:	c7 80 10 03 00 00 00 	movl   $0x0,0x310(%eax)
80102868:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
8010286b:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
8010286e:	c7 80 00 03 00 00 00 	movl   $0x88500,0x300(%eax)
80102875:	85 08 00 
  lapic[ID];  // wait for write to finish, by reading
80102878:	8b 50 20             	mov    0x20(%eax),%edx
8010287b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010287f:	90                   	nop
  lapicw(EOI, 0);

  // Send an Init Level De-Assert to synchronise arbitration ID's.
  lapicw(ICRHI, 0);
  lapicw(ICRLO, BCAST | INIT | LEVEL);
  while(lapic[ICRLO] & DELIVS)
80102880:	8b 90 00 03 00 00    	mov    0x300(%eax),%edx
80102886:	80 e6 10             	and    $0x10,%dh
80102889:	75 f5                	jne    80102880 <lapicinit+0xc0>
  lapic[index] = value;
8010288b:	c7 80 80 00 00 00 00 	movl   $0x0,0x80(%eax)
80102892:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102895:	8b 40 20             	mov    0x20(%eax),%eax
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
}
80102898:	c3                   	ret    
80102899:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  lapic[index] = value;
801028a0:	c7 80 40 03 00 00 00 	movl   $0x10000,0x340(%eax)
801028a7:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
801028aa:	8b 50 20             	mov    0x20(%eax),%edx
}
801028ad:	e9 7b ff ff ff       	jmp    8010282d <lapicinit+0x6d>
801028b2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801028b9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801028c0 <lapicid>:

int
lapicid(void)
{
801028c0:	f3 0f 1e fb          	endbr32 
  if (!lapic)
801028c4:	a1 7c 26 11 80       	mov    0x8011267c,%eax
801028c9:	85 c0                	test   %eax,%eax
801028cb:	74 0b                	je     801028d8 <lapicid+0x18>
    return 0;
  return lapic[ID] >> 24;
801028cd:	8b 40 20             	mov    0x20(%eax),%eax
801028d0:	c1 e8 18             	shr    $0x18,%eax
801028d3:	c3                   	ret    
801028d4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return 0;
801028d8:	31 c0                	xor    %eax,%eax
}
801028da:	c3                   	ret    
801028db:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801028df:	90                   	nop

801028e0 <lapiceoi>:

// Acknowledge interrupt.
void
lapiceoi(void)
{
801028e0:	f3 0f 1e fb          	endbr32 
  if(lapic)
801028e4:	a1 7c 26 11 80       	mov    0x8011267c,%eax
801028e9:	85 c0                	test   %eax,%eax
801028eb:	74 0d                	je     801028fa <lapiceoi+0x1a>
  lapic[index] = value;
801028ed:	c7 80 b0 00 00 00 00 	movl   $0x0,0xb0(%eax)
801028f4:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
801028f7:	8b 40 20             	mov    0x20(%eax),%eax
    lapicw(EOI, 0);
}
801028fa:	c3                   	ret    
801028fb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801028ff:	90                   	nop

80102900 <microdelay>:

// Spin for a given number of microseconds.
// On real hardware would want to tune this dynamically.
void
microdelay(int us)
{
80102900:	f3 0f 1e fb          	endbr32 
}
80102904:	c3                   	ret    
80102905:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010290c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80102910 <lapicstartap>:

// Start additional processor running entry code at addr.
// See Appendix B of MultiProcessor Specification.
void
lapicstartap(uchar apicid, uint addr)
{
80102910:	f3 0f 1e fb          	endbr32 
80102914:	55                   	push   %ebp
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102915:	b8 0f 00 00 00       	mov    $0xf,%eax
8010291a:	ba 70 00 00 00       	mov    $0x70,%edx
8010291f:	89 e5                	mov    %esp,%ebp
80102921:	53                   	push   %ebx
80102922:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80102925:	8b 5d 08             	mov    0x8(%ebp),%ebx
80102928:	ee                   	out    %al,(%dx)
80102929:	b8 0a 00 00 00       	mov    $0xa,%eax
8010292e:	ba 71 00 00 00       	mov    $0x71,%edx
80102933:	ee                   	out    %al,(%dx)
  // and the warm reset vector (DWORD based at 40:67) to point at
  // the AP startup code prior to the [universal startup algorithm]."
  outb(CMOS_PORT, 0xF);  // offset 0xF is shutdown code
  outb(CMOS_PORT+1, 0x0A);
  wrv = (ushort*)P2V((0x40<<4 | 0x67));  // Warm reset vector
  wrv[0] = 0;
80102934:	31 c0                	xor    %eax,%eax
  wrv[1] = addr >> 4;

  // "Universal startup algorithm."
  // Send INIT (level-triggered) interrupt to reset other CPU.
  lapicw(ICRHI, apicid<<24);
80102936:	c1 e3 18             	shl    $0x18,%ebx
  wrv[0] = 0;
80102939:	66 a3 67 04 00 80    	mov    %ax,0x80000467
  wrv[1] = addr >> 4;
8010293f:	89 c8                	mov    %ecx,%eax
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
    lapicw(ICRHI, apicid<<24);
    lapicw(ICRLO, STARTUP | (addr>>12));
80102941:	c1 e9 0c             	shr    $0xc,%ecx
  lapicw(ICRHI, apicid<<24);
80102944:	89 da                	mov    %ebx,%edx
  wrv[1] = addr >> 4;
80102946:	c1 e8 04             	shr    $0x4,%eax
    lapicw(ICRLO, STARTUP | (addr>>12));
80102949:	80 cd 06             	or     $0x6,%ch
  wrv[1] = addr >> 4;
8010294c:	66 a3 69 04 00 80    	mov    %ax,0x80000469
  lapic[index] = value;
80102952:	a1 7c 26 11 80       	mov    0x8011267c,%eax
80102957:	89 98 10 03 00 00    	mov    %ebx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
8010295d:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102960:	c7 80 00 03 00 00 00 	movl   $0xc500,0x300(%eax)
80102967:	c5 00 00 
  lapic[ID];  // wait for write to finish, by reading
8010296a:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
8010296d:	c7 80 00 03 00 00 00 	movl   $0x8500,0x300(%eax)
80102974:	85 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102977:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
8010297a:	89 90 10 03 00 00    	mov    %edx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102980:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102983:	89 88 00 03 00 00    	mov    %ecx,0x300(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102989:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
8010298c:	89 90 10 03 00 00    	mov    %edx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102992:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102995:	89 88 00 03 00 00    	mov    %ecx,0x300(%eax)
    microdelay(200);
  }
}
8010299b:	5b                   	pop    %ebx
  lapic[ID];  // wait for write to finish, by reading
8010299c:	8b 40 20             	mov    0x20(%eax),%eax
}
8010299f:	5d                   	pop    %ebp
801029a0:	c3                   	ret    
801029a1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801029a8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801029af:	90                   	nop

801029b0 <cmostime>:
}

// qemu seems to use 24-hour GWT and the values are BCD encoded
void
cmostime(struct rtcdate *r)
{
801029b0:	f3 0f 1e fb          	endbr32 
801029b4:	55                   	push   %ebp
801029b5:	b8 0b 00 00 00       	mov    $0xb,%eax
801029ba:	ba 70 00 00 00       	mov    $0x70,%edx
801029bf:	89 e5                	mov    %esp,%ebp
801029c1:	57                   	push   %edi
801029c2:	56                   	push   %esi
801029c3:	53                   	push   %ebx
801029c4:	83 ec 4c             	sub    $0x4c,%esp
801029c7:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801029c8:	ba 71 00 00 00       	mov    $0x71,%edx
801029cd:	ec                   	in     (%dx),%al
  struct rtcdate t1, t2;
  int sb, bcd;

  sb = cmos_read(CMOS_STATB);

  bcd = (sb & (1 << 2)) == 0;
801029ce:	83 e0 04             	and    $0x4,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801029d1:	bb 70 00 00 00       	mov    $0x70,%ebx
801029d6:	88 45 b3             	mov    %al,-0x4d(%ebp)
801029d9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801029e0:	31 c0                	xor    %eax,%eax
801029e2:	89 da                	mov    %ebx,%edx
801029e4:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801029e5:	b9 71 00 00 00       	mov    $0x71,%ecx
801029ea:	89 ca                	mov    %ecx,%edx
801029ec:	ec                   	in     (%dx),%al
801029ed:	88 45 b7             	mov    %al,-0x49(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801029f0:	89 da                	mov    %ebx,%edx
801029f2:	b8 02 00 00 00       	mov    $0x2,%eax
801029f7:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801029f8:	89 ca                	mov    %ecx,%edx
801029fa:	ec                   	in     (%dx),%al
801029fb:	88 45 b6             	mov    %al,-0x4a(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801029fe:	89 da                	mov    %ebx,%edx
80102a00:	b8 04 00 00 00       	mov    $0x4,%eax
80102a05:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102a06:	89 ca                	mov    %ecx,%edx
80102a08:	ec                   	in     (%dx),%al
80102a09:	88 45 b5             	mov    %al,-0x4b(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102a0c:	89 da                	mov    %ebx,%edx
80102a0e:	b8 07 00 00 00       	mov    $0x7,%eax
80102a13:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102a14:	89 ca                	mov    %ecx,%edx
80102a16:	ec                   	in     (%dx),%al
80102a17:	88 45 b4             	mov    %al,-0x4c(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102a1a:	89 da                	mov    %ebx,%edx
80102a1c:	b8 08 00 00 00       	mov    $0x8,%eax
80102a21:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102a22:	89 ca                	mov    %ecx,%edx
80102a24:	ec                   	in     (%dx),%al
80102a25:	89 c7                	mov    %eax,%edi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102a27:	89 da                	mov    %ebx,%edx
80102a29:	b8 09 00 00 00       	mov    $0x9,%eax
80102a2e:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102a2f:	89 ca                	mov    %ecx,%edx
80102a31:	ec                   	in     (%dx),%al
80102a32:	89 c6                	mov    %eax,%esi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102a34:	89 da                	mov    %ebx,%edx
80102a36:	b8 0a 00 00 00       	mov    $0xa,%eax
80102a3b:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102a3c:	89 ca                	mov    %ecx,%edx
80102a3e:	ec                   	in     (%dx),%al

  // make sure CMOS doesn't modify time while we read it
  for(;;) {
    fill_rtcdate(&t1);
    if(cmos_read(CMOS_STATA) & CMOS_UIP)
80102a3f:	84 c0                	test   %al,%al
80102a41:	78 9d                	js     801029e0 <cmostime+0x30>
  return inb(CMOS_RETURN);
80102a43:	0f b6 45 b7          	movzbl -0x49(%ebp),%eax
80102a47:	89 fa                	mov    %edi,%edx
80102a49:	0f b6 fa             	movzbl %dl,%edi
80102a4c:	89 f2                	mov    %esi,%edx
80102a4e:	89 45 b8             	mov    %eax,-0x48(%ebp)
80102a51:	0f b6 45 b6          	movzbl -0x4a(%ebp),%eax
80102a55:	0f b6 f2             	movzbl %dl,%esi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102a58:	89 da                	mov    %ebx,%edx
80102a5a:	89 7d c8             	mov    %edi,-0x38(%ebp)
80102a5d:	89 45 bc             	mov    %eax,-0x44(%ebp)
80102a60:	0f b6 45 b5          	movzbl -0x4b(%ebp),%eax
80102a64:	89 75 cc             	mov    %esi,-0x34(%ebp)
80102a67:	89 45 c0             	mov    %eax,-0x40(%ebp)
80102a6a:	0f b6 45 b4          	movzbl -0x4c(%ebp),%eax
80102a6e:	89 45 c4             	mov    %eax,-0x3c(%ebp)
80102a71:	31 c0                	xor    %eax,%eax
80102a73:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102a74:	89 ca                	mov    %ecx,%edx
80102a76:	ec                   	in     (%dx),%al
80102a77:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102a7a:	89 da                	mov    %ebx,%edx
80102a7c:	89 45 d0             	mov    %eax,-0x30(%ebp)
80102a7f:	b8 02 00 00 00       	mov    $0x2,%eax
80102a84:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102a85:	89 ca                	mov    %ecx,%edx
80102a87:	ec                   	in     (%dx),%al
80102a88:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102a8b:	89 da                	mov    %ebx,%edx
80102a8d:	89 45 d4             	mov    %eax,-0x2c(%ebp)
80102a90:	b8 04 00 00 00       	mov    $0x4,%eax
80102a95:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102a96:	89 ca                	mov    %ecx,%edx
80102a98:	ec                   	in     (%dx),%al
80102a99:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102a9c:	89 da                	mov    %ebx,%edx
80102a9e:	89 45 d8             	mov    %eax,-0x28(%ebp)
80102aa1:	b8 07 00 00 00       	mov    $0x7,%eax
80102aa6:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102aa7:	89 ca                	mov    %ecx,%edx
80102aa9:	ec                   	in     (%dx),%al
80102aaa:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102aad:	89 da                	mov    %ebx,%edx
80102aaf:	89 45 dc             	mov    %eax,-0x24(%ebp)
80102ab2:	b8 08 00 00 00       	mov    $0x8,%eax
80102ab7:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102ab8:	89 ca                	mov    %ecx,%edx
80102aba:	ec                   	in     (%dx),%al
80102abb:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102abe:	89 da                	mov    %ebx,%edx
80102ac0:	89 45 e0             	mov    %eax,-0x20(%ebp)
80102ac3:	b8 09 00 00 00       	mov    $0x9,%eax
80102ac8:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102ac9:	89 ca                	mov    %ecx,%edx
80102acb:	ec                   	in     (%dx),%al
80102acc:	0f b6 c0             	movzbl %al,%eax
        continue;
    fill_rtcdate(&t2);
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
80102acf:	83 ec 04             	sub    $0x4,%esp
  return inb(CMOS_RETURN);
80102ad2:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
80102ad5:	8d 45 d0             	lea    -0x30(%ebp),%eax
80102ad8:	6a 18                	push   $0x18
80102ada:	50                   	push   %eax
80102adb:	8d 45 b8             	lea    -0x48(%ebp),%eax
80102ade:	50                   	push   %eax
80102adf:	e8 ec 1c 00 00       	call   801047d0 <memcmp>
80102ae4:	83 c4 10             	add    $0x10,%esp
80102ae7:	85 c0                	test   %eax,%eax
80102ae9:	0f 85 f1 fe ff ff    	jne    801029e0 <cmostime+0x30>
      break;
  }

  // convert
  if(bcd) {
80102aef:	80 7d b3 00          	cmpb   $0x0,-0x4d(%ebp)
80102af3:	75 78                	jne    80102b6d <cmostime+0x1bd>
#define    CONV(x)     (t1.x = ((t1.x >> 4) * 10) + (t1.x & 0xf))
    CONV(second);
80102af5:	8b 45 b8             	mov    -0x48(%ebp),%eax
80102af8:	89 c2                	mov    %eax,%edx
80102afa:	83 e0 0f             	and    $0xf,%eax
80102afd:	c1 ea 04             	shr    $0x4,%edx
80102b00:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102b03:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102b06:	89 45 b8             	mov    %eax,-0x48(%ebp)
    CONV(minute);
80102b09:	8b 45 bc             	mov    -0x44(%ebp),%eax
80102b0c:	89 c2                	mov    %eax,%edx
80102b0e:	83 e0 0f             	and    $0xf,%eax
80102b11:	c1 ea 04             	shr    $0x4,%edx
80102b14:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102b17:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102b1a:	89 45 bc             	mov    %eax,-0x44(%ebp)
    CONV(hour  );
80102b1d:	8b 45 c0             	mov    -0x40(%ebp),%eax
80102b20:	89 c2                	mov    %eax,%edx
80102b22:	83 e0 0f             	and    $0xf,%eax
80102b25:	c1 ea 04             	shr    $0x4,%edx
80102b28:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102b2b:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102b2e:	89 45 c0             	mov    %eax,-0x40(%ebp)
    CONV(day   );
80102b31:	8b 45 c4             	mov    -0x3c(%ebp),%eax
80102b34:	89 c2                	mov    %eax,%edx
80102b36:	83 e0 0f             	and    $0xf,%eax
80102b39:	c1 ea 04             	shr    $0x4,%edx
80102b3c:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102b3f:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102b42:	89 45 c4             	mov    %eax,-0x3c(%ebp)
    CONV(month );
80102b45:	8b 45 c8             	mov    -0x38(%ebp),%eax
80102b48:	89 c2                	mov    %eax,%edx
80102b4a:	83 e0 0f             	and    $0xf,%eax
80102b4d:	c1 ea 04             	shr    $0x4,%edx
80102b50:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102b53:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102b56:	89 45 c8             	mov    %eax,-0x38(%ebp)
    CONV(year  );
80102b59:	8b 45 cc             	mov    -0x34(%ebp),%eax
80102b5c:	89 c2                	mov    %eax,%edx
80102b5e:	83 e0 0f             	and    $0xf,%eax
80102b61:	c1 ea 04             	shr    $0x4,%edx
80102b64:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102b67:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102b6a:	89 45 cc             	mov    %eax,-0x34(%ebp)
#undef     CONV
  }

  *r = t1;
80102b6d:	8b 75 08             	mov    0x8(%ebp),%esi
80102b70:	8b 45 b8             	mov    -0x48(%ebp),%eax
80102b73:	89 06                	mov    %eax,(%esi)
80102b75:	8b 45 bc             	mov    -0x44(%ebp),%eax
80102b78:	89 46 04             	mov    %eax,0x4(%esi)
80102b7b:	8b 45 c0             	mov    -0x40(%ebp),%eax
80102b7e:	89 46 08             	mov    %eax,0x8(%esi)
80102b81:	8b 45 c4             	mov    -0x3c(%ebp),%eax
80102b84:	89 46 0c             	mov    %eax,0xc(%esi)
80102b87:	8b 45 c8             	mov    -0x38(%ebp),%eax
80102b8a:	89 46 10             	mov    %eax,0x10(%esi)
80102b8d:	8b 45 cc             	mov    -0x34(%ebp),%eax
80102b90:	89 46 14             	mov    %eax,0x14(%esi)
  r->year += 2000;
80102b93:	81 46 14 d0 07 00 00 	addl   $0x7d0,0x14(%esi)
}
80102b9a:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102b9d:	5b                   	pop    %ebx
80102b9e:	5e                   	pop    %esi
80102b9f:	5f                   	pop    %edi
80102ba0:	5d                   	pop    %ebp
80102ba1:	c3                   	ret    
80102ba2:	66 90                	xchg   %ax,%ax
80102ba4:	66 90                	xchg   %ax,%ax
80102ba6:	66 90                	xchg   %ax,%ax
80102ba8:	66 90                	xchg   %ax,%ax
80102baa:	66 90                	xchg   %ax,%ax
80102bac:	66 90                	xchg   %ax,%ax
80102bae:	66 90                	xchg   %ax,%ax

80102bb0 <install_trans>:
static void
install_trans(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
80102bb0:	8b 0d c8 26 11 80    	mov    0x801126c8,%ecx
80102bb6:	85 c9                	test   %ecx,%ecx
80102bb8:	0f 8e 8a 00 00 00    	jle    80102c48 <install_trans+0x98>
{
80102bbe:	55                   	push   %ebp
80102bbf:	89 e5                	mov    %esp,%ebp
80102bc1:	57                   	push   %edi
  for (tail = 0; tail < log.lh.n; tail++) {
80102bc2:	31 ff                	xor    %edi,%edi
{
80102bc4:	56                   	push   %esi
80102bc5:	53                   	push   %ebx
80102bc6:	83 ec 0c             	sub    $0xc,%esp
80102bc9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    struct buf *lbuf = bread(log.dev, log.start+tail+1); // read log block
80102bd0:	a1 b4 26 11 80       	mov    0x801126b4,%eax
80102bd5:	83 ec 08             	sub    $0x8,%esp
80102bd8:	01 f8                	add    %edi,%eax
80102bda:	83 c0 01             	add    $0x1,%eax
80102bdd:	50                   	push   %eax
80102bde:	ff 35 c4 26 11 80    	pushl  0x801126c4
80102be4:	e8 e7 d4 ff ff       	call   801000d0 <bread>
80102be9:	89 c6                	mov    %eax,%esi
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
80102beb:	58                   	pop    %eax
80102bec:	5a                   	pop    %edx
80102bed:	ff 34 bd cc 26 11 80 	pushl  -0x7feed934(,%edi,4)
80102bf4:	ff 35 c4 26 11 80    	pushl  0x801126c4
  for (tail = 0; tail < log.lh.n; tail++) {
80102bfa:	83 c7 01             	add    $0x1,%edi
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
80102bfd:	e8 ce d4 ff ff       	call   801000d0 <bread>
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
80102c02:	83 c4 0c             	add    $0xc,%esp
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
80102c05:	89 c3                	mov    %eax,%ebx
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
80102c07:	8d 46 5c             	lea    0x5c(%esi),%eax
80102c0a:	68 00 02 00 00       	push   $0x200
80102c0f:	50                   	push   %eax
80102c10:	8d 43 5c             	lea    0x5c(%ebx),%eax
80102c13:	50                   	push   %eax
80102c14:	e8 07 1c 00 00       	call   80104820 <memmove>
    bwrite(dbuf);  // write dst to disk
80102c19:	89 1c 24             	mov    %ebx,(%esp)
80102c1c:	e8 8f d5 ff ff       	call   801001b0 <bwrite>
    brelse(lbuf);
80102c21:	89 34 24             	mov    %esi,(%esp)
80102c24:	e8 c7 d5 ff ff       	call   801001f0 <brelse>
    brelse(dbuf);
80102c29:	89 1c 24             	mov    %ebx,(%esp)
80102c2c:	e8 bf d5 ff ff       	call   801001f0 <brelse>
  for (tail = 0; tail < log.lh.n; tail++) {
80102c31:	83 c4 10             	add    $0x10,%esp
80102c34:	39 3d c8 26 11 80    	cmp    %edi,0x801126c8
80102c3a:	7f 94                	jg     80102bd0 <install_trans+0x20>
  }
}
80102c3c:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102c3f:	5b                   	pop    %ebx
80102c40:	5e                   	pop    %esi
80102c41:	5f                   	pop    %edi
80102c42:	5d                   	pop    %ebp
80102c43:	c3                   	ret    
80102c44:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102c48:	c3                   	ret    
80102c49:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80102c50 <write_head>:
// Write in-memory log header to disk.
// This is the true point at which the
// current transaction commits.
static void
write_head(void)
{
80102c50:	55                   	push   %ebp
80102c51:	89 e5                	mov    %esp,%ebp
80102c53:	53                   	push   %ebx
80102c54:	83 ec 0c             	sub    $0xc,%esp
  struct buf *buf = bread(log.dev, log.start);
80102c57:	ff 35 b4 26 11 80    	pushl  0x801126b4
80102c5d:	ff 35 c4 26 11 80    	pushl  0x801126c4
80102c63:	e8 68 d4 ff ff       	call   801000d0 <bread>
  struct logheader *hb = (struct logheader *) (buf->data);
  int i;
  hb->n = log.lh.n;
  for (i = 0; i < log.lh.n; i++) {
80102c68:	83 c4 10             	add    $0x10,%esp
  struct buf *buf = bread(log.dev, log.start);
80102c6b:	89 c3                	mov    %eax,%ebx
  hb->n = log.lh.n;
80102c6d:	a1 c8 26 11 80       	mov    0x801126c8,%eax
80102c72:	89 43 5c             	mov    %eax,0x5c(%ebx)
  for (i = 0; i < log.lh.n; i++) {
80102c75:	85 c0                	test   %eax,%eax
80102c77:	7e 19                	jle    80102c92 <write_head+0x42>
80102c79:	31 d2                	xor    %edx,%edx
80102c7b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102c7f:	90                   	nop
    hb->block[i] = log.lh.block[i];
80102c80:	8b 0c 95 cc 26 11 80 	mov    -0x7feed934(,%edx,4),%ecx
80102c87:	89 4c 93 60          	mov    %ecx,0x60(%ebx,%edx,4)
  for (i = 0; i < log.lh.n; i++) {
80102c8b:	83 c2 01             	add    $0x1,%edx
80102c8e:	39 d0                	cmp    %edx,%eax
80102c90:	75 ee                	jne    80102c80 <write_head+0x30>
  }
  bwrite(buf);
80102c92:	83 ec 0c             	sub    $0xc,%esp
80102c95:	53                   	push   %ebx
80102c96:	e8 15 d5 ff ff       	call   801001b0 <bwrite>
  brelse(buf);
80102c9b:	89 1c 24             	mov    %ebx,(%esp)
80102c9e:	e8 4d d5 ff ff       	call   801001f0 <brelse>
}
80102ca3:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102ca6:	83 c4 10             	add    $0x10,%esp
80102ca9:	c9                   	leave  
80102caa:	c3                   	ret    
80102cab:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102caf:	90                   	nop

80102cb0 <initlog>:
{
80102cb0:	f3 0f 1e fb          	endbr32 
80102cb4:	55                   	push   %ebp
80102cb5:	89 e5                	mov    %esp,%ebp
80102cb7:	53                   	push   %ebx
80102cb8:	83 ec 2c             	sub    $0x2c,%esp
80102cbb:	8b 5d 08             	mov    0x8(%ebp),%ebx
  initlock(&log.lock, "log");
80102cbe:	68 a0 76 10 80       	push   $0x801076a0
80102cc3:	68 80 26 11 80       	push   $0x80112680
80102cc8:	e8 23 18 00 00       	call   801044f0 <initlock>
  readsb(dev, &sb);
80102ccd:	58                   	pop    %eax
80102cce:	8d 45 dc             	lea    -0x24(%ebp),%eax
80102cd1:	5a                   	pop    %edx
80102cd2:	50                   	push   %eax
80102cd3:	53                   	push   %ebx
80102cd4:	e8 47 e8 ff ff       	call   80101520 <readsb>
  log.start = sb.logstart;
80102cd9:	8b 45 ec             	mov    -0x14(%ebp),%eax
  struct buf *buf = bread(log.dev, log.start);
80102cdc:	59                   	pop    %ecx
  log.dev = dev;
80102cdd:	89 1d c4 26 11 80    	mov    %ebx,0x801126c4
  log.size = sb.nlog;
80102ce3:	8b 55 e8             	mov    -0x18(%ebp),%edx
  log.start = sb.logstart;
80102ce6:	a3 b4 26 11 80       	mov    %eax,0x801126b4
  log.size = sb.nlog;
80102ceb:	89 15 b8 26 11 80    	mov    %edx,0x801126b8
  struct buf *buf = bread(log.dev, log.start);
80102cf1:	5a                   	pop    %edx
80102cf2:	50                   	push   %eax
80102cf3:	53                   	push   %ebx
80102cf4:	e8 d7 d3 ff ff       	call   801000d0 <bread>
  for (i = 0; i < log.lh.n; i++) {
80102cf9:	83 c4 10             	add    $0x10,%esp
  log.lh.n = lh->n;
80102cfc:	8b 48 5c             	mov    0x5c(%eax),%ecx
80102cff:	89 0d c8 26 11 80    	mov    %ecx,0x801126c8
  for (i = 0; i < log.lh.n; i++) {
80102d05:	85 c9                	test   %ecx,%ecx
80102d07:	7e 19                	jle    80102d22 <initlog+0x72>
80102d09:	31 d2                	xor    %edx,%edx
80102d0b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102d0f:	90                   	nop
    log.lh.block[i] = lh->block[i];
80102d10:	8b 5c 90 60          	mov    0x60(%eax,%edx,4),%ebx
80102d14:	89 1c 95 cc 26 11 80 	mov    %ebx,-0x7feed934(,%edx,4)
  for (i = 0; i < log.lh.n; i++) {
80102d1b:	83 c2 01             	add    $0x1,%edx
80102d1e:	39 d1                	cmp    %edx,%ecx
80102d20:	75 ee                	jne    80102d10 <initlog+0x60>
  brelse(buf);
80102d22:	83 ec 0c             	sub    $0xc,%esp
80102d25:	50                   	push   %eax
80102d26:	e8 c5 d4 ff ff       	call   801001f0 <brelse>

static void
recover_from_log(void)
{
  read_head();
  install_trans(); // if committed, copy from log to disk
80102d2b:	e8 80 fe ff ff       	call   80102bb0 <install_trans>
  log.lh.n = 0;
80102d30:	c7 05 c8 26 11 80 00 	movl   $0x0,0x801126c8
80102d37:	00 00 00 
  write_head(); // clear the log
80102d3a:	e8 11 ff ff ff       	call   80102c50 <write_head>
}
80102d3f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102d42:	83 c4 10             	add    $0x10,%esp
80102d45:	c9                   	leave  
80102d46:	c3                   	ret    
80102d47:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102d4e:	66 90                	xchg   %ax,%ax

80102d50 <begin_op>:
}

// called at the start of each FS system call.
void
begin_op(void)
{
80102d50:	f3 0f 1e fb          	endbr32 
80102d54:	55                   	push   %ebp
80102d55:	89 e5                	mov    %esp,%ebp
80102d57:	83 ec 14             	sub    $0x14,%esp
  acquire(&log.lock);
80102d5a:	68 80 26 11 80       	push   $0x80112680
80102d5f:	e8 0c 19 00 00       	call   80104670 <acquire>
80102d64:	83 c4 10             	add    $0x10,%esp
80102d67:	eb 1c                	jmp    80102d85 <begin_op+0x35>
80102d69:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  while(1){
    if(log.committing){
      sleep(&log, &log.lock);
80102d70:	83 ec 08             	sub    $0x8,%esp
80102d73:	68 80 26 11 80       	push   $0x80112680
80102d78:	68 80 26 11 80       	push   $0x80112680
80102d7d:	e8 7e 12 00 00       	call   80104000 <sleep>
80102d82:	83 c4 10             	add    $0x10,%esp
    if(log.committing){
80102d85:	a1 c0 26 11 80       	mov    0x801126c0,%eax
80102d8a:	85 c0                	test   %eax,%eax
80102d8c:	75 e2                	jne    80102d70 <begin_op+0x20>
    } else if(log.lh.n + (log.outstanding+1)*MAXOPBLOCKS > LOGSIZE){
80102d8e:	a1 bc 26 11 80       	mov    0x801126bc,%eax
80102d93:	8b 15 c8 26 11 80    	mov    0x801126c8,%edx
80102d99:	83 c0 01             	add    $0x1,%eax
80102d9c:	8d 0c 80             	lea    (%eax,%eax,4),%ecx
80102d9f:	8d 14 4a             	lea    (%edx,%ecx,2),%edx
80102da2:	83 fa 1e             	cmp    $0x1e,%edx
80102da5:	7f c9                	jg     80102d70 <begin_op+0x20>
      // this op might exhaust log space; wait for commit.
      sleep(&log, &log.lock);
    } else {
      log.outstanding += 1;
      release(&log.lock);
80102da7:	83 ec 0c             	sub    $0xc,%esp
      log.outstanding += 1;
80102daa:	a3 bc 26 11 80       	mov    %eax,0x801126bc
      release(&log.lock);
80102daf:	68 80 26 11 80       	push   $0x80112680
80102db4:	e8 77 19 00 00       	call   80104730 <release>
      break;
    }
  }
}
80102db9:	83 c4 10             	add    $0x10,%esp
80102dbc:	c9                   	leave  
80102dbd:	c3                   	ret    
80102dbe:	66 90                	xchg   %ax,%ax

80102dc0 <end_op>:

// called at the end of each FS system call.
// commits if this was the last outstanding operation.
void
end_op(void)
{
80102dc0:	f3 0f 1e fb          	endbr32 
80102dc4:	55                   	push   %ebp
80102dc5:	89 e5                	mov    %esp,%ebp
80102dc7:	57                   	push   %edi
80102dc8:	56                   	push   %esi
80102dc9:	53                   	push   %ebx
80102dca:	83 ec 18             	sub    $0x18,%esp
  int do_commit = 0;

  acquire(&log.lock);
80102dcd:	68 80 26 11 80       	push   $0x80112680
80102dd2:	e8 99 18 00 00       	call   80104670 <acquire>
  log.outstanding -= 1;
80102dd7:	a1 bc 26 11 80       	mov    0x801126bc,%eax
  if(log.committing)
80102ddc:	8b 35 c0 26 11 80    	mov    0x801126c0,%esi
80102de2:	83 c4 10             	add    $0x10,%esp
  log.outstanding -= 1;
80102de5:	8d 58 ff             	lea    -0x1(%eax),%ebx
80102de8:	89 1d bc 26 11 80    	mov    %ebx,0x801126bc
  if(log.committing)
80102dee:	85 f6                	test   %esi,%esi
80102df0:	0f 85 1e 01 00 00    	jne    80102f14 <end_op+0x154>
    panic("log.committing");
  if(log.outstanding == 0){
80102df6:	85 db                	test   %ebx,%ebx
80102df8:	0f 85 f2 00 00 00    	jne    80102ef0 <end_op+0x130>
    do_commit = 1;
    log.committing = 1;
80102dfe:	c7 05 c0 26 11 80 01 	movl   $0x1,0x801126c0
80102e05:	00 00 00 
    // begin_op() may be waiting for log space,
    // and decrementing log.outstanding has decreased
    // the amount of reserved space.
    wakeup(&log);
  }
  release(&log.lock);
80102e08:	83 ec 0c             	sub    $0xc,%esp
80102e0b:	68 80 26 11 80       	push   $0x80112680
80102e10:	e8 1b 19 00 00       	call   80104730 <release>
}

static void
commit()
{
  if (log.lh.n > 0) {
80102e15:	8b 0d c8 26 11 80    	mov    0x801126c8,%ecx
80102e1b:	83 c4 10             	add    $0x10,%esp
80102e1e:	85 c9                	test   %ecx,%ecx
80102e20:	7f 3e                	jg     80102e60 <end_op+0xa0>
    acquire(&log.lock);
80102e22:	83 ec 0c             	sub    $0xc,%esp
80102e25:	68 80 26 11 80       	push   $0x80112680
80102e2a:	e8 41 18 00 00       	call   80104670 <acquire>
    wakeup(&log);
80102e2f:	c7 04 24 80 26 11 80 	movl   $0x80112680,(%esp)
    log.committing = 0;
80102e36:	c7 05 c0 26 11 80 00 	movl   $0x0,0x801126c0
80102e3d:	00 00 00 
    wakeup(&log);
80102e40:	e8 bb 13 00 00       	call   80104200 <wakeup>
    release(&log.lock);
80102e45:	c7 04 24 80 26 11 80 	movl   $0x80112680,(%esp)
80102e4c:	e8 df 18 00 00       	call   80104730 <release>
80102e51:	83 c4 10             	add    $0x10,%esp
}
80102e54:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102e57:	5b                   	pop    %ebx
80102e58:	5e                   	pop    %esi
80102e59:	5f                   	pop    %edi
80102e5a:	5d                   	pop    %ebp
80102e5b:	c3                   	ret    
80102e5c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    struct buf *to = bread(log.dev, log.start+tail+1); // log block
80102e60:	a1 b4 26 11 80       	mov    0x801126b4,%eax
80102e65:	83 ec 08             	sub    $0x8,%esp
80102e68:	01 d8                	add    %ebx,%eax
80102e6a:	83 c0 01             	add    $0x1,%eax
80102e6d:	50                   	push   %eax
80102e6e:	ff 35 c4 26 11 80    	pushl  0x801126c4
80102e74:	e8 57 d2 ff ff       	call   801000d0 <bread>
80102e79:	89 c6                	mov    %eax,%esi
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
80102e7b:	58                   	pop    %eax
80102e7c:	5a                   	pop    %edx
80102e7d:	ff 34 9d cc 26 11 80 	pushl  -0x7feed934(,%ebx,4)
80102e84:	ff 35 c4 26 11 80    	pushl  0x801126c4
  for (tail = 0; tail < log.lh.n; tail++) {
80102e8a:	83 c3 01             	add    $0x1,%ebx
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
80102e8d:	e8 3e d2 ff ff       	call   801000d0 <bread>
    memmove(to->data, from->data, BSIZE);
80102e92:	83 c4 0c             	add    $0xc,%esp
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
80102e95:	89 c7                	mov    %eax,%edi
    memmove(to->data, from->data, BSIZE);
80102e97:	8d 40 5c             	lea    0x5c(%eax),%eax
80102e9a:	68 00 02 00 00       	push   $0x200
80102e9f:	50                   	push   %eax
80102ea0:	8d 46 5c             	lea    0x5c(%esi),%eax
80102ea3:	50                   	push   %eax
80102ea4:	e8 77 19 00 00       	call   80104820 <memmove>
    bwrite(to);  // write the log
80102ea9:	89 34 24             	mov    %esi,(%esp)
80102eac:	e8 ff d2 ff ff       	call   801001b0 <bwrite>
    brelse(from);
80102eb1:	89 3c 24             	mov    %edi,(%esp)
80102eb4:	e8 37 d3 ff ff       	call   801001f0 <brelse>
    brelse(to);
80102eb9:	89 34 24             	mov    %esi,(%esp)
80102ebc:	e8 2f d3 ff ff       	call   801001f0 <brelse>
  for (tail = 0; tail < log.lh.n; tail++) {
80102ec1:	83 c4 10             	add    $0x10,%esp
80102ec4:	3b 1d c8 26 11 80    	cmp    0x801126c8,%ebx
80102eca:	7c 94                	jl     80102e60 <end_op+0xa0>
    write_log();     // Write modified blocks from cache to log
    write_head();    // Write header to disk -- the real commit
80102ecc:	e8 7f fd ff ff       	call   80102c50 <write_head>
    install_trans(); // Now install writes to home locations
80102ed1:	e8 da fc ff ff       	call   80102bb0 <install_trans>
    log.lh.n = 0;
80102ed6:	c7 05 c8 26 11 80 00 	movl   $0x0,0x801126c8
80102edd:	00 00 00 
    write_head();    // Erase the transaction from the log
80102ee0:	e8 6b fd ff ff       	call   80102c50 <write_head>
80102ee5:	e9 38 ff ff ff       	jmp    80102e22 <end_op+0x62>
80102eea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    wakeup(&log);
80102ef0:	83 ec 0c             	sub    $0xc,%esp
80102ef3:	68 80 26 11 80       	push   $0x80112680
80102ef8:	e8 03 13 00 00       	call   80104200 <wakeup>
  release(&log.lock);
80102efd:	c7 04 24 80 26 11 80 	movl   $0x80112680,(%esp)
80102f04:	e8 27 18 00 00       	call   80104730 <release>
80102f09:	83 c4 10             	add    $0x10,%esp
}
80102f0c:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102f0f:	5b                   	pop    %ebx
80102f10:	5e                   	pop    %esi
80102f11:	5f                   	pop    %edi
80102f12:	5d                   	pop    %ebp
80102f13:	c3                   	ret    
    panic("log.committing");
80102f14:	83 ec 0c             	sub    $0xc,%esp
80102f17:	68 a4 76 10 80       	push   $0x801076a4
80102f1c:	e8 6f d4 ff ff       	call   80100390 <panic>
80102f21:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102f28:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102f2f:	90                   	nop

80102f30 <log_write>:
//   modify bp->data[]
//   log_write(bp)
//   brelse(bp)
void
log_write(struct buf *b)
{
80102f30:	f3 0f 1e fb          	endbr32 
80102f34:	55                   	push   %ebp
80102f35:	89 e5                	mov    %esp,%ebp
80102f37:	53                   	push   %ebx
80102f38:	83 ec 04             	sub    $0x4,%esp
  int i;

  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80102f3b:	8b 15 c8 26 11 80    	mov    0x801126c8,%edx
{
80102f41:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80102f44:	83 fa 1d             	cmp    $0x1d,%edx
80102f47:	0f 8f 91 00 00 00    	jg     80102fde <log_write+0xae>
80102f4d:	a1 b8 26 11 80       	mov    0x801126b8,%eax
80102f52:	83 e8 01             	sub    $0x1,%eax
80102f55:	39 c2                	cmp    %eax,%edx
80102f57:	0f 8d 81 00 00 00    	jge    80102fde <log_write+0xae>
    panic("too big a transaction");
  if (log.outstanding < 1)
80102f5d:	a1 bc 26 11 80       	mov    0x801126bc,%eax
80102f62:	85 c0                	test   %eax,%eax
80102f64:	0f 8e 81 00 00 00    	jle    80102feb <log_write+0xbb>
    panic("log_write outside of trans");

  acquire(&log.lock);
80102f6a:	83 ec 0c             	sub    $0xc,%esp
80102f6d:	68 80 26 11 80       	push   $0x80112680
80102f72:	e8 f9 16 00 00       	call   80104670 <acquire>
  for (i = 0; i < log.lh.n; i++) {
80102f77:	8b 15 c8 26 11 80    	mov    0x801126c8,%edx
80102f7d:	83 c4 10             	add    $0x10,%esp
80102f80:	85 d2                	test   %edx,%edx
80102f82:	7e 4e                	jle    80102fd2 <log_write+0xa2>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
80102f84:	8b 4b 08             	mov    0x8(%ebx),%ecx
  for (i = 0; i < log.lh.n; i++) {
80102f87:	31 c0                	xor    %eax,%eax
80102f89:	eb 0c                	jmp    80102f97 <log_write+0x67>
80102f8b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102f8f:	90                   	nop
80102f90:	83 c0 01             	add    $0x1,%eax
80102f93:	39 c2                	cmp    %eax,%edx
80102f95:	74 29                	je     80102fc0 <log_write+0x90>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
80102f97:	39 0c 85 cc 26 11 80 	cmp    %ecx,-0x7feed934(,%eax,4)
80102f9e:	75 f0                	jne    80102f90 <log_write+0x60>
      break;
  }
  log.lh.block[i] = b->blockno;
80102fa0:	89 0c 85 cc 26 11 80 	mov    %ecx,-0x7feed934(,%eax,4)
  if (i == log.lh.n)
    log.lh.n++;
  b->flags |= B_DIRTY; // prevent eviction
80102fa7:	83 0b 04             	orl    $0x4,(%ebx)
  release(&log.lock);
}
80102faa:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  release(&log.lock);
80102fad:	c7 45 08 80 26 11 80 	movl   $0x80112680,0x8(%ebp)
}
80102fb4:	c9                   	leave  
  release(&log.lock);
80102fb5:	e9 76 17 00 00       	jmp    80104730 <release>
80102fba:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  log.lh.block[i] = b->blockno;
80102fc0:	89 0c 95 cc 26 11 80 	mov    %ecx,-0x7feed934(,%edx,4)
    log.lh.n++;
80102fc7:	83 c2 01             	add    $0x1,%edx
80102fca:	89 15 c8 26 11 80    	mov    %edx,0x801126c8
80102fd0:	eb d5                	jmp    80102fa7 <log_write+0x77>
  log.lh.block[i] = b->blockno;
80102fd2:	8b 43 08             	mov    0x8(%ebx),%eax
80102fd5:	a3 cc 26 11 80       	mov    %eax,0x801126cc
  if (i == log.lh.n)
80102fda:	75 cb                	jne    80102fa7 <log_write+0x77>
80102fdc:	eb e9                	jmp    80102fc7 <log_write+0x97>
    panic("too big a transaction");
80102fde:	83 ec 0c             	sub    $0xc,%esp
80102fe1:	68 b3 76 10 80       	push   $0x801076b3
80102fe6:	e8 a5 d3 ff ff       	call   80100390 <panic>
    panic("log_write outside of trans");
80102feb:	83 ec 0c             	sub    $0xc,%esp
80102fee:	68 c9 76 10 80       	push   $0x801076c9
80102ff3:	e8 98 d3 ff ff       	call   80100390 <panic>
80102ff8:	66 90                	xchg   %ax,%ax
80102ffa:	66 90                	xchg   %ax,%ax
80102ffc:	66 90                	xchg   %ax,%ax
80102ffe:	66 90                	xchg   %ax,%ax

80103000 <mpmain>:
}

// Common CPU setup code.
static void
mpmain(void)
{
80103000:	55                   	push   %ebp
80103001:	89 e5                	mov    %esp,%ebp
80103003:	53                   	push   %ebx
80103004:	83 ec 04             	sub    $0x4,%esp
  cprintf("cpu%d: starting %d\n", cpuid(), cpuid());
80103007:	e8 d4 09 00 00       	call   801039e0 <cpuid>
8010300c:	89 c3                	mov    %eax,%ebx
8010300e:	e8 cd 09 00 00       	call   801039e0 <cpuid>
80103013:	83 ec 04             	sub    $0x4,%esp
80103016:	53                   	push   %ebx
80103017:	50                   	push   %eax
80103018:	68 e4 76 10 80       	push   $0x801076e4
8010301d:	e8 8e d6 ff ff       	call   801006b0 <cprintf>
  idtinit();       // load idt register
80103022:	e8 09 2a 00 00       	call   80105a30 <idtinit>
  xchg(&(mycpu()->started), 1); // tell startothers() we're up
80103027:	e8 44 09 00 00       	call   80103970 <mycpu>
8010302c:	89 c2                	mov    %eax,%edx
xchg(volatile uint *addr, uint newval)
{
  uint result;

  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
8010302e:	b8 01 00 00 00       	mov    $0x1,%eax
80103033:	f0 87 82 a0 00 00 00 	lock xchg %eax,0xa0(%edx)
  scheduler();     // start running processes
8010303a:	e8 a1 0c 00 00       	call   80103ce0 <scheduler>
8010303f:	90                   	nop

80103040 <mpenter>:
{
80103040:	f3 0f 1e fb          	endbr32 
80103044:	55                   	push   %ebp
80103045:	89 e5                	mov    %esp,%ebp
80103047:	83 ec 08             	sub    $0x8,%esp
  switchkvm();
8010304a:	e8 b1 3a 00 00       	call   80106b00 <switchkvm>
  seginit();
8010304f:	e8 1c 3a 00 00       	call   80106a70 <seginit>
  lapicinit();
80103054:	e8 67 f7 ff ff       	call   801027c0 <lapicinit>
  mpmain();
80103059:	e8 a2 ff ff ff       	call   80103000 <mpmain>
8010305e:	66 90                	xchg   %ax,%ax

80103060 <main>:
{
80103060:	f3 0f 1e fb          	endbr32 
80103064:	8d 4c 24 04          	lea    0x4(%esp),%ecx
80103068:	83 e4 f0             	and    $0xfffffff0,%esp
8010306b:	ff 71 fc             	pushl  -0x4(%ecx)
8010306e:	55                   	push   %ebp
8010306f:	89 e5                	mov    %esp,%ebp
80103071:	53                   	push   %ebx
80103072:	51                   	push   %ecx
  kinit1(end, P2V(4*1024*1024)); // phys page allocator
80103073:	83 ec 08             	sub    $0x8,%esp
80103076:	68 00 00 40 80       	push   $0x80400000
8010307b:	68 a8 54 11 80       	push   $0x801154a8
80103080:	e8 fb f4 ff ff       	call   80102580 <kinit1>
  kvmalloc();      // kernel page table
80103085:	e8 56 3f 00 00       	call   80106fe0 <kvmalloc>
  mpinit();        // detect other processors
8010308a:	e8 81 01 00 00       	call   80103210 <mpinit>
  lapicinit();     // interrupt controller
8010308f:	e8 2c f7 ff ff       	call   801027c0 <lapicinit>
  seginit();       // segment descriptors
80103094:	e8 d7 39 00 00       	call   80106a70 <seginit>
  picinit();       // disable pic
80103099:	e8 52 03 00 00       	call   801033f0 <picinit>
  ioapicinit();    // another interrupt controller
8010309e:	e8 fd f2 ff ff       	call   801023a0 <ioapicinit>
  consoleinit();   // console hardware
801030a3:	e8 a8 d9 ff ff       	call   80100a50 <consoleinit>
  uartinit();      // serial port
801030a8:	e8 83 2c 00 00       	call   80105d30 <uartinit>
  pinit();         // process table
801030ad:	e8 9e 08 00 00       	call   80103950 <pinit>
  tvinit();        // trap vectors
801030b2:	e8 f9 28 00 00       	call   801059b0 <tvinit>
  binit();         // buffer cache
801030b7:	e8 84 cf ff ff       	call   80100040 <binit>
  fileinit();      // file table
801030bc:	e8 3f dd ff ff       	call   80100e00 <fileinit>
  ideinit();       // disk 
801030c1:	e8 aa f0 ff ff       	call   80102170 <ideinit>

  // Write entry code to unused memory at 0x7000.
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = P2V(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);
801030c6:	83 c4 0c             	add    $0xc,%esp
801030c9:	68 8a 00 00 00       	push   $0x8a
801030ce:	68 8c a4 10 80       	push   $0x8010a48c
801030d3:	68 00 70 00 80       	push   $0x80007000
801030d8:	e8 43 17 00 00       	call   80104820 <memmove>

  for(c = cpus; c < cpus+ncpu; c++){
801030dd:	83 c4 10             	add    $0x10,%esp
801030e0:	69 05 00 2d 11 80 b0 	imul   $0xb0,0x80112d00,%eax
801030e7:	00 00 00 
801030ea:	05 80 27 11 80       	add    $0x80112780,%eax
801030ef:	3d 80 27 11 80       	cmp    $0x80112780,%eax
801030f4:	76 7a                	jbe    80103170 <main+0x110>
801030f6:	bb 80 27 11 80       	mov    $0x80112780,%ebx
801030fb:	eb 1c                	jmp    80103119 <main+0xb9>
801030fd:	8d 76 00             	lea    0x0(%esi),%esi
80103100:	69 05 00 2d 11 80 b0 	imul   $0xb0,0x80112d00,%eax
80103107:	00 00 00 
8010310a:	81 c3 b0 00 00 00    	add    $0xb0,%ebx
80103110:	05 80 27 11 80       	add    $0x80112780,%eax
80103115:	39 c3                	cmp    %eax,%ebx
80103117:	73 57                	jae    80103170 <main+0x110>
    if(c == mycpu())  // We've started already.
80103119:	e8 52 08 00 00       	call   80103970 <mycpu>
8010311e:	39 c3                	cmp    %eax,%ebx
80103120:	74 de                	je     80103100 <main+0xa0>
      continue;

    // Tell entryother.S what stack to use, where to enter, and what
    // pgdir to use. We cannot use kpgdir yet, because the AP processor
    // is running in low  memory, so we use entrypgdir for the APs too.
    stack = kalloc();
80103122:	e8 29 f5 ff ff       	call   80102650 <kalloc>
    *(void**)(code-4) = stack + KSTACKSIZE;
    *(void(**)(void))(code-8) = mpenter;
    *(int**)(code-12) = (void *) V2P(entrypgdir);

    lapicstartap(c->apicid, V2P(code));
80103127:	83 ec 08             	sub    $0x8,%esp
    *(void(**)(void))(code-8) = mpenter;
8010312a:	c7 05 f8 6f 00 80 40 	movl   $0x80103040,0x80006ff8
80103131:	30 10 80 
    *(int**)(code-12) = (void *) V2P(entrypgdir);
80103134:	c7 05 f4 6f 00 80 00 	movl   $0x109000,0x80006ff4
8010313b:	90 10 00 
    *(void**)(code-4) = stack + KSTACKSIZE;
8010313e:	05 00 10 00 00       	add    $0x1000,%eax
80103143:	a3 fc 6f 00 80       	mov    %eax,0x80006ffc
    lapicstartap(c->apicid, V2P(code));
80103148:	0f b6 03             	movzbl (%ebx),%eax
8010314b:	68 00 70 00 00       	push   $0x7000
80103150:	50                   	push   %eax
80103151:	e8 ba f7 ff ff       	call   80102910 <lapicstartap>

    // wait for cpu to finish mpmain()
    while(c->started == 0)
80103156:	83 c4 10             	add    $0x10,%esp
80103159:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103160:	8b 83 a0 00 00 00    	mov    0xa0(%ebx),%eax
80103166:	85 c0                	test   %eax,%eax
80103168:	74 f6                	je     80103160 <main+0x100>
8010316a:	eb 94                	jmp    80103100 <main+0xa0>
8010316c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  kinit2(P2V(4*1024*1024), P2V(PHYSTOP)); // must come after startothers()
80103170:	83 ec 08             	sub    $0x8,%esp
80103173:	68 00 00 00 8e       	push   $0x8e000000
80103178:	68 00 00 40 80       	push   $0x80400000
8010317d:	e8 6e f4 ff ff       	call   801025f0 <kinit2>
  userinit();      // first user process
80103182:	e8 a9 08 00 00       	call   80103a30 <userinit>
  mpmain();        // finish this processor's setup
80103187:	e8 74 fe ff ff       	call   80103000 <mpmain>
8010318c:	66 90                	xchg   %ax,%ax
8010318e:	66 90                	xchg   %ax,%ax

80103190 <mpsearch1>:
}

// Look for an MP structure in the len bytes at addr.
static struct mp*
mpsearch1(uint a, int len)
{
80103190:	55                   	push   %ebp
80103191:	89 e5                	mov    %esp,%ebp
80103193:	57                   	push   %edi
80103194:	56                   	push   %esi
  uchar *e, *p, *addr;

  addr = P2V(a);
80103195:	8d b0 00 00 00 80    	lea    -0x80000000(%eax),%esi
{
8010319b:	53                   	push   %ebx
  e = addr+len;
8010319c:	8d 1c 16             	lea    (%esi,%edx,1),%ebx
{
8010319f:	83 ec 0c             	sub    $0xc,%esp
  for(p = addr; p < e; p += sizeof(struct mp))
801031a2:	39 de                	cmp    %ebx,%esi
801031a4:	72 10                	jb     801031b6 <mpsearch1+0x26>
801031a6:	eb 50                	jmp    801031f8 <mpsearch1+0x68>
801031a8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801031af:	90                   	nop
801031b0:	89 fe                	mov    %edi,%esi
801031b2:	39 fb                	cmp    %edi,%ebx
801031b4:	76 42                	jbe    801031f8 <mpsearch1+0x68>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
801031b6:	83 ec 04             	sub    $0x4,%esp
801031b9:	8d 7e 10             	lea    0x10(%esi),%edi
801031bc:	6a 04                	push   $0x4
801031be:	68 f8 76 10 80       	push   $0x801076f8
801031c3:	56                   	push   %esi
801031c4:	e8 07 16 00 00       	call   801047d0 <memcmp>
801031c9:	83 c4 10             	add    $0x10,%esp
801031cc:	85 c0                	test   %eax,%eax
801031ce:	75 e0                	jne    801031b0 <mpsearch1+0x20>
801031d0:	89 f2                	mov    %esi,%edx
801031d2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    sum += addr[i];
801031d8:	0f b6 0a             	movzbl (%edx),%ecx
801031db:	83 c2 01             	add    $0x1,%edx
801031de:	01 c8                	add    %ecx,%eax
  for(i=0; i<len; i++)
801031e0:	39 fa                	cmp    %edi,%edx
801031e2:	75 f4                	jne    801031d8 <mpsearch1+0x48>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
801031e4:	84 c0                	test   %al,%al
801031e6:	75 c8                	jne    801031b0 <mpsearch1+0x20>
      return (struct mp*)p;
  return 0;
}
801031e8:	8d 65 f4             	lea    -0xc(%ebp),%esp
801031eb:	89 f0                	mov    %esi,%eax
801031ed:	5b                   	pop    %ebx
801031ee:	5e                   	pop    %esi
801031ef:	5f                   	pop    %edi
801031f0:	5d                   	pop    %ebp
801031f1:	c3                   	ret    
801031f2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801031f8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
801031fb:	31 f6                	xor    %esi,%esi
}
801031fd:	5b                   	pop    %ebx
801031fe:	89 f0                	mov    %esi,%eax
80103200:	5e                   	pop    %esi
80103201:	5f                   	pop    %edi
80103202:	5d                   	pop    %ebp
80103203:	c3                   	ret    
80103204:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010320b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010320f:	90                   	nop

80103210 <mpinit>:
  return conf;
}

void
mpinit(void)
{
80103210:	f3 0f 1e fb          	endbr32 
80103214:	55                   	push   %ebp
80103215:	89 e5                	mov    %esp,%ebp
80103217:	57                   	push   %edi
80103218:	56                   	push   %esi
80103219:	53                   	push   %ebx
8010321a:	83 ec 1c             	sub    $0x1c,%esp
  if((p = ((bda[0x0F]<<8)| bda[0x0E]) << 4)){
8010321d:	0f b6 05 0f 04 00 80 	movzbl 0x8000040f,%eax
80103224:	0f b6 15 0e 04 00 80 	movzbl 0x8000040e,%edx
8010322b:	c1 e0 08             	shl    $0x8,%eax
8010322e:	09 d0                	or     %edx,%eax
80103230:	c1 e0 04             	shl    $0x4,%eax
80103233:	75 1b                	jne    80103250 <mpinit+0x40>
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
80103235:	0f b6 05 14 04 00 80 	movzbl 0x80000414,%eax
8010323c:	0f b6 15 13 04 00 80 	movzbl 0x80000413,%edx
80103243:	c1 e0 08             	shl    $0x8,%eax
80103246:	09 d0                	or     %edx,%eax
80103248:	c1 e0 0a             	shl    $0xa,%eax
    if((mp = mpsearch1(p-1024, 1024)))
8010324b:	2d 00 04 00 00       	sub    $0x400,%eax
    if((mp = mpsearch1(p, 1024)))
80103250:	ba 00 04 00 00       	mov    $0x400,%edx
80103255:	e8 36 ff ff ff       	call   80103190 <mpsearch1>
8010325a:	89 c6                	mov    %eax,%esi
8010325c:	85 c0                	test   %eax,%eax
8010325e:	0f 84 4c 01 00 00    	je     801033b0 <mpinit+0x1a0>
  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
80103264:	8b 5e 04             	mov    0x4(%esi),%ebx
80103267:	85 db                	test   %ebx,%ebx
80103269:	0f 84 61 01 00 00    	je     801033d0 <mpinit+0x1c0>
  if(memcmp(conf, "PCMP", 4) != 0)
8010326f:	83 ec 04             	sub    $0x4,%esp
  conf = (struct mpconf*) P2V((uint) mp->physaddr);
80103272:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
  if(memcmp(conf, "PCMP", 4) != 0)
80103278:	6a 04                	push   $0x4
8010327a:	68 fd 76 10 80       	push   $0x801076fd
8010327f:	50                   	push   %eax
  conf = (struct mpconf*) P2V((uint) mp->physaddr);
80103280:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if(memcmp(conf, "PCMP", 4) != 0)
80103283:	e8 48 15 00 00       	call   801047d0 <memcmp>
80103288:	83 c4 10             	add    $0x10,%esp
8010328b:	85 c0                	test   %eax,%eax
8010328d:	0f 85 3d 01 00 00    	jne    801033d0 <mpinit+0x1c0>
  if(conf->version != 1 && conf->version != 4)
80103293:	0f b6 83 06 00 00 80 	movzbl -0x7ffffffa(%ebx),%eax
8010329a:	3c 01                	cmp    $0x1,%al
8010329c:	74 08                	je     801032a6 <mpinit+0x96>
8010329e:	3c 04                	cmp    $0x4,%al
801032a0:	0f 85 2a 01 00 00    	jne    801033d0 <mpinit+0x1c0>
  if(sum((uchar*)conf, conf->length) != 0)
801032a6:	0f b7 93 04 00 00 80 	movzwl -0x7ffffffc(%ebx),%edx
  for(i=0; i<len; i++)
801032ad:	66 85 d2             	test   %dx,%dx
801032b0:	74 26                	je     801032d8 <mpinit+0xc8>
801032b2:	8d 3c 1a             	lea    (%edx,%ebx,1),%edi
801032b5:	89 d8                	mov    %ebx,%eax
  sum = 0;
801032b7:	31 d2                	xor    %edx,%edx
801032b9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    sum += addr[i];
801032c0:	0f b6 88 00 00 00 80 	movzbl -0x80000000(%eax),%ecx
801032c7:	83 c0 01             	add    $0x1,%eax
801032ca:	01 ca                	add    %ecx,%edx
  for(i=0; i<len; i++)
801032cc:	39 f8                	cmp    %edi,%eax
801032ce:	75 f0                	jne    801032c0 <mpinit+0xb0>
  if(sum((uchar*)conf, conf->length) != 0)
801032d0:	84 d2                	test   %dl,%dl
801032d2:	0f 85 f8 00 00 00    	jne    801033d0 <mpinit+0x1c0>
  struct mpioapic *ioapic;

  if((conf = mpconfig(&mp)) == 0)
    panic("Expect to run on an SMP");
  ismp = 1;
  lapic = (uint*)conf->lapicaddr;
801032d8:	8b 83 24 00 00 80    	mov    -0x7fffffdc(%ebx),%eax
801032de:	a3 7c 26 11 80       	mov    %eax,0x8011267c
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
801032e3:	8d 83 2c 00 00 80    	lea    -0x7fffffd4(%ebx),%eax
801032e9:	0f b7 93 04 00 00 80 	movzwl -0x7ffffffc(%ebx),%edx
  ismp = 1;
801032f0:	bb 01 00 00 00       	mov    $0x1,%ebx
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
801032f5:	03 55 e4             	add    -0x1c(%ebp),%edx
801032f8:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
801032fb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801032ff:	90                   	nop
80103300:	39 c2                	cmp    %eax,%edx
80103302:	76 15                	jbe    80103319 <mpinit+0x109>
    switch(*p){
80103304:	0f b6 08             	movzbl (%eax),%ecx
80103307:	80 f9 02             	cmp    $0x2,%cl
8010330a:	74 5c                	je     80103368 <mpinit+0x158>
8010330c:	77 42                	ja     80103350 <mpinit+0x140>
8010330e:	84 c9                	test   %cl,%cl
80103310:	74 6e                	je     80103380 <mpinit+0x170>
      p += sizeof(struct mpioapic);
      continue;
    case MPBUS:
    case MPIOINTR:
    case MPLINTR:
      p += 8;
80103312:	83 c0 08             	add    $0x8,%eax
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
80103315:	39 c2                	cmp    %eax,%edx
80103317:	77 eb                	ja     80103304 <mpinit+0xf4>
80103319:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
    default:
      ismp = 0;
      break;
    }
  }
  if(!ismp)
8010331c:	85 db                	test   %ebx,%ebx
8010331e:	0f 84 b9 00 00 00    	je     801033dd <mpinit+0x1cd>
    panic("Didn't find a suitable machine");

  if(mp->imcrp){
80103324:	80 7e 0c 00          	cmpb   $0x0,0xc(%esi)
80103328:	74 15                	je     8010333f <mpinit+0x12f>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010332a:	b8 70 00 00 00       	mov    $0x70,%eax
8010332f:	ba 22 00 00 00       	mov    $0x22,%edx
80103334:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103335:	ba 23 00 00 00       	mov    $0x23,%edx
8010333a:	ec                   	in     (%dx),%al
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
8010333b:	83 c8 01             	or     $0x1,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010333e:	ee                   	out    %al,(%dx)
  }
}
8010333f:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103342:	5b                   	pop    %ebx
80103343:	5e                   	pop    %esi
80103344:	5f                   	pop    %edi
80103345:	5d                   	pop    %ebp
80103346:	c3                   	ret    
80103347:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010334e:	66 90                	xchg   %ax,%ax
    switch(*p){
80103350:	83 e9 03             	sub    $0x3,%ecx
80103353:	80 f9 01             	cmp    $0x1,%cl
80103356:	76 ba                	jbe    80103312 <mpinit+0x102>
80103358:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
8010335f:	eb 9f                	jmp    80103300 <mpinit+0xf0>
80103361:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      ioapicid = ioapic->apicno;
80103368:	0f b6 48 01          	movzbl 0x1(%eax),%ecx
      p += sizeof(struct mpioapic);
8010336c:	83 c0 08             	add    $0x8,%eax
      ioapicid = ioapic->apicno;
8010336f:	88 0d 60 27 11 80    	mov    %cl,0x80112760
      continue;
80103375:	eb 89                	jmp    80103300 <mpinit+0xf0>
80103377:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010337e:	66 90                	xchg   %ax,%ax
      if(ncpu < NCPU) {
80103380:	8b 0d 00 2d 11 80    	mov    0x80112d00,%ecx
80103386:	83 f9 07             	cmp    $0x7,%ecx
80103389:	7f 19                	jg     801033a4 <mpinit+0x194>
        cpus[ncpu].apicid = proc->apicid;  // apicid may differ from ncpu
8010338b:	69 f9 b0 00 00 00    	imul   $0xb0,%ecx,%edi
80103391:	0f b6 58 01          	movzbl 0x1(%eax),%ebx
        ncpu++;
80103395:	83 c1 01             	add    $0x1,%ecx
80103398:	89 0d 00 2d 11 80    	mov    %ecx,0x80112d00
        cpus[ncpu].apicid = proc->apicid;  // apicid may differ from ncpu
8010339e:	88 9f 80 27 11 80    	mov    %bl,-0x7feed880(%edi)
      p += sizeof(struct mpproc);
801033a4:	83 c0 14             	add    $0x14,%eax
      continue;
801033a7:	e9 54 ff ff ff       	jmp    80103300 <mpinit+0xf0>
801033ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  return mpsearch1(0xF0000, 0x10000);
801033b0:	ba 00 00 01 00       	mov    $0x10000,%edx
801033b5:	b8 00 00 0f 00       	mov    $0xf0000,%eax
801033ba:	e8 d1 fd ff ff       	call   80103190 <mpsearch1>
801033bf:	89 c6                	mov    %eax,%esi
  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
801033c1:	85 c0                	test   %eax,%eax
801033c3:	0f 85 9b fe ff ff    	jne    80103264 <mpinit+0x54>
801033c9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    panic("Expect to run on an SMP");
801033d0:	83 ec 0c             	sub    $0xc,%esp
801033d3:	68 02 77 10 80       	push   $0x80107702
801033d8:	e8 b3 cf ff ff       	call   80100390 <panic>
    panic("Didn't find a suitable machine");
801033dd:	83 ec 0c             	sub    $0xc,%esp
801033e0:	68 1c 77 10 80       	push   $0x8010771c
801033e5:	e8 a6 cf ff ff       	call   80100390 <panic>
801033ea:	66 90                	xchg   %ax,%ax
801033ec:	66 90                	xchg   %ax,%ax
801033ee:	66 90                	xchg   %ax,%ax

801033f0 <picinit>:
#define IO_PIC2         0xA0    // Slave (IRQs 8-15)

// Don't use the 8259A interrupt controllers.  Xv6 assumes SMP hardware.
void
picinit(void)
{
801033f0:	f3 0f 1e fb          	endbr32 
801033f4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801033f9:	ba 21 00 00 00       	mov    $0x21,%edx
801033fe:	ee                   	out    %al,(%dx)
801033ff:	ba a1 00 00 00       	mov    $0xa1,%edx
80103404:	ee                   	out    %al,(%dx)
  // mask all interrupts
  outb(IO_PIC1+1, 0xFF);
  outb(IO_PIC2+1, 0xFF);
}
80103405:	c3                   	ret    
80103406:	66 90                	xchg   %ax,%ax
80103408:	66 90                	xchg   %ax,%ax
8010340a:	66 90                	xchg   %ax,%ax
8010340c:	66 90                	xchg   %ax,%ax
8010340e:	66 90                	xchg   %ax,%ax

80103410 <pipealloc>:
  int writeopen;  // write fd is still open
};

int
pipealloc(struct file **f0, struct file **f1)
{
80103410:	f3 0f 1e fb          	endbr32 
80103414:	55                   	push   %ebp
80103415:	89 e5                	mov    %esp,%ebp
80103417:	57                   	push   %edi
80103418:	56                   	push   %esi
80103419:	53                   	push   %ebx
8010341a:	83 ec 0c             	sub    $0xc,%esp
8010341d:	8b 5d 08             	mov    0x8(%ebp),%ebx
80103420:	8b 75 0c             	mov    0xc(%ebp),%esi
  struct pipe *p;

  p = 0;
  *f0 = *f1 = 0;
80103423:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
80103429:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
8010342f:	e8 ec d9 ff ff       	call   80100e20 <filealloc>
80103434:	89 03                	mov    %eax,(%ebx)
80103436:	85 c0                	test   %eax,%eax
80103438:	0f 84 ac 00 00 00    	je     801034ea <pipealloc+0xda>
8010343e:	e8 dd d9 ff ff       	call   80100e20 <filealloc>
80103443:	89 06                	mov    %eax,(%esi)
80103445:	85 c0                	test   %eax,%eax
80103447:	0f 84 8b 00 00 00    	je     801034d8 <pipealloc+0xc8>
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
8010344d:	e8 fe f1 ff ff       	call   80102650 <kalloc>
80103452:	89 c7                	mov    %eax,%edi
80103454:	85 c0                	test   %eax,%eax
80103456:	0f 84 b4 00 00 00    	je     80103510 <pipealloc+0x100>
    goto bad;
  p->readopen = 1;
8010345c:	c7 80 3c 02 00 00 01 	movl   $0x1,0x23c(%eax)
80103463:	00 00 00 
  p->writeopen = 1;
  p->nwrite = 0;
  p->nread = 0;
  initlock(&p->lock, "pipe");
80103466:	83 ec 08             	sub    $0x8,%esp
  p->writeopen = 1;
80103469:	c7 80 40 02 00 00 01 	movl   $0x1,0x240(%eax)
80103470:	00 00 00 
  p->nwrite = 0;
80103473:	c7 80 38 02 00 00 00 	movl   $0x0,0x238(%eax)
8010347a:	00 00 00 
  p->nread = 0;
8010347d:	c7 80 34 02 00 00 00 	movl   $0x0,0x234(%eax)
80103484:	00 00 00 
  initlock(&p->lock, "pipe");
80103487:	68 3b 77 10 80       	push   $0x8010773b
8010348c:	50                   	push   %eax
8010348d:	e8 5e 10 00 00       	call   801044f0 <initlock>
  (*f0)->type = FD_PIPE;
80103492:	8b 03                	mov    (%ebx),%eax
  (*f0)->pipe = p;
  (*f1)->type = FD_PIPE;
  (*f1)->readable = 0;
  (*f1)->writable = 1;
  (*f1)->pipe = p;
  return 0;
80103494:	83 c4 10             	add    $0x10,%esp
  (*f0)->type = FD_PIPE;
80103497:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f0)->readable = 1;
8010349d:	8b 03                	mov    (%ebx),%eax
8010349f:	c6 40 08 01          	movb   $0x1,0x8(%eax)
  (*f0)->writable = 0;
801034a3:	8b 03                	mov    (%ebx),%eax
801034a5:	c6 40 09 00          	movb   $0x0,0x9(%eax)
  (*f0)->pipe = p;
801034a9:	8b 03                	mov    (%ebx),%eax
801034ab:	89 78 0c             	mov    %edi,0xc(%eax)
  (*f1)->type = FD_PIPE;
801034ae:	8b 06                	mov    (%esi),%eax
801034b0:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f1)->readable = 0;
801034b6:	8b 06                	mov    (%esi),%eax
801034b8:	c6 40 08 00          	movb   $0x0,0x8(%eax)
  (*f1)->writable = 1;
801034bc:	8b 06                	mov    (%esi),%eax
801034be:	c6 40 09 01          	movb   $0x1,0x9(%eax)
  (*f1)->pipe = p;
801034c2:	8b 06                	mov    (%esi),%eax
801034c4:	89 78 0c             	mov    %edi,0xc(%eax)
  if(*f0)
    fileclose(*f0);
  if(*f1)
    fileclose(*f1);
  return -1;
}
801034c7:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
801034ca:	31 c0                	xor    %eax,%eax
}
801034cc:	5b                   	pop    %ebx
801034cd:	5e                   	pop    %esi
801034ce:	5f                   	pop    %edi
801034cf:	5d                   	pop    %ebp
801034d0:	c3                   	ret    
801034d1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  if(*f0)
801034d8:	8b 03                	mov    (%ebx),%eax
801034da:	85 c0                	test   %eax,%eax
801034dc:	74 1e                	je     801034fc <pipealloc+0xec>
    fileclose(*f0);
801034de:	83 ec 0c             	sub    $0xc,%esp
801034e1:	50                   	push   %eax
801034e2:	e8 f9 d9 ff ff       	call   80100ee0 <fileclose>
801034e7:	83 c4 10             	add    $0x10,%esp
  if(*f1)
801034ea:	8b 06                	mov    (%esi),%eax
801034ec:	85 c0                	test   %eax,%eax
801034ee:	74 0c                	je     801034fc <pipealloc+0xec>
    fileclose(*f1);
801034f0:	83 ec 0c             	sub    $0xc,%esp
801034f3:	50                   	push   %eax
801034f4:	e8 e7 d9 ff ff       	call   80100ee0 <fileclose>
801034f9:	83 c4 10             	add    $0x10,%esp
}
801034fc:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return -1;
801034ff:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80103504:	5b                   	pop    %ebx
80103505:	5e                   	pop    %esi
80103506:	5f                   	pop    %edi
80103507:	5d                   	pop    %ebp
80103508:	c3                   	ret    
80103509:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  if(*f0)
80103510:	8b 03                	mov    (%ebx),%eax
80103512:	85 c0                	test   %eax,%eax
80103514:	75 c8                	jne    801034de <pipealloc+0xce>
80103516:	eb d2                	jmp    801034ea <pipealloc+0xda>
80103518:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010351f:	90                   	nop

80103520 <pipeclose>:

void
pipeclose(struct pipe *p, int writable)
{
80103520:	f3 0f 1e fb          	endbr32 
80103524:	55                   	push   %ebp
80103525:	89 e5                	mov    %esp,%ebp
80103527:	56                   	push   %esi
80103528:	53                   	push   %ebx
80103529:	8b 5d 08             	mov    0x8(%ebp),%ebx
8010352c:	8b 75 0c             	mov    0xc(%ebp),%esi
  acquire(&p->lock);
8010352f:	83 ec 0c             	sub    $0xc,%esp
80103532:	53                   	push   %ebx
80103533:	e8 38 11 00 00       	call   80104670 <acquire>
  if(writable){
80103538:	83 c4 10             	add    $0x10,%esp
8010353b:	85 f6                	test   %esi,%esi
8010353d:	74 41                	je     80103580 <pipeclose+0x60>
    p->writeopen = 0;
    wakeup(&p->nread);
8010353f:	83 ec 0c             	sub    $0xc,%esp
80103542:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
    p->writeopen = 0;
80103548:	c7 83 40 02 00 00 00 	movl   $0x0,0x240(%ebx)
8010354f:	00 00 00 
    wakeup(&p->nread);
80103552:	50                   	push   %eax
80103553:	e8 a8 0c 00 00       	call   80104200 <wakeup>
80103558:	83 c4 10             	add    $0x10,%esp
  } else {
    p->readopen = 0;
    wakeup(&p->nwrite);
  }
  if(p->readopen == 0 && p->writeopen == 0){
8010355b:	8b 93 3c 02 00 00    	mov    0x23c(%ebx),%edx
80103561:	85 d2                	test   %edx,%edx
80103563:	75 0a                	jne    8010356f <pipeclose+0x4f>
80103565:	8b 83 40 02 00 00    	mov    0x240(%ebx),%eax
8010356b:	85 c0                	test   %eax,%eax
8010356d:	74 31                	je     801035a0 <pipeclose+0x80>
    release(&p->lock);
    kfree((char*)p);
  } else
    release(&p->lock);
8010356f:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
80103572:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103575:	5b                   	pop    %ebx
80103576:	5e                   	pop    %esi
80103577:	5d                   	pop    %ebp
    release(&p->lock);
80103578:	e9 b3 11 00 00       	jmp    80104730 <release>
8010357d:	8d 76 00             	lea    0x0(%esi),%esi
    wakeup(&p->nwrite);
80103580:	83 ec 0c             	sub    $0xc,%esp
80103583:	8d 83 38 02 00 00    	lea    0x238(%ebx),%eax
    p->readopen = 0;
80103589:	c7 83 3c 02 00 00 00 	movl   $0x0,0x23c(%ebx)
80103590:	00 00 00 
    wakeup(&p->nwrite);
80103593:	50                   	push   %eax
80103594:	e8 67 0c 00 00       	call   80104200 <wakeup>
80103599:	83 c4 10             	add    $0x10,%esp
8010359c:	eb bd                	jmp    8010355b <pipeclose+0x3b>
8010359e:	66 90                	xchg   %ax,%ax
    release(&p->lock);
801035a0:	83 ec 0c             	sub    $0xc,%esp
801035a3:	53                   	push   %ebx
801035a4:	e8 87 11 00 00       	call   80104730 <release>
    kfree((char*)p);
801035a9:	89 5d 08             	mov    %ebx,0x8(%ebp)
801035ac:	83 c4 10             	add    $0x10,%esp
}
801035af:	8d 65 f8             	lea    -0x8(%ebp),%esp
801035b2:	5b                   	pop    %ebx
801035b3:	5e                   	pop    %esi
801035b4:	5d                   	pop    %ebp
    kfree((char*)p);
801035b5:	e9 d6 ee ff ff       	jmp    80102490 <kfree>
801035ba:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801035c0 <pipewrite>:

//PAGEBREAK: 40
int
pipewrite(struct pipe *p, char *addr, int n)
{
801035c0:	f3 0f 1e fb          	endbr32 
801035c4:	55                   	push   %ebp
801035c5:	89 e5                	mov    %esp,%ebp
801035c7:	57                   	push   %edi
801035c8:	56                   	push   %esi
801035c9:	53                   	push   %ebx
801035ca:	83 ec 28             	sub    $0x28,%esp
801035cd:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int i;

  acquire(&p->lock);
801035d0:	53                   	push   %ebx
801035d1:	e8 9a 10 00 00       	call   80104670 <acquire>
  for(i = 0; i < n; i++){
801035d6:	8b 45 10             	mov    0x10(%ebp),%eax
801035d9:	83 c4 10             	add    $0x10,%esp
801035dc:	85 c0                	test   %eax,%eax
801035de:	0f 8e bc 00 00 00    	jle    801036a0 <pipewrite+0xe0>
801035e4:	8b 45 0c             	mov    0xc(%ebp),%eax
801035e7:	8b 8b 38 02 00 00    	mov    0x238(%ebx),%ecx
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
      if(p->readopen == 0 || myproc()->killed){
        release(&p->lock);
        return -1;
      }
      wakeup(&p->nread);
801035ed:	8d bb 34 02 00 00    	lea    0x234(%ebx),%edi
801035f3:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801035f6:	03 45 10             	add    0x10(%ebp),%eax
801035f9:	89 45 e0             	mov    %eax,-0x20(%ebp)
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
801035fc:	8b 83 34 02 00 00    	mov    0x234(%ebx),%eax
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
80103602:	8d b3 38 02 00 00    	lea    0x238(%ebx),%esi
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80103608:	89 ca                	mov    %ecx,%edx
8010360a:	05 00 02 00 00       	add    $0x200,%eax
8010360f:	39 c1                	cmp    %eax,%ecx
80103611:	74 3b                	je     8010364e <pipewrite+0x8e>
80103613:	eb 63                	jmp    80103678 <pipewrite+0xb8>
80103615:	8d 76 00             	lea    0x0(%esi),%esi
      if(p->readopen == 0 || myproc()->killed){
80103618:	e8 e3 03 00 00       	call   80103a00 <myproc>
8010361d:	8b 48 24             	mov    0x24(%eax),%ecx
80103620:	85 c9                	test   %ecx,%ecx
80103622:	75 34                	jne    80103658 <pipewrite+0x98>
      wakeup(&p->nread);
80103624:	83 ec 0c             	sub    $0xc,%esp
80103627:	57                   	push   %edi
80103628:	e8 d3 0b 00 00       	call   80104200 <wakeup>
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
8010362d:	58                   	pop    %eax
8010362e:	5a                   	pop    %edx
8010362f:	53                   	push   %ebx
80103630:	56                   	push   %esi
80103631:	e8 ca 09 00 00       	call   80104000 <sleep>
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80103636:	8b 83 34 02 00 00    	mov    0x234(%ebx),%eax
8010363c:	8b 93 38 02 00 00    	mov    0x238(%ebx),%edx
80103642:	83 c4 10             	add    $0x10,%esp
80103645:	05 00 02 00 00       	add    $0x200,%eax
8010364a:	39 c2                	cmp    %eax,%edx
8010364c:	75 2a                	jne    80103678 <pipewrite+0xb8>
      if(p->readopen == 0 || myproc()->killed){
8010364e:	8b 83 3c 02 00 00    	mov    0x23c(%ebx),%eax
80103654:	85 c0                	test   %eax,%eax
80103656:	75 c0                	jne    80103618 <pipewrite+0x58>
        release(&p->lock);
80103658:	83 ec 0c             	sub    $0xc,%esp
8010365b:	53                   	push   %ebx
8010365c:	e8 cf 10 00 00       	call   80104730 <release>
        return -1;
80103661:	83 c4 10             	add    $0x10,%esp
80103664:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
  }
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
  release(&p->lock);
  return n;
}
80103669:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010366c:	5b                   	pop    %ebx
8010366d:	5e                   	pop    %esi
8010366e:	5f                   	pop    %edi
8010366f:	5d                   	pop    %ebp
80103670:	c3                   	ret    
80103671:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
80103678:	8b 75 e4             	mov    -0x1c(%ebp),%esi
8010367b:	8d 4a 01             	lea    0x1(%edx),%ecx
8010367e:	81 e2 ff 01 00 00    	and    $0x1ff,%edx
80103684:	89 8b 38 02 00 00    	mov    %ecx,0x238(%ebx)
8010368a:	0f b6 06             	movzbl (%esi),%eax
8010368d:	83 c6 01             	add    $0x1,%esi
80103690:	89 75 e4             	mov    %esi,-0x1c(%ebp)
80103693:	88 44 13 34          	mov    %al,0x34(%ebx,%edx,1)
  for(i = 0; i < n; i++){
80103697:	3b 75 e0             	cmp    -0x20(%ebp),%esi
8010369a:	0f 85 5c ff ff ff    	jne    801035fc <pipewrite+0x3c>
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
801036a0:	83 ec 0c             	sub    $0xc,%esp
801036a3:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
801036a9:	50                   	push   %eax
801036aa:	e8 51 0b 00 00       	call   80104200 <wakeup>
  release(&p->lock);
801036af:	89 1c 24             	mov    %ebx,(%esp)
801036b2:	e8 79 10 00 00       	call   80104730 <release>
  return n;
801036b7:	8b 45 10             	mov    0x10(%ebp),%eax
801036ba:	83 c4 10             	add    $0x10,%esp
801036bd:	eb aa                	jmp    80103669 <pipewrite+0xa9>
801036bf:	90                   	nop

801036c0 <piperead>:

int
piperead(struct pipe *p, char *addr, int n)
{
801036c0:	f3 0f 1e fb          	endbr32 
801036c4:	55                   	push   %ebp
801036c5:	89 e5                	mov    %esp,%ebp
801036c7:	57                   	push   %edi
801036c8:	56                   	push   %esi
801036c9:	53                   	push   %ebx
801036ca:	83 ec 18             	sub    $0x18,%esp
801036cd:	8b 75 08             	mov    0x8(%ebp),%esi
801036d0:	8b 7d 0c             	mov    0xc(%ebp),%edi
  int i;

  acquire(&p->lock);
801036d3:	56                   	push   %esi
801036d4:	8d 9e 34 02 00 00    	lea    0x234(%esi),%ebx
801036da:	e8 91 0f 00 00       	call   80104670 <acquire>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
801036df:	8b 86 34 02 00 00    	mov    0x234(%esi),%eax
801036e5:	83 c4 10             	add    $0x10,%esp
801036e8:	39 86 38 02 00 00    	cmp    %eax,0x238(%esi)
801036ee:	74 33                	je     80103723 <piperead+0x63>
801036f0:	eb 3b                	jmp    8010372d <piperead+0x6d>
801036f2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    if(myproc()->killed){
801036f8:	e8 03 03 00 00       	call   80103a00 <myproc>
801036fd:	8b 48 24             	mov    0x24(%eax),%ecx
80103700:	85 c9                	test   %ecx,%ecx
80103702:	0f 85 88 00 00 00    	jne    80103790 <piperead+0xd0>
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
80103708:	83 ec 08             	sub    $0x8,%esp
8010370b:	56                   	push   %esi
8010370c:	53                   	push   %ebx
8010370d:	e8 ee 08 00 00       	call   80104000 <sleep>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
80103712:	8b 86 38 02 00 00    	mov    0x238(%esi),%eax
80103718:	83 c4 10             	add    $0x10,%esp
8010371b:	39 86 34 02 00 00    	cmp    %eax,0x234(%esi)
80103721:	75 0a                	jne    8010372d <piperead+0x6d>
80103723:	8b 86 40 02 00 00    	mov    0x240(%esi),%eax
80103729:	85 c0                	test   %eax,%eax
8010372b:	75 cb                	jne    801036f8 <piperead+0x38>
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
8010372d:	8b 55 10             	mov    0x10(%ebp),%edx
80103730:	31 db                	xor    %ebx,%ebx
80103732:	85 d2                	test   %edx,%edx
80103734:	7f 28                	jg     8010375e <piperead+0x9e>
80103736:	eb 34                	jmp    8010376c <piperead+0xac>
80103738:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010373f:	90                   	nop
    if(p->nread == p->nwrite)
      break;
    addr[i] = p->data[p->nread++ % PIPESIZE];
80103740:	8d 48 01             	lea    0x1(%eax),%ecx
80103743:	25 ff 01 00 00       	and    $0x1ff,%eax
80103748:	89 8e 34 02 00 00    	mov    %ecx,0x234(%esi)
8010374e:	0f b6 44 06 34       	movzbl 0x34(%esi,%eax,1),%eax
80103753:	88 04 1f             	mov    %al,(%edi,%ebx,1)
  for(i = 0; i < n; i++){  //DOC: piperead-copy
80103756:	83 c3 01             	add    $0x1,%ebx
80103759:	39 5d 10             	cmp    %ebx,0x10(%ebp)
8010375c:	74 0e                	je     8010376c <piperead+0xac>
    if(p->nread == p->nwrite)
8010375e:	8b 86 34 02 00 00    	mov    0x234(%esi),%eax
80103764:	3b 86 38 02 00 00    	cmp    0x238(%esi),%eax
8010376a:	75 d4                	jne    80103740 <piperead+0x80>
  }
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
8010376c:	83 ec 0c             	sub    $0xc,%esp
8010376f:	8d 86 38 02 00 00    	lea    0x238(%esi),%eax
80103775:	50                   	push   %eax
80103776:	e8 85 0a 00 00       	call   80104200 <wakeup>
  release(&p->lock);
8010377b:	89 34 24             	mov    %esi,(%esp)
8010377e:	e8 ad 0f 00 00       	call   80104730 <release>
  return i;
80103783:	83 c4 10             	add    $0x10,%esp
}
80103786:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103789:	89 d8                	mov    %ebx,%eax
8010378b:	5b                   	pop    %ebx
8010378c:	5e                   	pop    %esi
8010378d:	5f                   	pop    %edi
8010378e:	5d                   	pop    %ebp
8010378f:	c3                   	ret    
      release(&p->lock);
80103790:	83 ec 0c             	sub    $0xc,%esp
      return -1;
80103793:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
      release(&p->lock);
80103798:	56                   	push   %esi
80103799:	e8 92 0f 00 00       	call   80104730 <release>
      return -1;
8010379e:	83 c4 10             	add    $0x10,%esp
}
801037a1:	8d 65 f4             	lea    -0xc(%ebp),%esp
801037a4:	89 d8                	mov    %ebx,%eax
801037a6:	5b                   	pop    %ebx
801037a7:	5e                   	pop    %esi
801037a8:	5f                   	pop    %edi
801037a9:	5d                   	pop    %ebp
801037aa:	c3                   	ret    
801037ab:	66 90                	xchg   %ax,%ax
801037ad:	66 90                	xchg   %ax,%ax
801037af:	90                   	nop

801037b0 <wakeup1>:
//PAGEBREAK!
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
801037b0:	55                   	push   %ebp
801037b1:	89 e5                	mov    %esp,%ebp
801037b3:	56                   	push   %esi
801037b4:	89 c6                	mov    %eax,%esi
801037b6:	53                   	push   %ebx
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801037b7:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
801037bc:	eb 0d                	jmp    801037cb <wakeup1+0x1b>
801037be:	66 90                	xchg   %ax,%ax
801037c0:	83 c3 7c             	add    $0x7c,%ebx
801037c3:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
801037c9:	74 30                	je     801037fb <wakeup1+0x4b>
    if(p->state == SLEEPING && p->chan == chan){
801037cb:	83 7b 0c 02          	cmpl   $0x2,0xc(%ebx)
801037cf:	75 ef                	jne    801037c0 <wakeup1+0x10>
801037d1:	39 73 20             	cmp    %esi,0x20(%ebx)
801037d4:	75 ea                	jne    801037c0 <wakeup1+0x10>
      p->state = RUNNABLE;
      cprintf("xv6 : wakeup1() :pid %d - SLEEPING -> RUNNABLE\n" ,p->pid);
801037d6:	83 ec 08             	sub    $0x8,%esp
801037d9:	ff 73 10             	pushl  0x10(%ebx)
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801037dc:	83 c3 7c             	add    $0x7c,%ebx
      cprintf("xv6 : wakeup1() :pid %d - SLEEPING -> RUNNABLE\n" ,p->pid);
801037df:	68 40 77 10 80       	push   $0x80107740
      p->state = RUNNABLE;
801037e4:	c7 43 90 03 00 00 00 	movl   $0x3,-0x70(%ebx)
      cprintf("xv6 : wakeup1() :pid %d - SLEEPING -> RUNNABLE\n" ,p->pid);
801037eb:	e8 c0 ce ff ff       	call   801006b0 <cprintf>
801037f0:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801037f3:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
801037f9:	75 d0                	jne    801037cb <wakeup1+0x1b>
      }
}
801037fb:	8d 65 f8             	lea    -0x8(%ebp),%esp
801037fe:	5b                   	pop    %ebx
801037ff:	5e                   	pop    %esi
80103800:	5d                   	pop    %ebp
80103801:	c3                   	ret    
80103802:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103809:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80103810 <allocproc>:
{
80103810:	55                   	push   %ebp
80103811:	89 e5                	mov    %esp,%ebp
80103813:	53                   	push   %ebx
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103814:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
{
80103819:	83 ec 10             	sub    $0x10,%esp
  acquire(&ptable.lock);
8010381c:	68 20 2d 11 80       	push   $0x80112d20
80103821:	e8 4a 0e 00 00       	call   80104670 <acquire>
80103826:	83 c4 10             	add    $0x10,%esp
80103829:	eb 14                	jmp    8010383f <allocproc+0x2f>
8010382b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010382f:	90                   	nop
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103830:	83 c3 7c             	add    $0x7c,%ebx
80103833:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
80103839:	0f 84 81 00 00 00    	je     801038c0 <allocproc+0xb0>
    if(p->state == UNUSED)
8010383f:	8b 43 0c             	mov    0xc(%ebx),%eax
80103842:	85 c0                	test   %eax,%eax
80103844:	75 ea                	jne    80103830 <allocproc+0x20>
  p->pid = nextpid++;
80103846:	a1 04 a0 10 80       	mov    0x8010a004,%eax
	cprintf("xv6 : allocproc() :pid %d - UNUSED	-> EMBRYO\n",p->pid);
8010384b:	83 ec 08             	sub    $0x8,%esp
  p->state = EMBRYO;
8010384e:	c7 43 0c 01 00 00 00 	movl   $0x1,0xc(%ebx)
  p->pid = nextpid++;
80103855:	89 43 10             	mov    %eax,0x10(%ebx)
80103858:	8d 50 01             	lea    0x1(%eax),%edx
	cprintf("xv6 : allocproc() :pid %d - UNUSED	-> EMBRYO\n",p->pid);
8010385b:	50                   	push   %eax
8010385c:	68 70 77 10 80       	push   $0x80107770
  p->pid = nextpid++;
80103861:	89 15 04 a0 10 80    	mov    %edx,0x8010a004
	cprintf("xv6 : allocproc() :pid %d - UNUSED	-> EMBRYO\n",p->pid);
80103867:	e8 44 ce ff ff       	call   801006b0 <cprintf>
  release(&ptable.lock);
8010386c:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103873:	e8 b8 0e 00 00       	call   80104730 <release>
  if((p->kstack = kalloc()) == 0){
80103878:	e8 d3 ed ff ff       	call   80102650 <kalloc>
8010387d:	83 c4 10             	add    $0x10,%esp
80103880:	89 43 08             	mov    %eax,0x8(%ebx)
80103883:	85 c0                	test   %eax,%eax
80103885:	74 52                	je     801038d9 <allocproc+0xc9>
  sp -= sizeof *p->tf;
80103887:	8d 90 b4 0f 00 00    	lea    0xfb4(%eax),%edx
  memset(p->context, 0, sizeof *p->context);
8010388d:	83 ec 04             	sub    $0x4,%esp
  sp -= sizeof *p->context;
80103890:	05 9c 0f 00 00       	add    $0xf9c,%eax
  sp -= sizeof *p->tf;
80103895:	89 53 18             	mov    %edx,0x18(%ebx)
  *(uint*)sp = (uint)trapret;
80103898:	c7 40 14 96 59 10 80 	movl   $0x80105996,0x14(%eax)
  p->context = (struct context*)sp;
8010389f:	89 43 1c             	mov    %eax,0x1c(%ebx)
  memset(p->context, 0, sizeof *p->context);
801038a2:	6a 14                	push   $0x14
801038a4:	6a 00                	push   $0x0
801038a6:	50                   	push   %eax
801038a7:	e8 d4 0e 00 00       	call   80104780 <memset>
  p->context->eip = (uint)forkret;
801038ac:	8b 43 1c             	mov    0x1c(%ebx),%eax
  return p;
801038af:	83 c4 10             	add    $0x10,%esp
  p->context->eip = (uint)forkret;
801038b2:	c7 40 10 00 39 10 80 	movl   $0x80103900,0x10(%eax)
}
801038b9:	89 d8                	mov    %ebx,%eax
801038bb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801038be:	c9                   	leave  
801038bf:	c3                   	ret    
  release(&ptable.lock);
801038c0:	83 ec 0c             	sub    $0xc,%esp
  return 0;
801038c3:	31 db                	xor    %ebx,%ebx
  release(&ptable.lock);
801038c5:	68 20 2d 11 80       	push   $0x80112d20
801038ca:	e8 61 0e 00 00       	call   80104730 <release>
}
801038cf:	89 d8                	mov    %ebx,%eax
  return 0;
801038d1:	83 c4 10             	add    $0x10,%esp
}
801038d4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801038d7:	c9                   	leave  
801038d8:	c3                   	ret    
  	cprintf("xv6 : allocproc() :pid %d - EMBRYO	-> UNUSED\n",p->pid);
801038d9:	83 ec 08             	sub    $0x8,%esp
801038dc:	ff 73 10             	pushl  0x10(%ebx)
801038df:	68 a0 77 10 80       	push   $0x801077a0
801038e4:	e8 c7 cd ff ff       	call   801006b0 <cprintf>
    p->state = UNUSED;
801038e9:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
    return 0;
801038f0:	83 c4 10             	add    $0x10,%esp
801038f3:	31 db                	xor    %ebx,%ebx
801038f5:	eb c2                	jmp    801038b9 <allocproc+0xa9>
801038f7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801038fe:	66 90                	xchg   %ax,%ax

80103900 <forkret>:
{
80103900:	f3 0f 1e fb          	endbr32 
80103904:	55                   	push   %ebp
80103905:	89 e5                	mov    %esp,%ebp
80103907:	83 ec 14             	sub    $0x14,%esp
  release(&ptable.lock);
8010390a:	68 20 2d 11 80       	push   $0x80112d20
8010390f:	e8 1c 0e 00 00       	call   80104730 <release>
  if (first) {
80103914:	a1 00 a0 10 80       	mov    0x8010a000,%eax
80103919:	83 c4 10             	add    $0x10,%esp
8010391c:	85 c0                	test   %eax,%eax
8010391e:	75 08                	jne    80103928 <forkret+0x28>
}
80103920:	c9                   	leave  
80103921:	c3                   	ret    
80103922:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    first = 0;
80103928:	c7 05 00 a0 10 80 00 	movl   $0x0,0x8010a000
8010392f:	00 00 00 
    iinit(ROOTDEV);
80103932:	83 ec 0c             	sub    $0xc,%esp
80103935:	6a 01                	push   $0x1
80103937:	e8 24 dc ff ff       	call   80101560 <iinit>
    initlog(ROOTDEV);
8010393c:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80103943:	e8 68 f3 ff ff       	call   80102cb0 <initlog>
}
80103948:	83 c4 10             	add    $0x10,%esp
8010394b:	c9                   	leave  
8010394c:	c3                   	ret    
8010394d:	8d 76 00             	lea    0x0(%esi),%esi

80103950 <pinit>:
{
80103950:	f3 0f 1e fb          	endbr32 
80103954:	55                   	push   %ebp
80103955:	89 e5                	mov    %esp,%ebp
80103957:	83 ec 10             	sub    $0x10,%esp
  initlock(&ptable.lock, "ptable");
8010395a:	68 71 79 10 80       	push   $0x80107971
8010395f:	68 20 2d 11 80       	push   $0x80112d20
80103964:	e8 87 0b 00 00       	call   801044f0 <initlock>
}
80103969:	83 c4 10             	add    $0x10,%esp
8010396c:	c9                   	leave  
8010396d:	c3                   	ret    
8010396e:	66 90                	xchg   %ax,%ax

80103970 <mycpu>:
{
80103970:	f3 0f 1e fb          	endbr32 
80103974:	55                   	push   %ebp
80103975:	89 e5                	mov    %esp,%ebp
80103977:	56                   	push   %esi
80103978:	53                   	push   %ebx
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80103979:	9c                   	pushf  
8010397a:	58                   	pop    %eax
  if(readeflags()&FL_IF)
8010397b:	f6 c4 02             	test   $0x2,%ah
8010397e:	75 4a                	jne    801039ca <mycpu+0x5a>
  apicid = lapicid();
80103980:	e8 3b ef ff ff       	call   801028c0 <lapicid>
  for (i = 0; i < ncpu; ++i) {
80103985:	8b 35 00 2d 11 80    	mov    0x80112d00,%esi
  apicid = lapicid();
8010398b:	89 c3                	mov    %eax,%ebx
  for (i = 0; i < ncpu; ++i) {
8010398d:	85 f6                	test   %esi,%esi
8010398f:	7e 2c                	jle    801039bd <mycpu+0x4d>
80103991:	31 d2                	xor    %edx,%edx
80103993:	eb 0a                	jmp    8010399f <mycpu+0x2f>
80103995:	8d 76 00             	lea    0x0(%esi),%esi
80103998:	83 c2 01             	add    $0x1,%edx
8010399b:	39 f2                	cmp    %esi,%edx
8010399d:	74 1e                	je     801039bd <mycpu+0x4d>
    if (cpus[i].apicid == apicid)
8010399f:	69 ca b0 00 00 00    	imul   $0xb0,%edx,%ecx
801039a5:	0f b6 81 80 27 11 80 	movzbl -0x7feed880(%ecx),%eax
801039ac:	39 d8                	cmp    %ebx,%eax
801039ae:	75 e8                	jne    80103998 <mycpu+0x28>
}
801039b0:	8d 65 f8             	lea    -0x8(%ebp),%esp
      return &cpus[i];
801039b3:	8d 81 80 27 11 80    	lea    -0x7feed880(%ecx),%eax
}
801039b9:	5b                   	pop    %ebx
801039ba:	5e                   	pop    %esi
801039bb:	5d                   	pop    %ebp
801039bc:	c3                   	ret    
  panic("unknown apicid\n");
801039bd:	83 ec 0c             	sub    $0xc,%esp
801039c0:	68 78 79 10 80       	push   $0x80107978
801039c5:	e8 c6 c9 ff ff       	call   80100390 <panic>
    panic("mycpu called with interrupts enabled\n");
801039ca:	83 ec 0c             	sub    $0xc,%esp
801039cd:	68 d0 77 10 80       	push   $0x801077d0
801039d2:	e8 b9 c9 ff ff       	call   80100390 <panic>
801039d7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801039de:	66 90                	xchg   %ax,%ax

801039e0 <cpuid>:
cpuid() {
801039e0:	f3 0f 1e fb          	endbr32 
801039e4:	55                   	push   %ebp
801039e5:	89 e5                	mov    %esp,%ebp
801039e7:	83 ec 08             	sub    $0x8,%esp
  return mycpu()-cpus;
801039ea:	e8 81 ff ff ff       	call   80103970 <mycpu>
}
801039ef:	c9                   	leave  
  return mycpu()-cpus;
801039f0:	2d 80 27 11 80       	sub    $0x80112780,%eax
801039f5:	c1 f8 04             	sar    $0x4,%eax
801039f8:	69 c0 a3 8b 2e ba    	imul   $0xba2e8ba3,%eax,%eax
}
801039fe:	c3                   	ret    
801039ff:	90                   	nop

80103a00 <myproc>:
myproc(void) {
80103a00:	f3 0f 1e fb          	endbr32 
80103a04:	55                   	push   %ebp
80103a05:	89 e5                	mov    %esp,%ebp
80103a07:	53                   	push   %ebx
80103a08:	83 ec 04             	sub    $0x4,%esp
  pushcli();
80103a0b:	e8 60 0b 00 00       	call   80104570 <pushcli>
  c = mycpu();
80103a10:	e8 5b ff ff ff       	call   80103970 <mycpu>
  p = c->proc;
80103a15:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103a1b:	e8 a0 0b 00 00       	call   801045c0 <popcli>
}
80103a20:	83 c4 04             	add    $0x4,%esp
80103a23:	89 d8                	mov    %ebx,%eax
80103a25:	5b                   	pop    %ebx
80103a26:	5d                   	pop    %ebp
80103a27:	c3                   	ret    
80103a28:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103a2f:	90                   	nop

80103a30 <userinit>:
{
80103a30:	f3 0f 1e fb          	endbr32 
80103a34:	55                   	push   %ebp
80103a35:	89 e5                	mov    %esp,%ebp
80103a37:	53                   	push   %ebx
80103a38:	83 ec 04             	sub    $0x4,%esp
  p = allocproc();
80103a3b:	e8 d0 fd ff ff       	call   80103810 <allocproc>
80103a40:	89 c3                	mov    %eax,%ebx
  initproc = p;
80103a42:	a3 b8 a5 10 80       	mov    %eax,0x8010a5b8
  if((p->pgdir = setupkvm()) == 0)
80103a47:	e8 14 35 00 00       	call   80106f60 <setupkvm>
80103a4c:	89 43 04             	mov    %eax,0x4(%ebx)
80103a4f:	85 c0                	test   %eax,%eax
80103a51:	0f 84 cc 00 00 00    	je     80103b23 <userinit+0xf3>
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
80103a57:	83 ec 04             	sub    $0x4,%esp
80103a5a:	68 2c 00 00 00       	push   $0x2c
80103a5f:	68 60 a4 10 80       	push   $0x8010a460
80103a64:	50                   	push   %eax
80103a65:	e8 c6 31 00 00       	call   80106c30 <inituvm>
  memset(p->tf, 0, sizeof(*p->tf));
80103a6a:	83 c4 0c             	add    $0xc,%esp
  p->sz = PGSIZE;
80103a6d:	c7 03 00 10 00 00    	movl   $0x1000,(%ebx)
  memset(p->tf, 0, sizeof(*p->tf));
80103a73:	6a 4c                	push   $0x4c
80103a75:	6a 00                	push   $0x0
80103a77:	ff 73 18             	pushl  0x18(%ebx)
80103a7a:	e8 01 0d 00 00       	call   80104780 <memset>
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
80103a7f:	8b 43 18             	mov    0x18(%ebx),%eax
80103a82:	ba 1b 00 00 00       	mov    $0x1b,%edx
  safestrcpy(p->name, "initcode", sizeof(p->name));
80103a87:	83 c4 0c             	add    $0xc,%esp
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
80103a8a:	b9 23 00 00 00       	mov    $0x23,%ecx
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
80103a8f:	66 89 50 3c          	mov    %dx,0x3c(%eax)
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
80103a93:	8b 43 18             	mov    0x18(%ebx),%eax
80103a96:	66 89 48 2c          	mov    %cx,0x2c(%eax)
  p->tf->es = p->tf->ds;
80103a9a:	8b 43 18             	mov    0x18(%ebx),%eax
80103a9d:	0f b7 50 2c          	movzwl 0x2c(%eax),%edx
80103aa1:	66 89 50 28          	mov    %dx,0x28(%eax)
  p->tf->ss = p->tf->ds;
80103aa5:	8b 43 18             	mov    0x18(%ebx),%eax
80103aa8:	0f b7 50 2c          	movzwl 0x2c(%eax),%edx
80103aac:	66 89 50 48          	mov    %dx,0x48(%eax)
  p->tf->eflags = FL_IF;
80103ab0:	8b 43 18             	mov    0x18(%ebx),%eax
80103ab3:	c7 40 40 00 02 00 00 	movl   $0x200,0x40(%eax)
  p->tf->esp = PGSIZE;
80103aba:	8b 43 18             	mov    0x18(%ebx),%eax
80103abd:	c7 40 44 00 10 00 00 	movl   $0x1000,0x44(%eax)
  p->tf->eip = 0;  // beginning of initcode.S
80103ac4:	8b 43 18             	mov    0x18(%ebx),%eax
80103ac7:	c7 40 38 00 00 00 00 	movl   $0x0,0x38(%eax)
  safestrcpy(p->name, "initcode", sizeof(p->name));
80103ace:	8d 43 6c             	lea    0x6c(%ebx),%eax
80103ad1:	6a 10                	push   $0x10
80103ad3:	68 a1 79 10 80       	push   $0x801079a1
80103ad8:	50                   	push   %eax
80103ad9:	e8 62 0e 00 00       	call   80104940 <safestrcpy>
  p->cwd = namei("/");
80103ade:	c7 04 24 aa 79 10 80 	movl   $0x801079aa,(%esp)
80103ae5:	e8 66 e5 ff ff       	call   80102050 <namei>
80103aea:	89 43 68             	mov    %eax,0x68(%ebx)
  acquire(&ptable.lock);
80103aed:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103af4:	e8 77 0b 00 00       	call   80104670 <acquire>
  p->state = RUNNABLE;
80103af9:	c7 43 0c 03 00 00 00 	movl   $0x3,0xc(%ebx)
	cprintf("xv6 : Userinit() :pid %d - EMBRYO -> RUNNABLE\n",p->pid);
80103b00:	58                   	pop    %eax
80103b01:	5a                   	pop    %edx
80103b02:	ff 73 10             	pushl  0x10(%ebx)
80103b05:	68 f8 77 10 80       	push   $0x801077f8
80103b0a:	e8 a1 cb ff ff       	call   801006b0 <cprintf>
  release(&ptable.lock);
80103b0f:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103b16:	e8 15 0c 00 00       	call   80104730 <release>
}
80103b1b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103b1e:	83 c4 10             	add    $0x10,%esp
80103b21:	c9                   	leave  
80103b22:	c3                   	ret    
    panic("userinit: out of memory?");
80103b23:	83 ec 0c             	sub    $0xc,%esp
80103b26:	68 88 79 10 80       	push   $0x80107988
80103b2b:	e8 60 c8 ff ff       	call   80100390 <panic>

80103b30 <growproc>:
{
80103b30:	f3 0f 1e fb          	endbr32 
80103b34:	55                   	push   %ebp
80103b35:	89 e5                	mov    %esp,%ebp
80103b37:	56                   	push   %esi
80103b38:	53                   	push   %ebx
80103b39:	8b 75 08             	mov    0x8(%ebp),%esi
  pushcli();
80103b3c:	e8 2f 0a 00 00       	call   80104570 <pushcli>
  c = mycpu();
80103b41:	e8 2a fe ff ff       	call   80103970 <mycpu>
  p = c->proc;
80103b46:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103b4c:	e8 6f 0a 00 00       	call   801045c0 <popcli>
  sz = curproc->sz;
80103b51:	8b 03                	mov    (%ebx),%eax
  if(n > 0){
80103b53:	85 f6                	test   %esi,%esi
80103b55:	7f 19                	jg     80103b70 <growproc+0x40>
  } else if(n < 0){
80103b57:	75 37                	jne    80103b90 <growproc+0x60>
  switchuvm(curproc);
80103b59:	83 ec 0c             	sub    $0xc,%esp
  curproc->sz = sz;
80103b5c:	89 03                	mov    %eax,(%ebx)
  switchuvm(curproc);
80103b5e:	53                   	push   %ebx
80103b5f:	e8 bc 2f 00 00       	call   80106b20 <switchuvm>
  return 0;
80103b64:	83 c4 10             	add    $0x10,%esp
80103b67:	31 c0                	xor    %eax,%eax
}
80103b69:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103b6c:	5b                   	pop    %ebx
80103b6d:	5e                   	pop    %esi
80103b6e:	5d                   	pop    %ebp
80103b6f:	c3                   	ret    
    if((sz = allocuvm(curproc->pgdir, sz, sz + n)) == 0)
80103b70:	83 ec 04             	sub    $0x4,%esp
80103b73:	01 c6                	add    %eax,%esi
80103b75:	56                   	push   %esi
80103b76:	50                   	push   %eax
80103b77:	ff 73 04             	pushl  0x4(%ebx)
80103b7a:	e8 01 32 00 00       	call   80106d80 <allocuvm>
80103b7f:	83 c4 10             	add    $0x10,%esp
80103b82:	85 c0                	test   %eax,%eax
80103b84:	75 d3                	jne    80103b59 <growproc+0x29>
      return -1;
80103b86:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80103b8b:	eb dc                	jmp    80103b69 <growproc+0x39>
80103b8d:	8d 76 00             	lea    0x0(%esi),%esi
    if((sz = deallocuvm(curproc->pgdir, sz, sz + n)) == 0)
80103b90:	83 ec 04             	sub    $0x4,%esp
80103b93:	01 c6                	add    %eax,%esi
80103b95:	56                   	push   %esi
80103b96:	50                   	push   %eax
80103b97:	ff 73 04             	pushl  0x4(%ebx)
80103b9a:	e8 11 33 00 00       	call   80106eb0 <deallocuvm>
80103b9f:	83 c4 10             	add    $0x10,%esp
80103ba2:	85 c0                	test   %eax,%eax
80103ba4:	75 b3                	jne    80103b59 <growproc+0x29>
80103ba6:	eb de                	jmp    80103b86 <growproc+0x56>
80103ba8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103baf:	90                   	nop

80103bb0 <fork>:
{
80103bb0:	f3 0f 1e fb          	endbr32 
80103bb4:	55                   	push   %ebp
80103bb5:	89 e5                	mov    %esp,%ebp
80103bb7:	57                   	push   %edi
80103bb8:	56                   	push   %esi
80103bb9:	53                   	push   %ebx
80103bba:	83 ec 1c             	sub    $0x1c,%esp
  pushcli();
80103bbd:	e8 ae 09 00 00       	call   80104570 <pushcli>
  c = mycpu();
80103bc2:	e8 a9 fd ff ff       	call   80103970 <mycpu>
  p = c->proc;
80103bc7:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103bcd:	e8 ee 09 00 00       	call   801045c0 <popcli>
  if((np = allocproc()) == 0){
80103bd2:	e8 39 fc ff ff       	call   80103810 <allocproc>
80103bd7:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80103bda:	85 c0                	test   %eax,%eax
80103bdc:	0f 84 ca 00 00 00    	je     80103cac <fork+0xfc>
  if((np->pgdir = copyuvm(curproc->pgdir, curproc->sz)) == 0){
80103be2:	83 ec 08             	sub    $0x8,%esp
80103be5:	ff 33                	pushl  (%ebx)
80103be7:	89 c7                	mov    %eax,%edi
80103be9:	ff 73 04             	pushl  0x4(%ebx)
80103bec:	e8 3f 34 00 00       	call   80107030 <copyuvm>
80103bf1:	83 c4 10             	add    $0x10,%esp
80103bf4:	89 47 04             	mov    %eax,0x4(%edi)
80103bf7:	85 c0                	test   %eax,%eax
80103bf9:	0f 84 b4 00 00 00    	je     80103cb3 <fork+0x103>
  np->sz = curproc->sz;
80103bff:	8b 03                	mov    (%ebx),%eax
80103c01:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80103c04:	89 01                	mov    %eax,(%ecx)
  *np->tf = *curproc->tf;
80103c06:	8b 79 18             	mov    0x18(%ecx),%edi
  np->parent = curproc;
80103c09:	89 c8                	mov    %ecx,%eax
80103c0b:	89 59 14             	mov    %ebx,0x14(%ecx)
  *np->tf = *curproc->tf;
80103c0e:	b9 13 00 00 00       	mov    $0x13,%ecx
80103c13:	8b 73 18             	mov    0x18(%ebx),%esi
80103c16:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  for(i = 0; i < NOFILE; i++)
80103c18:	31 f6                	xor    %esi,%esi
  np->tf->eax = 0;
80103c1a:	8b 40 18             	mov    0x18(%eax),%eax
80103c1d:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)
  for(i = 0; i < NOFILE; i++)
80103c24:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(curproc->ofile[i])
80103c28:	8b 44 b3 28          	mov    0x28(%ebx,%esi,4),%eax
80103c2c:	85 c0                	test   %eax,%eax
80103c2e:	74 13                	je     80103c43 <fork+0x93>
      np->ofile[i] = filedup(curproc->ofile[i]);
80103c30:	83 ec 0c             	sub    $0xc,%esp
80103c33:	50                   	push   %eax
80103c34:	e8 57 d2 ff ff       	call   80100e90 <filedup>
80103c39:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80103c3c:	83 c4 10             	add    $0x10,%esp
80103c3f:	89 44 b2 28          	mov    %eax,0x28(%edx,%esi,4)
  for(i = 0; i < NOFILE; i++)
80103c43:	83 c6 01             	add    $0x1,%esi
80103c46:	83 fe 10             	cmp    $0x10,%esi
80103c49:	75 dd                	jne    80103c28 <fork+0x78>
  np->cwd = idup(curproc->cwd);
80103c4b:	83 ec 0c             	sub    $0xc,%esp
80103c4e:	ff 73 68             	pushl  0x68(%ebx)
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80103c51:	83 c3 6c             	add    $0x6c,%ebx
  np->cwd = idup(curproc->cwd);
80103c54:	e8 f7 da ff ff       	call   80101750 <idup>
80103c59:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80103c5c:	83 c4 0c             	add    $0xc,%esp
  np->cwd = idup(curproc->cwd);
80103c5f:	89 47 68             	mov    %eax,0x68(%edi)
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80103c62:	8d 47 6c             	lea    0x6c(%edi),%eax
80103c65:	6a 10                	push   $0x10
80103c67:	53                   	push   %ebx
80103c68:	50                   	push   %eax
80103c69:	e8 d2 0c 00 00       	call   80104940 <safestrcpy>
  pid = np->pid;
80103c6e:	8b 5f 10             	mov    0x10(%edi),%ebx
  acquire(&ptable.lock);
80103c71:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103c78:	e8 f3 09 00 00       	call   80104670 <acquire>
  np->state = RUNNABLE;
80103c7d:	c7 47 0c 03 00 00 00 	movl   $0x3,0xc(%edi)
	cprintf("xv6 : fork() :pid %d - EMBRYO -> RUNNABLE\n", np->pid);
80103c84:	58                   	pop    %eax
80103c85:	5a                   	pop    %edx
80103c86:	ff 77 10             	pushl  0x10(%edi)
80103c89:	68 28 78 10 80       	push   $0x80107828
80103c8e:	e8 1d ca ff ff       	call   801006b0 <cprintf>
  release(&ptable.lock);
80103c93:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103c9a:	e8 91 0a 00 00       	call   80104730 <release>
  return pid;
80103c9f:	83 c4 10             	add    $0x10,%esp
}
80103ca2:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103ca5:	89 d8                	mov    %ebx,%eax
80103ca7:	5b                   	pop    %ebx
80103ca8:	5e                   	pop    %esi
80103ca9:	5f                   	pop    %edi
80103caa:	5d                   	pop    %ebp
80103cab:	c3                   	ret    
    return -1;
80103cac:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80103cb1:	eb ef                	jmp    80103ca2 <fork+0xf2>
    kfree(np->kstack);
80103cb3:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
80103cb6:	83 ec 0c             	sub    $0xc,%esp
80103cb9:	ff 73 08             	pushl  0x8(%ebx)
80103cbc:	e8 cf e7 ff ff       	call   80102490 <kfree>
    np->kstack = 0;
80103cc1:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
    return -1;
80103cc8:	83 c4 10             	add    $0x10,%esp
    np->state = UNUSED;
80103ccb:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
    return -1;
80103cd2:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80103cd7:	eb c9                	jmp    80103ca2 <fork+0xf2>
80103cd9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80103ce0 <scheduler>:
{
80103ce0:	f3 0f 1e fb          	endbr32 
80103ce4:	55                   	push   %ebp
80103ce5:	89 e5                	mov    %esp,%ebp
80103ce7:	57                   	push   %edi
80103ce8:	56                   	push   %esi
80103ce9:	53                   	push   %ebx
80103cea:	83 ec 0c             	sub    $0xc,%esp
  struct cpu *c = mycpu();
80103ced:	e8 7e fc ff ff       	call   80103970 <mycpu>
  c->proc = 0;
80103cf2:	c7 80 ac 00 00 00 00 	movl   $0x0,0xac(%eax)
80103cf9:	00 00 00 
  struct cpu *c = mycpu();
80103cfc:	89 c6                	mov    %eax,%esi
  c->proc = 0;
80103cfe:	8d 78 04             	lea    0x4(%eax),%edi
80103d01:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  asm volatile("sti");
80103d08:	fb                   	sti    
    acquire(&ptable.lock);
80103d09:	83 ec 0c             	sub    $0xc,%esp
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103d0c:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
    acquire(&ptable.lock);
80103d11:	68 20 2d 11 80       	push   $0x80112d20
80103d16:	e8 55 09 00 00       	call   80104670 <acquire>
80103d1b:	83 c4 10             	add    $0x10,%esp
80103d1e:	66 90                	xchg   %ax,%ax
      if(p->state != RUNNABLE)
80103d20:	83 7b 0c 03          	cmpl   $0x3,0xc(%ebx)
80103d24:	75 42                	jne    80103d68 <scheduler+0x88>
      switchuvm(p);
80103d26:	83 ec 0c             	sub    $0xc,%esp
      c->proc = p;
80103d29:	89 9e ac 00 00 00    	mov    %ebx,0xac(%esi)
      switchuvm(p);
80103d2f:	53                   	push   %ebx
80103d30:	e8 eb 2d 00 00       	call   80106b20 <switchuvm>
	cprintf("xv6 : scheduler() :pid %d - RUNNABLE -> RUNNING\n", p->pid);
80103d35:	58                   	pop    %eax
80103d36:	5a                   	pop    %edx
80103d37:	ff 73 10             	pushl  0x10(%ebx)
80103d3a:	68 54 78 10 80       	push   $0x80107854
      p->state = RUNNING;
80103d3f:	c7 43 0c 04 00 00 00 	movl   $0x4,0xc(%ebx)
	cprintf("xv6 : scheduler() :pid %d - RUNNABLE -> RUNNING\n", p->pid);
80103d46:	e8 65 c9 ff ff       	call   801006b0 <cprintf>
      swtch(&(c->scheduler), p->context);
80103d4b:	59                   	pop    %ecx
80103d4c:	58                   	pop    %eax
80103d4d:	ff 73 1c             	pushl  0x1c(%ebx)
80103d50:	57                   	push   %edi
80103d51:	e8 4d 0c 00 00       	call   801049a3 <swtch>
      switchkvm();
80103d56:	e8 a5 2d 00 00       	call   80106b00 <switchkvm>
      c->proc = 0;
80103d5b:	83 c4 10             	add    $0x10,%esp
80103d5e:	c7 86 ac 00 00 00 00 	movl   $0x0,0xac(%esi)
80103d65:	00 00 00 
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103d68:	83 c3 7c             	add    $0x7c,%ebx
80103d6b:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
80103d71:	75 ad                	jne    80103d20 <scheduler+0x40>
    release(&ptable.lock);
80103d73:	83 ec 0c             	sub    $0xc,%esp
80103d76:	68 20 2d 11 80       	push   $0x80112d20
80103d7b:	e8 b0 09 00 00       	call   80104730 <release>
    sti();
80103d80:	83 c4 10             	add    $0x10,%esp
80103d83:	eb 83                	jmp    80103d08 <scheduler+0x28>
80103d85:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103d8c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80103d90 <sched>:
{
80103d90:	f3 0f 1e fb          	endbr32 
80103d94:	55                   	push   %ebp
80103d95:	89 e5                	mov    %esp,%ebp
80103d97:	56                   	push   %esi
80103d98:	53                   	push   %ebx
  pushcli();
80103d99:	e8 d2 07 00 00       	call   80104570 <pushcli>
  c = mycpu();
80103d9e:	e8 cd fb ff ff       	call   80103970 <mycpu>
  p = c->proc;
80103da3:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103da9:	e8 12 08 00 00       	call   801045c0 <popcli>
  if(!holding(&ptable.lock))
80103dae:	83 ec 0c             	sub    $0xc,%esp
80103db1:	68 20 2d 11 80       	push   $0x80112d20
80103db6:	e8 65 08 00 00       	call   80104620 <holding>
80103dbb:	83 c4 10             	add    $0x10,%esp
80103dbe:	85 c0                	test   %eax,%eax
80103dc0:	74 4f                	je     80103e11 <sched+0x81>
  if(mycpu()->ncli != 1)
80103dc2:	e8 a9 fb ff ff       	call   80103970 <mycpu>
80103dc7:	83 b8 a4 00 00 00 01 	cmpl   $0x1,0xa4(%eax)
80103dce:	75 68                	jne    80103e38 <sched+0xa8>
  if(p->state == RUNNING)
80103dd0:	83 7b 0c 04          	cmpl   $0x4,0xc(%ebx)
80103dd4:	74 55                	je     80103e2b <sched+0x9b>
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80103dd6:	9c                   	pushf  
80103dd7:	58                   	pop    %eax
  if(readeflags()&FL_IF)
80103dd8:	f6 c4 02             	test   $0x2,%ah
80103ddb:	75 41                	jne    80103e1e <sched+0x8e>
  intena = mycpu()->intena;
80103ddd:	e8 8e fb ff ff       	call   80103970 <mycpu>
  swtch(&p->context, mycpu()->scheduler);
80103de2:	83 c3 1c             	add    $0x1c,%ebx
  intena = mycpu()->intena;
80103de5:	8b b0 a8 00 00 00    	mov    0xa8(%eax),%esi
  swtch(&p->context, mycpu()->scheduler);
80103deb:	e8 80 fb ff ff       	call   80103970 <mycpu>
80103df0:	83 ec 08             	sub    $0x8,%esp
80103df3:	ff 70 04             	pushl  0x4(%eax)
80103df6:	53                   	push   %ebx
80103df7:	e8 a7 0b 00 00       	call   801049a3 <swtch>
  mycpu()->intena = intena;
80103dfc:	e8 6f fb ff ff       	call   80103970 <mycpu>
}
80103e01:	83 c4 10             	add    $0x10,%esp
  mycpu()->intena = intena;
80103e04:	89 b0 a8 00 00 00    	mov    %esi,0xa8(%eax)
}
80103e0a:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103e0d:	5b                   	pop    %ebx
80103e0e:	5e                   	pop    %esi
80103e0f:	5d                   	pop    %ebp
80103e10:	c3                   	ret    
    panic("sched ptable.lock");
80103e11:	83 ec 0c             	sub    $0xc,%esp
80103e14:	68 ac 79 10 80       	push   $0x801079ac
80103e19:	e8 72 c5 ff ff       	call   80100390 <panic>
    panic("sched interruptible");
80103e1e:	83 ec 0c             	sub    $0xc,%esp
80103e21:	68 d8 79 10 80       	push   $0x801079d8
80103e26:	e8 65 c5 ff ff       	call   80100390 <panic>
    panic("sched running");
80103e2b:	83 ec 0c             	sub    $0xc,%esp
80103e2e:	68 ca 79 10 80       	push   $0x801079ca
80103e33:	e8 58 c5 ff ff       	call   80100390 <panic>
    panic("sched locks");
80103e38:	83 ec 0c             	sub    $0xc,%esp
80103e3b:	68 be 79 10 80       	push   $0x801079be
80103e40:	e8 4b c5 ff ff       	call   80100390 <panic>
80103e45:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103e4c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80103e50 <exit>:
{
80103e50:	f3 0f 1e fb          	endbr32 
80103e54:	55                   	push   %ebp
80103e55:	89 e5                	mov    %esp,%ebp
80103e57:	57                   	push   %edi
80103e58:	56                   	push   %esi
80103e59:	53                   	push   %ebx
80103e5a:	83 ec 0c             	sub    $0xc,%esp
  pushcli();
80103e5d:	e8 0e 07 00 00       	call   80104570 <pushcli>
  c = mycpu();
80103e62:	e8 09 fb ff ff       	call   80103970 <mycpu>
  p = c->proc;
80103e67:	8b b0 ac 00 00 00    	mov    0xac(%eax),%esi
  popcli();
80103e6d:	e8 4e 07 00 00       	call   801045c0 <popcli>
  if(curproc == initproc)
80103e72:	8d 5e 28             	lea    0x28(%esi),%ebx
80103e75:	8d 7e 68             	lea    0x68(%esi),%edi
80103e78:	39 35 b8 a5 10 80    	cmp    %esi,0x8010a5b8
80103e7e:	0f 84 c4 00 00 00    	je     80103f48 <exit+0xf8>
80103e84:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(curproc->ofile[fd]){
80103e88:	8b 03                	mov    (%ebx),%eax
80103e8a:	85 c0                	test   %eax,%eax
80103e8c:	74 12                	je     80103ea0 <exit+0x50>
      fileclose(curproc->ofile[fd]);
80103e8e:	83 ec 0c             	sub    $0xc,%esp
80103e91:	50                   	push   %eax
80103e92:	e8 49 d0 ff ff       	call   80100ee0 <fileclose>
      curproc->ofile[fd] = 0;
80103e97:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
80103e9d:	83 c4 10             	add    $0x10,%esp
  for(fd = 0; fd < NOFILE; fd++){
80103ea0:	83 c3 04             	add    $0x4,%ebx
80103ea3:	39 fb                	cmp    %edi,%ebx
80103ea5:	75 e1                	jne    80103e88 <exit+0x38>
  begin_op();
80103ea7:	e8 a4 ee ff ff       	call   80102d50 <begin_op>
  iput(curproc->cwd);
80103eac:	83 ec 0c             	sub    $0xc,%esp
80103eaf:	ff 76 68             	pushl  0x68(%esi)
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103eb2:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
  iput(curproc->cwd);
80103eb7:	e8 f4 d9 ff ff       	call   801018b0 <iput>
  end_op();
80103ebc:	e8 ff ee ff ff       	call   80102dc0 <end_op>
  curproc->cwd = 0;
80103ec1:	c7 46 68 00 00 00 00 	movl   $0x0,0x68(%esi)
  acquire(&ptable.lock);
80103ec8:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103ecf:	e8 9c 07 00 00       	call   80104670 <acquire>
  wakeup1(curproc->parent);
80103ed4:	8b 46 14             	mov    0x14(%esi),%eax
80103ed7:	e8 d4 f8 ff ff       	call   801037b0 <wakeup1>
80103edc:	83 c4 10             	add    $0x10,%esp
80103edf:	eb 12                	jmp    80103ef3 <exit+0xa3>
80103ee1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103ee8:	83 c3 7c             	add    $0x7c,%ebx
80103eeb:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
80103ef1:	74 2d                	je     80103f20 <exit+0xd0>
    if(p->parent == curproc){
80103ef3:	39 73 14             	cmp    %esi,0x14(%ebx)
80103ef6:	75 f0                	jne    80103ee8 <exit+0x98>
      p->parent = initproc;
80103ef8:	a1 b8 a5 10 80       	mov    0x8010a5b8,%eax
      if(p->state == ZOMBIE)
80103efd:	83 7b 0c 05          	cmpl   $0x5,0xc(%ebx)
      p->parent = initproc;
80103f01:	89 43 14             	mov    %eax,0x14(%ebx)
      if(p->state == ZOMBIE)
80103f04:	75 e2                	jne    80103ee8 <exit+0x98>
        wakeup1(initproc);
80103f06:	e8 a5 f8 ff ff       	call   801037b0 <wakeup1>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103f0b:	83 c3 7c             	add    $0x7c,%ebx
80103f0e:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
80103f14:	75 dd                	jne    80103ef3 <exit+0xa3>
80103f16:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103f1d:	8d 76 00             	lea    0x0(%esi),%esi
  cprintf("xv6 : exit() :pid %d - RUNNING -> ZOMBIE\n",curproc->pid);
80103f20:	83 ec 08             	sub    $0x8,%esp
  curproc->state = ZOMBIE;
80103f23:	c7 46 0c 05 00 00 00 	movl   $0x5,0xc(%esi)
  cprintf("xv6 : exit() :pid %d - RUNNING -> ZOMBIE\n",curproc->pid);
80103f2a:	ff 76 10             	pushl  0x10(%esi)
80103f2d:	68 88 78 10 80       	push   $0x80107888
80103f32:	e8 79 c7 ff ff       	call   801006b0 <cprintf>
  sched();
80103f37:	e8 54 fe ff ff       	call   80103d90 <sched>
  panic("zombie exit");
80103f3c:	c7 04 24 f9 79 10 80 	movl   $0x801079f9,(%esp)
80103f43:	e8 48 c4 ff ff       	call   80100390 <panic>
    panic("init exiting");
80103f48:	83 ec 0c             	sub    $0xc,%esp
80103f4b:	68 ec 79 10 80       	push   $0x801079ec
80103f50:	e8 3b c4 ff ff       	call   80100390 <panic>
80103f55:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103f5c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80103f60 <yield>:
{
80103f60:	f3 0f 1e fb          	endbr32 
80103f64:	55                   	push   %ebp
80103f65:	89 e5                	mov    %esp,%ebp
80103f67:	56                   	push   %esi
80103f68:	53                   	push   %ebx
  pushcli();
80103f69:	e8 02 06 00 00       	call   80104570 <pushcli>
  c = mycpu();
80103f6e:	e8 fd f9 ff ff       	call   80103970 <mycpu>
  p = c->proc;
80103f73:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103f79:	e8 42 06 00 00       	call   801045c0 <popcli>
  acquire(&ptable.lock);  //DOC: yieldlock
80103f7e:	83 ec 0c             	sub    $0xc,%esp
	char *state= states[myproc()->state];
80103f81:	8b 43 0c             	mov    0xc(%ebx),%eax
  acquire(&ptable.lock);  //DOC: yieldlock
80103f84:	68 20 2d 11 80       	push   $0x80112d20
	char *state= states[myproc()->state];
80103f89:	8b 34 85 9c 7a 10 80 	mov    -0x7fef8564(,%eax,4),%esi
  acquire(&ptable.lock);  //DOC: yieldlock
80103f90:	e8 db 06 00 00       	call   80104670 <acquire>
  pushcli();
80103f95:	e8 d6 05 00 00       	call   80104570 <pushcli>
  c = mycpu();
80103f9a:	e8 d1 f9 ff ff       	call   80103970 <mycpu>
  p = c->proc;
80103f9f:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103fa5:	e8 16 06 00 00       	call   801045c0 <popcli>
  myproc()->state = RUNNABLE;
80103faa:	c7 43 0c 03 00 00 00 	movl   $0x3,0xc(%ebx)
  pushcli();
80103fb1:	e8 ba 05 00 00       	call   80104570 <pushcli>
  c = mycpu();
80103fb6:	e8 b5 f9 ff ff       	call   80103970 <mycpu>
  p = c->proc;
80103fbb:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103fc1:	e8 fa 05 00 00       	call   801045c0 <popcli>
  cprintf("xv6 : yield() :pid %d , %s -> RUNNABLE\n",myproc()->pid,state);
80103fc6:	83 c4 0c             	add    $0xc,%esp
80103fc9:	56                   	push   %esi
80103fca:	ff 73 10             	pushl  0x10(%ebx)
80103fcd:	68 b4 78 10 80       	push   $0x801078b4
80103fd2:	e8 d9 c6 ff ff       	call   801006b0 <cprintf>
  sched();
80103fd7:	e8 b4 fd ff ff       	call   80103d90 <sched>
  release(&ptable.lock);
80103fdc:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103fe3:	e8 48 07 00 00       	call   80104730 <release>
}
80103fe8:	83 c4 10             	add    $0x10,%esp
80103feb:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103fee:	5b                   	pop    %ebx
80103fef:	5e                   	pop    %esi
80103ff0:	5d                   	pop    %ebp
80103ff1:	c3                   	ret    
80103ff2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103ff9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80104000 <sleep>:
{
80104000:	f3 0f 1e fb          	endbr32 
80104004:	55                   	push   %ebp
80104005:	89 e5                	mov    %esp,%ebp
80104007:	57                   	push   %edi
80104008:	56                   	push   %esi
80104009:	53                   	push   %ebx
8010400a:	83 ec 0c             	sub    $0xc,%esp
8010400d:	8b 7d 08             	mov    0x8(%ebp),%edi
80104010:	8b 75 0c             	mov    0xc(%ebp),%esi
  pushcli();
80104013:	e8 58 05 00 00       	call   80104570 <pushcli>
  c = mycpu();
80104018:	e8 53 f9 ff ff       	call   80103970 <mycpu>
  p = c->proc;
8010401d:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80104023:	e8 98 05 00 00       	call   801045c0 <popcli>
  if(p == 0)
80104028:	85 db                	test   %ebx,%ebx
8010402a:	0f 84 ae 00 00 00    	je     801040de <sleep+0xde>
  if(lk == 0)
80104030:	85 f6                	test   %esi,%esi
80104032:	0f 84 99 00 00 00    	je     801040d1 <sleep+0xd1>
  if(lk != &ptable.lock){  //DOC: sleeplock0
80104038:	81 fe 20 2d 11 80    	cmp    $0x80112d20,%esi
8010403e:	74 60                	je     801040a0 <sleep+0xa0>
    acquire(&ptable.lock);  //DOC: sleeplock1
80104040:	83 ec 0c             	sub    $0xc,%esp
80104043:	68 20 2d 11 80       	push   $0x80112d20
80104048:	e8 23 06 00 00       	call   80104670 <acquire>
    release(lk);
8010404d:	89 34 24             	mov    %esi,(%esp)
80104050:	e8 db 06 00 00       	call   80104730 <release>
  p->chan = chan;
80104055:	89 7b 20             	mov    %edi,0x20(%ebx)
  p->state = SLEEPING;
80104058:	c7 43 0c 02 00 00 00 	movl   $0x2,0xc(%ebx)
	cprintf("xv6 : sleep() :pid %d , RUNNING -> SLEEPING\n", p->pid);
8010405f:	58                   	pop    %eax
80104060:	5a                   	pop    %edx
80104061:	ff 73 10             	pushl  0x10(%ebx)
80104064:	68 dc 78 10 80       	push   $0x801078dc
80104069:	e8 42 c6 ff ff       	call   801006b0 <cprintf>
  sched();
8010406e:	e8 1d fd ff ff       	call   80103d90 <sched>
  p->chan = 0;
80104073:	c7 43 20 00 00 00 00 	movl   $0x0,0x20(%ebx)
    release(&ptable.lock);
8010407a:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80104081:	e8 aa 06 00 00       	call   80104730 <release>
    acquire(lk);
80104086:	89 75 08             	mov    %esi,0x8(%ebp)
80104089:	83 c4 10             	add    $0x10,%esp
}
8010408c:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010408f:	5b                   	pop    %ebx
80104090:	5e                   	pop    %esi
80104091:	5f                   	pop    %edi
80104092:	5d                   	pop    %ebp
    acquire(lk);
80104093:	e9 d8 05 00 00       	jmp    80104670 <acquire>
80104098:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010409f:	90                   	nop
	cprintf("xv6 : sleep() :pid %d , RUNNING -> SLEEPING\n", p->pid);
801040a0:	83 ec 08             	sub    $0x8,%esp
  p->chan = chan;
801040a3:	89 7b 20             	mov    %edi,0x20(%ebx)
  p->state = SLEEPING;
801040a6:	c7 43 0c 02 00 00 00 	movl   $0x2,0xc(%ebx)
	cprintf("xv6 : sleep() :pid %d , RUNNING -> SLEEPING\n", p->pid);
801040ad:	ff 73 10             	pushl  0x10(%ebx)
801040b0:	68 dc 78 10 80       	push   $0x801078dc
801040b5:	e8 f6 c5 ff ff       	call   801006b0 <cprintf>
  sched();
801040ba:	e8 d1 fc ff ff       	call   80103d90 <sched>
  p->chan = 0;
801040bf:	c7 43 20 00 00 00 00 	movl   $0x0,0x20(%ebx)
801040c6:	83 c4 10             	add    $0x10,%esp
}
801040c9:	8d 65 f4             	lea    -0xc(%ebp),%esp
801040cc:	5b                   	pop    %ebx
801040cd:	5e                   	pop    %esi
801040ce:	5f                   	pop    %edi
801040cf:	5d                   	pop    %ebp
801040d0:	c3                   	ret    
    panic("sleep without lk");
801040d1:	83 ec 0c             	sub    $0xc,%esp
801040d4:	68 0b 7a 10 80       	push   $0x80107a0b
801040d9:	e8 b2 c2 ff ff       	call   80100390 <panic>
    panic("sleep");
801040de:	83 ec 0c             	sub    $0xc,%esp
801040e1:	68 05 7a 10 80       	push   $0x80107a05
801040e6:	e8 a5 c2 ff ff       	call   80100390 <panic>
801040eb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801040ef:	90                   	nop

801040f0 <wait>:
{
801040f0:	f3 0f 1e fb          	endbr32 
801040f4:	55                   	push   %ebp
801040f5:	89 e5                	mov    %esp,%ebp
801040f7:	57                   	push   %edi
801040f8:	56                   	push   %esi
801040f9:	53                   	push   %ebx
801040fa:	83 ec 0c             	sub    $0xc,%esp
  pushcli();
801040fd:	e8 6e 04 00 00       	call   80104570 <pushcli>
  c = mycpu();
80104102:	e8 69 f8 ff ff       	call   80103970 <mycpu>
  p = c->proc;
80104107:	8b b0 ac 00 00 00    	mov    0xac(%eax),%esi
  popcli();
8010410d:	e8 ae 04 00 00       	call   801045c0 <popcli>
  acquire(&ptable.lock);
80104112:	83 ec 0c             	sub    $0xc,%esp
80104115:	68 20 2d 11 80       	push   $0x80112d20
8010411a:	e8 51 05 00 00       	call   80104670 <acquire>
8010411f:	83 c4 10             	add    $0x10,%esp
    havekids = 0;
80104122:	31 c0                	xor    %eax,%eax
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104124:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
80104129:	eb 10                	jmp    8010413b <wait+0x4b>
8010412b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010412f:	90                   	nop
80104130:	83 c3 7c             	add    $0x7c,%ebx
80104133:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
80104139:	74 1b                	je     80104156 <wait+0x66>
      if(p->parent != curproc)
8010413b:	39 73 14             	cmp    %esi,0x14(%ebx)
8010413e:	75 f0                	jne    80104130 <wait+0x40>
      if(p->state == ZOMBIE){
80104140:	83 7b 0c 05          	cmpl   $0x5,0xc(%ebx)
80104144:	74 3a                	je     80104180 <wait+0x90>
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104146:	83 c3 7c             	add    $0x7c,%ebx
      havekids = 1;
80104149:	b8 01 00 00 00       	mov    $0x1,%eax
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010414e:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
80104154:	75 e5                	jne    8010413b <wait+0x4b>
    if(!havekids || curproc->killed){
80104156:	85 c0                	test   %eax,%eax
80104158:	0f 84 8b 00 00 00    	je     801041e9 <wait+0xf9>
8010415e:	8b 46 24             	mov    0x24(%esi),%eax
80104161:	85 c0                	test   %eax,%eax
80104163:	0f 85 80 00 00 00    	jne    801041e9 <wait+0xf9>
    sleep(curproc, &ptable.lock);  //DOC: wait-sleep
80104169:	83 ec 08             	sub    $0x8,%esp
8010416c:	68 20 2d 11 80       	push   $0x80112d20
80104171:	56                   	push   %esi
80104172:	e8 89 fe ff ff       	call   80104000 <sleep>
    havekids = 0;
80104177:	83 c4 10             	add    $0x10,%esp
8010417a:	eb a6                	jmp    80104122 <wait+0x32>
8010417c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        kfree(p->kstack);
80104180:	83 ec 0c             	sub    $0xc,%esp
80104183:	ff 73 08             	pushl  0x8(%ebx)
        pid = p->pid;
80104186:	8b 7b 10             	mov    0x10(%ebx),%edi
        kfree(p->kstack);
80104189:	e8 02 e3 ff ff       	call   80102490 <kfree>
        freevm(p->pgdir);
8010418e:	5a                   	pop    %edx
8010418f:	ff 73 04             	pushl  0x4(%ebx)
        p->kstack = 0;
80104192:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
        freevm(p->pgdir);
80104199:	e8 42 2d 00 00       	call   80106ee0 <freevm>
        cprintf("xv6 : wait() :pid %d - ZOMBIE -> UNUSED, reaped by %d\n", p->pid, curproc->pid);
8010419e:	83 c4 0c             	add    $0xc,%esp
        p->pid = 0;
801041a1:	c7 43 10 00 00 00 00 	movl   $0x0,0x10(%ebx)
        p->parent = 0;
801041a8:	c7 43 14 00 00 00 00 	movl   $0x0,0x14(%ebx)
        p->name[0] = 0;
801041af:	c6 43 6c 00          	movb   $0x0,0x6c(%ebx)
        p->killed = 0;
801041b3:	c7 43 24 00 00 00 00 	movl   $0x0,0x24(%ebx)
        p->state = UNUSED;
801041ba:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
        cprintf("xv6 : wait() :pid %d - ZOMBIE -> UNUSED, reaped by %d\n", p->pid, curproc->pid);
801041c1:	ff 76 10             	pushl  0x10(%esi)
801041c4:	6a 00                	push   $0x0
801041c6:	68 0c 79 10 80       	push   $0x8010790c
801041cb:	e8 e0 c4 ff ff       	call   801006b0 <cprintf>
        release(&ptable.lock);
801041d0:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
801041d7:	e8 54 05 00 00       	call   80104730 <release>
        return pid;
801041dc:	83 c4 10             	add    $0x10,%esp
}
801041df:	8d 65 f4             	lea    -0xc(%ebp),%esp
801041e2:	89 f8                	mov    %edi,%eax
801041e4:	5b                   	pop    %ebx
801041e5:	5e                   	pop    %esi
801041e6:	5f                   	pop    %edi
801041e7:	5d                   	pop    %ebp
801041e8:	c3                   	ret    
      release(&ptable.lock);
801041e9:	83 ec 0c             	sub    $0xc,%esp
      return -1;
801041ec:	bf ff ff ff ff       	mov    $0xffffffff,%edi
      release(&ptable.lock);
801041f1:	68 20 2d 11 80       	push   $0x80112d20
801041f6:	e8 35 05 00 00       	call   80104730 <release>
      return -1;
801041fb:	83 c4 10             	add    $0x10,%esp
801041fe:	eb df                	jmp    801041df <wait+0xef>

80104200 <wakeup>:

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
80104200:	f3 0f 1e fb          	endbr32 
80104204:	55                   	push   %ebp
80104205:	89 e5                	mov    %esp,%ebp
80104207:	53                   	push   %ebx
80104208:	83 ec 10             	sub    $0x10,%esp
8010420b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&ptable.lock);
8010420e:	68 20 2d 11 80       	push   $0x80112d20
80104213:	e8 58 04 00 00       	call   80104670 <acquire>
  wakeup1(chan);
80104218:	89 d8                	mov    %ebx,%eax
8010421a:	e8 91 f5 ff ff       	call   801037b0 <wakeup1>
  release(&ptable.lock);
8010421f:	c7 45 08 20 2d 11 80 	movl   $0x80112d20,0x8(%ebp)
}
80104226:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  release(&ptable.lock);
80104229:	83 c4 10             	add    $0x10,%esp
}
8010422c:	c9                   	leave  
  release(&ptable.lock);
8010422d:	e9 fe 04 00 00       	jmp    80104730 <release>
80104232:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104239:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80104240 <kill>:
// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
80104240:	f3 0f 1e fb          	endbr32 
80104244:	55                   	push   %ebp
80104245:	89 e5                	mov    %esp,%ebp
80104247:	53                   	push   %ebx
80104248:	83 ec 10             	sub    $0x10,%esp
8010424b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct proc *p;

  acquire(&ptable.lock);
8010424e:	68 20 2d 11 80       	push   $0x80112d20
80104253:	e8 18 04 00 00       	call   80104670 <acquire>
80104258:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010425b:	b8 54 2d 11 80       	mov    $0x80112d54,%eax
80104260:	eb 10                	jmp    80104272 <kill+0x32>
80104262:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104268:	83 c0 7c             	add    $0x7c,%eax
8010426b:	3d 54 4c 11 80       	cmp    $0x80114c54,%eax
80104270:	74 2e                	je     801042a0 <kill+0x60>
    if(p->pid == pid){
80104272:	39 58 10             	cmp    %ebx,0x10(%eax)
80104275:	75 f1                	jne    80104268 <kill+0x28>
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING){
80104277:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
      p->killed = 1;
8010427b:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
      if(p->state == SLEEPING){
80104282:	74 3c                	je     801042c0 <kill+0x80>
      
        p->state = RUNNABLE;
        cprintf("xv6 : kill() :pid %d - SLEEPING -> RUNNABLE\n" ,p->pid);
       }
      release(&ptable.lock);
80104284:	83 ec 0c             	sub    $0xc,%esp
80104287:	68 20 2d 11 80       	push   $0x80112d20
8010428c:	e8 9f 04 00 00       	call   80104730 <release>
      return 0;
    }
  }
  release(&ptable.lock);
  return -1;
}
80104291:	8b 5d fc             	mov    -0x4(%ebp),%ebx
      return 0;
80104294:	83 c4 10             	add    $0x10,%esp
80104297:	31 c0                	xor    %eax,%eax
}
80104299:	c9                   	leave  
8010429a:	c3                   	ret    
8010429b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010429f:	90                   	nop
  release(&ptable.lock);
801042a0:	83 ec 0c             	sub    $0xc,%esp
801042a3:	68 20 2d 11 80       	push   $0x80112d20
801042a8:	e8 83 04 00 00       	call   80104730 <release>
}
801042ad:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  return -1;
801042b0:	83 c4 10             	add    $0x10,%esp
801042b3:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801042b8:	c9                   	leave  
801042b9:	c3                   	ret    
801042ba:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
        cprintf("xv6 : kill() :pid %d - SLEEPING -> RUNNABLE\n" ,p->pid);
801042c0:	83 ec 08             	sub    $0x8,%esp
        p->state = RUNNABLE;
801042c3:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
        cprintf("xv6 : kill() :pid %d - SLEEPING -> RUNNABLE\n" ,p->pid);
801042ca:	53                   	push   %ebx
801042cb:	68 44 79 10 80       	push   $0x80107944
801042d0:	e8 db c3 ff ff       	call   801006b0 <cprintf>
801042d5:	83 c4 10             	add    $0x10,%esp
801042d8:	eb aa                	jmp    80104284 <kill+0x44>
801042da:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801042e0 <procdump>:
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
801042e0:	f3 0f 1e fb          	endbr32 
801042e4:	55                   	push   %ebp
801042e5:	89 e5                	mov    %esp,%ebp
801042e7:	57                   	push   %edi
801042e8:	56                   	push   %esi
801042e9:	8d 75 e8             	lea    -0x18(%ebp),%esi
801042ec:	53                   	push   %ebx
801042ed:	bb c0 2d 11 80       	mov    $0x80112dc0,%ebx
801042f2:	83 ec 3c             	sub    $0x3c,%esp
801042f5:	eb 28                	jmp    8010431f <procdump+0x3f>
801042f7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801042fe:	66 90                	xchg   %ax,%ax
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
80104300:	83 ec 0c             	sub    $0xc,%esp
80104303:	68 b7 7d 10 80       	push   $0x80107db7
80104308:	e8 a3 c3 ff ff       	call   801006b0 <cprintf>
8010430d:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104310:	83 c3 7c             	add    $0x7c,%ebx
80104313:	81 fb c0 4c 11 80    	cmp    $0x80114cc0,%ebx
80104319:	0f 84 81 00 00 00    	je     801043a0 <procdump+0xc0>
    if(p->state == UNUSED)
8010431f:	8b 43 a0             	mov    -0x60(%ebx),%eax
80104322:	85 c0                	test   %eax,%eax
80104324:	74 ea                	je     80104310 <procdump+0x30>
      state = "???";
80104326:	ba 1c 7a 10 80       	mov    $0x80107a1c,%edx
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
8010432b:	83 f8 05             	cmp    $0x5,%eax
8010432e:	77 11                	ja     80104341 <procdump+0x61>
80104330:	8b 14 85 84 7a 10 80 	mov    -0x7fef857c(,%eax,4),%edx
      state = "???";
80104337:	b8 1c 7a 10 80       	mov    $0x80107a1c,%eax
8010433c:	85 d2                	test   %edx,%edx
8010433e:	0f 44 d0             	cmove  %eax,%edx
    cprintf("%d %s %s", p->pid, state, p->name);
80104341:	53                   	push   %ebx
80104342:	52                   	push   %edx
80104343:	ff 73 a4             	pushl  -0x5c(%ebx)
80104346:	68 20 7a 10 80       	push   $0x80107a20
8010434b:	e8 60 c3 ff ff       	call   801006b0 <cprintf>
    if(p->state == SLEEPING){
80104350:	83 c4 10             	add    $0x10,%esp
80104353:	83 7b a0 02          	cmpl   $0x2,-0x60(%ebx)
80104357:	75 a7                	jne    80104300 <procdump+0x20>
      getcallerpcs((uint*)p->context->ebp+2, pc);
80104359:	83 ec 08             	sub    $0x8,%esp
8010435c:	8d 45 c0             	lea    -0x40(%ebp),%eax
8010435f:	8d 7d c0             	lea    -0x40(%ebp),%edi
80104362:	50                   	push   %eax
80104363:	8b 43 b0             	mov    -0x50(%ebx),%eax
80104366:	8b 40 0c             	mov    0xc(%eax),%eax
80104369:	83 c0 08             	add    $0x8,%eax
8010436c:	50                   	push   %eax
8010436d:	e8 9e 01 00 00       	call   80104510 <getcallerpcs>
      for(i=0; i<10 && pc[i] != 0; i++)
80104372:	83 c4 10             	add    $0x10,%esp
80104375:	8d 76 00             	lea    0x0(%esi),%esi
80104378:	8b 17                	mov    (%edi),%edx
8010437a:	85 d2                	test   %edx,%edx
8010437c:	74 82                	je     80104300 <procdump+0x20>
        cprintf(" %p", pc[i]);
8010437e:	83 ec 08             	sub    $0x8,%esp
80104381:	83 c7 04             	add    $0x4,%edi
80104384:	52                   	push   %edx
80104385:	68 41 72 10 80       	push   $0x80107241
8010438a:	e8 21 c3 ff ff       	call   801006b0 <cprintf>
      for(i=0; i<10 && pc[i] != 0; i++)
8010438f:	83 c4 10             	add    $0x10,%esp
80104392:	39 fe                	cmp    %edi,%esi
80104394:	75 e2                	jne    80104378 <procdump+0x98>
80104396:	e9 65 ff ff ff       	jmp    80104300 <procdump+0x20>
8010439b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010439f:	90                   	nop
  }
}
801043a0:	8d 65 f4             	lea    -0xc(%ebp),%esp
801043a3:	5b                   	pop    %ebx
801043a4:	5e                   	pop    %esi
801043a5:	5f                   	pop    %edi
801043a6:	5d                   	pop    %ebp
801043a7:	c3                   	ret    
801043a8:	66 90                	xchg   %ax,%ax
801043aa:	66 90                	xchg   %ax,%ax
801043ac:	66 90                	xchg   %ax,%ax
801043ae:	66 90                	xchg   %ax,%ax

801043b0 <initsleeplock>:
#include "spinlock.h"
#include "sleeplock.h"

void
initsleeplock(struct sleeplock *lk, char *name)
{
801043b0:	f3 0f 1e fb          	endbr32 
801043b4:	55                   	push   %ebp
801043b5:	89 e5                	mov    %esp,%ebp
801043b7:	53                   	push   %ebx
801043b8:	83 ec 0c             	sub    $0xc,%esp
801043bb:	8b 5d 08             	mov    0x8(%ebp),%ebx
  initlock(&lk->lk, "sleep lock");
801043be:	68 b4 7a 10 80       	push   $0x80107ab4
801043c3:	8d 43 04             	lea    0x4(%ebx),%eax
801043c6:	50                   	push   %eax
801043c7:	e8 24 01 00 00       	call   801044f0 <initlock>
  lk->name = name;
801043cc:	8b 45 0c             	mov    0xc(%ebp),%eax
  lk->locked = 0;
801043cf:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  lk->pid = 0;
}
801043d5:	83 c4 10             	add    $0x10,%esp
  lk->pid = 0;
801043d8:	c7 43 3c 00 00 00 00 	movl   $0x0,0x3c(%ebx)
  lk->name = name;
801043df:	89 43 38             	mov    %eax,0x38(%ebx)
}
801043e2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801043e5:	c9                   	leave  
801043e6:	c3                   	ret    
801043e7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801043ee:	66 90                	xchg   %ax,%ax

801043f0 <acquiresleep>:

void
acquiresleep(struct sleeplock *lk)
{
801043f0:	f3 0f 1e fb          	endbr32 
801043f4:	55                   	push   %ebp
801043f5:	89 e5                	mov    %esp,%ebp
801043f7:	56                   	push   %esi
801043f8:	53                   	push   %ebx
801043f9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&lk->lk);
801043fc:	8d 73 04             	lea    0x4(%ebx),%esi
801043ff:	83 ec 0c             	sub    $0xc,%esp
80104402:	56                   	push   %esi
80104403:	e8 68 02 00 00       	call   80104670 <acquire>
  while (lk->locked) {
80104408:	8b 13                	mov    (%ebx),%edx
8010440a:	83 c4 10             	add    $0x10,%esp
8010440d:	85 d2                	test   %edx,%edx
8010440f:	74 1a                	je     8010442b <acquiresleep+0x3b>
80104411:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    sleep(lk, &lk->lk);
80104418:	83 ec 08             	sub    $0x8,%esp
8010441b:	56                   	push   %esi
8010441c:	53                   	push   %ebx
8010441d:	e8 de fb ff ff       	call   80104000 <sleep>
  while (lk->locked) {
80104422:	8b 03                	mov    (%ebx),%eax
80104424:	83 c4 10             	add    $0x10,%esp
80104427:	85 c0                	test   %eax,%eax
80104429:	75 ed                	jne    80104418 <acquiresleep+0x28>
  }
  lk->locked = 1;
8010442b:	c7 03 01 00 00 00    	movl   $0x1,(%ebx)
  lk->pid = myproc()->pid;
80104431:	e8 ca f5 ff ff       	call   80103a00 <myproc>
80104436:	8b 40 10             	mov    0x10(%eax),%eax
80104439:	89 43 3c             	mov    %eax,0x3c(%ebx)
  release(&lk->lk);
8010443c:	89 75 08             	mov    %esi,0x8(%ebp)
}
8010443f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104442:	5b                   	pop    %ebx
80104443:	5e                   	pop    %esi
80104444:	5d                   	pop    %ebp
  release(&lk->lk);
80104445:	e9 e6 02 00 00       	jmp    80104730 <release>
8010444a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80104450 <releasesleep>:

void
releasesleep(struct sleeplock *lk)
{
80104450:	f3 0f 1e fb          	endbr32 
80104454:	55                   	push   %ebp
80104455:	89 e5                	mov    %esp,%ebp
80104457:	56                   	push   %esi
80104458:	53                   	push   %ebx
80104459:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&lk->lk);
8010445c:	8d 73 04             	lea    0x4(%ebx),%esi
8010445f:	83 ec 0c             	sub    $0xc,%esp
80104462:	56                   	push   %esi
80104463:	e8 08 02 00 00       	call   80104670 <acquire>
  lk->locked = 0;
80104468:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  lk->pid = 0;
8010446e:	c7 43 3c 00 00 00 00 	movl   $0x0,0x3c(%ebx)
  wakeup(lk);
80104475:	89 1c 24             	mov    %ebx,(%esp)
80104478:	e8 83 fd ff ff       	call   80104200 <wakeup>
  release(&lk->lk);
8010447d:	89 75 08             	mov    %esi,0x8(%ebp)
80104480:	83 c4 10             	add    $0x10,%esp
}
80104483:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104486:	5b                   	pop    %ebx
80104487:	5e                   	pop    %esi
80104488:	5d                   	pop    %ebp
  release(&lk->lk);
80104489:	e9 a2 02 00 00       	jmp    80104730 <release>
8010448e:	66 90                	xchg   %ax,%ax

80104490 <holdingsleep>:

int
holdingsleep(struct sleeplock *lk)
{
80104490:	f3 0f 1e fb          	endbr32 
80104494:	55                   	push   %ebp
80104495:	89 e5                	mov    %esp,%ebp
80104497:	57                   	push   %edi
80104498:	31 ff                	xor    %edi,%edi
8010449a:	56                   	push   %esi
8010449b:	53                   	push   %ebx
8010449c:	83 ec 18             	sub    $0x18,%esp
8010449f:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int r;
  
  acquire(&lk->lk);
801044a2:	8d 73 04             	lea    0x4(%ebx),%esi
801044a5:	56                   	push   %esi
801044a6:	e8 c5 01 00 00       	call   80104670 <acquire>
  r = lk->locked && (lk->pid == myproc()->pid);
801044ab:	8b 03                	mov    (%ebx),%eax
801044ad:	83 c4 10             	add    $0x10,%esp
801044b0:	85 c0                	test   %eax,%eax
801044b2:	75 1c                	jne    801044d0 <holdingsleep+0x40>
  release(&lk->lk);
801044b4:	83 ec 0c             	sub    $0xc,%esp
801044b7:	56                   	push   %esi
801044b8:	e8 73 02 00 00       	call   80104730 <release>
  return r;
}
801044bd:	8d 65 f4             	lea    -0xc(%ebp),%esp
801044c0:	89 f8                	mov    %edi,%eax
801044c2:	5b                   	pop    %ebx
801044c3:	5e                   	pop    %esi
801044c4:	5f                   	pop    %edi
801044c5:	5d                   	pop    %ebp
801044c6:	c3                   	ret    
801044c7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801044ce:	66 90                	xchg   %ax,%ax
  r = lk->locked && (lk->pid == myproc()->pid);
801044d0:	8b 5b 3c             	mov    0x3c(%ebx),%ebx
801044d3:	e8 28 f5 ff ff       	call   80103a00 <myproc>
801044d8:	39 58 10             	cmp    %ebx,0x10(%eax)
801044db:	0f 94 c0             	sete   %al
801044de:	0f b6 c0             	movzbl %al,%eax
801044e1:	89 c7                	mov    %eax,%edi
801044e3:	eb cf                	jmp    801044b4 <holdingsleep+0x24>
801044e5:	66 90                	xchg   %ax,%ax
801044e7:	66 90                	xchg   %ax,%ax
801044e9:	66 90                	xchg   %ax,%ax
801044eb:	66 90                	xchg   %ax,%ax
801044ed:	66 90                	xchg   %ax,%ax
801044ef:	90                   	nop

801044f0 <initlock>:
#include "proc.h"
#include "spinlock.h"

void
initlock(struct spinlock *lk, char *name)
{
801044f0:	f3 0f 1e fb          	endbr32 
801044f4:	55                   	push   %ebp
801044f5:	89 e5                	mov    %esp,%ebp
801044f7:	8b 45 08             	mov    0x8(%ebp),%eax
  lk->name = name;
801044fa:	8b 55 0c             	mov    0xc(%ebp),%edx
  lk->locked = 0;
801044fd:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  lk->name = name;
80104503:	89 50 04             	mov    %edx,0x4(%eax)
  lk->cpu = 0;
80104506:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
}
8010450d:	5d                   	pop    %ebp
8010450e:	c3                   	ret    
8010450f:	90                   	nop

80104510 <getcallerpcs>:
}

// Record the current call stack in pcs[] by following the %ebp chain.
void
getcallerpcs(void *v, uint pcs[])
{
80104510:	f3 0f 1e fb          	endbr32 
80104514:	55                   	push   %ebp
  uint *ebp;
  int i;

  ebp = (uint*)v - 2;
  for(i = 0; i < 10; i++){
80104515:	31 d2                	xor    %edx,%edx
{
80104517:	89 e5                	mov    %esp,%ebp
80104519:	53                   	push   %ebx
  ebp = (uint*)v - 2;
8010451a:	8b 45 08             	mov    0x8(%ebp),%eax
{
8010451d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  ebp = (uint*)v - 2;
80104520:	83 e8 08             	sub    $0x8,%eax
  for(i = 0; i < 10; i++){
80104523:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104527:	90                   	nop
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
80104528:	8d 98 00 00 00 80    	lea    -0x80000000(%eax),%ebx
8010452e:	81 fb fe ff ff 7f    	cmp    $0x7ffffffe,%ebx
80104534:	77 1a                	ja     80104550 <getcallerpcs+0x40>
      break;
    pcs[i] = ebp[1];     // saved %eip
80104536:	8b 58 04             	mov    0x4(%eax),%ebx
80104539:	89 1c 91             	mov    %ebx,(%ecx,%edx,4)
  for(i = 0; i < 10; i++){
8010453c:	83 c2 01             	add    $0x1,%edx
    ebp = (uint*)ebp[0]; // saved %ebp
8010453f:	8b 00                	mov    (%eax),%eax
  for(i = 0; i < 10; i++){
80104541:	83 fa 0a             	cmp    $0xa,%edx
80104544:	75 e2                	jne    80104528 <getcallerpcs+0x18>
  }
  for(; i < 10; i++)
    pcs[i] = 0;
}
80104546:	5b                   	pop    %ebx
80104547:	5d                   	pop    %ebp
80104548:	c3                   	ret    
80104549:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  for(; i < 10; i++)
80104550:	8d 04 91             	lea    (%ecx,%edx,4),%eax
80104553:	8d 51 28             	lea    0x28(%ecx),%edx
80104556:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010455d:	8d 76 00             	lea    0x0(%esi),%esi
    pcs[i] = 0;
80104560:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  for(; i < 10; i++)
80104566:	83 c0 04             	add    $0x4,%eax
80104569:	39 d0                	cmp    %edx,%eax
8010456b:	75 f3                	jne    80104560 <getcallerpcs+0x50>
}
8010456d:	5b                   	pop    %ebx
8010456e:	5d                   	pop    %ebp
8010456f:	c3                   	ret    

80104570 <pushcli>:
// it takes two popcli to undo two pushcli.  Also, if interrupts
// are off, then pushcli, popcli leaves them off.

void
pushcli(void)
{
80104570:	f3 0f 1e fb          	endbr32 
80104574:	55                   	push   %ebp
80104575:	89 e5                	mov    %esp,%ebp
80104577:	53                   	push   %ebx
80104578:	83 ec 04             	sub    $0x4,%esp
8010457b:	9c                   	pushf  
8010457c:	5b                   	pop    %ebx
  asm volatile("cli");
8010457d:	fa                   	cli    
  int eflags;

  eflags = readeflags();
  cli();
  if(mycpu()->ncli == 0)
8010457e:	e8 ed f3 ff ff       	call   80103970 <mycpu>
80104583:	8b 80 a4 00 00 00    	mov    0xa4(%eax),%eax
80104589:	85 c0                	test   %eax,%eax
8010458b:	74 13                	je     801045a0 <pushcli+0x30>
    mycpu()->intena = eflags & FL_IF;
  mycpu()->ncli += 1;
8010458d:	e8 de f3 ff ff       	call   80103970 <mycpu>
80104592:	83 80 a4 00 00 00 01 	addl   $0x1,0xa4(%eax)
}
80104599:	83 c4 04             	add    $0x4,%esp
8010459c:	5b                   	pop    %ebx
8010459d:	5d                   	pop    %ebp
8010459e:	c3                   	ret    
8010459f:	90                   	nop
    mycpu()->intena = eflags & FL_IF;
801045a0:	e8 cb f3 ff ff       	call   80103970 <mycpu>
801045a5:	81 e3 00 02 00 00    	and    $0x200,%ebx
801045ab:	89 98 a8 00 00 00    	mov    %ebx,0xa8(%eax)
801045b1:	eb da                	jmp    8010458d <pushcli+0x1d>
801045b3:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801045ba:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801045c0 <popcli>:

void
popcli(void)
{
801045c0:	f3 0f 1e fb          	endbr32 
801045c4:	55                   	push   %ebp
801045c5:	89 e5                	mov    %esp,%ebp
801045c7:	83 ec 08             	sub    $0x8,%esp
  asm volatile("pushfl; popl %0" : "=r" (eflags));
801045ca:	9c                   	pushf  
801045cb:	58                   	pop    %eax
  if(readeflags()&FL_IF)
801045cc:	f6 c4 02             	test   $0x2,%ah
801045cf:	75 31                	jne    80104602 <popcli+0x42>
    panic("popcli - interruptible");
  if(--mycpu()->ncli < 0)
801045d1:	e8 9a f3 ff ff       	call   80103970 <mycpu>
801045d6:	83 a8 a4 00 00 00 01 	subl   $0x1,0xa4(%eax)
801045dd:	78 30                	js     8010460f <popcli+0x4f>
    panic("popcli");
  if(mycpu()->ncli == 0 && mycpu()->intena)
801045df:	e8 8c f3 ff ff       	call   80103970 <mycpu>
801045e4:	8b 90 a4 00 00 00    	mov    0xa4(%eax),%edx
801045ea:	85 d2                	test   %edx,%edx
801045ec:	74 02                	je     801045f0 <popcli+0x30>
    sti();
}
801045ee:	c9                   	leave  
801045ef:	c3                   	ret    
  if(mycpu()->ncli == 0 && mycpu()->intena)
801045f0:	e8 7b f3 ff ff       	call   80103970 <mycpu>
801045f5:	8b 80 a8 00 00 00    	mov    0xa8(%eax),%eax
801045fb:	85 c0                	test   %eax,%eax
801045fd:	74 ef                	je     801045ee <popcli+0x2e>
  asm volatile("sti");
801045ff:	fb                   	sti    
}
80104600:	c9                   	leave  
80104601:	c3                   	ret    
    panic("popcli - interruptible");
80104602:	83 ec 0c             	sub    $0xc,%esp
80104605:	68 bf 7a 10 80       	push   $0x80107abf
8010460a:	e8 81 bd ff ff       	call   80100390 <panic>
    panic("popcli");
8010460f:	83 ec 0c             	sub    $0xc,%esp
80104612:	68 d6 7a 10 80       	push   $0x80107ad6
80104617:	e8 74 bd ff ff       	call   80100390 <panic>
8010461c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80104620 <holding>:
{
80104620:	f3 0f 1e fb          	endbr32 
80104624:	55                   	push   %ebp
80104625:	89 e5                	mov    %esp,%ebp
80104627:	56                   	push   %esi
80104628:	53                   	push   %ebx
80104629:	8b 75 08             	mov    0x8(%ebp),%esi
8010462c:	31 db                	xor    %ebx,%ebx
  pushcli();
8010462e:	e8 3d ff ff ff       	call   80104570 <pushcli>
  r = lock->locked && lock->cpu == mycpu();
80104633:	8b 06                	mov    (%esi),%eax
80104635:	85 c0                	test   %eax,%eax
80104637:	75 0f                	jne    80104648 <holding+0x28>
  popcli();
80104639:	e8 82 ff ff ff       	call   801045c0 <popcli>
}
8010463e:	89 d8                	mov    %ebx,%eax
80104640:	5b                   	pop    %ebx
80104641:	5e                   	pop    %esi
80104642:	5d                   	pop    %ebp
80104643:	c3                   	ret    
80104644:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  r = lock->locked && lock->cpu == mycpu();
80104648:	8b 5e 08             	mov    0x8(%esi),%ebx
8010464b:	e8 20 f3 ff ff       	call   80103970 <mycpu>
80104650:	39 c3                	cmp    %eax,%ebx
80104652:	0f 94 c3             	sete   %bl
  popcli();
80104655:	e8 66 ff ff ff       	call   801045c0 <popcli>
  r = lock->locked && lock->cpu == mycpu();
8010465a:	0f b6 db             	movzbl %bl,%ebx
}
8010465d:	89 d8                	mov    %ebx,%eax
8010465f:	5b                   	pop    %ebx
80104660:	5e                   	pop    %esi
80104661:	5d                   	pop    %ebp
80104662:	c3                   	ret    
80104663:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010466a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80104670 <acquire>:
{
80104670:	f3 0f 1e fb          	endbr32 
80104674:	55                   	push   %ebp
80104675:	89 e5                	mov    %esp,%ebp
80104677:	56                   	push   %esi
80104678:	53                   	push   %ebx
  pushcli(); // disable interrupts to avoid deadlock.
80104679:	e8 f2 fe ff ff       	call   80104570 <pushcli>
  if(holding(lk))
8010467e:	8b 5d 08             	mov    0x8(%ebp),%ebx
80104681:	83 ec 0c             	sub    $0xc,%esp
80104684:	53                   	push   %ebx
80104685:	e8 96 ff ff ff       	call   80104620 <holding>
8010468a:	83 c4 10             	add    $0x10,%esp
8010468d:	85 c0                	test   %eax,%eax
8010468f:	0f 85 7f 00 00 00    	jne    80104714 <acquire+0xa4>
80104695:	89 c6                	mov    %eax,%esi
  asm volatile("lock; xchgl %0, %1" :
80104697:	ba 01 00 00 00       	mov    $0x1,%edx
8010469c:	eb 05                	jmp    801046a3 <acquire+0x33>
8010469e:	66 90                	xchg   %ax,%ax
801046a0:	8b 5d 08             	mov    0x8(%ebp),%ebx
801046a3:	89 d0                	mov    %edx,%eax
801046a5:	f0 87 03             	lock xchg %eax,(%ebx)
  while(xchg(&lk->locked, 1) != 0)
801046a8:	85 c0                	test   %eax,%eax
801046aa:	75 f4                	jne    801046a0 <acquire+0x30>
  __sync_synchronize();
801046ac:	f0 83 0c 24 00       	lock orl $0x0,(%esp)
  lk->cpu = mycpu();
801046b1:	8b 5d 08             	mov    0x8(%ebp),%ebx
801046b4:	e8 b7 f2 ff ff       	call   80103970 <mycpu>
801046b9:	89 43 08             	mov    %eax,0x8(%ebx)
  ebp = (uint*)v - 2;
801046bc:	89 e8                	mov    %ebp,%eax
801046be:	66 90                	xchg   %ax,%ax
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
801046c0:	8d 90 00 00 00 80    	lea    -0x80000000(%eax),%edx
801046c6:	81 fa fe ff ff 7f    	cmp    $0x7ffffffe,%edx
801046cc:	77 22                	ja     801046f0 <acquire+0x80>
    pcs[i] = ebp[1];     // saved %eip
801046ce:	8b 50 04             	mov    0x4(%eax),%edx
801046d1:	89 54 b3 0c          	mov    %edx,0xc(%ebx,%esi,4)
  for(i = 0; i < 10; i++){
801046d5:	83 c6 01             	add    $0x1,%esi
    ebp = (uint*)ebp[0]; // saved %ebp
801046d8:	8b 00                	mov    (%eax),%eax
  for(i = 0; i < 10; i++){
801046da:	83 fe 0a             	cmp    $0xa,%esi
801046dd:	75 e1                	jne    801046c0 <acquire+0x50>
}
801046df:	8d 65 f8             	lea    -0x8(%ebp),%esp
801046e2:	5b                   	pop    %ebx
801046e3:	5e                   	pop    %esi
801046e4:	5d                   	pop    %ebp
801046e5:	c3                   	ret    
801046e6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801046ed:	8d 76 00             	lea    0x0(%esi),%esi
  for(; i < 10; i++)
801046f0:	8d 44 b3 0c          	lea    0xc(%ebx,%esi,4),%eax
801046f4:	83 c3 34             	add    $0x34,%ebx
801046f7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801046fe:	66 90                	xchg   %ax,%ax
    pcs[i] = 0;
80104700:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  for(; i < 10; i++)
80104706:	83 c0 04             	add    $0x4,%eax
80104709:	39 d8                	cmp    %ebx,%eax
8010470b:	75 f3                	jne    80104700 <acquire+0x90>
}
8010470d:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104710:	5b                   	pop    %ebx
80104711:	5e                   	pop    %esi
80104712:	5d                   	pop    %ebp
80104713:	c3                   	ret    
    panic("acquire");
80104714:	83 ec 0c             	sub    $0xc,%esp
80104717:	68 dd 7a 10 80       	push   $0x80107add
8010471c:	e8 6f bc ff ff       	call   80100390 <panic>
80104721:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104728:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010472f:	90                   	nop

80104730 <release>:
{
80104730:	f3 0f 1e fb          	endbr32 
80104734:	55                   	push   %ebp
80104735:	89 e5                	mov    %esp,%ebp
80104737:	53                   	push   %ebx
80104738:	83 ec 10             	sub    $0x10,%esp
8010473b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holding(lk))
8010473e:	53                   	push   %ebx
8010473f:	e8 dc fe ff ff       	call   80104620 <holding>
80104744:	83 c4 10             	add    $0x10,%esp
80104747:	85 c0                	test   %eax,%eax
80104749:	74 22                	je     8010476d <release+0x3d>
  lk->pcs[0] = 0;
8010474b:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
  lk->cpu = 0;
80104752:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
  __sync_synchronize();
80104759:	f0 83 0c 24 00       	lock orl $0x0,(%esp)
  asm volatile("movl $0, %0" : "+m" (lk->locked) : );
8010475e:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
}
80104764:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104767:	c9                   	leave  
  popcli();
80104768:	e9 53 fe ff ff       	jmp    801045c0 <popcli>
    panic("release");
8010476d:	83 ec 0c             	sub    $0xc,%esp
80104770:	68 e5 7a 10 80       	push   $0x80107ae5
80104775:	e8 16 bc ff ff       	call   80100390 <panic>
8010477a:	66 90                	xchg   %ax,%ax
8010477c:	66 90                	xchg   %ax,%ax
8010477e:	66 90                	xchg   %ax,%ax

80104780 <memset>:
#include "types.h"
#include "x86.h"

void*
memset(void *dst, int c, uint n)
{
80104780:	f3 0f 1e fb          	endbr32 
80104784:	55                   	push   %ebp
80104785:	89 e5                	mov    %esp,%ebp
80104787:	57                   	push   %edi
80104788:	8b 55 08             	mov    0x8(%ebp),%edx
8010478b:	8b 4d 10             	mov    0x10(%ebp),%ecx
8010478e:	53                   	push   %ebx
8010478f:	8b 45 0c             	mov    0xc(%ebp),%eax
  if ((int)dst%4 == 0 && n%4 == 0){
80104792:	89 d7                	mov    %edx,%edi
80104794:	09 cf                	or     %ecx,%edi
80104796:	83 e7 03             	and    $0x3,%edi
80104799:	75 25                	jne    801047c0 <memset+0x40>
    c &= 0xFF;
8010479b:	0f b6 f8             	movzbl %al,%edi
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
8010479e:	c1 e0 18             	shl    $0x18,%eax
801047a1:	89 fb                	mov    %edi,%ebx
801047a3:	c1 e9 02             	shr    $0x2,%ecx
801047a6:	c1 e3 10             	shl    $0x10,%ebx
801047a9:	09 d8                	or     %ebx,%eax
801047ab:	09 f8                	or     %edi,%eax
801047ad:	c1 e7 08             	shl    $0x8,%edi
801047b0:	09 f8                	or     %edi,%eax
  asm volatile("cld; rep stosl" :
801047b2:	89 d7                	mov    %edx,%edi
801047b4:	fc                   	cld    
801047b5:	f3 ab                	rep stos %eax,%es:(%edi)
  } else
    stosb(dst, c, n);
  return dst;
}
801047b7:	5b                   	pop    %ebx
801047b8:	89 d0                	mov    %edx,%eax
801047ba:	5f                   	pop    %edi
801047bb:	5d                   	pop    %ebp
801047bc:	c3                   	ret    
801047bd:	8d 76 00             	lea    0x0(%esi),%esi
  asm volatile("cld; rep stosb" :
801047c0:	89 d7                	mov    %edx,%edi
801047c2:	fc                   	cld    
801047c3:	f3 aa                	rep stos %al,%es:(%edi)
801047c5:	5b                   	pop    %ebx
801047c6:	89 d0                	mov    %edx,%eax
801047c8:	5f                   	pop    %edi
801047c9:	5d                   	pop    %ebp
801047ca:	c3                   	ret    
801047cb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801047cf:	90                   	nop

801047d0 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint n)
{
801047d0:	f3 0f 1e fb          	endbr32 
801047d4:	55                   	push   %ebp
801047d5:	89 e5                	mov    %esp,%ebp
801047d7:	56                   	push   %esi
801047d8:	8b 75 10             	mov    0x10(%ebp),%esi
801047db:	8b 55 08             	mov    0x8(%ebp),%edx
801047de:	53                   	push   %ebx
801047df:	8b 45 0c             	mov    0xc(%ebp),%eax
  const uchar *s1, *s2;

  s1 = v1;
  s2 = v2;
  while(n-- > 0){
801047e2:	85 f6                	test   %esi,%esi
801047e4:	74 2a                	je     80104810 <memcmp+0x40>
801047e6:	01 c6                	add    %eax,%esi
801047e8:	eb 10                	jmp    801047fa <memcmp+0x2a>
801047ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    if(*s1 != *s2)
      return *s1 - *s2;
    s1++, s2++;
801047f0:	83 c0 01             	add    $0x1,%eax
801047f3:	83 c2 01             	add    $0x1,%edx
  while(n-- > 0){
801047f6:	39 f0                	cmp    %esi,%eax
801047f8:	74 16                	je     80104810 <memcmp+0x40>
    if(*s1 != *s2)
801047fa:	0f b6 0a             	movzbl (%edx),%ecx
801047fd:	0f b6 18             	movzbl (%eax),%ebx
80104800:	38 d9                	cmp    %bl,%cl
80104802:	74 ec                	je     801047f0 <memcmp+0x20>
      return *s1 - *s2;
80104804:	0f b6 c1             	movzbl %cl,%eax
80104807:	29 d8                	sub    %ebx,%eax
  }

  return 0;
}
80104809:	5b                   	pop    %ebx
8010480a:	5e                   	pop    %esi
8010480b:	5d                   	pop    %ebp
8010480c:	c3                   	ret    
8010480d:	8d 76 00             	lea    0x0(%esi),%esi
80104810:	5b                   	pop    %ebx
  return 0;
80104811:	31 c0                	xor    %eax,%eax
}
80104813:	5e                   	pop    %esi
80104814:	5d                   	pop    %ebp
80104815:	c3                   	ret    
80104816:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010481d:	8d 76 00             	lea    0x0(%esi),%esi

80104820 <memmove>:

void*
memmove(void *dst, const void *src, uint n)
{
80104820:	f3 0f 1e fb          	endbr32 
80104824:	55                   	push   %ebp
80104825:	89 e5                	mov    %esp,%ebp
80104827:	57                   	push   %edi
80104828:	8b 55 08             	mov    0x8(%ebp),%edx
8010482b:	8b 4d 10             	mov    0x10(%ebp),%ecx
8010482e:	56                   	push   %esi
8010482f:	8b 75 0c             	mov    0xc(%ebp),%esi
  const char *s;
  char *d;

  s = src;
  d = dst;
  if(s < d && s + n > d){
80104832:	39 d6                	cmp    %edx,%esi
80104834:	73 2a                	jae    80104860 <memmove+0x40>
80104836:	8d 3c 0e             	lea    (%esi,%ecx,1),%edi
80104839:	39 fa                	cmp    %edi,%edx
8010483b:	73 23                	jae    80104860 <memmove+0x40>
8010483d:	8d 41 ff             	lea    -0x1(%ecx),%eax
    s += n;
    d += n;
    while(n-- > 0)
80104840:	85 c9                	test   %ecx,%ecx
80104842:	74 13                	je     80104857 <memmove+0x37>
80104844:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      *--d = *--s;
80104848:	0f b6 0c 06          	movzbl (%esi,%eax,1),%ecx
8010484c:	88 0c 02             	mov    %cl,(%edx,%eax,1)
    while(n-- > 0)
8010484f:	83 e8 01             	sub    $0x1,%eax
80104852:	83 f8 ff             	cmp    $0xffffffff,%eax
80104855:	75 f1                	jne    80104848 <memmove+0x28>
  } else
    while(n-- > 0)
      *d++ = *s++;

  return dst;
}
80104857:	5e                   	pop    %esi
80104858:	89 d0                	mov    %edx,%eax
8010485a:	5f                   	pop    %edi
8010485b:	5d                   	pop    %ebp
8010485c:	c3                   	ret    
8010485d:	8d 76 00             	lea    0x0(%esi),%esi
    while(n-- > 0)
80104860:	8d 04 0e             	lea    (%esi,%ecx,1),%eax
80104863:	89 d7                	mov    %edx,%edi
80104865:	85 c9                	test   %ecx,%ecx
80104867:	74 ee                	je     80104857 <memmove+0x37>
80104869:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      *d++ = *s++;
80104870:	a4                   	movsb  %ds:(%esi),%es:(%edi)
    while(n-- > 0)
80104871:	39 f0                	cmp    %esi,%eax
80104873:	75 fb                	jne    80104870 <memmove+0x50>
}
80104875:	5e                   	pop    %esi
80104876:	89 d0                	mov    %edx,%eax
80104878:	5f                   	pop    %edi
80104879:	5d                   	pop    %ebp
8010487a:	c3                   	ret    
8010487b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010487f:	90                   	nop

80104880 <memcpy>:

// memcpy exists to placate GCC.  Use memmove.
void*
memcpy(void *dst, const void *src, uint n)
{
80104880:	f3 0f 1e fb          	endbr32 
  return memmove(dst, src, n);
80104884:	eb 9a                	jmp    80104820 <memmove>
80104886:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010488d:	8d 76 00             	lea    0x0(%esi),%esi

80104890 <strncmp>:
}

int
strncmp(const char *p, const char *q, uint n)
{
80104890:	f3 0f 1e fb          	endbr32 
80104894:	55                   	push   %ebp
80104895:	89 e5                	mov    %esp,%ebp
80104897:	56                   	push   %esi
80104898:	8b 75 10             	mov    0x10(%ebp),%esi
8010489b:	8b 4d 08             	mov    0x8(%ebp),%ecx
8010489e:	53                   	push   %ebx
8010489f:	8b 45 0c             	mov    0xc(%ebp),%eax
  while(n > 0 && *p && *p == *q)
801048a2:	85 f6                	test   %esi,%esi
801048a4:	74 32                	je     801048d8 <strncmp+0x48>
801048a6:	01 c6                	add    %eax,%esi
801048a8:	eb 14                	jmp    801048be <strncmp+0x2e>
801048aa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801048b0:	38 da                	cmp    %bl,%dl
801048b2:	75 14                	jne    801048c8 <strncmp+0x38>
    n--, p++, q++;
801048b4:	83 c0 01             	add    $0x1,%eax
801048b7:	83 c1 01             	add    $0x1,%ecx
  while(n > 0 && *p && *p == *q)
801048ba:	39 f0                	cmp    %esi,%eax
801048bc:	74 1a                	je     801048d8 <strncmp+0x48>
801048be:	0f b6 11             	movzbl (%ecx),%edx
801048c1:	0f b6 18             	movzbl (%eax),%ebx
801048c4:	84 d2                	test   %dl,%dl
801048c6:	75 e8                	jne    801048b0 <strncmp+0x20>
  if(n == 0)
    return 0;
  return (uchar)*p - (uchar)*q;
801048c8:	0f b6 c2             	movzbl %dl,%eax
801048cb:	29 d8                	sub    %ebx,%eax
}
801048cd:	5b                   	pop    %ebx
801048ce:	5e                   	pop    %esi
801048cf:	5d                   	pop    %ebp
801048d0:	c3                   	ret    
801048d1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801048d8:	5b                   	pop    %ebx
    return 0;
801048d9:	31 c0                	xor    %eax,%eax
}
801048db:	5e                   	pop    %esi
801048dc:	5d                   	pop    %ebp
801048dd:	c3                   	ret    
801048de:	66 90                	xchg   %ax,%ax

801048e0 <strncpy>:

char*
strncpy(char *s, const char *t, int n)
{
801048e0:	f3 0f 1e fb          	endbr32 
801048e4:	55                   	push   %ebp
801048e5:	89 e5                	mov    %esp,%ebp
801048e7:	57                   	push   %edi
801048e8:	56                   	push   %esi
801048e9:	8b 75 08             	mov    0x8(%ebp),%esi
801048ec:	53                   	push   %ebx
801048ed:	8b 45 10             	mov    0x10(%ebp),%eax
  char *os;

  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
801048f0:	89 f2                	mov    %esi,%edx
801048f2:	eb 1b                	jmp    8010490f <strncpy+0x2f>
801048f4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801048f8:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
801048fc:	8b 7d 0c             	mov    0xc(%ebp),%edi
801048ff:	83 c2 01             	add    $0x1,%edx
80104902:	0f b6 7f ff          	movzbl -0x1(%edi),%edi
80104906:	89 f9                	mov    %edi,%ecx
80104908:	88 4a ff             	mov    %cl,-0x1(%edx)
8010490b:	84 c9                	test   %cl,%cl
8010490d:	74 09                	je     80104918 <strncpy+0x38>
8010490f:	89 c3                	mov    %eax,%ebx
80104911:	83 e8 01             	sub    $0x1,%eax
80104914:	85 db                	test   %ebx,%ebx
80104916:	7f e0                	jg     801048f8 <strncpy+0x18>
    ;
  while(n-- > 0)
80104918:	89 d1                	mov    %edx,%ecx
8010491a:	85 c0                	test   %eax,%eax
8010491c:	7e 15                	jle    80104933 <strncpy+0x53>
8010491e:	66 90                	xchg   %ax,%ax
    *s++ = 0;
80104920:	83 c1 01             	add    $0x1,%ecx
80104923:	c6 41 ff 00          	movb   $0x0,-0x1(%ecx)
  while(n-- > 0)
80104927:	89 c8                	mov    %ecx,%eax
80104929:	f7 d0                	not    %eax
8010492b:	01 d0                	add    %edx,%eax
8010492d:	01 d8                	add    %ebx,%eax
8010492f:	85 c0                	test   %eax,%eax
80104931:	7f ed                	jg     80104920 <strncpy+0x40>
  return os;
}
80104933:	5b                   	pop    %ebx
80104934:	89 f0                	mov    %esi,%eax
80104936:	5e                   	pop    %esi
80104937:	5f                   	pop    %edi
80104938:	5d                   	pop    %ebp
80104939:	c3                   	ret    
8010493a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80104940 <safestrcpy>:

// Like strncpy but guaranteed to NUL-terminate.
char*
safestrcpy(char *s, const char *t, int n)
{
80104940:	f3 0f 1e fb          	endbr32 
80104944:	55                   	push   %ebp
80104945:	89 e5                	mov    %esp,%ebp
80104947:	56                   	push   %esi
80104948:	8b 55 10             	mov    0x10(%ebp),%edx
8010494b:	8b 75 08             	mov    0x8(%ebp),%esi
8010494e:	53                   	push   %ebx
8010494f:	8b 45 0c             	mov    0xc(%ebp),%eax
  char *os;

  os = s;
  if(n <= 0)
80104952:	85 d2                	test   %edx,%edx
80104954:	7e 21                	jle    80104977 <safestrcpy+0x37>
80104956:	8d 5c 10 ff          	lea    -0x1(%eax,%edx,1),%ebx
8010495a:	89 f2                	mov    %esi,%edx
8010495c:	eb 12                	jmp    80104970 <safestrcpy+0x30>
8010495e:	66 90                	xchg   %ax,%ax
    return os;
  while(--n > 0 && (*s++ = *t++) != 0)
80104960:	0f b6 08             	movzbl (%eax),%ecx
80104963:	83 c0 01             	add    $0x1,%eax
80104966:	83 c2 01             	add    $0x1,%edx
80104969:	88 4a ff             	mov    %cl,-0x1(%edx)
8010496c:	84 c9                	test   %cl,%cl
8010496e:	74 04                	je     80104974 <safestrcpy+0x34>
80104970:	39 d8                	cmp    %ebx,%eax
80104972:	75 ec                	jne    80104960 <safestrcpy+0x20>
    ;
  *s = 0;
80104974:	c6 02 00             	movb   $0x0,(%edx)
  return os;
}
80104977:	89 f0                	mov    %esi,%eax
80104979:	5b                   	pop    %ebx
8010497a:	5e                   	pop    %esi
8010497b:	5d                   	pop    %ebp
8010497c:	c3                   	ret    
8010497d:	8d 76 00             	lea    0x0(%esi),%esi

80104980 <strlen>:

int
strlen(const char *s)
{
80104980:	f3 0f 1e fb          	endbr32 
80104984:	55                   	push   %ebp
  int n;

  for(n = 0; s[n]; n++)
80104985:	31 c0                	xor    %eax,%eax
{
80104987:	89 e5                	mov    %esp,%ebp
80104989:	8b 55 08             	mov    0x8(%ebp),%edx
  for(n = 0; s[n]; n++)
8010498c:	80 3a 00             	cmpb   $0x0,(%edx)
8010498f:	74 10                	je     801049a1 <strlen+0x21>
80104991:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104998:	83 c0 01             	add    $0x1,%eax
8010499b:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
8010499f:	75 f7                	jne    80104998 <strlen+0x18>
    ;
  return n;
}
801049a1:	5d                   	pop    %ebp
801049a2:	c3                   	ret    

801049a3 <swtch>:
# a struct context, and save its address in *old.
# Switch stacks to new and pop previously-saved registers.

.globl swtch
swtch:
  movl 4(%esp), %eax
801049a3:	8b 44 24 04          	mov    0x4(%esp),%eax
  movl 8(%esp), %edx
801049a7:	8b 54 24 08          	mov    0x8(%esp),%edx

  # Save old callee-saved registers
  pushl %ebp
801049ab:	55                   	push   %ebp
  pushl %ebx
801049ac:	53                   	push   %ebx
  pushl %esi
801049ad:	56                   	push   %esi
  pushl %edi
801049ae:	57                   	push   %edi

  # Switch stacks
  movl %esp, (%eax)
801049af:	89 20                	mov    %esp,(%eax)
  movl %edx, %esp
801049b1:	89 d4                	mov    %edx,%esp

  # Load new callee-saved registers
  popl %edi
801049b3:	5f                   	pop    %edi
  popl %esi
801049b4:	5e                   	pop    %esi
  popl %ebx
801049b5:	5b                   	pop    %ebx
  popl %ebp
801049b6:	5d                   	pop    %ebp
  ret
801049b7:	c3                   	ret    
801049b8:	66 90                	xchg   %ax,%ax
801049ba:	66 90                	xchg   %ax,%ax
801049bc:	66 90                	xchg   %ax,%ax
801049be:	66 90                	xchg   %ax,%ax

801049c0 <fetchint>:
// to a saved program counter, and then the first argument.

// Fetch the int at addr from the current process.
int
fetchint(uint addr, int *ip)
{
801049c0:	f3 0f 1e fb          	endbr32 
801049c4:	55                   	push   %ebp
801049c5:	89 e5                	mov    %esp,%ebp
801049c7:	53                   	push   %ebx
801049c8:	83 ec 04             	sub    $0x4,%esp
801049cb:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct proc *curproc = myproc();
801049ce:	e8 2d f0 ff ff       	call   80103a00 <myproc>

  if(addr >= curproc->sz || addr+4 > curproc->sz)
801049d3:	8b 00                	mov    (%eax),%eax
801049d5:	39 d8                	cmp    %ebx,%eax
801049d7:	76 17                	jbe    801049f0 <fetchint+0x30>
801049d9:	8d 53 04             	lea    0x4(%ebx),%edx
801049dc:	39 d0                	cmp    %edx,%eax
801049de:	72 10                	jb     801049f0 <fetchint+0x30>
    return -1;
  *ip = *(int*)(addr);
801049e0:	8b 45 0c             	mov    0xc(%ebp),%eax
801049e3:	8b 13                	mov    (%ebx),%edx
801049e5:	89 10                	mov    %edx,(%eax)
  return 0;
801049e7:	31 c0                	xor    %eax,%eax
}
801049e9:	83 c4 04             	add    $0x4,%esp
801049ec:	5b                   	pop    %ebx
801049ed:	5d                   	pop    %ebp
801049ee:	c3                   	ret    
801049ef:	90                   	nop
    return -1;
801049f0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801049f5:	eb f2                	jmp    801049e9 <fetchint+0x29>
801049f7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801049fe:	66 90                	xchg   %ax,%ax

80104a00 <fetchstr>:
// Fetch the nul-terminated string at addr from the current process.
// Doesn't actually copy the string - just sets *pp to point at it.
// Returns length of string, not including nul.
int
fetchstr(uint addr, char **pp)
{
80104a00:	f3 0f 1e fb          	endbr32 
80104a04:	55                   	push   %ebp
80104a05:	89 e5                	mov    %esp,%ebp
80104a07:	53                   	push   %ebx
80104a08:	83 ec 04             	sub    $0x4,%esp
80104a0b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  char *s, *ep;
  struct proc *curproc = myproc();
80104a0e:	e8 ed ef ff ff       	call   80103a00 <myproc>

  if(addr >= curproc->sz)
80104a13:	39 18                	cmp    %ebx,(%eax)
80104a15:	76 31                	jbe    80104a48 <fetchstr+0x48>
    return -1;
  *pp = (char*)addr;
80104a17:	8b 55 0c             	mov    0xc(%ebp),%edx
80104a1a:	89 1a                	mov    %ebx,(%edx)
  ep = (char*)curproc->sz;
80104a1c:	8b 10                	mov    (%eax),%edx
  for(s = *pp; s < ep; s++){
80104a1e:	39 d3                	cmp    %edx,%ebx
80104a20:	73 26                	jae    80104a48 <fetchstr+0x48>
80104a22:	89 d8                	mov    %ebx,%eax
80104a24:	eb 11                	jmp    80104a37 <fetchstr+0x37>
80104a26:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104a2d:	8d 76 00             	lea    0x0(%esi),%esi
80104a30:	83 c0 01             	add    $0x1,%eax
80104a33:	39 c2                	cmp    %eax,%edx
80104a35:	76 11                	jbe    80104a48 <fetchstr+0x48>
    if(*s == 0)
80104a37:	80 38 00             	cmpb   $0x0,(%eax)
80104a3a:	75 f4                	jne    80104a30 <fetchstr+0x30>
      return s - *pp;
  }
  return -1;
}
80104a3c:	83 c4 04             	add    $0x4,%esp
      return s - *pp;
80104a3f:	29 d8                	sub    %ebx,%eax
}
80104a41:	5b                   	pop    %ebx
80104a42:	5d                   	pop    %ebp
80104a43:	c3                   	ret    
80104a44:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104a48:	83 c4 04             	add    $0x4,%esp
    return -1;
80104a4b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104a50:	5b                   	pop    %ebx
80104a51:	5d                   	pop    %ebp
80104a52:	c3                   	ret    
80104a53:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104a5a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80104a60 <argint>:

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
80104a60:	f3 0f 1e fb          	endbr32 
80104a64:	55                   	push   %ebp
80104a65:	89 e5                	mov    %esp,%ebp
80104a67:	56                   	push   %esi
80104a68:	53                   	push   %ebx
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
80104a69:	e8 92 ef ff ff       	call   80103a00 <myproc>
80104a6e:	8b 55 08             	mov    0x8(%ebp),%edx
80104a71:	8b 40 18             	mov    0x18(%eax),%eax
80104a74:	8b 40 44             	mov    0x44(%eax),%eax
80104a77:	8d 1c 90             	lea    (%eax,%edx,4),%ebx
  struct proc *curproc = myproc();
80104a7a:	e8 81 ef ff ff       	call   80103a00 <myproc>
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
80104a7f:	8d 73 04             	lea    0x4(%ebx),%esi
  if(addr >= curproc->sz || addr+4 > curproc->sz)
80104a82:	8b 00                	mov    (%eax),%eax
80104a84:	39 c6                	cmp    %eax,%esi
80104a86:	73 18                	jae    80104aa0 <argint+0x40>
80104a88:	8d 53 08             	lea    0x8(%ebx),%edx
80104a8b:	39 d0                	cmp    %edx,%eax
80104a8d:	72 11                	jb     80104aa0 <argint+0x40>
  *ip = *(int*)(addr);
80104a8f:	8b 45 0c             	mov    0xc(%ebp),%eax
80104a92:	8b 53 04             	mov    0x4(%ebx),%edx
80104a95:	89 10                	mov    %edx,(%eax)
  return 0;
80104a97:	31 c0                	xor    %eax,%eax
}
80104a99:	5b                   	pop    %ebx
80104a9a:	5e                   	pop    %esi
80104a9b:	5d                   	pop    %ebp
80104a9c:	c3                   	ret    
80104a9d:	8d 76 00             	lea    0x0(%esi),%esi
    return -1;
80104aa0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
80104aa5:	eb f2                	jmp    80104a99 <argint+0x39>
80104aa7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104aae:	66 90                	xchg   %ax,%ax

80104ab0 <argptr>:
// Fetch the nth word-sized system call argument as a pointer
// to a block of memory of size bytes.  Check that the pointer
// lies within the process address space.
int
argptr(int n, char **pp, int size)
{
80104ab0:	f3 0f 1e fb          	endbr32 
80104ab4:	55                   	push   %ebp
80104ab5:	89 e5                	mov    %esp,%ebp
80104ab7:	56                   	push   %esi
80104ab8:	53                   	push   %ebx
80104ab9:	83 ec 10             	sub    $0x10,%esp
80104abc:	8b 5d 10             	mov    0x10(%ebp),%ebx
  int i;
  struct proc *curproc = myproc();
80104abf:	e8 3c ef ff ff       	call   80103a00 <myproc>
 
  if(argint(n, &i) < 0)
80104ac4:	83 ec 08             	sub    $0x8,%esp
  struct proc *curproc = myproc();
80104ac7:	89 c6                	mov    %eax,%esi
  if(argint(n, &i) < 0)
80104ac9:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104acc:	50                   	push   %eax
80104acd:	ff 75 08             	pushl  0x8(%ebp)
80104ad0:	e8 8b ff ff ff       	call   80104a60 <argint>
    return -1;
  if(size < 0 || (uint)i >= curproc->sz || (uint)i+size > curproc->sz)
80104ad5:	83 c4 10             	add    $0x10,%esp
80104ad8:	85 c0                	test   %eax,%eax
80104ada:	78 24                	js     80104b00 <argptr+0x50>
80104adc:	85 db                	test   %ebx,%ebx
80104ade:	78 20                	js     80104b00 <argptr+0x50>
80104ae0:	8b 16                	mov    (%esi),%edx
80104ae2:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104ae5:	39 c2                	cmp    %eax,%edx
80104ae7:	76 17                	jbe    80104b00 <argptr+0x50>
80104ae9:	01 c3                	add    %eax,%ebx
80104aeb:	39 da                	cmp    %ebx,%edx
80104aed:	72 11                	jb     80104b00 <argptr+0x50>
    return -1;
  *pp = (char*)i;
80104aef:	8b 55 0c             	mov    0xc(%ebp),%edx
80104af2:	89 02                	mov    %eax,(%edx)
  return 0;
80104af4:	31 c0                	xor    %eax,%eax
}
80104af6:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104af9:	5b                   	pop    %ebx
80104afa:	5e                   	pop    %esi
80104afb:	5d                   	pop    %ebp
80104afc:	c3                   	ret    
80104afd:	8d 76 00             	lea    0x0(%esi),%esi
    return -1;
80104b00:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104b05:	eb ef                	jmp    80104af6 <argptr+0x46>
80104b07:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104b0e:	66 90                	xchg   %ax,%ax

80104b10 <argstr>:
// Check that the pointer is valid and the string is nul-terminated.
// (There is no shared writable memory, so the string can't change
// between this check and being used by the kernel.)
int
argstr(int n, char **pp)
{
80104b10:	f3 0f 1e fb          	endbr32 
80104b14:	55                   	push   %ebp
80104b15:	89 e5                	mov    %esp,%ebp
80104b17:	83 ec 20             	sub    $0x20,%esp
  int addr;
  if(argint(n, &addr) < 0)
80104b1a:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104b1d:	50                   	push   %eax
80104b1e:	ff 75 08             	pushl  0x8(%ebp)
80104b21:	e8 3a ff ff ff       	call   80104a60 <argint>
80104b26:	83 c4 10             	add    $0x10,%esp
80104b29:	85 c0                	test   %eax,%eax
80104b2b:	78 13                	js     80104b40 <argstr+0x30>
    return -1;
  return fetchstr(addr, pp);
80104b2d:	83 ec 08             	sub    $0x8,%esp
80104b30:	ff 75 0c             	pushl  0xc(%ebp)
80104b33:	ff 75 f4             	pushl  -0xc(%ebp)
80104b36:	e8 c5 fe ff ff       	call   80104a00 <fetchstr>
80104b3b:	83 c4 10             	add    $0x10,%esp
}
80104b3e:	c9                   	leave  
80104b3f:	c3                   	ret    
80104b40:	c9                   	leave  
    return -1;
80104b41:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104b46:	c3                   	ret    
80104b47:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104b4e:	66 90                	xchg   %ax,%ax

80104b50 <syscall>:
[SYS_close]   sys_close,
};

void
syscall(void)
{
80104b50:	f3 0f 1e fb          	endbr32 
80104b54:	55                   	push   %ebp
80104b55:	89 e5                	mov    %esp,%ebp
80104b57:	53                   	push   %ebx
80104b58:	83 ec 04             	sub    $0x4,%esp
  int num;
  struct proc *curproc = myproc();
80104b5b:	e8 a0 ee ff ff       	call   80103a00 <myproc>
80104b60:	89 c3                	mov    %eax,%ebx

  num = curproc->tf->eax;
80104b62:	8b 40 18             	mov    0x18(%eax),%eax
80104b65:	8b 40 1c             	mov    0x1c(%eax),%eax
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
80104b68:	8d 50 ff             	lea    -0x1(%eax),%edx
80104b6b:	83 fa 14             	cmp    $0x14,%edx
80104b6e:	77 20                	ja     80104b90 <syscall+0x40>
80104b70:	8b 14 85 20 7b 10 80 	mov    -0x7fef84e0(,%eax,4),%edx
80104b77:	85 d2                	test   %edx,%edx
80104b79:	74 15                	je     80104b90 <syscall+0x40>
    curproc->tf->eax = syscalls[num]();
80104b7b:	ff d2                	call   *%edx
80104b7d:	89 c2                	mov    %eax,%edx
80104b7f:	8b 43 18             	mov    0x18(%ebx),%eax
80104b82:	89 50 1c             	mov    %edx,0x1c(%eax)
  } else {
    cprintf("%d %s: unknown sys call %d\n",
            curproc->pid, curproc->name, num);
    curproc->tf->eax = -1;
  }
}
80104b85:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104b88:	c9                   	leave  
80104b89:	c3                   	ret    
80104b8a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    cprintf("%d %s: unknown sys call %d\n",
80104b90:	50                   	push   %eax
            curproc->pid, curproc->name, num);
80104b91:	8d 43 6c             	lea    0x6c(%ebx),%eax
    cprintf("%d %s: unknown sys call %d\n",
80104b94:	50                   	push   %eax
80104b95:	ff 73 10             	pushl  0x10(%ebx)
80104b98:	68 ed 7a 10 80       	push   $0x80107aed
80104b9d:	e8 0e bb ff ff       	call   801006b0 <cprintf>
    curproc->tf->eax = -1;
80104ba2:	8b 43 18             	mov    0x18(%ebx),%eax
80104ba5:	83 c4 10             	add    $0x10,%esp
80104ba8:	c7 40 1c ff ff ff ff 	movl   $0xffffffff,0x1c(%eax)
}
80104baf:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104bb2:	c9                   	leave  
80104bb3:	c3                   	ret    
80104bb4:	66 90                	xchg   %ax,%ax
80104bb6:	66 90                	xchg   %ax,%ax
80104bb8:	66 90                	xchg   %ax,%ax
80104bba:	66 90                	xchg   %ax,%ax
80104bbc:	66 90                	xchg   %ax,%ax
80104bbe:	66 90                	xchg   %ax,%ax

80104bc0 <create>:
  return -1;
}

static struct inode*
create(char *path, short type, short major, short minor)
{
80104bc0:	55                   	push   %ebp
80104bc1:	89 e5                	mov    %esp,%ebp
80104bc3:	57                   	push   %edi
80104bc4:	56                   	push   %esi
  struct inode *ip, *dp;
  char name[DIRSIZ];

  if((dp = nameiparent(path, name)) == 0)
80104bc5:	8d 7d da             	lea    -0x26(%ebp),%edi
{
80104bc8:	53                   	push   %ebx
80104bc9:	83 ec 34             	sub    $0x34,%esp
80104bcc:	89 4d d0             	mov    %ecx,-0x30(%ebp)
80104bcf:	8b 4d 08             	mov    0x8(%ebp),%ecx
  if((dp = nameiparent(path, name)) == 0)
80104bd2:	57                   	push   %edi
80104bd3:	50                   	push   %eax
{
80104bd4:	89 55 d4             	mov    %edx,-0x2c(%ebp)
80104bd7:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  if((dp = nameiparent(path, name)) == 0)
80104bda:	e8 91 d4 ff ff       	call   80102070 <nameiparent>
80104bdf:	83 c4 10             	add    $0x10,%esp
80104be2:	85 c0                	test   %eax,%eax
80104be4:	0f 84 46 01 00 00    	je     80104d30 <create+0x170>
    return 0;
  ilock(dp);
80104bea:	83 ec 0c             	sub    $0xc,%esp
80104bed:	89 c3                	mov    %eax,%ebx
80104bef:	50                   	push   %eax
80104bf0:	e8 8b cb ff ff       	call   80101780 <ilock>

  if((ip = dirlookup(dp, name, 0)) != 0){
80104bf5:	83 c4 0c             	add    $0xc,%esp
80104bf8:	6a 00                	push   $0x0
80104bfa:	57                   	push   %edi
80104bfb:	53                   	push   %ebx
80104bfc:	e8 cf d0 ff ff       	call   80101cd0 <dirlookup>
80104c01:	83 c4 10             	add    $0x10,%esp
80104c04:	89 c6                	mov    %eax,%esi
80104c06:	85 c0                	test   %eax,%eax
80104c08:	74 56                	je     80104c60 <create+0xa0>
    iunlockput(dp);
80104c0a:	83 ec 0c             	sub    $0xc,%esp
80104c0d:	53                   	push   %ebx
80104c0e:	e8 0d ce ff ff       	call   80101a20 <iunlockput>
    ilock(ip);
80104c13:	89 34 24             	mov    %esi,(%esp)
80104c16:	e8 65 cb ff ff       	call   80101780 <ilock>
    if(type == T_FILE && ip->type == T_FILE)
80104c1b:	83 c4 10             	add    $0x10,%esp
80104c1e:	66 83 7d d4 02       	cmpw   $0x2,-0x2c(%ebp)
80104c23:	75 1b                	jne    80104c40 <create+0x80>
80104c25:	66 83 7e 50 02       	cmpw   $0x2,0x50(%esi)
80104c2a:	75 14                	jne    80104c40 <create+0x80>
    panic("create: dirlink");

  iunlockput(dp);

  return ip;
}
80104c2c:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104c2f:	89 f0                	mov    %esi,%eax
80104c31:	5b                   	pop    %ebx
80104c32:	5e                   	pop    %esi
80104c33:	5f                   	pop    %edi
80104c34:	5d                   	pop    %ebp
80104c35:	c3                   	ret    
80104c36:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104c3d:	8d 76 00             	lea    0x0(%esi),%esi
    iunlockput(ip);
80104c40:	83 ec 0c             	sub    $0xc,%esp
80104c43:	56                   	push   %esi
    return 0;
80104c44:	31 f6                	xor    %esi,%esi
    iunlockput(ip);
80104c46:	e8 d5 cd ff ff       	call   80101a20 <iunlockput>
    return 0;
80104c4b:	83 c4 10             	add    $0x10,%esp
}
80104c4e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104c51:	89 f0                	mov    %esi,%eax
80104c53:	5b                   	pop    %ebx
80104c54:	5e                   	pop    %esi
80104c55:	5f                   	pop    %edi
80104c56:	5d                   	pop    %ebp
80104c57:	c3                   	ret    
80104c58:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104c5f:	90                   	nop
  if((ip = ialloc(dp->dev, type)) == 0)
80104c60:	0f bf 45 d4          	movswl -0x2c(%ebp),%eax
80104c64:	83 ec 08             	sub    $0x8,%esp
80104c67:	50                   	push   %eax
80104c68:	ff 33                	pushl  (%ebx)
80104c6a:	e8 91 c9 ff ff       	call   80101600 <ialloc>
80104c6f:	83 c4 10             	add    $0x10,%esp
80104c72:	89 c6                	mov    %eax,%esi
80104c74:	85 c0                	test   %eax,%eax
80104c76:	0f 84 cd 00 00 00    	je     80104d49 <create+0x189>
  ilock(ip);
80104c7c:	83 ec 0c             	sub    $0xc,%esp
80104c7f:	50                   	push   %eax
80104c80:	e8 fb ca ff ff       	call   80101780 <ilock>
  ip->major = major;
80104c85:	0f b7 45 d0          	movzwl -0x30(%ebp),%eax
80104c89:	66 89 46 52          	mov    %ax,0x52(%esi)
  ip->minor = minor;
80104c8d:	0f b7 45 cc          	movzwl -0x34(%ebp),%eax
80104c91:	66 89 46 54          	mov    %ax,0x54(%esi)
  ip->nlink = 1;
80104c95:	b8 01 00 00 00       	mov    $0x1,%eax
80104c9a:	66 89 46 56          	mov    %ax,0x56(%esi)
  iupdate(ip);
80104c9e:	89 34 24             	mov    %esi,(%esp)
80104ca1:	e8 1a ca ff ff       	call   801016c0 <iupdate>
  if(type == T_DIR){  // Create . and .. entries.
80104ca6:	83 c4 10             	add    $0x10,%esp
80104ca9:	66 83 7d d4 01       	cmpw   $0x1,-0x2c(%ebp)
80104cae:	74 30                	je     80104ce0 <create+0x120>
  if(dirlink(dp, name, ip->inum) < 0)
80104cb0:	83 ec 04             	sub    $0x4,%esp
80104cb3:	ff 76 04             	pushl  0x4(%esi)
80104cb6:	57                   	push   %edi
80104cb7:	53                   	push   %ebx
80104cb8:	e8 d3 d2 ff ff       	call   80101f90 <dirlink>
80104cbd:	83 c4 10             	add    $0x10,%esp
80104cc0:	85 c0                	test   %eax,%eax
80104cc2:	78 78                	js     80104d3c <create+0x17c>
  iunlockput(dp);
80104cc4:	83 ec 0c             	sub    $0xc,%esp
80104cc7:	53                   	push   %ebx
80104cc8:	e8 53 cd ff ff       	call   80101a20 <iunlockput>
  return ip;
80104ccd:	83 c4 10             	add    $0x10,%esp
}
80104cd0:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104cd3:	89 f0                	mov    %esi,%eax
80104cd5:	5b                   	pop    %ebx
80104cd6:	5e                   	pop    %esi
80104cd7:	5f                   	pop    %edi
80104cd8:	5d                   	pop    %ebp
80104cd9:	c3                   	ret    
80104cda:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    iupdate(dp);
80104ce0:	83 ec 0c             	sub    $0xc,%esp
    dp->nlink++;  // for ".."
80104ce3:	66 83 43 56 01       	addw   $0x1,0x56(%ebx)
    iupdate(dp);
80104ce8:	53                   	push   %ebx
80104ce9:	e8 d2 c9 ff ff       	call   801016c0 <iupdate>
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
80104cee:	83 c4 0c             	add    $0xc,%esp
80104cf1:	ff 76 04             	pushl  0x4(%esi)
80104cf4:	68 94 7b 10 80       	push   $0x80107b94
80104cf9:	56                   	push   %esi
80104cfa:	e8 91 d2 ff ff       	call   80101f90 <dirlink>
80104cff:	83 c4 10             	add    $0x10,%esp
80104d02:	85 c0                	test   %eax,%eax
80104d04:	78 18                	js     80104d1e <create+0x15e>
80104d06:	83 ec 04             	sub    $0x4,%esp
80104d09:	ff 73 04             	pushl  0x4(%ebx)
80104d0c:	68 93 7b 10 80       	push   $0x80107b93
80104d11:	56                   	push   %esi
80104d12:	e8 79 d2 ff ff       	call   80101f90 <dirlink>
80104d17:	83 c4 10             	add    $0x10,%esp
80104d1a:	85 c0                	test   %eax,%eax
80104d1c:	79 92                	jns    80104cb0 <create+0xf0>
      panic("create dots");
80104d1e:	83 ec 0c             	sub    $0xc,%esp
80104d21:	68 87 7b 10 80       	push   $0x80107b87
80104d26:	e8 65 b6 ff ff       	call   80100390 <panic>
80104d2b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104d2f:	90                   	nop
}
80104d30:	8d 65 f4             	lea    -0xc(%ebp),%esp
    return 0;
80104d33:	31 f6                	xor    %esi,%esi
}
80104d35:	5b                   	pop    %ebx
80104d36:	89 f0                	mov    %esi,%eax
80104d38:	5e                   	pop    %esi
80104d39:	5f                   	pop    %edi
80104d3a:	5d                   	pop    %ebp
80104d3b:	c3                   	ret    
    panic("create: dirlink");
80104d3c:	83 ec 0c             	sub    $0xc,%esp
80104d3f:	68 96 7b 10 80       	push   $0x80107b96
80104d44:	e8 47 b6 ff ff       	call   80100390 <panic>
    panic("create: ialloc");
80104d49:	83 ec 0c             	sub    $0xc,%esp
80104d4c:	68 78 7b 10 80       	push   $0x80107b78
80104d51:	e8 3a b6 ff ff       	call   80100390 <panic>
80104d56:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104d5d:	8d 76 00             	lea    0x0(%esi),%esi

80104d60 <argfd.constprop.0>:
argfd(int n, int *pfd, struct file **pf)
80104d60:	55                   	push   %ebp
80104d61:	89 e5                	mov    %esp,%ebp
80104d63:	56                   	push   %esi
80104d64:	89 d6                	mov    %edx,%esi
80104d66:	53                   	push   %ebx
80104d67:	89 c3                	mov    %eax,%ebx
  if(argint(n, &fd) < 0)
80104d69:	8d 45 f4             	lea    -0xc(%ebp),%eax
argfd(int n, int *pfd, struct file **pf)
80104d6c:	83 ec 18             	sub    $0x18,%esp
  if(argint(n, &fd) < 0)
80104d6f:	50                   	push   %eax
80104d70:	6a 00                	push   $0x0
80104d72:	e8 e9 fc ff ff       	call   80104a60 <argint>
80104d77:	83 c4 10             	add    $0x10,%esp
80104d7a:	85 c0                	test   %eax,%eax
80104d7c:	78 2a                	js     80104da8 <argfd.constprop.0+0x48>
  if(fd < 0 || fd >= NOFILE || (f=myproc()->ofile[fd]) == 0)
80104d7e:	83 7d f4 0f          	cmpl   $0xf,-0xc(%ebp)
80104d82:	77 24                	ja     80104da8 <argfd.constprop.0+0x48>
80104d84:	e8 77 ec ff ff       	call   80103a00 <myproc>
80104d89:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104d8c:	8b 44 90 28          	mov    0x28(%eax,%edx,4),%eax
80104d90:	85 c0                	test   %eax,%eax
80104d92:	74 14                	je     80104da8 <argfd.constprop.0+0x48>
  if(pfd)
80104d94:	85 db                	test   %ebx,%ebx
80104d96:	74 02                	je     80104d9a <argfd.constprop.0+0x3a>
    *pfd = fd;
80104d98:	89 13                	mov    %edx,(%ebx)
    *pf = f;
80104d9a:	89 06                	mov    %eax,(%esi)
  return 0;
80104d9c:	31 c0                	xor    %eax,%eax
}
80104d9e:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104da1:	5b                   	pop    %ebx
80104da2:	5e                   	pop    %esi
80104da3:	5d                   	pop    %ebp
80104da4:	c3                   	ret    
80104da5:	8d 76 00             	lea    0x0(%esi),%esi
    return -1;
80104da8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104dad:	eb ef                	jmp    80104d9e <argfd.constprop.0+0x3e>
80104daf:	90                   	nop

80104db0 <sys_dup>:
{
80104db0:	f3 0f 1e fb          	endbr32 
80104db4:	55                   	push   %ebp
  if(argfd(0, 0, &f) < 0)
80104db5:	31 c0                	xor    %eax,%eax
{
80104db7:	89 e5                	mov    %esp,%ebp
80104db9:	56                   	push   %esi
80104dba:	53                   	push   %ebx
  if(argfd(0, 0, &f) < 0)
80104dbb:	8d 55 f4             	lea    -0xc(%ebp),%edx
{
80104dbe:	83 ec 10             	sub    $0x10,%esp
  if(argfd(0, 0, &f) < 0)
80104dc1:	e8 9a ff ff ff       	call   80104d60 <argfd.constprop.0>
80104dc6:	85 c0                	test   %eax,%eax
80104dc8:	78 1e                	js     80104de8 <sys_dup+0x38>
  if((fd=fdalloc(f)) < 0)
80104dca:	8b 75 f4             	mov    -0xc(%ebp),%esi
  for(fd = 0; fd < NOFILE; fd++){
80104dcd:	31 db                	xor    %ebx,%ebx
  struct proc *curproc = myproc();
80104dcf:	e8 2c ec ff ff       	call   80103a00 <myproc>
  for(fd = 0; fd < NOFILE; fd++){
80104dd4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(curproc->ofile[fd] == 0){
80104dd8:	8b 54 98 28          	mov    0x28(%eax,%ebx,4),%edx
80104ddc:	85 d2                	test   %edx,%edx
80104dde:	74 20                	je     80104e00 <sys_dup+0x50>
  for(fd = 0; fd < NOFILE; fd++){
80104de0:	83 c3 01             	add    $0x1,%ebx
80104de3:	83 fb 10             	cmp    $0x10,%ebx
80104de6:	75 f0                	jne    80104dd8 <sys_dup+0x28>
}
80104de8:	8d 65 f8             	lea    -0x8(%ebp),%esp
    return -1;
80104deb:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
}
80104df0:	89 d8                	mov    %ebx,%eax
80104df2:	5b                   	pop    %ebx
80104df3:	5e                   	pop    %esi
80104df4:	5d                   	pop    %ebp
80104df5:	c3                   	ret    
80104df6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104dfd:	8d 76 00             	lea    0x0(%esi),%esi
      curproc->ofile[fd] = f;
80104e00:	89 74 98 28          	mov    %esi,0x28(%eax,%ebx,4)
  filedup(f);
80104e04:	83 ec 0c             	sub    $0xc,%esp
80104e07:	ff 75 f4             	pushl  -0xc(%ebp)
80104e0a:	e8 81 c0 ff ff       	call   80100e90 <filedup>
  return fd;
80104e0f:	83 c4 10             	add    $0x10,%esp
}
80104e12:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104e15:	89 d8                	mov    %ebx,%eax
80104e17:	5b                   	pop    %ebx
80104e18:	5e                   	pop    %esi
80104e19:	5d                   	pop    %ebp
80104e1a:	c3                   	ret    
80104e1b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104e1f:	90                   	nop

80104e20 <sys_read>:
{
80104e20:	f3 0f 1e fb          	endbr32 
80104e24:	55                   	push   %ebp
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104e25:	31 c0                	xor    %eax,%eax
{
80104e27:	89 e5                	mov    %esp,%ebp
80104e29:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104e2c:	8d 55 ec             	lea    -0x14(%ebp),%edx
80104e2f:	e8 2c ff ff ff       	call   80104d60 <argfd.constprop.0>
80104e34:	85 c0                	test   %eax,%eax
80104e36:	78 48                	js     80104e80 <sys_read+0x60>
80104e38:	83 ec 08             	sub    $0x8,%esp
80104e3b:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104e3e:	50                   	push   %eax
80104e3f:	6a 02                	push   $0x2
80104e41:	e8 1a fc ff ff       	call   80104a60 <argint>
80104e46:	83 c4 10             	add    $0x10,%esp
80104e49:	85 c0                	test   %eax,%eax
80104e4b:	78 33                	js     80104e80 <sys_read+0x60>
80104e4d:	83 ec 04             	sub    $0x4,%esp
80104e50:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104e53:	ff 75 f0             	pushl  -0x10(%ebp)
80104e56:	50                   	push   %eax
80104e57:	6a 01                	push   $0x1
80104e59:	e8 52 fc ff ff       	call   80104ab0 <argptr>
80104e5e:	83 c4 10             	add    $0x10,%esp
80104e61:	85 c0                	test   %eax,%eax
80104e63:	78 1b                	js     80104e80 <sys_read+0x60>
  return fileread(f, p, n);
80104e65:	83 ec 04             	sub    $0x4,%esp
80104e68:	ff 75 f0             	pushl  -0x10(%ebp)
80104e6b:	ff 75 f4             	pushl  -0xc(%ebp)
80104e6e:	ff 75 ec             	pushl  -0x14(%ebp)
80104e71:	e8 9a c1 ff ff       	call   80101010 <fileread>
80104e76:	83 c4 10             	add    $0x10,%esp
}
80104e79:	c9                   	leave  
80104e7a:	c3                   	ret    
80104e7b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104e7f:	90                   	nop
80104e80:	c9                   	leave  
    return -1;
80104e81:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104e86:	c3                   	ret    
80104e87:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104e8e:	66 90                	xchg   %ax,%ax

80104e90 <sys_write>:
{
80104e90:	f3 0f 1e fb          	endbr32 
80104e94:	55                   	push   %ebp
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104e95:	31 c0                	xor    %eax,%eax
{
80104e97:	89 e5                	mov    %esp,%ebp
80104e99:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104e9c:	8d 55 ec             	lea    -0x14(%ebp),%edx
80104e9f:	e8 bc fe ff ff       	call   80104d60 <argfd.constprop.0>
80104ea4:	85 c0                	test   %eax,%eax
80104ea6:	78 48                	js     80104ef0 <sys_write+0x60>
80104ea8:	83 ec 08             	sub    $0x8,%esp
80104eab:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104eae:	50                   	push   %eax
80104eaf:	6a 02                	push   $0x2
80104eb1:	e8 aa fb ff ff       	call   80104a60 <argint>
80104eb6:	83 c4 10             	add    $0x10,%esp
80104eb9:	85 c0                	test   %eax,%eax
80104ebb:	78 33                	js     80104ef0 <sys_write+0x60>
80104ebd:	83 ec 04             	sub    $0x4,%esp
80104ec0:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104ec3:	ff 75 f0             	pushl  -0x10(%ebp)
80104ec6:	50                   	push   %eax
80104ec7:	6a 01                	push   $0x1
80104ec9:	e8 e2 fb ff ff       	call   80104ab0 <argptr>
80104ece:	83 c4 10             	add    $0x10,%esp
80104ed1:	85 c0                	test   %eax,%eax
80104ed3:	78 1b                	js     80104ef0 <sys_write+0x60>
  return filewrite(f, p, n);
80104ed5:	83 ec 04             	sub    $0x4,%esp
80104ed8:	ff 75 f0             	pushl  -0x10(%ebp)
80104edb:	ff 75 f4             	pushl  -0xc(%ebp)
80104ede:	ff 75 ec             	pushl  -0x14(%ebp)
80104ee1:	e8 ca c1 ff ff       	call   801010b0 <filewrite>
80104ee6:	83 c4 10             	add    $0x10,%esp
}
80104ee9:	c9                   	leave  
80104eea:	c3                   	ret    
80104eeb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104eef:	90                   	nop
80104ef0:	c9                   	leave  
    return -1;
80104ef1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104ef6:	c3                   	ret    
80104ef7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104efe:	66 90                	xchg   %ax,%ax

80104f00 <sys_close>:
{
80104f00:	f3 0f 1e fb          	endbr32 
80104f04:	55                   	push   %ebp
80104f05:	89 e5                	mov    %esp,%ebp
80104f07:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, &fd, &f) < 0)
80104f0a:	8d 55 f4             	lea    -0xc(%ebp),%edx
80104f0d:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104f10:	e8 4b fe ff ff       	call   80104d60 <argfd.constprop.0>
80104f15:	85 c0                	test   %eax,%eax
80104f17:	78 27                	js     80104f40 <sys_close+0x40>
  myproc()->ofile[fd] = 0;
80104f19:	e8 e2 ea ff ff       	call   80103a00 <myproc>
80104f1e:	8b 55 f0             	mov    -0x10(%ebp),%edx
  fileclose(f);
80104f21:	83 ec 0c             	sub    $0xc,%esp
  myproc()->ofile[fd] = 0;
80104f24:	c7 44 90 28 00 00 00 	movl   $0x0,0x28(%eax,%edx,4)
80104f2b:	00 
  fileclose(f);
80104f2c:	ff 75 f4             	pushl  -0xc(%ebp)
80104f2f:	e8 ac bf ff ff       	call   80100ee0 <fileclose>
  return 0;
80104f34:	83 c4 10             	add    $0x10,%esp
80104f37:	31 c0                	xor    %eax,%eax
}
80104f39:	c9                   	leave  
80104f3a:	c3                   	ret    
80104f3b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104f3f:	90                   	nop
80104f40:	c9                   	leave  
    return -1;
80104f41:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104f46:	c3                   	ret    
80104f47:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104f4e:	66 90                	xchg   %ax,%ax

80104f50 <sys_fstat>:
{
80104f50:	f3 0f 1e fb          	endbr32 
80104f54:	55                   	push   %ebp
  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
80104f55:	31 c0                	xor    %eax,%eax
{
80104f57:	89 e5                	mov    %esp,%ebp
80104f59:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
80104f5c:	8d 55 f0             	lea    -0x10(%ebp),%edx
80104f5f:	e8 fc fd ff ff       	call   80104d60 <argfd.constprop.0>
80104f64:	85 c0                	test   %eax,%eax
80104f66:	78 30                	js     80104f98 <sys_fstat+0x48>
80104f68:	83 ec 04             	sub    $0x4,%esp
80104f6b:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104f6e:	6a 14                	push   $0x14
80104f70:	50                   	push   %eax
80104f71:	6a 01                	push   $0x1
80104f73:	e8 38 fb ff ff       	call   80104ab0 <argptr>
80104f78:	83 c4 10             	add    $0x10,%esp
80104f7b:	85 c0                	test   %eax,%eax
80104f7d:	78 19                	js     80104f98 <sys_fstat+0x48>
  return filestat(f, st);
80104f7f:	83 ec 08             	sub    $0x8,%esp
80104f82:	ff 75 f4             	pushl  -0xc(%ebp)
80104f85:	ff 75 f0             	pushl  -0x10(%ebp)
80104f88:	e8 33 c0 ff ff       	call   80100fc0 <filestat>
80104f8d:	83 c4 10             	add    $0x10,%esp
}
80104f90:	c9                   	leave  
80104f91:	c3                   	ret    
80104f92:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104f98:	c9                   	leave  
    return -1;
80104f99:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104f9e:	c3                   	ret    
80104f9f:	90                   	nop

80104fa0 <sys_link>:
{
80104fa0:	f3 0f 1e fb          	endbr32 
80104fa4:	55                   	push   %ebp
80104fa5:	89 e5                	mov    %esp,%ebp
80104fa7:	57                   	push   %edi
80104fa8:	56                   	push   %esi
  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
80104fa9:	8d 45 d4             	lea    -0x2c(%ebp),%eax
{
80104fac:	53                   	push   %ebx
80104fad:	83 ec 34             	sub    $0x34,%esp
  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
80104fb0:	50                   	push   %eax
80104fb1:	6a 00                	push   $0x0
80104fb3:	e8 58 fb ff ff       	call   80104b10 <argstr>
80104fb8:	83 c4 10             	add    $0x10,%esp
80104fbb:	85 c0                	test   %eax,%eax
80104fbd:	0f 88 ff 00 00 00    	js     801050c2 <sys_link+0x122>
80104fc3:	83 ec 08             	sub    $0x8,%esp
80104fc6:	8d 45 d0             	lea    -0x30(%ebp),%eax
80104fc9:	50                   	push   %eax
80104fca:	6a 01                	push   $0x1
80104fcc:	e8 3f fb ff ff       	call   80104b10 <argstr>
80104fd1:	83 c4 10             	add    $0x10,%esp
80104fd4:	85 c0                	test   %eax,%eax
80104fd6:	0f 88 e6 00 00 00    	js     801050c2 <sys_link+0x122>
  begin_op();
80104fdc:	e8 6f dd ff ff       	call   80102d50 <begin_op>
  if((ip = namei(old)) == 0){
80104fe1:	83 ec 0c             	sub    $0xc,%esp
80104fe4:	ff 75 d4             	pushl  -0x2c(%ebp)
80104fe7:	e8 64 d0 ff ff       	call   80102050 <namei>
80104fec:	83 c4 10             	add    $0x10,%esp
80104fef:	89 c3                	mov    %eax,%ebx
80104ff1:	85 c0                	test   %eax,%eax
80104ff3:	0f 84 e8 00 00 00    	je     801050e1 <sys_link+0x141>
  ilock(ip);
80104ff9:	83 ec 0c             	sub    $0xc,%esp
80104ffc:	50                   	push   %eax
80104ffd:	e8 7e c7 ff ff       	call   80101780 <ilock>
  if(ip->type == T_DIR){
80105002:	83 c4 10             	add    $0x10,%esp
80105005:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
8010500a:	0f 84 b9 00 00 00    	je     801050c9 <sys_link+0x129>
  iupdate(ip);
80105010:	83 ec 0c             	sub    $0xc,%esp
  ip->nlink++;
80105013:	66 83 43 56 01       	addw   $0x1,0x56(%ebx)
  if((dp = nameiparent(new, name)) == 0)
80105018:	8d 7d da             	lea    -0x26(%ebp),%edi
  iupdate(ip);
8010501b:	53                   	push   %ebx
8010501c:	e8 9f c6 ff ff       	call   801016c0 <iupdate>
  iunlock(ip);
80105021:	89 1c 24             	mov    %ebx,(%esp)
80105024:	e8 37 c8 ff ff       	call   80101860 <iunlock>
  if((dp = nameiparent(new, name)) == 0)
80105029:	58                   	pop    %eax
8010502a:	5a                   	pop    %edx
8010502b:	57                   	push   %edi
8010502c:	ff 75 d0             	pushl  -0x30(%ebp)
8010502f:	e8 3c d0 ff ff       	call   80102070 <nameiparent>
80105034:	83 c4 10             	add    $0x10,%esp
80105037:	89 c6                	mov    %eax,%esi
80105039:	85 c0                	test   %eax,%eax
8010503b:	74 5f                	je     8010509c <sys_link+0xfc>
  ilock(dp);
8010503d:	83 ec 0c             	sub    $0xc,%esp
80105040:	50                   	push   %eax
80105041:	e8 3a c7 ff ff       	call   80101780 <ilock>
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
80105046:	8b 03                	mov    (%ebx),%eax
80105048:	83 c4 10             	add    $0x10,%esp
8010504b:	39 06                	cmp    %eax,(%esi)
8010504d:	75 41                	jne    80105090 <sys_link+0xf0>
8010504f:	83 ec 04             	sub    $0x4,%esp
80105052:	ff 73 04             	pushl  0x4(%ebx)
80105055:	57                   	push   %edi
80105056:	56                   	push   %esi
80105057:	e8 34 cf ff ff       	call   80101f90 <dirlink>
8010505c:	83 c4 10             	add    $0x10,%esp
8010505f:	85 c0                	test   %eax,%eax
80105061:	78 2d                	js     80105090 <sys_link+0xf0>
  iunlockput(dp);
80105063:	83 ec 0c             	sub    $0xc,%esp
80105066:	56                   	push   %esi
80105067:	e8 b4 c9 ff ff       	call   80101a20 <iunlockput>
  iput(ip);
8010506c:	89 1c 24             	mov    %ebx,(%esp)
8010506f:	e8 3c c8 ff ff       	call   801018b0 <iput>
  end_op();
80105074:	e8 47 dd ff ff       	call   80102dc0 <end_op>
  return 0;
80105079:	83 c4 10             	add    $0x10,%esp
8010507c:	31 c0                	xor    %eax,%eax
}
8010507e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105081:	5b                   	pop    %ebx
80105082:	5e                   	pop    %esi
80105083:	5f                   	pop    %edi
80105084:	5d                   	pop    %ebp
80105085:	c3                   	ret    
80105086:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010508d:	8d 76 00             	lea    0x0(%esi),%esi
    iunlockput(dp);
80105090:	83 ec 0c             	sub    $0xc,%esp
80105093:	56                   	push   %esi
80105094:	e8 87 c9 ff ff       	call   80101a20 <iunlockput>
    goto bad;
80105099:	83 c4 10             	add    $0x10,%esp
  ilock(ip);
8010509c:	83 ec 0c             	sub    $0xc,%esp
8010509f:	53                   	push   %ebx
801050a0:	e8 db c6 ff ff       	call   80101780 <ilock>
  ip->nlink--;
801050a5:	66 83 6b 56 01       	subw   $0x1,0x56(%ebx)
  iupdate(ip);
801050aa:	89 1c 24             	mov    %ebx,(%esp)
801050ad:	e8 0e c6 ff ff       	call   801016c0 <iupdate>
  iunlockput(ip);
801050b2:	89 1c 24             	mov    %ebx,(%esp)
801050b5:	e8 66 c9 ff ff       	call   80101a20 <iunlockput>
  end_op();
801050ba:	e8 01 dd ff ff       	call   80102dc0 <end_op>
  return -1;
801050bf:	83 c4 10             	add    $0x10,%esp
801050c2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801050c7:	eb b5                	jmp    8010507e <sys_link+0xde>
    iunlockput(ip);
801050c9:	83 ec 0c             	sub    $0xc,%esp
801050cc:	53                   	push   %ebx
801050cd:	e8 4e c9 ff ff       	call   80101a20 <iunlockput>
    end_op();
801050d2:	e8 e9 dc ff ff       	call   80102dc0 <end_op>
    return -1;
801050d7:	83 c4 10             	add    $0x10,%esp
801050da:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801050df:	eb 9d                	jmp    8010507e <sys_link+0xde>
    end_op();
801050e1:	e8 da dc ff ff       	call   80102dc0 <end_op>
    return -1;
801050e6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801050eb:	eb 91                	jmp    8010507e <sys_link+0xde>
801050ed:	8d 76 00             	lea    0x0(%esi),%esi

801050f0 <sys_unlink>:
{
801050f0:	f3 0f 1e fb          	endbr32 
801050f4:	55                   	push   %ebp
801050f5:	89 e5                	mov    %esp,%ebp
801050f7:	57                   	push   %edi
801050f8:	56                   	push   %esi
  if(argstr(0, &path) < 0)
801050f9:	8d 45 c0             	lea    -0x40(%ebp),%eax
{
801050fc:	53                   	push   %ebx
801050fd:	83 ec 54             	sub    $0x54,%esp
  if(argstr(0, &path) < 0)
80105100:	50                   	push   %eax
80105101:	6a 00                	push   $0x0
80105103:	e8 08 fa ff ff       	call   80104b10 <argstr>
80105108:	83 c4 10             	add    $0x10,%esp
8010510b:	85 c0                	test   %eax,%eax
8010510d:	0f 88 7d 01 00 00    	js     80105290 <sys_unlink+0x1a0>
  begin_op();
80105113:	e8 38 dc ff ff       	call   80102d50 <begin_op>
  if((dp = nameiparent(path, name)) == 0){
80105118:	8d 5d ca             	lea    -0x36(%ebp),%ebx
8010511b:	83 ec 08             	sub    $0x8,%esp
8010511e:	53                   	push   %ebx
8010511f:	ff 75 c0             	pushl  -0x40(%ebp)
80105122:	e8 49 cf ff ff       	call   80102070 <nameiparent>
80105127:	83 c4 10             	add    $0x10,%esp
8010512a:	89 c6                	mov    %eax,%esi
8010512c:	85 c0                	test   %eax,%eax
8010512e:	0f 84 66 01 00 00    	je     8010529a <sys_unlink+0x1aa>
  ilock(dp);
80105134:	83 ec 0c             	sub    $0xc,%esp
80105137:	50                   	push   %eax
80105138:	e8 43 c6 ff ff       	call   80101780 <ilock>
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
8010513d:	58                   	pop    %eax
8010513e:	5a                   	pop    %edx
8010513f:	68 94 7b 10 80       	push   $0x80107b94
80105144:	53                   	push   %ebx
80105145:	e8 66 cb ff ff       	call   80101cb0 <namecmp>
8010514a:	83 c4 10             	add    $0x10,%esp
8010514d:	85 c0                	test   %eax,%eax
8010514f:	0f 84 03 01 00 00    	je     80105258 <sys_unlink+0x168>
80105155:	83 ec 08             	sub    $0x8,%esp
80105158:	68 93 7b 10 80       	push   $0x80107b93
8010515d:	53                   	push   %ebx
8010515e:	e8 4d cb ff ff       	call   80101cb0 <namecmp>
80105163:	83 c4 10             	add    $0x10,%esp
80105166:	85 c0                	test   %eax,%eax
80105168:	0f 84 ea 00 00 00    	je     80105258 <sys_unlink+0x168>
  if((ip = dirlookup(dp, name, &off)) == 0)
8010516e:	83 ec 04             	sub    $0x4,%esp
80105171:	8d 45 c4             	lea    -0x3c(%ebp),%eax
80105174:	50                   	push   %eax
80105175:	53                   	push   %ebx
80105176:	56                   	push   %esi
80105177:	e8 54 cb ff ff       	call   80101cd0 <dirlookup>
8010517c:	83 c4 10             	add    $0x10,%esp
8010517f:	89 c3                	mov    %eax,%ebx
80105181:	85 c0                	test   %eax,%eax
80105183:	0f 84 cf 00 00 00    	je     80105258 <sys_unlink+0x168>
  ilock(ip);
80105189:	83 ec 0c             	sub    $0xc,%esp
8010518c:	50                   	push   %eax
8010518d:	e8 ee c5 ff ff       	call   80101780 <ilock>
  if(ip->nlink < 1)
80105192:	83 c4 10             	add    $0x10,%esp
80105195:	66 83 7b 56 00       	cmpw   $0x0,0x56(%ebx)
8010519a:	0f 8e 23 01 00 00    	jle    801052c3 <sys_unlink+0x1d3>
  if(ip->type == T_DIR && !isdirempty(ip)){
801051a0:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
801051a5:	8d 7d d8             	lea    -0x28(%ebp),%edi
801051a8:	74 66                	je     80105210 <sys_unlink+0x120>
  memset(&de, 0, sizeof(de));
801051aa:	83 ec 04             	sub    $0x4,%esp
801051ad:	6a 10                	push   $0x10
801051af:	6a 00                	push   $0x0
801051b1:	57                   	push   %edi
801051b2:	e8 c9 f5 ff ff       	call   80104780 <memset>
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801051b7:	6a 10                	push   $0x10
801051b9:	ff 75 c4             	pushl  -0x3c(%ebp)
801051bc:	57                   	push   %edi
801051bd:	56                   	push   %esi
801051be:	e8 bd c9 ff ff       	call   80101b80 <writei>
801051c3:	83 c4 20             	add    $0x20,%esp
801051c6:	83 f8 10             	cmp    $0x10,%eax
801051c9:	0f 85 e7 00 00 00    	jne    801052b6 <sys_unlink+0x1c6>
  if(ip->type == T_DIR){
801051cf:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
801051d4:	0f 84 96 00 00 00    	je     80105270 <sys_unlink+0x180>
  iunlockput(dp);
801051da:	83 ec 0c             	sub    $0xc,%esp
801051dd:	56                   	push   %esi
801051de:	e8 3d c8 ff ff       	call   80101a20 <iunlockput>
  ip->nlink--;
801051e3:	66 83 6b 56 01       	subw   $0x1,0x56(%ebx)
  iupdate(ip);
801051e8:	89 1c 24             	mov    %ebx,(%esp)
801051eb:	e8 d0 c4 ff ff       	call   801016c0 <iupdate>
  iunlockput(ip);
801051f0:	89 1c 24             	mov    %ebx,(%esp)
801051f3:	e8 28 c8 ff ff       	call   80101a20 <iunlockput>
  end_op();
801051f8:	e8 c3 db ff ff       	call   80102dc0 <end_op>
  return 0;
801051fd:	83 c4 10             	add    $0x10,%esp
80105200:	31 c0                	xor    %eax,%eax
}
80105202:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105205:	5b                   	pop    %ebx
80105206:	5e                   	pop    %esi
80105207:	5f                   	pop    %edi
80105208:	5d                   	pop    %ebp
80105209:	c3                   	ret    
8010520a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
80105210:	83 7b 58 20          	cmpl   $0x20,0x58(%ebx)
80105214:	76 94                	jbe    801051aa <sys_unlink+0xba>
80105216:	ba 20 00 00 00       	mov    $0x20,%edx
8010521b:	eb 0b                	jmp    80105228 <sys_unlink+0x138>
8010521d:	8d 76 00             	lea    0x0(%esi),%esi
80105220:	83 c2 10             	add    $0x10,%edx
80105223:	39 53 58             	cmp    %edx,0x58(%ebx)
80105226:	76 82                	jbe    801051aa <sys_unlink+0xba>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80105228:	6a 10                	push   $0x10
8010522a:	52                   	push   %edx
8010522b:	57                   	push   %edi
8010522c:	53                   	push   %ebx
8010522d:	89 55 b4             	mov    %edx,-0x4c(%ebp)
80105230:	e8 4b c8 ff ff       	call   80101a80 <readi>
80105235:	83 c4 10             	add    $0x10,%esp
80105238:	8b 55 b4             	mov    -0x4c(%ebp),%edx
8010523b:	83 f8 10             	cmp    $0x10,%eax
8010523e:	75 69                	jne    801052a9 <sys_unlink+0x1b9>
    if(de.inum != 0)
80105240:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
80105245:	74 d9                	je     80105220 <sys_unlink+0x130>
    iunlockput(ip);
80105247:	83 ec 0c             	sub    $0xc,%esp
8010524a:	53                   	push   %ebx
8010524b:	e8 d0 c7 ff ff       	call   80101a20 <iunlockput>
    goto bad;
80105250:	83 c4 10             	add    $0x10,%esp
80105253:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105257:	90                   	nop
  iunlockput(dp);
80105258:	83 ec 0c             	sub    $0xc,%esp
8010525b:	56                   	push   %esi
8010525c:	e8 bf c7 ff ff       	call   80101a20 <iunlockput>
  end_op();
80105261:	e8 5a db ff ff       	call   80102dc0 <end_op>
  return -1;
80105266:	83 c4 10             	add    $0x10,%esp
80105269:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010526e:	eb 92                	jmp    80105202 <sys_unlink+0x112>
    iupdate(dp);
80105270:	83 ec 0c             	sub    $0xc,%esp
    dp->nlink--;
80105273:	66 83 6e 56 01       	subw   $0x1,0x56(%esi)
    iupdate(dp);
80105278:	56                   	push   %esi
80105279:	e8 42 c4 ff ff       	call   801016c0 <iupdate>
8010527e:	83 c4 10             	add    $0x10,%esp
80105281:	e9 54 ff ff ff       	jmp    801051da <sys_unlink+0xea>
80105286:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010528d:	8d 76 00             	lea    0x0(%esi),%esi
    return -1;
80105290:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105295:	e9 68 ff ff ff       	jmp    80105202 <sys_unlink+0x112>
    end_op();
8010529a:	e8 21 db ff ff       	call   80102dc0 <end_op>
    return -1;
8010529f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801052a4:	e9 59 ff ff ff       	jmp    80105202 <sys_unlink+0x112>
      panic("isdirempty: readi");
801052a9:	83 ec 0c             	sub    $0xc,%esp
801052ac:	68 b8 7b 10 80       	push   $0x80107bb8
801052b1:	e8 da b0 ff ff       	call   80100390 <panic>
    panic("unlink: writei");
801052b6:	83 ec 0c             	sub    $0xc,%esp
801052b9:	68 ca 7b 10 80       	push   $0x80107bca
801052be:	e8 cd b0 ff ff       	call   80100390 <panic>
    panic("unlink: nlink < 1");
801052c3:	83 ec 0c             	sub    $0xc,%esp
801052c6:	68 a6 7b 10 80       	push   $0x80107ba6
801052cb:	e8 c0 b0 ff ff       	call   80100390 <panic>

801052d0 <sys_open>:

int
sys_open(void)
{
801052d0:	f3 0f 1e fb          	endbr32 
801052d4:	55                   	push   %ebp
801052d5:	89 e5                	mov    %esp,%ebp
801052d7:	57                   	push   %edi
801052d8:	56                   	push   %esi
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
801052d9:	8d 45 e0             	lea    -0x20(%ebp),%eax
{
801052dc:	53                   	push   %ebx
801052dd:	83 ec 24             	sub    $0x24,%esp
  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
801052e0:	50                   	push   %eax
801052e1:	6a 00                	push   $0x0
801052e3:	e8 28 f8 ff ff       	call   80104b10 <argstr>
801052e8:	83 c4 10             	add    $0x10,%esp
801052eb:	85 c0                	test   %eax,%eax
801052ed:	0f 88 8a 00 00 00    	js     8010537d <sys_open+0xad>
801052f3:	83 ec 08             	sub    $0x8,%esp
801052f6:	8d 45 e4             	lea    -0x1c(%ebp),%eax
801052f9:	50                   	push   %eax
801052fa:	6a 01                	push   $0x1
801052fc:	e8 5f f7 ff ff       	call   80104a60 <argint>
80105301:	83 c4 10             	add    $0x10,%esp
80105304:	85 c0                	test   %eax,%eax
80105306:	78 75                	js     8010537d <sys_open+0xad>
    return -1;

  begin_op();
80105308:	e8 43 da ff ff       	call   80102d50 <begin_op>

  if(omode & O_CREATE){
8010530d:	f6 45 e5 02          	testb  $0x2,-0x1b(%ebp)
80105311:	75 75                	jne    80105388 <sys_open+0xb8>
    if(ip == 0){
      end_op();
      return -1;
    }
  } else {
    if((ip = namei(path)) == 0){
80105313:	83 ec 0c             	sub    $0xc,%esp
80105316:	ff 75 e0             	pushl  -0x20(%ebp)
80105319:	e8 32 cd ff ff       	call   80102050 <namei>
8010531e:	83 c4 10             	add    $0x10,%esp
80105321:	89 c6                	mov    %eax,%esi
80105323:	85 c0                	test   %eax,%eax
80105325:	74 7e                	je     801053a5 <sys_open+0xd5>
      end_op();
      return -1;
    }
    ilock(ip);
80105327:	83 ec 0c             	sub    $0xc,%esp
8010532a:	50                   	push   %eax
8010532b:	e8 50 c4 ff ff       	call   80101780 <ilock>
    if(ip->type == T_DIR && omode != O_RDONLY){
80105330:	83 c4 10             	add    $0x10,%esp
80105333:	66 83 7e 50 01       	cmpw   $0x1,0x50(%esi)
80105338:	0f 84 c2 00 00 00    	je     80105400 <sys_open+0x130>
      end_op();
      return -1;
    }
  }

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
8010533e:	e8 dd ba ff ff       	call   80100e20 <filealloc>
80105343:	89 c7                	mov    %eax,%edi
80105345:	85 c0                	test   %eax,%eax
80105347:	74 23                	je     8010536c <sys_open+0x9c>
  struct proc *curproc = myproc();
80105349:	e8 b2 e6 ff ff       	call   80103a00 <myproc>
  for(fd = 0; fd < NOFILE; fd++){
8010534e:	31 db                	xor    %ebx,%ebx
    if(curproc->ofile[fd] == 0){
80105350:	8b 54 98 28          	mov    0x28(%eax,%ebx,4),%edx
80105354:	85 d2                	test   %edx,%edx
80105356:	74 60                	je     801053b8 <sys_open+0xe8>
  for(fd = 0; fd < NOFILE; fd++){
80105358:	83 c3 01             	add    $0x1,%ebx
8010535b:	83 fb 10             	cmp    $0x10,%ebx
8010535e:	75 f0                	jne    80105350 <sys_open+0x80>
    if(f)
      fileclose(f);
80105360:	83 ec 0c             	sub    $0xc,%esp
80105363:	57                   	push   %edi
80105364:	e8 77 bb ff ff       	call   80100ee0 <fileclose>
80105369:	83 c4 10             	add    $0x10,%esp
    iunlockput(ip);
8010536c:	83 ec 0c             	sub    $0xc,%esp
8010536f:	56                   	push   %esi
80105370:	e8 ab c6 ff ff       	call   80101a20 <iunlockput>
    end_op();
80105375:	e8 46 da ff ff       	call   80102dc0 <end_op>
    return -1;
8010537a:	83 c4 10             	add    $0x10,%esp
8010537d:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80105382:	eb 6d                	jmp    801053f1 <sys_open+0x121>
80105384:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    ip = create(path, T_FILE, 0, 0);
80105388:	83 ec 0c             	sub    $0xc,%esp
8010538b:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010538e:	31 c9                	xor    %ecx,%ecx
80105390:	ba 02 00 00 00       	mov    $0x2,%edx
80105395:	6a 00                	push   $0x0
80105397:	e8 24 f8 ff ff       	call   80104bc0 <create>
    if(ip == 0){
8010539c:	83 c4 10             	add    $0x10,%esp
    ip = create(path, T_FILE, 0, 0);
8010539f:	89 c6                	mov    %eax,%esi
    if(ip == 0){
801053a1:	85 c0                	test   %eax,%eax
801053a3:	75 99                	jne    8010533e <sys_open+0x6e>
      end_op();
801053a5:	e8 16 da ff ff       	call   80102dc0 <end_op>
      return -1;
801053aa:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
801053af:	eb 40                	jmp    801053f1 <sys_open+0x121>
801053b1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  }
  iunlock(ip);
801053b8:	83 ec 0c             	sub    $0xc,%esp
      curproc->ofile[fd] = f;
801053bb:	89 7c 98 28          	mov    %edi,0x28(%eax,%ebx,4)
  iunlock(ip);
801053bf:	56                   	push   %esi
801053c0:	e8 9b c4 ff ff       	call   80101860 <iunlock>
  end_op();
801053c5:	e8 f6 d9 ff ff       	call   80102dc0 <end_op>

  f->type = FD_INODE;
801053ca:	c7 07 02 00 00 00    	movl   $0x2,(%edi)
  f->ip = ip;
  f->off = 0;
  f->readable = !(omode & O_WRONLY);
801053d0:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
801053d3:	83 c4 10             	add    $0x10,%esp
  f->ip = ip;
801053d6:	89 77 10             	mov    %esi,0x10(%edi)
  f->readable = !(omode & O_WRONLY);
801053d9:	89 d0                	mov    %edx,%eax
  f->off = 0;
801053db:	c7 47 14 00 00 00 00 	movl   $0x0,0x14(%edi)
  f->readable = !(omode & O_WRONLY);
801053e2:	f7 d0                	not    %eax
801053e4:	83 e0 01             	and    $0x1,%eax
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
801053e7:	83 e2 03             	and    $0x3,%edx
  f->readable = !(omode & O_WRONLY);
801053ea:	88 47 08             	mov    %al,0x8(%edi)
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
801053ed:	0f 95 47 09          	setne  0x9(%edi)
  return fd;
}
801053f1:	8d 65 f4             	lea    -0xc(%ebp),%esp
801053f4:	89 d8                	mov    %ebx,%eax
801053f6:	5b                   	pop    %ebx
801053f7:	5e                   	pop    %esi
801053f8:	5f                   	pop    %edi
801053f9:	5d                   	pop    %ebp
801053fa:	c3                   	ret    
801053fb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801053ff:	90                   	nop
    if(ip->type == T_DIR && omode != O_RDONLY){
80105400:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80105403:	85 c9                	test   %ecx,%ecx
80105405:	0f 84 33 ff ff ff    	je     8010533e <sys_open+0x6e>
8010540b:	e9 5c ff ff ff       	jmp    8010536c <sys_open+0x9c>

80105410 <sys_mkdir>:

int
sys_mkdir(void)
{
80105410:	f3 0f 1e fb          	endbr32 
80105414:	55                   	push   %ebp
80105415:	89 e5                	mov    %esp,%ebp
80105417:	83 ec 18             	sub    $0x18,%esp
  char *path;
  struct inode *ip;

  begin_op();
8010541a:	e8 31 d9 ff ff       	call   80102d50 <begin_op>
  if(argstr(0, &path) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0){
8010541f:	83 ec 08             	sub    $0x8,%esp
80105422:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105425:	50                   	push   %eax
80105426:	6a 00                	push   $0x0
80105428:	e8 e3 f6 ff ff       	call   80104b10 <argstr>
8010542d:	83 c4 10             	add    $0x10,%esp
80105430:	85 c0                	test   %eax,%eax
80105432:	78 34                	js     80105468 <sys_mkdir+0x58>
80105434:	83 ec 0c             	sub    $0xc,%esp
80105437:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010543a:	31 c9                	xor    %ecx,%ecx
8010543c:	ba 01 00 00 00       	mov    $0x1,%edx
80105441:	6a 00                	push   $0x0
80105443:	e8 78 f7 ff ff       	call   80104bc0 <create>
80105448:	83 c4 10             	add    $0x10,%esp
8010544b:	85 c0                	test   %eax,%eax
8010544d:	74 19                	je     80105468 <sys_mkdir+0x58>
    end_op();
    return -1;
  }
  iunlockput(ip);
8010544f:	83 ec 0c             	sub    $0xc,%esp
80105452:	50                   	push   %eax
80105453:	e8 c8 c5 ff ff       	call   80101a20 <iunlockput>
  end_op();
80105458:	e8 63 d9 ff ff       	call   80102dc0 <end_op>
  return 0;
8010545d:	83 c4 10             	add    $0x10,%esp
80105460:	31 c0                	xor    %eax,%eax
}
80105462:	c9                   	leave  
80105463:	c3                   	ret    
80105464:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    end_op();
80105468:	e8 53 d9 ff ff       	call   80102dc0 <end_op>
    return -1;
8010546d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105472:	c9                   	leave  
80105473:	c3                   	ret    
80105474:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010547b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010547f:	90                   	nop

80105480 <sys_mknod>:

int
sys_mknod(void)
{
80105480:	f3 0f 1e fb          	endbr32 
80105484:	55                   	push   %ebp
80105485:	89 e5                	mov    %esp,%ebp
80105487:	83 ec 18             	sub    $0x18,%esp
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
8010548a:	e8 c1 d8 ff ff       	call   80102d50 <begin_op>
  if((argstr(0, &path)) < 0 ||
8010548f:	83 ec 08             	sub    $0x8,%esp
80105492:	8d 45 ec             	lea    -0x14(%ebp),%eax
80105495:	50                   	push   %eax
80105496:	6a 00                	push   $0x0
80105498:	e8 73 f6 ff ff       	call   80104b10 <argstr>
8010549d:	83 c4 10             	add    $0x10,%esp
801054a0:	85 c0                	test   %eax,%eax
801054a2:	78 64                	js     80105508 <sys_mknod+0x88>
     argint(1, &major) < 0 ||
801054a4:	83 ec 08             	sub    $0x8,%esp
801054a7:	8d 45 f0             	lea    -0x10(%ebp),%eax
801054aa:	50                   	push   %eax
801054ab:	6a 01                	push   $0x1
801054ad:	e8 ae f5 ff ff       	call   80104a60 <argint>
  if((argstr(0, &path)) < 0 ||
801054b2:	83 c4 10             	add    $0x10,%esp
801054b5:	85 c0                	test   %eax,%eax
801054b7:	78 4f                	js     80105508 <sys_mknod+0x88>
     argint(2, &minor) < 0 ||
801054b9:	83 ec 08             	sub    $0x8,%esp
801054bc:	8d 45 f4             	lea    -0xc(%ebp),%eax
801054bf:	50                   	push   %eax
801054c0:	6a 02                	push   $0x2
801054c2:	e8 99 f5 ff ff       	call   80104a60 <argint>
     argint(1, &major) < 0 ||
801054c7:	83 c4 10             	add    $0x10,%esp
801054ca:	85 c0                	test   %eax,%eax
801054cc:	78 3a                	js     80105508 <sys_mknod+0x88>
     (ip = create(path, T_DEV, major, minor)) == 0){
801054ce:	0f bf 45 f4          	movswl -0xc(%ebp),%eax
801054d2:	83 ec 0c             	sub    $0xc,%esp
801054d5:	0f bf 4d f0          	movswl -0x10(%ebp),%ecx
801054d9:	ba 03 00 00 00       	mov    $0x3,%edx
801054de:	50                   	push   %eax
801054df:	8b 45 ec             	mov    -0x14(%ebp),%eax
801054e2:	e8 d9 f6 ff ff       	call   80104bc0 <create>
     argint(2, &minor) < 0 ||
801054e7:	83 c4 10             	add    $0x10,%esp
801054ea:	85 c0                	test   %eax,%eax
801054ec:	74 1a                	je     80105508 <sys_mknod+0x88>
    end_op();
    return -1;
  }
  iunlockput(ip);
801054ee:	83 ec 0c             	sub    $0xc,%esp
801054f1:	50                   	push   %eax
801054f2:	e8 29 c5 ff ff       	call   80101a20 <iunlockput>
  end_op();
801054f7:	e8 c4 d8 ff ff       	call   80102dc0 <end_op>
  return 0;
801054fc:	83 c4 10             	add    $0x10,%esp
801054ff:	31 c0                	xor    %eax,%eax
}
80105501:	c9                   	leave  
80105502:	c3                   	ret    
80105503:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105507:	90                   	nop
    end_op();
80105508:	e8 b3 d8 ff ff       	call   80102dc0 <end_op>
    return -1;
8010550d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105512:	c9                   	leave  
80105513:	c3                   	ret    
80105514:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010551b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010551f:	90                   	nop

80105520 <sys_chdir>:

int
sys_chdir(void)
{
80105520:	f3 0f 1e fb          	endbr32 
80105524:	55                   	push   %ebp
80105525:	89 e5                	mov    %esp,%ebp
80105527:	56                   	push   %esi
80105528:	53                   	push   %ebx
80105529:	83 ec 10             	sub    $0x10,%esp
  char *path;
  struct inode *ip;
  struct proc *curproc = myproc();
8010552c:	e8 cf e4 ff ff       	call   80103a00 <myproc>
80105531:	89 c6                	mov    %eax,%esi
  
  begin_op();
80105533:	e8 18 d8 ff ff       	call   80102d50 <begin_op>
  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0){
80105538:	83 ec 08             	sub    $0x8,%esp
8010553b:	8d 45 f4             	lea    -0xc(%ebp),%eax
8010553e:	50                   	push   %eax
8010553f:	6a 00                	push   $0x0
80105541:	e8 ca f5 ff ff       	call   80104b10 <argstr>
80105546:	83 c4 10             	add    $0x10,%esp
80105549:	85 c0                	test   %eax,%eax
8010554b:	78 73                	js     801055c0 <sys_chdir+0xa0>
8010554d:	83 ec 0c             	sub    $0xc,%esp
80105550:	ff 75 f4             	pushl  -0xc(%ebp)
80105553:	e8 f8 ca ff ff       	call   80102050 <namei>
80105558:	83 c4 10             	add    $0x10,%esp
8010555b:	89 c3                	mov    %eax,%ebx
8010555d:	85 c0                	test   %eax,%eax
8010555f:	74 5f                	je     801055c0 <sys_chdir+0xa0>
    end_op();
    return -1;
  }
  ilock(ip);
80105561:	83 ec 0c             	sub    $0xc,%esp
80105564:	50                   	push   %eax
80105565:	e8 16 c2 ff ff       	call   80101780 <ilock>
  if(ip->type != T_DIR){
8010556a:	83 c4 10             	add    $0x10,%esp
8010556d:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80105572:	75 2c                	jne    801055a0 <sys_chdir+0x80>
    iunlockput(ip);
    end_op();
    return -1;
  }
  iunlock(ip);
80105574:	83 ec 0c             	sub    $0xc,%esp
80105577:	53                   	push   %ebx
80105578:	e8 e3 c2 ff ff       	call   80101860 <iunlock>
  iput(curproc->cwd);
8010557d:	58                   	pop    %eax
8010557e:	ff 76 68             	pushl  0x68(%esi)
80105581:	e8 2a c3 ff ff       	call   801018b0 <iput>
  end_op();
80105586:	e8 35 d8 ff ff       	call   80102dc0 <end_op>
  curproc->cwd = ip;
8010558b:	89 5e 68             	mov    %ebx,0x68(%esi)
  return 0;
8010558e:	83 c4 10             	add    $0x10,%esp
80105591:	31 c0                	xor    %eax,%eax
}
80105593:	8d 65 f8             	lea    -0x8(%ebp),%esp
80105596:	5b                   	pop    %ebx
80105597:	5e                   	pop    %esi
80105598:	5d                   	pop    %ebp
80105599:	c3                   	ret    
8010559a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    iunlockput(ip);
801055a0:	83 ec 0c             	sub    $0xc,%esp
801055a3:	53                   	push   %ebx
801055a4:	e8 77 c4 ff ff       	call   80101a20 <iunlockput>
    end_op();
801055a9:	e8 12 d8 ff ff       	call   80102dc0 <end_op>
    return -1;
801055ae:	83 c4 10             	add    $0x10,%esp
801055b1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801055b6:	eb db                	jmp    80105593 <sys_chdir+0x73>
801055b8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801055bf:	90                   	nop
    end_op();
801055c0:	e8 fb d7 ff ff       	call   80102dc0 <end_op>
    return -1;
801055c5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801055ca:	eb c7                	jmp    80105593 <sys_chdir+0x73>
801055cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801055d0 <sys_exec>:

int
sys_exec(void)
{
801055d0:	f3 0f 1e fb          	endbr32 
801055d4:	55                   	push   %ebp
801055d5:	89 e5                	mov    %esp,%ebp
801055d7:	57                   	push   %edi
801055d8:	56                   	push   %esi
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
801055d9:	8d 85 5c ff ff ff    	lea    -0xa4(%ebp),%eax
{
801055df:	53                   	push   %ebx
801055e0:	81 ec a4 00 00 00    	sub    $0xa4,%esp
  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
801055e6:	50                   	push   %eax
801055e7:	6a 00                	push   $0x0
801055e9:	e8 22 f5 ff ff       	call   80104b10 <argstr>
801055ee:	83 c4 10             	add    $0x10,%esp
801055f1:	85 c0                	test   %eax,%eax
801055f3:	0f 88 8b 00 00 00    	js     80105684 <sys_exec+0xb4>
801055f9:	83 ec 08             	sub    $0x8,%esp
801055fc:	8d 85 60 ff ff ff    	lea    -0xa0(%ebp),%eax
80105602:	50                   	push   %eax
80105603:	6a 01                	push   $0x1
80105605:	e8 56 f4 ff ff       	call   80104a60 <argint>
8010560a:	83 c4 10             	add    $0x10,%esp
8010560d:	85 c0                	test   %eax,%eax
8010560f:	78 73                	js     80105684 <sys_exec+0xb4>
    return -1;
  }
  memset(argv, 0, sizeof(argv));
80105611:	83 ec 04             	sub    $0x4,%esp
80105614:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
  for(i=0;; i++){
8010561a:	31 db                	xor    %ebx,%ebx
  memset(argv, 0, sizeof(argv));
8010561c:	68 80 00 00 00       	push   $0x80
80105621:	8d bd 64 ff ff ff    	lea    -0x9c(%ebp),%edi
80105627:	6a 00                	push   $0x0
80105629:	50                   	push   %eax
8010562a:	e8 51 f1 ff ff       	call   80104780 <memset>
8010562f:	83 c4 10             	add    $0x10,%esp
80105632:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    if(i >= NELEM(argv))
      return -1;
    if(fetchint(uargv+4*i, (int*)&uarg) < 0)
80105638:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
8010563e:	8d 34 9d 00 00 00 00 	lea    0x0(,%ebx,4),%esi
80105645:	83 ec 08             	sub    $0x8,%esp
80105648:	57                   	push   %edi
80105649:	01 f0                	add    %esi,%eax
8010564b:	50                   	push   %eax
8010564c:	e8 6f f3 ff ff       	call   801049c0 <fetchint>
80105651:	83 c4 10             	add    $0x10,%esp
80105654:	85 c0                	test   %eax,%eax
80105656:	78 2c                	js     80105684 <sys_exec+0xb4>
      return -1;
    if(uarg == 0){
80105658:	8b 85 64 ff ff ff    	mov    -0x9c(%ebp),%eax
8010565e:	85 c0                	test   %eax,%eax
80105660:	74 36                	je     80105698 <sys_exec+0xc8>
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
80105662:	8d 8d 68 ff ff ff    	lea    -0x98(%ebp),%ecx
80105668:	83 ec 08             	sub    $0x8,%esp
8010566b:	8d 14 31             	lea    (%ecx,%esi,1),%edx
8010566e:	52                   	push   %edx
8010566f:	50                   	push   %eax
80105670:	e8 8b f3 ff ff       	call   80104a00 <fetchstr>
80105675:	83 c4 10             	add    $0x10,%esp
80105678:	85 c0                	test   %eax,%eax
8010567a:	78 08                	js     80105684 <sys_exec+0xb4>
  for(i=0;; i++){
8010567c:	83 c3 01             	add    $0x1,%ebx
    if(i >= NELEM(argv))
8010567f:	83 fb 20             	cmp    $0x20,%ebx
80105682:	75 b4                	jne    80105638 <sys_exec+0x68>
      return -1;
  }
  return exec(path, argv);
}
80105684:	8d 65 f4             	lea    -0xc(%ebp),%esp
    return -1;
80105687:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010568c:	5b                   	pop    %ebx
8010568d:	5e                   	pop    %esi
8010568e:	5f                   	pop    %edi
8010568f:	5d                   	pop    %ebp
80105690:	c3                   	ret    
80105691:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  return exec(path, argv);
80105698:	83 ec 08             	sub    $0x8,%esp
8010569b:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
      argv[i] = 0;
801056a1:	c7 84 9d 68 ff ff ff 	movl   $0x0,-0x98(%ebp,%ebx,4)
801056a8:	00 00 00 00 
  return exec(path, argv);
801056ac:	50                   	push   %eax
801056ad:	ff b5 5c ff ff ff    	pushl  -0xa4(%ebp)
801056b3:	e8 e8 b3 ff ff       	call   80100aa0 <exec>
801056b8:	83 c4 10             	add    $0x10,%esp
}
801056bb:	8d 65 f4             	lea    -0xc(%ebp),%esp
801056be:	5b                   	pop    %ebx
801056bf:	5e                   	pop    %esi
801056c0:	5f                   	pop    %edi
801056c1:	5d                   	pop    %ebp
801056c2:	c3                   	ret    
801056c3:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801056ca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801056d0 <sys_pipe>:

int
sys_pipe(void)
{
801056d0:	f3 0f 1e fb          	endbr32 
801056d4:	55                   	push   %ebp
801056d5:	89 e5                	mov    %esp,%ebp
801056d7:	57                   	push   %edi
801056d8:	56                   	push   %esi
  int *fd;
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
801056d9:	8d 45 dc             	lea    -0x24(%ebp),%eax
{
801056dc:	53                   	push   %ebx
801056dd:	83 ec 20             	sub    $0x20,%esp
  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
801056e0:	6a 08                	push   $0x8
801056e2:	50                   	push   %eax
801056e3:	6a 00                	push   $0x0
801056e5:	e8 c6 f3 ff ff       	call   80104ab0 <argptr>
801056ea:	83 c4 10             	add    $0x10,%esp
801056ed:	85 c0                	test   %eax,%eax
801056ef:	78 4e                	js     8010573f <sys_pipe+0x6f>
    return -1;
  if(pipealloc(&rf, &wf) < 0)
801056f1:	83 ec 08             	sub    $0x8,%esp
801056f4:	8d 45 e4             	lea    -0x1c(%ebp),%eax
801056f7:	50                   	push   %eax
801056f8:	8d 45 e0             	lea    -0x20(%ebp),%eax
801056fb:	50                   	push   %eax
801056fc:	e8 0f dd ff ff       	call   80103410 <pipealloc>
80105701:	83 c4 10             	add    $0x10,%esp
80105704:	85 c0                	test   %eax,%eax
80105706:	78 37                	js     8010573f <sys_pipe+0x6f>
    return -1;
  fd0 = -1;
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
80105708:	8b 7d e0             	mov    -0x20(%ebp),%edi
  for(fd = 0; fd < NOFILE; fd++){
8010570b:	31 db                	xor    %ebx,%ebx
  struct proc *curproc = myproc();
8010570d:	e8 ee e2 ff ff       	call   80103a00 <myproc>
  for(fd = 0; fd < NOFILE; fd++){
80105712:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    if(curproc->ofile[fd] == 0){
80105718:	8b 74 98 28          	mov    0x28(%eax,%ebx,4),%esi
8010571c:	85 f6                	test   %esi,%esi
8010571e:	74 30                	je     80105750 <sys_pipe+0x80>
  for(fd = 0; fd < NOFILE; fd++){
80105720:	83 c3 01             	add    $0x1,%ebx
80105723:	83 fb 10             	cmp    $0x10,%ebx
80105726:	75 f0                	jne    80105718 <sys_pipe+0x48>
    if(fd0 >= 0)
      myproc()->ofile[fd0] = 0;
    fileclose(rf);
80105728:	83 ec 0c             	sub    $0xc,%esp
8010572b:	ff 75 e0             	pushl  -0x20(%ebp)
8010572e:	e8 ad b7 ff ff       	call   80100ee0 <fileclose>
    fileclose(wf);
80105733:	58                   	pop    %eax
80105734:	ff 75 e4             	pushl  -0x1c(%ebp)
80105737:	e8 a4 b7 ff ff       	call   80100ee0 <fileclose>
    return -1;
8010573c:	83 c4 10             	add    $0x10,%esp
8010573f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105744:	eb 5b                	jmp    801057a1 <sys_pipe+0xd1>
80105746:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010574d:	8d 76 00             	lea    0x0(%esi),%esi
      curproc->ofile[fd] = f;
80105750:	8d 73 08             	lea    0x8(%ebx),%esi
80105753:	89 7c b0 08          	mov    %edi,0x8(%eax,%esi,4)
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
80105757:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  struct proc *curproc = myproc();
8010575a:	e8 a1 e2 ff ff       	call   80103a00 <myproc>
  for(fd = 0; fd < NOFILE; fd++){
8010575f:	31 d2                	xor    %edx,%edx
80105761:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if(curproc->ofile[fd] == 0){
80105768:	8b 4c 90 28          	mov    0x28(%eax,%edx,4),%ecx
8010576c:	85 c9                	test   %ecx,%ecx
8010576e:	74 20                	je     80105790 <sys_pipe+0xc0>
  for(fd = 0; fd < NOFILE; fd++){
80105770:	83 c2 01             	add    $0x1,%edx
80105773:	83 fa 10             	cmp    $0x10,%edx
80105776:	75 f0                	jne    80105768 <sys_pipe+0x98>
      myproc()->ofile[fd0] = 0;
80105778:	e8 83 e2 ff ff       	call   80103a00 <myproc>
8010577d:	c7 44 b0 08 00 00 00 	movl   $0x0,0x8(%eax,%esi,4)
80105784:	00 
80105785:	eb a1                	jmp    80105728 <sys_pipe+0x58>
80105787:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010578e:	66 90                	xchg   %ax,%ax
      curproc->ofile[fd] = f;
80105790:	89 7c 90 28          	mov    %edi,0x28(%eax,%edx,4)
  }
  fd[0] = fd0;
80105794:	8b 45 dc             	mov    -0x24(%ebp),%eax
80105797:	89 18                	mov    %ebx,(%eax)
  fd[1] = fd1;
80105799:	8b 45 dc             	mov    -0x24(%ebp),%eax
8010579c:	89 50 04             	mov    %edx,0x4(%eax)
  return 0;
8010579f:	31 c0                	xor    %eax,%eax
}
801057a1:	8d 65 f4             	lea    -0xc(%ebp),%esp
801057a4:	5b                   	pop    %ebx
801057a5:	5e                   	pop    %esi
801057a6:	5f                   	pop    %edi
801057a7:	5d                   	pop    %ebp
801057a8:	c3                   	ret    
801057a9:	66 90                	xchg   %ax,%ax
801057ab:	66 90                	xchg   %ax,%ax
801057ad:	66 90                	xchg   %ax,%ax
801057af:	90                   	nop

801057b0 <sys_fork>:
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
801057b0:	f3 0f 1e fb          	endbr32 
  return fork();
801057b4:	e9 f7 e3 ff ff       	jmp    80103bb0 <fork>
801057b9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801057c0 <sys_exit>:
}

int
sys_exit(void)
{
801057c0:	f3 0f 1e fb          	endbr32 
801057c4:	55                   	push   %ebp
801057c5:	89 e5                	mov    %esp,%ebp
801057c7:	83 ec 08             	sub    $0x8,%esp
  exit();
801057ca:	e8 81 e6 ff ff       	call   80103e50 <exit>
  return 0;  // not reached
}
801057cf:	31 c0                	xor    %eax,%eax
801057d1:	c9                   	leave  
801057d2:	c3                   	ret    
801057d3:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801057da:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801057e0 <sys_wait>:

int
sys_wait(void)
{
801057e0:	f3 0f 1e fb          	endbr32 
  return wait();
801057e4:	e9 07 e9 ff ff       	jmp    801040f0 <wait>
801057e9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801057f0 <sys_kill>:
}

int
sys_kill(void)
{
801057f0:	f3 0f 1e fb          	endbr32 
801057f4:	55                   	push   %ebp
801057f5:	89 e5                	mov    %esp,%ebp
801057f7:	83 ec 20             	sub    $0x20,%esp
  int pid;

  if(argint(0, &pid) < 0)
801057fa:	8d 45 f4             	lea    -0xc(%ebp),%eax
801057fd:	50                   	push   %eax
801057fe:	6a 00                	push   $0x0
80105800:	e8 5b f2 ff ff       	call   80104a60 <argint>
80105805:	83 c4 10             	add    $0x10,%esp
80105808:	85 c0                	test   %eax,%eax
8010580a:	78 14                	js     80105820 <sys_kill+0x30>
    return -1;
  return kill(pid);
8010580c:	83 ec 0c             	sub    $0xc,%esp
8010580f:	ff 75 f4             	pushl  -0xc(%ebp)
80105812:	e8 29 ea ff ff       	call   80104240 <kill>
80105817:	83 c4 10             	add    $0x10,%esp
}
8010581a:	c9                   	leave  
8010581b:	c3                   	ret    
8010581c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105820:	c9                   	leave  
    return -1;
80105821:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105826:	c3                   	ret    
80105827:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010582e:	66 90                	xchg   %ax,%ax

80105830 <sys_getpid>:

int
sys_getpid(void)
{
80105830:	f3 0f 1e fb          	endbr32 
80105834:	55                   	push   %ebp
80105835:	89 e5                	mov    %esp,%ebp
80105837:	83 ec 08             	sub    $0x8,%esp
  return myproc()->pid;
8010583a:	e8 c1 e1 ff ff       	call   80103a00 <myproc>
8010583f:	8b 40 10             	mov    0x10(%eax),%eax
}
80105842:	c9                   	leave  
80105843:	c3                   	ret    
80105844:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010584b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010584f:	90                   	nop

80105850 <sys_sbrk>:

int
sys_sbrk(void)
{
80105850:	f3 0f 1e fb          	endbr32 
80105854:	55                   	push   %ebp
80105855:	89 e5                	mov    %esp,%ebp
80105857:	53                   	push   %ebx
  int addr;
  int n;

  if(argint(0, &n) < 0)
80105858:	8d 45 f4             	lea    -0xc(%ebp),%eax
{
8010585b:	83 ec 1c             	sub    $0x1c,%esp
  if(argint(0, &n) < 0)
8010585e:	50                   	push   %eax
8010585f:	6a 00                	push   $0x0
80105861:	e8 fa f1 ff ff       	call   80104a60 <argint>
80105866:	83 c4 10             	add    $0x10,%esp
80105869:	85 c0                	test   %eax,%eax
8010586b:	78 23                	js     80105890 <sys_sbrk+0x40>
    return -1;
  addr = myproc()->sz;
8010586d:	e8 8e e1 ff ff       	call   80103a00 <myproc>
  if(growproc(n) < 0)
80105872:	83 ec 0c             	sub    $0xc,%esp
  addr = myproc()->sz;
80105875:	8b 18                	mov    (%eax),%ebx
  if(growproc(n) < 0)
80105877:	ff 75 f4             	pushl  -0xc(%ebp)
8010587a:	e8 b1 e2 ff ff       	call   80103b30 <growproc>
8010587f:	83 c4 10             	add    $0x10,%esp
80105882:	85 c0                	test   %eax,%eax
80105884:	78 0a                	js     80105890 <sys_sbrk+0x40>
    return -1;
  return addr;
}
80105886:	89 d8                	mov    %ebx,%eax
80105888:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010588b:	c9                   	leave  
8010588c:	c3                   	ret    
8010588d:	8d 76 00             	lea    0x0(%esi),%esi
    return -1;
80105890:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80105895:	eb ef                	jmp    80105886 <sys_sbrk+0x36>
80105897:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010589e:	66 90                	xchg   %ax,%ax

801058a0 <sys_sleep>:

int
sys_sleep(void)
{
801058a0:	f3 0f 1e fb          	endbr32 
801058a4:	55                   	push   %ebp
801058a5:	89 e5                	mov    %esp,%ebp
801058a7:	53                   	push   %ebx
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
801058a8:	8d 45 f4             	lea    -0xc(%ebp),%eax
{
801058ab:	83 ec 1c             	sub    $0x1c,%esp
  if(argint(0, &n) < 0)
801058ae:	50                   	push   %eax
801058af:	6a 00                	push   $0x0
801058b1:	e8 aa f1 ff ff       	call   80104a60 <argint>
801058b6:	83 c4 10             	add    $0x10,%esp
801058b9:	85 c0                	test   %eax,%eax
801058bb:	0f 88 86 00 00 00    	js     80105947 <sys_sleep+0xa7>
    return -1;
  acquire(&tickslock);
801058c1:	83 ec 0c             	sub    $0xc,%esp
801058c4:	68 60 4c 11 80       	push   $0x80114c60
801058c9:	e8 a2 ed ff ff       	call   80104670 <acquire>
  ticks0 = ticks;
  while(ticks - ticks0 < n){
801058ce:	8b 55 f4             	mov    -0xc(%ebp),%edx
  ticks0 = ticks;
801058d1:	8b 1d a0 54 11 80    	mov    0x801154a0,%ebx
  while(ticks - ticks0 < n){
801058d7:	83 c4 10             	add    $0x10,%esp
801058da:	85 d2                	test   %edx,%edx
801058dc:	75 23                	jne    80105901 <sys_sleep+0x61>
801058de:	eb 50                	jmp    80105930 <sys_sleep+0x90>
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
801058e0:	83 ec 08             	sub    $0x8,%esp
801058e3:	68 60 4c 11 80       	push   $0x80114c60
801058e8:	68 a0 54 11 80       	push   $0x801154a0
801058ed:	e8 0e e7 ff ff       	call   80104000 <sleep>
  while(ticks - ticks0 < n){
801058f2:	a1 a0 54 11 80       	mov    0x801154a0,%eax
801058f7:	83 c4 10             	add    $0x10,%esp
801058fa:	29 d8                	sub    %ebx,%eax
801058fc:	3b 45 f4             	cmp    -0xc(%ebp),%eax
801058ff:	73 2f                	jae    80105930 <sys_sleep+0x90>
    if(myproc()->killed){
80105901:	e8 fa e0 ff ff       	call   80103a00 <myproc>
80105906:	8b 40 24             	mov    0x24(%eax),%eax
80105909:	85 c0                	test   %eax,%eax
8010590b:	74 d3                	je     801058e0 <sys_sleep+0x40>
      release(&tickslock);
8010590d:	83 ec 0c             	sub    $0xc,%esp
80105910:	68 60 4c 11 80       	push   $0x80114c60
80105915:	e8 16 ee ff ff       	call   80104730 <release>
  }
  release(&tickslock);
  return 0;
}
8010591a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
      return -1;
8010591d:	83 c4 10             	add    $0x10,%esp
80105920:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105925:	c9                   	leave  
80105926:	c3                   	ret    
80105927:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010592e:	66 90                	xchg   %ax,%ax
  release(&tickslock);
80105930:	83 ec 0c             	sub    $0xc,%esp
80105933:	68 60 4c 11 80       	push   $0x80114c60
80105938:	e8 f3 ed ff ff       	call   80104730 <release>
  return 0;
8010593d:	83 c4 10             	add    $0x10,%esp
80105940:	31 c0                	xor    %eax,%eax
}
80105942:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80105945:	c9                   	leave  
80105946:	c3                   	ret    
    return -1;
80105947:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010594c:	eb f4                	jmp    80105942 <sys_sleep+0xa2>
8010594e:	66 90                	xchg   %ax,%ax

80105950 <sys_uptime>:

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
80105950:	f3 0f 1e fb          	endbr32 
80105954:	55                   	push   %ebp
80105955:	89 e5                	mov    %esp,%ebp
80105957:	53                   	push   %ebx
80105958:	83 ec 10             	sub    $0x10,%esp
  uint xticks;

  acquire(&tickslock);
8010595b:	68 60 4c 11 80       	push   $0x80114c60
80105960:	e8 0b ed ff ff       	call   80104670 <acquire>
  xticks = ticks;
80105965:	8b 1d a0 54 11 80    	mov    0x801154a0,%ebx
  release(&tickslock);
8010596b:	c7 04 24 60 4c 11 80 	movl   $0x80114c60,(%esp)
80105972:	e8 b9 ed ff ff       	call   80104730 <release>
  return xticks;
}
80105977:	89 d8                	mov    %ebx,%eax
80105979:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010597c:	c9                   	leave  
8010597d:	c3                   	ret    

8010597e <alltraps>:

  # vectors.S sends all traps here.
.globl alltraps
alltraps:
  # Build trap frame.
  pushl %ds
8010597e:	1e                   	push   %ds
  pushl %es
8010597f:	06                   	push   %es
  pushl %fs
80105980:	0f a0                	push   %fs
  pushl %gs
80105982:	0f a8                	push   %gs
  pushal
80105984:	60                   	pusha  
  
  # Set up data segments.
  movw $(SEG_KDATA<<3), %ax
80105985:	66 b8 10 00          	mov    $0x10,%ax
  movw %ax, %ds
80105989:	8e d8                	mov    %eax,%ds
  movw %ax, %es
8010598b:	8e c0                	mov    %eax,%es

  # Call trap(tf), where tf=%esp
  pushl %esp
8010598d:	54                   	push   %esp
  call trap
8010598e:	e8 cd 00 00 00       	call   80105a60 <trap>
  addl $4, %esp
80105993:	83 c4 04             	add    $0x4,%esp

80105996 <trapret>:

  # Return falls through to trapret...
.globl trapret
trapret:
  popal
80105996:	61                   	popa   
  popl %gs
80105997:	0f a9                	pop    %gs
  popl %fs
80105999:	0f a1                	pop    %fs
  popl %es
8010599b:	07                   	pop    %es
  popl %ds
8010599c:	1f                   	pop    %ds
  addl $0x8, %esp  # trapno and errcode
8010599d:	83 c4 08             	add    $0x8,%esp
  iret
801059a0:	cf                   	iret   
801059a1:	66 90                	xchg   %ax,%ax
801059a3:	66 90                	xchg   %ax,%ax
801059a5:	66 90                	xchg   %ax,%ax
801059a7:	66 90                	xchg   %ax,%ax
801059a9:	66 90                	xchg   %ax,%ax
801059ab:	66 90                	xchg   %ax,%ax
801059ad:	66 90                	xchg   %ax,%ax
801059af:	90                   	nop

801059b0 <tvinit>:
struct spinlock tickslock;
uint ticks;

void
tvinit(void)
{
801059b0:	f3 0f 1e fb          	endbr32 
801059b4:	55                   	push   %ebp
  int i;

  for(i = 0; i < 256; i++)
801059b5:	31 c0                	xor    %eax,%eax
{
801059b7:	89 e5                	mov    %esp,%ebp
801059b9:	83 ec 08             	sub    $0x8,%esp
801059bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
801059c0:	8b 14 85 08 a0 10 80 	mov    -0x7fef5ff8(,%eax,4),%edx
801059c7:	c7 04 c5 a2 4c 11 80 	movl   $0x8e000008,-0x7feeb35e(,%eax,8)
801059ce:	08 00 00 8e 
801059d2:	66 89 14 c5 a0 4c 11 	mov    %dx,-0x7feeb360(,%eax,8)
801059d9:	80 
801059da:	c1 ea 10             	shr    $0x10,%edx
801059dd:	66 89 14 c5 a6 4c 11 	mov    %dx,-0x7feeb35a(,%eax,8)
801059e4:	80 
  for(i = 0; i < 256; i++)
801059e5:	83 c0 01             	add    $0x1,%eax
801059e8:	3d 00 01 00 00       	cmp    $0x100,%eax
801059ed:	75 d1                	jne    801059c0 <tvinit+0x10>
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);

  initlock(&tickslock, "time");
801059ef:	83 ec 08             	sub    $0x8,%esp
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
801059f2:	a1 08 a1 10 80       	mov    0x8010a108,%eax
801059f7:	c7 05 a2 4e 11 80 08 	movl   $0xef000008,0x80114ea2
801059fe:	00 00 ef 
  initlock(&tickslock, "time");
80105a01:	68 d9 7b 10 80       	push   $0x80107bd9
80105a06:	68 60 4c 11 80       	push   $0x80114c60
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
80105a0b:	66 a3 a0 4e 11 80    	mov    %ax,0x80114ea0
80105a11:	c1 e8 10             	shr    $0x10,%eax
80105a14:	66 a3 a6 4e 11 80    	mov    %ax,0x80114ea6
  initlock(&tickslock, "time");
80105a1a:	e8 d1 ea ff ff       	call   801044f0 <initlock>
}
80105a1f:	83 c4 10             	add    $0x10,%esp
80105a22:	c9                   	leave  
80105a23:	c3                   	ret    
80105a24:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105a2b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105a2f:	90                   	nop

80105a30 <idtinit>:

void
idtinit(void)
{
80105a30:	f3 0f 1e fb          	endbr32 
80105a34:	55                   	push   %ebp
  pd[0] = size-1;
80105a35:	b8 ff 07 00 00       	mov    $0x7ff,%eax
80105a3a:	89 e5                	mov    %esp,%ebp
80105a3c:	83 ec 10             	sub    $0x10,%esp
80105a3f:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
80105a43:	b8 a0 4c 11 80       	mov    $0x80114ca0,%eax
80105a48:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
80105a4c:	c1 e8 10             	shr    $0x10,%eax
80105a4f:	66 89 45 fe          	mov    %ax,-0x2(%ebp)
  asm volatile("lidt (%0)" : : "r" (pd));
80105a53:	8d 45 fa             	lea    -0x6(%ebp),%eax
80105a56:	0f 01 18             	lidtl  (%eax)
  lidt(idt, sizeof(idt));
}
80105a59:	c9                   	leave  
80105a5a:	c3                   	ret    
80105a5b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105a5f:	90                   	nop

80105a60 <trap>:

//PAGEBREAK: 41
void
trap(struct trapframe *tf)
{
80105a60:	f3 0f 1e fb          	endbr32 
80105a64:	55                   	push   %ebp
80105a65:	89 e5                	mov    %esp,%ebp
80105a67:	57                   	push   %edi
80105a68:	56                   	push   %esi
80105a69:	53                   	push   %ebx
80105a6a:	83 ec 1c             	sub    $0x1c,%esp
80105a6d:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(tf->trapno == T_SYSCALL){
80105a70:	8b 43 30             	mov    0x30(%ebx),%eax
80105a73:	83 f8 40             	cmp    $0x40,%eax
80105a76:	0f 84 bc 01 00 00    	je     80105c38 <trap+0x1d8>
    if(myproc()->killed)
      exit();
    return;
  }

  switch(tf->trapno){
80105a7c:	83 e8 20             	sub    $0x20,%eax
80105a7f:	83 f8 1f             	cmp    $0x1f,%eax
80105a82:	77 08                	ja     80105a8c <trap+0x2c>
80105a84:	3e ff 24 85 80 7c 10 	notrack jmp *-0x7fef8380(,%eax,4)
80105a8b:	80 
    lapiceoi();
    break;

  //PAGEBREAK: 13
  default:
    if(myproc() == 0 || (tf->cs&3) == 0){
80105a8c:	e8 6f df ff ff       	call   80103a00 <myproc>
80105a91:	8b 7b 38             	mov    0x38(%ebx),%edi
80105a94:	85 c0                	test   %eax,%eax
80105a96:	0f 84 eb 01 00 00    	je     80105c87 <trap+0x227>
80105a9c:	f6 43 3c 03          	testb  $0x3,0x3c(%ebx)
80105aa0:	0f 84 e1 01 00 00    	je     80105c87 <trap+0x227>

static inline uint
rcr2(void)
{
  uint val;
  asm volatile("movl %%cr2,%0" : "=r" (val));
80105aa6:	0f 20 d1             	mov    %cr2,%ecx
80105aa9:	89 4d d8             	mov    %ecx,-0x28(%ebp)
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpuid(), tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80105aac:	e8 2f df ff ff       	call   801039e0 <cpuid>
80105ab1:	8b 73 30             	mov    0x30(%ebx),%esi
80105ab4:	89 45 dc             	mov    %eax,-0x24(%ebp)
80105ab7:	8b 43 34             	mov    0x34(%ebx),%eax
80105aba:	89 45 e4             	mov    %eax,-0x1c(%ebp)
            "eip 0x%x addr 0x%x--kill proc\n",
            myproc()->pid, myproc()->name, tf->trapno,
80105abd:	e8 3e df ff ff       	call   80103a00 <myproc>
80105ac2:	89 45 e0             	mov    %eax,-0x20(%ebp)
80105ac5:	e8 36 df ff ff       	call   80103a00 <myproc>
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80105aca:	8b 4d d8             	mov    -0x28(%ebp),%ecx
80105acd:	8b 55 dc             	mov    -0x24(%ebp),%edx
80105ad0:	51                   	push   %ecx
80105ad1:	57                   	push   %edi
80105ad2:	52                   	push   %edx
80105ad3:	ff 75 e4             	pushl  -0x1c(%ebp)
80105ad6:	56                   	push   %esi
            myproc()->pid, myproc()->name, tf->trapno,
80105ad7:	8b 75 e0             	mov    -0x20(%ebp),%esi
80105ada:	83 c6 6c             	add    $0x6c,%esi
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80105add:	56                   	push   %esi
80105ade:	ff 70 10             	pushl  0x10(%eax)
80105ae1:	68 3c 7c 10 80       	push   $0x80107c3c
80105ae6:	e8 c5 ab ff ff       	call   801006b0 <cprintf>
            tf->err, cpuid(), tf->eip, rcr2());
    myproc()->killed = 1;
80105aeb:	83 c4 20             	add    $0x20,%esp
80105aee:	e8 0d df ff ff       	call   80103a00 <myproc>
80105af3:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
  }

  // Force process exit if it has been killed and is in user space.
  // (If it is still executing in the kernel, let it keep running
  // until it gets to the regular system call return.)
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105afa:	e8 01 df ff ff       	call   80103a00 <myproc>
80105aff:	85 c0                	test   %eax,%eax
80105b01:	74 1d                	je     80105b20 <trap+0xc0>
80105b03:	e8 f8 de ff ff       	call   80103a00 <myproc>
80105b08:	8b 50 24             	mov    0x24(%eax),%edx
80105b0b:	85 d2                	test   %edx,%edx
80105b0d:	74 11                	je     80105b20 <trap+0xc0>
80105b0f:	0f b7 43 3c          	movzwl 0x3c(%ebx),%eax
80105b13:	83 e0 03             	and    $0x3,%eax
80105b16:	66 83 f8 03          	cmp    $0x3,%ax
80105b1a:	0f 84 50 01 00 00    	je     80105c70 <trap+0x210>
    exit();

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if(myproc() && myproc()->state == RUNNING &&
80105b20:	e8 db de ff ff       	call   80103a00 <myproc>
80105b25:	85 c0                	test   %eax,%eax
80105b27:	74 0f                	je     80105b38 <trap+0xd8>
80105b29:	e8 d2 de ff ff       	call   80103a00 <myproc>
80105b2e:	83 78 0c 04          	cmpl   $0x4,0xc(%eax)
80105b32:	0f 84 e8 00 00 00    	je     80105c20 <trap+0x1c0>
     tf->trapno == T_IRQ0+IRQ_TIMER)
    yield();

  // Check if the process has been killed since we yielded
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105b38:	e8 c3 de ff ff       	call   80103a00 <myproc>
80105b3d:	85 c0                	test   %eax,%eax
80105b3f:	74 1d                	je     80105b5e <trap+0xfe>
80105b41:	e8 ba de ff ff       	call   80103a00 <myproc>
80105b46:	8b 40 24             	mov    0x24(%eax),%eax
80105b49:	85 c0                	test   %eax,%eax
80105b4b:	74 11                	je     80105b5e <trap+0xfe>
80105b4d:	0f b7 43 3c          	movzwl 0x3c(%ebx),%eax
80105b51:	83 e0 03             	and    $0x3,%eax
80105b54:	66 83 f8 03          	cmp    $0x3,%ax
80105b58:	0f 84 03 01 00 00    	je     80105c61 <trap+0x201>
    exit();
}
80105b5e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105b61:	5b                   	pop    %ebx
80105b62:	5e                   	pop    %esi
80105b63:	5f                   	pop    %edi
80105b64:	5d                   	pop    %ebp
80105b65:	c3                   	ret    
    ideintr();
80105b66:	e8 95 c6 ff ff       	call   80102200 <ideintr>
    lapiceoi();
80105b6b:	e8 70 cd ff ff       	call   801028e0 <lapiceoi>
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105b70:	e8 8b de ff ff       	call   80103a00 <myproc>
80105b75:	85 c0                	test   %eax,%eax
80105b77:	75 8a                	jne    80105b03 <trap+0xa3>
80105b79:	eb a5                	jmp    80105b20 <trap+0xc0>
    if(cpuid() == 0){
80105b7b:	e8 60 de ff ff       	call   801039e0 <cpuid>
80105b80:	85 c0                	test   %eax,%eax
80105b82:	75 e7                	jne    80105b6b <trap+0x10b>
      acquire(&tickslock);
80105b84:	83 ec 0c             	sub    $0xc,%esp
80105b87:	68 60 4c 11 80       	push   $0x80114c60
80105b8c:	e8 df ea ff ff       	call   80104670 <acquire>
      wakeup(&ticks);
80105b91:	c7 04 24 a0 54 11 80 	movl   $0x801154a0,(%esp)
      ticks++;
80105b98:	83 05 a0 54 11 80 01 	addl   $0x1,0x801154a0
      wakeup(&ticks);
80105b9f:	e8 5c e6 ff ff       	call   80104200 <wakeup>
      release(&tickslock);
80105ba4:	c7 04 24 60 4c 11 80 	movl   $0x80114c60,(%esp)
80105bab:	e8 80 eb ff ff       	call   80104730 <release>
80105bb0:	83 c4 10             	add    $0x10,%esp
    lapiceoi();
80105bb3:	eb b6                	jmp    80105b6b <trap+0x10b>
    kbdintr();
80105bb5:	e8 e6 cb ff ff       	call   801027a0 <kbdintr>
    lapiceoi();
80105bba:	e8 21 cd ff ff       	call   801028e0 <lapiceoi>
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105bbf:	e8 3c de ff ff       	call   80103a00 <myproc>
80105bc4:	85 c0                	test   %eax,%eax
80105bc6:	0f 85 37 ff ff ff    	jne    80105b03 <trap+0xa3>
80105bcc:	e9 4f ff ff ff       	jmp    80105b20 <trap+0xc0>
    uartintr();
80105bd1:	e8 4a 02 00 00       	call   80105e20 <uartintr>
    lapiceoi();
80105bd6:	e8 05 cd ff ff       	call   801028e0 <lapiceoi>
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105bdb:	e8 20 de ff ff       	call   80103a00 <myproc>
80105be0:	85 c0                	test   %eax,%eax
80105be2:	0f 85 1b ff ff ff    	jne    80105b03 <trap+0xa3>
80105be8:	e9 33 ff ff ff       	jmp    80105b20 <trap+0xc0>
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
80105bed:	8b 7b 38             	mov    0x38(%ebx),%edi
80105bf0:	0f b7 73 3c          	movzwl 0x3c(%ebx),%esi
80105bf4:	e8 e7 dd ff ff       	call   801039e0 <cpuid>
80105bf9:	57                   	push   %edi
80105bfa:	56                   	push   %esi
80105bfb:	50                   	push   %eax
80105bfc:	68 e4 7b 10 80       	push   $0x80107be4
80105c01:	e8 aa aa ff ff       	call   801006b0 <cprintf>
    lapiceoi();
80105c06:	e8 d5 cc ff ff       	call   801028e0 <lapiceoi>
    break;
80105c0b:	83 c4 10             	add    $0x10,%esp
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105c0e:	e8 ed dd ff ff       	call   80103a00 <myproc>
80105c13:	85 c0                	test   %eax,%eax
80105c15:	0f 85 e8 fe ff ff    	jne    80105b03 <trap+0xa3>
80105c1b:	e9 00 ff ff ff       	jmp    80105b20 <trap+0xc0>
  if(myproc() && myproc()->state == RUNNING &&
80105c20:	83 7b 30 20          	cmpl   $0x20,0x30(%ebx)
80105c24:	0f 85 0e ff ff ff    	jne    80105b38 <trap+0xd8>
    yield();
80105c2a:	e8 31 e3 ff ff       	call   80103f60 <yield>
80105c2f:	e9 04 ff ff ff       	jmp    80105b38 <trap+0xd8>
80105c34:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(myproc()->killed)
80105c38:	e8 c3 dd ff ff       	call   80103a00 <myproc>
80105c3d:	8b 70 24             	mov    0x24(%eax),%esi
80105c40:	85 f6                	test   %esi,%esi
80105c42:	75 3c                	jne    80105c80 <trap+0x220>
    myproc()->tf = tf;
80105c44:	e8 b7 dd ff ff       	call   80103a00 <myproc>
80105c49:	89 58 18             	mov    %ebx,0x18(%eax)
    syscall();
80105c4c:	e8 ff ee ff ff       	call   80104b50 <syscall>
    if(myproc()->killed)
80105c51:	e8 aa dd ff ff       	call   80103a00 <myproc>
80105c56:	8b 48 24             	mov    0x24(%eax),%ecx
80105c59:	85 c9                	test   %ecx,%ecx
80105c5b:	0f 84 fd fe ff ff    	je     80105b5e <trap+0xfe>
}
80105c61:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105c64:	5b                   	pop    %ebx
80105c65:	5e                   	pop    %esi
80105c66:	5f                   	pop    %edi
80105c67:	5d                   	pop    %ebp
      exit();
80105c68:	e9 e3 e1 ff ff       	jmp    80103e50 <exit>
80105c6d:	8d 76 00             	lea    0x0(%esi),%esi
    exit();
80105c70:	e8 db e1 ff ff       	call   80103e50 <exit>
80105c75:	e9 a6 fe ff ff       	jmp    80105b20 <trap+0xc0>
80105c7a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      exit();
80105c80:	e8 cb e1 ff ff       	call   80103e50 <exit>
80105c85:	eb bd                	jmp    80105c44 <trap+0x1e4>
80105c87:	0f 20 d6             	mov    %cr2,%esi
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
80105c8a:	e8 51 dd ff ff       	call   801039e0 <cpuid>
80105c8f:	83 ec 0c             	sub    $0xc,%esp
80105c92:	56                   	push   %esi
80105c93:	57                   	push   %edi
80105c94:	50                   	push   %eax
80105c95:	ff 73 30             	pushl  0x30(%ebx)
80105c98:	68 08 7c 10 80       	push   $0x80107c08
80105c9d:	e8 0e aa ff ff       	call   801006b0 <cprintf>
      panic("trap");
80105ca2:	83 c4 14             	add    $0x14,%esp
80105ca5:	68 de 7b 10 80       	push   $0x80107bde
80105caa:	e8 e1 a6 ff ff       	call   80100390 <panic>
80105caf:	90                   	nop

80105cb0 <uartgetc>:
  outb(COM1+0, c);
}

static int
uartgetc(void)
{
80105cb0:	f3 0f 1e fb          	endbr32 
  if(!uart)
80105cb4:	a1 bc a5 10 80       	mov    0x8010a5bc,%eax
80105cb9:	85 c0                	test   %eax,%eax
80105cbb:	74 1b                	je     80105cd8 <uartgetc+0x28>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80105cbd:	ba fd 03 00 00       	mov    $0x3fd,%edx
80105cc2:	ec                   	in     (%dx),%al
    return -1;
  if(!(inb(COM1+5) & 0x01))
80105cc3:	a8 01                	test   $0x1,%al
80105cc5:	74 11                	je     80105cd8 <uartgetc+0x28>
80105cc7:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105ccc:	ec                   	in     (%dx),%al
    return -1;
  return inb(COM1+0);
80105ccd:	0f b6 c0             	movzbl %al,%eax
80105cd0:	c3                   	ret    
80105cd1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80105cd8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105cdd:	c3                   	ret    
80105cde:	66 90                	xchg   %ax,%ax

80105ce0 <uartputc.part.0>:
uartputc(int c)
80105ce0:	55                   	push   %ebp
80105ce1:	89 e5                	mov    %esp,%ebp
80105ce3:	57                   	push   %edi
80105ce4:	89 c7                	mov    %eax,%edi
80105ce6:	56                   	push   %esi
80105ce7:	be fd 03 00 00       	mov    $0x3fd,%esi
80105cec:	53                   	push   %ebx
80105ced:	bb 80 00 00 00       	mov    $0x80,%ebx
80105cf2:	83 ec 0c             	sub    $0xc,%esp
80105cf5:	eb 1b                	jmp    80105d12 <uartputc.part.0+0x32>
80105cf7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105cfe:	66 90                	xchg   %ax,%ax
    microdelay(10);
80105d00:	83 ec 0c             	sub    $0xc,%esp
80105d03:	6a 0a                	push   $0xa
80105d05:	e8 f6 cb ff ff       	call   80102900 <microdelay>
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
80105d0a:	83 c4 10             	add    $0x10,%esp
80105d0d:	83 eb 01             	sub    $0x1,%ebx
80105d10:	74 07                	je     80105d19 <uartputc.part.0+0x39>
80105d12:	89 f2                	mov    %esi,%edx
80105d14:	ec                   	in     (%dx),%al
80105d15:	a8 20                	test   $0x20,%al
80105d17:	74 e7                	je     80105d00 <uartputc.part.0+0x20>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80105d19:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105d1e:	89 f8                	mov    %edi,%eax
80105d20:	ee                   	out    %al,(%dx)
}
80105d21:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105d24:	5b                   	pop    %ebx
80105d25:	5e                   	pop    %esi
80105d26:	5f                   	pop    %edi
80105d27:	5d                   	pop    %ebp
80105d28:	c3                   	ret    
80105d29:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80105d30 <uartinit>:
{
80105d30:	f3 0f 1e fb          	endbr32 
80105d34:	55                   	push   %ebp
80105d35:	31 c9                	xor    %ecx,%ecx
80105d37:	89 c8                	mov    %ecx,%eax
80105d39:	89 e5                	mov    %esp,%ebp
80105d3b:	57                   	push   %edi
80105d3c:	56                   	push   %esi
80105d3d:	53                   	push   %ebx
80105d3e:	bb fa 03 00 00       	mov    $0x3fa,%ebx
80105d43:	89 da                	mov    %ebx,%edx
80105d45:	83 ec 0c             	sub    $0xc,%esp
80105d48:	ee                   	out    %al,(%dx)
80105d49:	bf fb 03 00 00       	mov    $0x3fb,%edi
80105d4e:	b8 80 ff ff ff       	mov    $0xffffff80,%eax
80105d53:	89 fa                	mov    %edi,%edx
80105d55:	ee                   	out    %al,(%dx)
80105d56:	b8 0c 00 00 00       	mov    $0xc,%eax
80105d5b:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105d60:	ee                   	out    %al,(%dx)
80105d61:	be f9 03 00 00       	mov    $0x3f9,%esi
80105d66:	89 c8                	mov    %ecx,%eax
80105d68:	89 f2                	mov    %esi,%edx
80105d6a:	ee                   	out    %al,(%dx)
80105d6b:	b8 03 00 00 00       	mov    $0x3,%eax
80105d70:	89 fa                	mov    %edi,%edx
80105d72:	ee                   	out    %al,(%dx)
80105d73:	ba fc 03 00 00       	mov    $0x3fc,%edx
80105d78:	89 c8                	mov    %ecx,%eax
80105d7a:	ee                   	out    %al,(%dx)
80105d7b:	b8 01 00 00 00       	mov    $0x1,%eax
80105d80:	89 f2                	mov    %esi,%edx
80105d82:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80105d83:	ba fd 03 00 00       	mov    $0x3fd,%edx
80105d88:	ec                   	in     (%dx),%al
  if(inb(COM1+5) == 0xFF)
80105d89:	3c ff                	cmp    $0xff,%al
80105d8b:	74 52                	je     80105ddf <uartinit+0xaf>
  uart = 1;
80105d8d:	c7 05 bc a5 10 80 01 	movl   $0x1,0x8010a5bc
80105d94:	00 00 00 
80105d97:	89 da                	mov    %ebx,%edx
80105d99:	ec                   	in     (%dx),%al
80105d9a:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105d9f:	ec                   	in     (%dx),%al
  ioapicenable(IRQ_COM1, 0);
80105da0:	83 ec 08             	sub    $0x8,%esp
80105da3:	be 76 00 00 00       	mov    $0x76,%esi
  for(p="xv6...\n"; *p; p++)
80105da8:	bb 00 7d 10 80       	mov    $0x80107d00,%ebx
  ioapicenable(IRQ_COM1, 0);
80105dad:	6a 00                	push   $0x0
80105daf:	6a 04                	push   $0x4
80105db1:	e8 9a c6 ff ff       	call   80102450 <ioapicenable>
80105db6:	83 c4 10             	add    $0x10,%esp
  for(p="xv6...\n"; *p; p++)
80105db9:	b8 78 00 00 00       	mov    $0x78,%eax
80105dbe:	eb 04                	jmp    80105dc4 <uartinit+0x94>
80105dc0:	0f b6 73 01          	movzbl 0x1(%ebx),%esi
  if(!uart)
80105dc4:	8b 15 bc a5 10 80    	mov    0x8010a5bc,%edx
80105dca:	85 d2                	test   %edx,%edx
80105dcc:	74 08                	je     80105dd6 <uartinit+0xa6>
    uartputc(*p);
80105dce:	0f be c0             	movsbl %al,%eax
80105dd1:	e8 0a ff ff ff       	call   80105ce0 <uartputc.part.0>
  for(p="xv6...\n"; *p; p++)
80105dd6:	89 f0                	mov    %esi,%eax
80105dd8:	83 c3 01             	add    $0x1,%ebx
80105ddb:	84 c0                	test   %al,%al
80105ddd:	75 e1                	jne    80105dc0 <uartinit+0x90>
}
80105ddf:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105de2:	5b                   	pop    %ebx
80105de3:	5e                   	pop    %esi
80105de4:	5f                   	pop    %edi
80105de5:	5d                   	pop    %ebp
80105de6:	c3                   	ret    
80105de7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105dee:	66 90                	xchg   %ax,%ax

80105df0 <uartputc>:
{
80105df0:	f3 0f 1e fb          	endbr32 
80105df4:	55                   	push   %ebp
  if(!uart)
80105df5:	8b 15 bc a5 10 80    	mov    0x8010a5bc,%edx
{
80105dfb:	89 e5                	mov    %esp,%ebp
80105dfd:	8b 45 08             	mov    0x8(%ebp),%eax
  if(!uart)
80105e00:	85 d2                	test   %edx,%edx
80105e02:	74 0c                	je     80105e10 <uartputc+0x20>
}
80105e04:	5d                   	pop    %ebp
80105e05:	e9 d6 fe ff ff       	jmp    80105ce0 <uartputc.part.0>
80105e0a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80105e10:	5d                   	pop    %ebp
80105e11:	c3                   	ret    
80105e12:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105e19:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80105e20 <uartintr>:

void
uartintr(void)
{
80105e20:	f3 0f 1e fb          	endbr32 
80105e24:	55                   	push   %ebp
80105e25:	89 e5                	mov    %esp,%ebp
80105e27:	83 ec 14             	sub    $0x14,%esp
  consoleintr(uartgetc);
80105e2a:	68 b0 5c 10 80       	push   $0x80105cb0
80105e2f:	e8 2c aa ff ff       	call   80100860 <consoleintr>
}
80105e34:	83 c4 10             	add    $0x10,%esp
80105e37:	c9                   	leave  
80105e38:	c3                   	ret    

80105e39 <vector0>:
# generated by vectors.pl - do not edit
# handlers
.globl alltraps
.globl vector0
vector0:
  pushl $0
80105e39:	6a 00                	push   $0x0
  pushl $0
80105e3b:	6a 00                	push   $0x0
  jmp alltraps
80105e3d:	e9 3c fb ff ff       	jmp    8010597e <alltraps>

80105e42 <vector1>:
.globl vector1
vector1:
  pushl $0
80105e42:	6a 00                	push   $0x0
  pushl $1
80105e44:	6a 01                	push   $0x1
  jmp alltraps
80105e46:	e9 33 fb ff ff       	jmp    8010597e <alltraps>

80105e4b <vector2>:
.globl vector2
vector2:
  pushl $0
80105e4b:	6a 00                	push   $0x0
  pushl $2
80105e4d:	6a 02                	push   $0x2
  jmp alltraps
80105e4f:	e9 2a fb ff ff       	jmp    8010597e <alltraps>

80105e54 <vector3>:
.globl vector3
vector3:
  pushl $0
80105e54:	6a 00                	push   $0x0
  pushl $3
80105e56:	6a 03                	push   $0x3
  jmp alltraps
80105e58:	e9 21 fb ff ff       	jmp    8010597e <alltraps>

80105e5d <vector4>:
.globl vector4
vector4:
  pushl $0
80105e5d:	6a 00                	push   $0x0
  pushl $4
80105e5f:	6a 04                	push   $0x4
  jmp alltraps
80105e61:	e9 18 fb ff ff       	jmp    8010597e <alltraps>

80105e66 <vector5>:
.globl vector5
vector5:
  pushl $0
80105e66:	6a 00                	push   $0x0
  pushl $5
80105e68:	6a 05                	push   $0x5
  jmp alltraps
80105e6a:	e9 0f fb ff ff       	jmp    8010597e <alltraps>

80105e6f <vector6>:
.globl vector6
vector6:
  pushl $0
80105e6f:	6a 00                	push   $0x0
  pushl $6
80105e71:	6a 06                	push   $0x6
  jmp alltraps
80105e73:	e9 06 fb ff ff       	jmp    8010597e <alltraps>

80105e78 <vector7>:
.globl vector7
vector7:
  pushl $0
80105e78:	6a 00                	push   $0x0
  pushl $7
80105e7a:	6a 07                	push   $0x7
  jmp alltraps
80105e7c:	e9 fd fa ff ff       	jmp    8010597e <alltraps>

80105e81 <vector8>:
.globl vector8
vector8:
  pushl $8
80105e81:	6a 08                	push   $0x8
  jmp alltraps
80105e83:	e9 f6 fa ff ff       	jmp    8010597e <alltraps>

80105e88 <vector9>:
.globl vector9
vector9:
  pushl $0
80105e88:	6a 00                	push   $0x0
  pushl $9
80105e8a:	6a 09                	push   $0x9
  jmp alltraps
80105e8c:	e9 ed fa ff ff       	jmp    8010597e <alltraps>

80105e91 <vector10>:
.globl vector10
vector10:
  pushl $10
80105e91:	6a 0a                	push   $0xa
  jmp alltraps
80105e93:	e9 e6 fa ff ff       	jmp    8010597e <alltraps>

80105e98 <vector11>:
.globl vector11
vector11:
  pushl $11
80105e98:	6a 0b                	push   $0xb
  jmp alltraps
80105e9a:	e9 df fa ff ff       	jmp    8010597e <alltraps>

80105e9f <vector12>:
.globl vector12
vector12:
  pushl $12
80105e9f:	6a 0c                	push   $0xc
  jmp alltraps
80105ea1:	e9 d8 fa ff ff       	jmp    8010597e <alltraps>

80105ea6 <vector13>:
.globl vector13
vector13:
  pushl $13
80105ea6:	6a 0d                	push   $0xd
  jmp alltraps
80105ea8:	e9 d1 fa ff ff       	jmp    8010597e <alltraps>

80105ead <vector14>:
.globl vector14
vector14:
  pushl $14
80105ead:	6a 0e                	push   $0xe
  jmp alltraps
80105eaf:	e9 ca fa ff ff       	jmp    8010597e <alltraps>

80105eb4 <vector15>:
.globl vector15
vector15:
  pushl $0
80105eb4:	6a 00                	push   $0x0
  pushl $15
80105eb6:	6a 0f                	push   $0xf
  jmp alltraps
80105eb8:	e9 c1 fa ff ff       	jmp    8010597e <alltraps>

80105ebd <vector16>:
.globl vector16
vector16:
  pushl $0
80105ebd:	6a 00                	push   $0x0
  pushl $16
80105ebf:	6a 10                	push   $0x10
  jmp alltraps
80105ec1:	e9 b8 fa ff ff       	jmp    8010597e <alltraps>

80105ec6 <vector17>:
.globl vector17
vector17:
  pushl $17
80105ec6:	6a 11                	push   $0x11
  jmp alltraps
80105ec8:	e9 b1 fa ff ff       	jmp    8010597e <alltraps>

80105ecd <vector18>:
.globl vector18
vector18:
  pushl $0
80105ecd:	6a 00                	push   $0x0
  pushl $18
80105ecf:	6a 12                	push   $0x12
  jmp alltraps
80105ed1:	e9 a8 fa ff ff       	jmp    8010597e <alltraps>

80105ed6 <vector19>:
.globl vector19
vector19:
  pushl $0
80105ed6:	6a 00                	push   $0x0
  pushl $19
80105ed8:	6a 13                	push   $0x13
  jmp alltraps
80105eda:	e9 9f fa ff ff       	jmp    8010597e <alltraps>

80105edf <vector20>:
.globl vector20
vector20:
  pushl $0
80105edf:	6a 00                	push   $0x0
  pushl $20
80105ee1:	6a 14                	push   $0x14
  jmp alltraps
80105ee3:	e9 96 fa ff ff       	jmp    8010597e <alltraps>

80105ee8 <vector21>:
.globl vector21
vector21:
  pushl $0
80105ee8:	6a 00                	push   $0x0
  pushl $21
80105eea:	6a 15                	push   $0x15
  jmp alltraps
80105eec:	e9 8d fa ff ff       	jmp    8010597e <alltraps>

80105ef1 <vector22>:
.globl vector22
vector22:
  pushl $0
80105ef1:	6a 00                	push   $0x0
  pushl $22
80105ef3:	6a 16                	push   $0x16
  jmp alltraps
80105ef5:	e9 84 fa ff ff       	jmp    8010597e <alltraps>

80105efa <vector23>:
.globl vector23
vector23:
  pushl $0
80105efa:	6a 00                	push   $0x0
  pushl $23
80105efc:	6a 17                	push   $0x17
  jmp alltraps
80105efe:	e9 7b fa ff ff       	jmp    8010597e <alltraps>

80105f03 <vector24>:
.globl vector24
vector24:
  pushl $0
80105f03:	6a 00                	push   $0x0
  pushl $24
80105f05:	6a 18                	push   $0x18
  jmp alltraps
80105f07:	e9 72 fa ff ff       	jmp    8010597e <alltraps>

80105f0c <vector25>:
.globl vector25
vector25:
  pushl $0
80105f0c:	6a 00                	push   $0x0
  pushl $25
80105f0e:	6a 19                	push   $0x19
  jmp alltraps
80105f10:	e9 69 fa ff ff       	jmp    8010597e <alltraps>

80105f15 <vector26>:
.globl vector26
vector26:
  pushl $0
80105f15:	6a 00                	push   $0x0
  pushl $26
80105f17:	6a 1a                	push   $0x1a
  jmp alltraps
80105f19:	e9 60 fa ff ff       	jmp    8010597e <alltraps>

80105f1e <vector27>:
.globl vector27
vector27:
  pushl $0
80105f1e:	6a 00                	push   $0x0
  pushl $27
80105f20:	6a 1b                	push   $0x1b
  jmp alltraps
80105f22:	e9 57 fa ff ff       	jmp    8010597e <alltraps>

80105f27 <vector28>:
.globl vector28
vector28:
  pushl $0
80105f27:	6a 00                	push   $0x0
  pushl $28
80105f29:	6a 1c                	push   $0x1c
  jmp alltraps
80105f2b:	e9 4e fa ff ff       	jmp    8010597e <alltraps>

80105f30 <vector29>:
.globl vector29
vector29:
  pushl $0
80105f30:	6a 00                	push   $0x0
  pushl $29
80105f32:	6a 1d                	push   $0x1d
  jmp alltraps
80105f34:	e9 45 fa ff ff       	jmp    8010597e <alltraps>

80105f39 <vector30>:
.globl vector30
vector30:
  pushl $0
80105f39:	6a 00                	push   $0x0
  pushl $30
80105f3b:	6a 1e                	push   $0x1e
  jmp alltraps
80105f3d:	e9 3c fa ff ff       	jmp    8010597e <alltraps>

80105f42 <vector31>:
.globl vector31
vector31:
  pushl $0
80105f42:	6a 00                	push   $0x0
  pushl $31
80105f44:	6a 1f                	push   $0x1f
  jmp alltraps
80105f46:	e9 33 fa ff ff       	jmp    8010597e <alltraps>

80105f4b <vector32>:
.globl vector32
vector32:
  pushl $0
80105f4b:	6a 00                	push   $0x0
  pushl $32
80105f4d:	6a 20                	push   $0x20
  jmp alltraps
80105f4f:	e9 2a fa ff ff       	jmp    8010597e <alltraps>

80105f54 <vector33>:
.globl vector33
vector33:
  pushl $0
80105f54:	6a 00                	push   $0x0
  pushl $33
80105f56:	6a 21                	push   $0x21
  jmp alltraps
80105f58:	e9 21 fa ff ff       	jmp    8010597e <alltraps>

80105f5d <vector34>:
.globl vector34
vector34:
  pushl $0
80105f5d:	6a 00                	push   $0x0
  pushl $34
80105f5f:	6a 22                	push   $0x22
  jmp alltraps
80105f61:	e9 18 fa ff ff       	jmp    8010597e <alltraps>

80105f66 <vector35>:
.globl vector35
vector35:
  pushl $0
80105f66:	6a 00                	push   $0x0
  pushl $35
80105f68:	6a 23                	push   $0x23
  jmp alltraps
80105f6a:	e9 0f fa ff ff       	jmp    8010597e <alltraps>

80105f6f <vector36>:
.globl vector36
vector36:
  pushl $0
80105f6f:	6a 00                	push   $0x0
  pushl $36
80105f71:	6a 24                	push   $0x24
  jmp alltraps
80105f73:	e9 06 fa ff ff       	jmp    8010597e <alltraps>

80105f78 <vector37>:
.globl vector37
vector37:
  pushl $0
80105f78:	6a 00                	push   $0x0
  pushl $37
80105f7a:	6a 25                	push   $0x25
  jmp alltraps
80105f7c:	e9 fd f9 ff ff       	jmp    8010597e <alltraps>

80105f81 <vector38>:
.globl vector38
vector38:
  pushl $0
80105f81:	6a 00                	push   $0x0
  pushl $38
80105f83:	6a 26                	push   $0x26
  jmp alltraps
80105f85:	e9 f4 f9 ff ff       	jmp    8010597e <alltraps>

80105f8a <vector39>:
.globl vector39
vector39:
  pushl $0
80105f8a:	6a 00                	push   $0x0
  pushl $39
80105f8c:	6a 27                	push   $0x27
  jmp alltraps
80105f8e:	e9 eb f9 ff ff       	jmp    8010597e <alltraps>

80105f93 <vector40>:
.globl vector40
vector40:
  pushl $0
80105f93:	6a 00                	push   $0x0
  pushl $40
80105f95:	6a 28                	push   $0x28
  jmp alltraps
80105f97:	e9 e2 f9 ff ff       	jmp    8010597e <alltraps>

80105f9c <vector41>:
.globl vector41
vector41:
  pushl $0
80105f9c:	6a 00                	push   $0x0
  pushl $41
80105f9e:	6a 29                	push   $0x29
  jmp alltraps
80105fa0:	e9 d9 f9 ff ff       	jmp    8010597e <alltraps>

80105fa5 <vector42>:
.globl vector42
vector42:
  pushl $0
80105fa5:	6a 00                	push   $0x0
  pushl $42
80105fa7:	6a 2a                	push   $0x2a
  jmp alltraps
80105fa9:	e9 d0 f9 ff ff       	jmp    8010597e <alltraps>

80105fae <vector43>:
.globl vector43
vector43:
  pushl $0
80105fae:	6a 00                	push   $0x0
  pushl $43
80105fb0:	6a 2b                	push   $0x2b
  jmp alltraps
80105fb2:	e9 c7 f9 ff ff       	jmp    8010597e <alltraps>

80105fb7 <vector44>:
.globl vector44
vector44:
  pushl $0
80105fb7:	6a 00                	push   $0x0
  pushl $44
80105fb9:	6a 2c                	push   $0x2c
  jmp alltraps
80105fbb:	e9 be f9 ff ff       	jmp    8010597e <alltraps>

80105fc0 <vector45>:
.globl vector45
vector45:
  pushl $0
80105fc0:	6a 00                	push   $0x0
  pushl $45
80105fc2:	6a 2d                	push   $0x2d
  jmp alltraps
80105fc4:	e9 b5 f9 ff ff       	jmp    8010597e <alltraps>

80105fc9 <vector46>:
.globl vector46
vector46:
  pushl $0
80105fc9:	6a 00                	push   $0x0
  pushl $46
80105fcb:	6a 2e                	push   $0x2e
  jmp alltraps
80105fcd:	e9 ac f9 ff ff       	jmp    8010597e <alltraps>

80105fd2 <vector47>:
.globl vector47
vector47:
  pushl $0
80105fd2:	6a 00                	push   $0x0
  pushl $47
80105fd4:	6a 2f                	push   $0x2f
  jmp alltraps
80105fd6:	e9 a3 f9 ff ff       	jmp    8010597e <alltraps>

80105fdb <vector48>:
.globl vector48
vector48:
  pushl $0
80105fdb:	6a 00                	push   $0x0
  pushl $48
80105fdd:	6a 30                	push   $0x30
  jmp alltraps
80105fdf:	e9 9a f9 ff ff       	jmp    8010597e <alltraps>

80105fe4 <vector49>:
.globl vector49
vector49:
  pushl $0
80105fe4:	6a 00                	push   $0x0
  pushl $49
80105fe6:	6a 31                	push   $0x31
  jmp alltraps
80105fe8:	e9 91 f9 ff ff       	jmp    8010597e <alltraps>

80105fed <vector50>:
.globl vector50
vector50:
  pushl $0
80105fed:	6a 00                	push   $0x0
  pushl $50
80105fef:	6a 32                	push   $0x32
  jmp alltraps
80105ff1:	e9 88 f9 ff ff       	jmp    8010597e <alltraps>

80105ff6 <vector51>:
.globl vector51
vector51:
  pushl $0
80105ff6:	6a 00                	push   $0x0
  pushl $51
80105ff8:	6a 33                	push   $0x33
  jmp alltraps
80105ffa:	e9 7f f9 ff ff       	jmp    8010597e <alltraps>

80105fff <vector52>:
.globl vector52
vector52:
  pushl $0
80105fff:	6a 00                	push   $0x0
  pushl $52
80106001:	6a 34                	push   $0x34
  jmp alltraps
80106003:	e9 76 f9 ff ff       	jmp    8010597e <alltraps>

80106008 <vector53>:
.globl vector53
vector53:
  pushl $0
80106008:	6a 00                	push   $0x0
  pushl $53
8010600a:	6a 35                	push   $0x35
  jmp alltraps
8010600c:	e9 6d f9 ff ff       	jmp    8010597e <alltraps>

80106011 <vector54>:
.globl vector54
vector54:
  pushl $0
80106011:	6a 00                	push   $0x0
  pushl $54
80106013:	6a 36                	push   $0x36
  jmp alltraps
80106015:	e9 64 f9 ff ff       	jmp    8010597e <alltraps>

8010601a <vector55>:
.globl vector55
vector55:
  pushl $0
8010601a:	6a 00                	push   $0x0
  pushl $55
8010601c:	6a 37                	push   $0x37
  jmp alltraps
8010601e:	e9 5b f9 ff ff       	jmp    8010597e <alltraps>

80106023 <vector56>:
.globl vector56
vector56:
  pushl $0
80106023:	6a 00                	push   $0x0
  pushl $56
80106025:	6a 38                	push   $0x38
  jmp alltraps
80106027:	e9 52 f9 ff ff       	jmp    8010597e <alltraps>

8010602c <vector57>:
.globl vector57
vector57:
  pushl $0
8010602c:	6a 00                	push   $0x0
  pushl $57
8010602e:	6a 39                	push   $0x39
  jmp alltraps
80106030:	e9 49 f9 ff ff       	jmp    8010597e <alltraps>

80106035 <vector58>:
.globl vector58
vector58:
  pushl $0
80106035:	6a 00                	push   $0x0
  pushl $58
80106037:	6a 3a                	push   $0x3a
  jmp alltraps
80106039:	e9 40 f9 ff ff       	jmp    8010597e <alltraps>

8010603e <vector59>:
.globl vector59
vector59:
  pushl $0
8010603e:	6a 00                	push   $0x0
  pushl $59
80106040:	6a 3b                	push   $0x3b
  jmp alltraps
80106042:	e9 37 f9 ff ff       	jmp    8010597e <alltraps>

80106047 <vector60>:
.globl vector60
vector60:
  pushl $0
80106047:	6a 00                	push   $0x0
  pushl $60
80106049:	6a 3c                	push   $0x3c
  jmp alltraps
8010604b:	e9 2e f9 ff ff       	jmp    8010597e <alltraps>

80106050 <vector61>:
.globl vector61
vector61:
  pushl $0
80106050:	6a 00                	push   $0x0
  pushl $61
80106052:	6a 3d                	push   $0x3d
  jmp alltraps
80106054:	e9 25 f9 ff ff       	jmp    8010597e <alltraps>

80106059 <vector62>:
.globl vector62
vector62:
  pushl $0
80106059:	6a 00                	push   $0x0
  pushl $62
8010605b:	6a 3e                	push   $0x3e
  jmp alltraps
8010605d:	e9 1c f9 ff ff       	jmp    8010597e <alltraps>

80106062 <vector63>:
.globl vector63
vector63:
  pushl $0
80106062:	6a 00                	push   $0x0
  pushl $63
80106064:	6a 3f                	push   $0x3f
  jmp alltraps
80106066:	e9 13 f9 ff ff       	jmp    8010597e <alltraps>

8010606b <vector64>:
.globl vector64
vector64:
  pushl $0
8010606b:	6a 00                	push   $0x0
  pushl $64
8010606d:	6a 40                	push   $0x40
  jmp alltraps
8010606f:	e9 0a f9 ff ff       	jmp    8010597e <alltraps>

80106074 <vector65>:
.globl vector65
vector65:
  pushl $0
80106074:	6a 00                	push   $0x0
  pushl $65
80106076:	6a 41                	push   $0x41
  jmp alltraps
80106078:	e9 01 f9 ff ff       	jmp    8010597e <alltraps>

8010607d <vector66>:
.globl vector66
vector66:
  pushl $0
8010607d:	6a 00                	push   $0x0
  pushl $66
8010607f:	6a 42                	push   $0x42
  jmp alltraps
80106081:	e9 f8 f8 ff ff       	jmp    8010597e <alltraps>

80106086 <vector67>:
.globl vector67
vector67:
  pushl $0
80106086:	6a 00                	push   $0x0
  pushl $67
80106088:	6a 43                	push   $0x43
  jmp alltraps
8010608a:	e9 ef f8 ff ff       	jmp    8010597e <alltraps>

8010608f <vector68>:
.globl vector68
vector68:
  pushl $0
8010608f:	6a 00                	push   $0x0
  pushl $68
80106091:	6a 44                	push   $0x44
  jmp alltraps
80106093:	e9 e6 f8 ff ff       	jmp    8010597e <alltraps>

80106098 <vector69>:
.globl vector69
vector69:
  pushl $0
80106098:	6a 00                	push   $0x0
  pushl $69
8010609a:	6a 45                	push   $0x45
  jmp alltraps
8010609c:	e9 dd f8 ff ff       	jmp    8010597e <alltraps>

801060a1 <vector70>:
.globl vector70
vector70:
  pushl $0
801060a1:	6a 00                	push   $0x0
  pushl $70
801060a3:	6a 46                	push   $0x46
  jmp alltraps
801060a5:	e9 d4 f8 ff ff       	jmp    8010597e <alltraps>

801060aa <vector71>:
.globl vector71
vector71:
  pushl $0
801060aa:	6a 00                	push   $0x0
  pushl $71
801060ac:	6a 47                	push   $0x47
  jmp alltraps
801060ae:	e9 cb f8 ff ff       	jmp    8010597e <alltraps>

801060b3 <vector72>:
.globl vector72
vector72:
  pushl $0
801060b3:	6a 00                	push   $0x0
  pushl $72
801060b5:	6a 48                	push   $0x48
  jmp alltraps
801060b7:	e9 c2 f8 ff ff       	jmp    8010597e <alltraps>

801060bc <vector73>:
.globl vector73
vector73:
  pushl $0
801060bc:	6a 00                	push   $0x0
  pushl $73
801060be:	6a 49                	push   $0x49
  jmp alltraps
801060c0:	e9 b9 f8 ff ff       	jmp    8010597e <alltraps>

801060c5 <vector74>:
.globl vector74
vector74:
  pushl $0
801060c5:	6a 00                	push   $0x0
  pushl $74
801060c7:	6a 4a                	push   $0x4a
  jmp alltraps
801060c9:	e9 b0 f8 ff ff       	jmp    8010597e <alltraps>

801060ce <vector75>:
.globl vector75
vector75:
  pushl $0
801060ce:	6a 00                	push   $0x0
  pushl $75
801060d0:	6a 4b                	push   $0x4b
  jmp alltraps
801060d2:	e9 a7 f8 ff ff       	jmp    8010597e <alltraps>

801060d7 <vector76>:
.globl vector76
vector76:
  pushl $0
801060d7:	6a 00                	push   $0x0
  pushl $76
801060d9:	6a 4c                	push   $0x4c
  jmp alltraps
801060db:	e9 9e f8 ff ff       	jmp    8010597e <alltraps>

801060e0 <vector77>:
.globl vector77
vector77:
  pushl $0
801060e0:	6a 00                	push   $0x0
  pushl $77
801060e2:	6a 4d                	push   $0x4d
  jmp alltraps
801060e4:	e9 95 f8 ff ff       	jmp    8010597e <alltraps>

801060e9 <vector78>:
.globl vector78
vector78:
  pushl $0
801060e9:	6a 00                	push   $0x0
  pushl $78
801060eb:	6a 4e                	push   $0x4e
  jmp alltraps
801060ed:	e9 8c f8 ff ff       	jmp    8010597e <alltraps>

801060f2 <vector79>:
.globl vector79
vector79:
  pushl $0
801060f2:	6a 00                	push   $0x0
  pushl $79
801060f4:	6a 4f                	push   $0x4f
  jmp alltraps
801060f6:	e9 83 f8 ff ff       	jmp    8010597e <alltraps>

801060fb <vector80>:
.globl vector80
vector80:
  pushl $0
801060fb:	6a 00                	push   $0x0
  pushl $80
801060fd:	6a 50                	push   $0x50
  jmp alltraps
801060ff:	e9 7a f8 ff ff       	jmp    8010597e <alltraps>

80106104 <vector81>:
.globl vector81
vector81:
  pushl $0
80106104:	6a 00                	push   $0x0
  pushl $81
80106106:	6a 51                	push   $0x51
  jmp alltraps
80106108:	e9 71 f8 ff ff       	jmp    8010597e <alltraps>

8010610d <vector82>:
.globl vector82
vector82:
  pushl $0
8010610d:	6a 00                	push   $0x0
  pushl $82
8010610f:	6a 52                	push   $0x52
  jmp alltraps
80106111:	e9 68 f8 ff ff       	jmp    8010597e <alltraps>

80106116 <vector83>:
.globl vector83
vector83:
  pushl $0
80106116:	6a 00                	push   $0x0
  pushl $83
80106118:	6a 53                	push   $0x53
  jmp alltraps
8010611a:	e9 5f f8 ff ff       	jmp    8010597e <alltraps>

8010611f <vector84>:
.globl vector84
vector84:
  pushl $0
8010611f:	6a 00                	push   $0x0
  pushl $84
80106121:	6a 54                	push   $0x54
  jmp alltraps
80106123:	e9 56 f8 ff ff       	jmp    8010597e <alltraps>

80106128 <vector85>:
.globl vector85
vector85:
  pushl $0
80106128:	6a 00                	push   $0x0
  pushl $85
8010612a:	6a 55                	push   $0x55
  jmp alltraps
8010612c:	e9 4d f8 ff ff       	jmp    8010597e <alltraps>

80106131 <vector86>:
.globl vector86
vector86:
  pushl $0
80106131:	6a 00                	push   $0x0
  pushl $86
80106133:	6a 56                	push   $0x56
  jmp alltraps
80106135:	e9 44 f8 ff ff       	jmp    8010597e <alltraps>

8010613a <vector87>:
.globl vector87
vector87:
  pushl $0
8010613a:	6a 00                	push   $0x0
  pushl $87
8010613c:	6a 57                	push   $0x57
  jmp alltraps
8010613e:	e9 3b f8 ff ff       	jmp    8010597e <alltraps>

80106143 <vector88>:
.globl vector88
vector88:
  pushl $0
80106143:	6a 00                	push   $0x0
  pushl $88
80106145:	6a 58                	push   $0x58
  jmp alltraps
80106147:	e9 32 f8 ff ff       	jmp    8010597e <alltraps>

8010614c <vector89>:
.globl vector89
vector89:
  pushl $0
8010614c:	6a 00                	push   $0x0
  pushl $89
8010614e:	6a 59                	push   $0x59
  jmp alltraps
80106150:	e9 29 f8 ff ff       	jmp    8010597e <alltraps>

80106155 <vector90>:
.globl vector90
vector90:
  pushl $0
80106155:	6a 00                	push   $0x0
  pushl $90
80106157:	6a 5a                	push   $0x5a
  jmp alltraps
80106159:	e9 20 f8 ff ff       	jmp    8010597e <alltraps>

8010615e <vector91>:
.globl vector91
vector91:
  pushl $0
8010615e:	6a 00                	push   $0x0
  pushl $91
80106160:	6a 5b                	push   $0x5b
  jmp alltraps
80106162:	e9 17 f8 ff ff       	jmp    8010597e <alltraps>

80106167 <vector92>:
.globl vector92
vector92:
  pushl $0
80106167:	6a 00                	push   $0x0
  pushl $92
80106169:	6a 5c                	push   $0x5c
  jmp alltraps
8010616b:	e9 0e f8 ff ff       	jmp    8010597e <alltraps>

80106170 <vector93>:
.globl vector93
vector93:
  pushl $0
80106170:	6a 00                	push   $0x0
  pushl $93
80106172:	6a 5d                	push   $0x5d
  jmp alltraps
80106174:	e9 05 f8 ff ff       	jmp    8010597e <alltraps>

80106179 <vector94>:
.globl vector94
vector94:
  pushl $0
80106179:	6a 00                	push   $0x0
  pushl $94
8010617b:	6a 5e                	push   $0x5e
  jmp alltraps
8010617d:	e9 fc f7 ff ff       	jmp    8010597e <alltraps>

80106182 <vector95>:
.globl vector95
vector95:
  pushl $0
80106182:	6a 00                	push   $0x0
  pushl $95
80106184:	6a 5f                	push   $0x5f
  jmp alltraps
80106186:	e9 f3 f7 ff ff       	jmp    8010597e <alltraps>

8010618b <vector96>:
.globl vector96
vector96:
  pushl $0
8010618b:	6a 00                	push   $0x0
  pushl $96
8010618d:	6a 60                	push   $0x60
  jmp alltraps
8010618f:	e9 ea f7 ff ff       	jmp    8010597e <alltraps>

80106194 <vector97>:
.globl vector97
vector97:
  pushl $0
80106194:	6a 00                	push   $0x0
  pushl $97
80106196:	6a 61                	push   $0x61
  jmp alltraps
80106198:	e9 e1 f7 ff ff       	jmp    8010597e <alltraps>

8010619d <vector98>:
.globl vector98
vector98:
  pushl $0
8010619d:	6a 00                	push   $0x0
  pushl $98
8010619f:	6a 62                	push   $0x62
  jmp alltraps
801061a1:	e9 d8 f7 ff ff       	jmp    8010597e <alltraps>

801061a6 <vector99>:
.globl vector99
vector99:
  pushl $0
801061a6:	6a 00                	push   $0x0
  pushl $99
801061a8:	6a 63                	push   $0x63
  jmp alltraps
801061aa:	e9 cf f7 ff ff       	jmp    8010597e <alltraps>

801061af <vector100>:
.globl vector100
vector100:
  pushl $0
801061af:	6a 00                	push   $0x0
  pushl $100
801061b1:	6a 64                	push   $0x64
  jmp alltraps
801061b3:	e9 c6 f7 ff ff       	jmp    8010597e <alltraps>

801061b8 <vector101>:
.globl vector101
vector101:
  pushl $0
801061b8:	6a 00                	push   $0x0
  pushl $101
801061ba:	6a 65                	push   $0x65
  jmp alltraps
801061bc:	e9 bd f7 ff ff       	jmp    8010597e <alltraps>

801061c1 <vector102>:
.globl vector102
vector102:
  pushl $0
801061c1:	6a 00                	push   $0x0
  pushl $102
801061c3:	6a 66                	push   $0x66
  jmp alltraps
801061c5:	e9 b4 f7 ff ff       	jmp    8010597e <alltraps>

801061ca <vector103>:
.globl vector103
vector103:
  pushl $0
801061ca:	6a 00                	push   $0x0
  pushl $103
801061cc:	6a 67                	push   $0x67
  jmp alltraps
801061ce:	e9 ab f7 ff ff       	jmp    8010597e <alltraps>

801061d3 <vector104>:
.globl vector104
vector104:
  pushl $0
801061d3:	6a 00                	push   $0x0
  pushl $104
801061d5:	6a 68                	push   $0x68
  jmp alltraps
801061d7:	e9 a2 f7 ff ff       	jmp    8010597e <alltraps>

801061dc <vector105>:
.globl vector105
vector105:
  pushl $0
801061dc:	6a 00                	push   $0x0
  pushl $105
801061de:	6a 69                	push   $0x69
  jmp alltraps
801061e0:	e9 99 f7 ff ff       	jmp    8010597e <alltraps>

801061e5 <vector106>:
.globl vector106
vector106:
  pushl $0
801061e5:	6a 00                	push   $0x0
  pushl $106
801061e7:	6a 6a                	push   $0x6a
  jmp alltraps
801061e9:	e9 90 f7 ff ff       	jmp    8010597e <alltraps>

801061ee <vector107>:
.globl vector107
vector107:
  pushl $0
801061ee:	6a 00                	push   $0x0
  pushl $107
801061f0:	6a 6b                	push   $0x6b
  jmp alltraps
801061f2:	e9 87 f7 ff ff       	jmp    8010597e <alltraps>

801061f7 <vector108>:
.globl vector108
vector108:
  pushl $0
801061f7:	6a 00                	push   $0x0
  pushl $108
801061f9:	6a 6c                	push   $0x6c
  jmp alltraps
801061fb:	e9 7e f7 ff ff       	jmp    8010597e <alltraps>

80106200 <vector109>:
.globl vector109
vector109:
  pushl $0
80106200:	6a 00                	push   $0x0
  pushl $109
80106202:	6a 6d                	push   $0x6d
  jmp alltraps
80106204:	e9 75 f7 ff ff       	jmp    8010597e <alltraps>

80106209 <vector110>:
.globl vector110
vector110:
  pushl $0
80106209:	6a 00                	push   $0x0
  pushl $110
8010620b:	6a 6e                	push   $0x6e
  jmp alltraps
8010620d:	e9 6c f7 ff ff       	jmp    8010597e <alltraps>

80106212 <vector111>:
.globl vector111
vector111:
  pushl $0
80106212:	6a 00                	push   $0x0
  pushl $111
80106214:	6a 6f                	push   $0x6f
  jmp alltraps
80106216:	e9 63 f7 ff ff       	jmp    8010597e <alltraps>

8010621b <vector112>:
.globl vector112
vector112:
  pushl $0
8010621b:	6a 00                	push   $0x0
  pushl $112
8010621d:	6a 70                	push   $0x70
  jmp alltraps
8010621f:	e9 5a f7 ff ff       	jmp    8010597e <alltraps>

80106224 <vector113>:
.globl vector113
vector113:
  pushl $0
80106224:	6a 00                	push   $0x0
  pushl $113
80106226:	6a 71                	push   $0x71
  jmp alltraps
80106228:	e9 51 f7 ff ff       	jmp    8010597e <alltraps>

8010622d <vector114>:
.globl vector114
vector114:
  pushl $0
8010622d:	6a 00                	push   $0x0
  pushl $114
8010622f:	6a 72                	push   $0x72
  jmp alltraps
80106231:	e9 48 f7 ff ff       	jmp    8010597e <alltraps>

80106236 <vector115>:
.globl vector115
vector115:
  pushl $0
80106236:	6a 00                	push   $0x0
  pushl $115
80106238:	6a 73                	push   $0x73
  jmp alltraps
8010623a:	e9 3f f7 ff ff       	jmp    8010597e <alltraps>

8010623f <vector116>:
.globl vector116
vector116:
  pushl $0
8010623f:	6a 00                	push   $0x0
  pushl $116
80106241:	6a 74                	push   $0x74
  jmp alltraps
80106243:	e9 36 f7 ff ff       	jmp    8010597e <alltraps>

80106248 <vector117>:
.globl vector117
vector117:
  pushl $0
80106248:	6a 00                	push   $0x0
  pushl $117
8010624a:	6a 75                	push   $0x75
  jmp alltraps
8010624c:	e9 2d f7 ff ff       	jmp    8010597e <alltraps>

80106251 <vector118>:
.globl vector118
vector118:
  pushl $0
80106251:	6a 00                	push   $0x0
  pushl $118
80106253:	6a 76                	push   $0x76
  jmp alltraps
80106255:	e9 24 f7 ff ff       	jmp    8010597e <alltraps>

8010625a <vector119>:
.globl vector119
vector119:
  pushl $0
8010625a:	6a 00                	push   $0x0
  pushl $119
8010625c:	6a 77                	push   $0x77
  jmp alltraps
8010625e:	e9 1b f7 ff ff       	jmp    8010597e <alltraps>

80106263 <vector120>:
.globl vector120
vector120:
  pushl $0
80106263:	6a 00                	push   $0x0
  pushl $120
80106265:	6a 78                	push   $0x78
  jmp alltraps
80106267:	e9 12 f7 ff ff       	jmp    8010597e <alltraps>

8010626c <vector121>:
.globl vector121
vector121:
  pushl $0
8010626c:	6a 00                	push   $0x0
  pushl $121
8010626e:	6a 79                	push   $0x79
  jmp alltraps
80106270:	e9 09 f7 ff ff       	jmp    8010597e <alltraps>

80106275 <vector122>:
.globl vector122
vector122:
  pushl $0
80106275:	6a 00                	push   $0x0
  pushl $122
80106277:	6a 7a                	push   $0x7a
  jmp alltraps
80106279:	e9 00 f7 ff ff       	jmp    8010597e <alltraps>

8010627e <vector123>:
.globl vector123
vector123:
  pushl $0
8010627e:	6a 00                	push   $0x0
  pushl $123
80106280:	6a 7b                	push   $0x7b
  jmp alltraps
80106282:	e9 f7 f6 ff ff       	jmp    8010597e <alltraps>

80106287 <vector124>:
.globl vector124
vector124:
  pushl $0
80106287:	6a 00                	push   $0x0
  pushl $124
80106289:	6a 7c                	push   $0x7c
  jmp alltraps
8010628b:	e9 ee f6 ff ff       	jmp    8010597e <alltraps>

80106290 <vector125>:
.globl vector125
vector125:
  pushl $0
80106290:	6a 00                	push   $0x0
  pushl $125
80106292:	6a 7d                	push   $0x7d
  jmp alltraps
80106294:	e9 e5 f6 ff ff       	jmp    8010597e <alltraps>

80106299 <vector126>:
.globl vector126
vector126:
  pushl $0
80106299:	6a 00                	push   $0x0
  pushl $126
8010629b:	6a 7e                	push   $0x7e
  jmp alltraps
8010629d:	e9 dc f6 ff ff       	jmp    8010597e <alltraps>

801062a2 <vector127>:
.globl vector127
vector127:
  pushl $0
801062a2:	6a 00                	push   $0x0
  pushl $127
801062a4:	6a 7f                	push   $0x7f
  jmp alltraps
801062a6:	e9 d3 f6 ff ff       	jmp    8010597e <alltraps>

801062ab <vector128>:
.globl vector128
vector128:
  pushl $0
801062ab:	6a 00                	push   $0x0
  pushl $128
801062ad:	68 80 00 00 00       	push   $0x80
  jmp alltraps
801062b2:	e9 c7 f6 ff ff       	jmp    8010597e <alltraps>

801062b7 <vector129>:
.globl vector129
vector129:
  pushl $0
801062b7:	6a 00                	push   $0x0
  pushl $129
801062b9:	68 81 00 00 00       	push   $0x81
  jmp alltraps
801062be:	e9 bb f6 ff ff       	jmp    8010597e <alltraps>

801062c3 <vector130>:
.globl vector130
vector130:
  pushl $0
801062c3:	6a 00                	push   $0x0
  pushl $130
801062c5:	68 82 00 00 00       	push   $0x82
  jmp alltraps
801062ca:	e9 af f6 ff ff       	jmp    8010597e <alltraps>

801062cf <vector131>:
.globl vector131
vector131:
  pushl $0
801062cf:	6a 00                	push   $0x0
  pushl $131
801062d1:	68 83 00 00 00       	push   $0x83
  jmp alltraps
801062d6:	e9 a3 f6 ff ff       	jmp    8010597e <alltraps>

801062db <vector132>:
.globl vector132
vector132:
  pushl $0
801062db:	6a 00                	push   $0x0
  pushl $132
801062dd:	68 84 00 00 00       	push   $0x84
  jmp alltraps
801062e2:	e9 97 f6 ff ff       	jmp    8010597e <alltraps>

801062e7 <vector133>:
.globl vector133
vector133:
  pushl $0
801062e7:	6a 00                	push   $0x0
  pushl $133
801062e9:	68 85 00 00 00       	push   $0x85
  jmp alltraps
801062ee:	e9 8b f6 ff ff       	jmp    8010597e <alltraps>

801062f3 <vector134>:
.globl vector134
vector134:
  pushl $0
801062f3:	6a 00                	push   $0x0
  pushl $134
801062f5:	68 86 00 00 00       	push   $0x86
  jmp alltraps
801062fa:	e9 7f f6 ff ff       	jmp    8010597e <alltraps>

801062ff <vector135>:
.globl vector135
vector135:
  pushl $0
801062ff:	6a 00                	push   $0x0
  pushl $135
80106301:	68 87 00 00 00       	push   $0x87
  jmp alltraps
80106306:	e9 73 f6 ff ff       	jmp    8010597e <alltraps>

8010630b <vector136>:
.globl vector136
vector136:
  pushl $0
8010630b:	6a 00                	push   $0x0
  pushl $136
8010630d:	68 88 00 00 00       	push   $0x88
  jmp alltraps
80106312:	e9 67 f6 ff ff       	jmp    8010597e <alltraps>

80106317 <vector137>:
.globl vector137
vector137:
  pushl $0
80106317:	6a 00                	push   $0x0
  pushl $137
80106319:	68 89 00 00 00       	push   $0x89
  jmp alltraps
8010631e:	e9 5b f6 ff ff       	jmp    8010597e <alltraps>

80106323 <vector138>:
.globl vector138
vector138:
  pushl $0
80106323:	6a 00                	push   $0x0
  pushl $138
80106325:	68 8a 00 00 00       	push   $0x8a
  jmp alltraps
8010632a:	e9 4f f6 ff ff       	jmp    8010597e <alltraps>

8010632f <vector139>:
.globl vector139
vector139:
  pushl $0
8010632f:	6a 00                	push   $0x0
  pushl $139
80106331:	68 8b 00 00 00       	push   $0x8b
  jmp alltraps
80106336:	e9 43 f6 ff ff       	jmp    8010597e <alltraps>

8010633b <vector140>:
.globl vector140
vector140:
  pushl $0
8010633b:	6a 00                	push   $0x0
  pushl $140
8010633d:	68 8c 00 00 00       	push   $0x8c
  jmp alltraps
80106342:	e9 37 f6 ff ff       	jmp    8010597e <alltraps>

80106347 <vector141>:
.globl vector141
vector141:
  pushl $0
80106347:	6a 00                	push   $0x0
  pushl $141
80106349:	68 8d 00 00 00       	push   $0x8d
  jmp alltraps
8010634e:	e9 2b f6 ff ff       	jmp    8010597e <alltraps>

80106353 <vector142>:
.globl vector142
vector142:
  pushl $0
80106353:	6a 00                	push   $0x0
  pushl $142
80106355:	68 8e 00 00 00       	push   $0x8e
  jmp alltraps
8010635a:	e9 1f f6 ff ff       	jmp    8010597e <alltraps>

8010635f <vector143>:
.globl vector143
vector143:
  pushl $0
8010635f:	6a 00                	push   $0x0
  pushl $143
80106361:	68 8f 00 00 00       	push   $0x8f
  jmp alltraps
80106366:	e9 13 f6 ff ff       	jmp    8010597e <alltraps>

8010636b <vector144>:
.globl vector144
vector144:
  pushl $0
8010636b:	6a 00                	push   $0x0
  pushl $144
8010636d:	68 90 00 00 00       	push   $0x90
  jmp alltraps
80106372:	e9 07 f6 ff ff       	jmp    8010597e <alltraps>

80106377 <vector145>:
.globl vector145
vector145:
  pushl $0
80106377:	6a 00                	push   $0x0
  pushl $145
80106379:	68 91 00 00 00       	push   $0x91
  jmp alltraps
8010637e:	e9 fb f5 ff ff       	jmp    8010597e <alltraps>

80106383 <vector146>:
.globl vector146
vector146:
  pushl $0
80106383:	6a 00                	push   $0x0
  pushl $146
80106385:	68 92 00 00 00       	push   $0x92
  jmp alltraps
8010638a:	e9 ef f5 ff ff       	jmp    8010597e <alltraps>

8010638f <vector147>:
.globl vector147
vector147:
  pushl $0
8010638f:	6a 00                	push   $0x0
  pushl $147
80106391:	68 93 00 00 00       	push   $0x93
  jmp alltraps
80106396:	e9 e3 f5 ff ff       	jmp    8010597e <alltraps>

8010639b <vector148>:
.globl vector148
vector148:
  pushl $0
8010639b:	6a 00                	push   $0x0
  pushl $148
8010639d:	68 94 00 00 00       	push   $0x94
  jmp alltraps
801063a2:	e9 d7 f5 ff ff       	jmp    8010597e <alltraps>

801063a7 <vector149>:
.globl vector149
vector149:
  pushl $0
801063a7:	6a 00                	push   $0x0
  pushl $149
801063a9:	68 95 00 00 00       	push   $0x95
  jmp alltraps
801063ae:	e9 cb f5 ff ff       	jmp    8010597e <alltraps>

801063b3 <vector150>:
.globl vector150
vector150:
  pushl $0
801063b3:	6a 00                	push   $0x0
  pushl $150
801063b5:	68 96 00 00 00       	push   $0x96
  jmp alltraps
801063ba:	e9 bf f5 ff ff       	jmp    8010597e <alltraps>

801063bf <vector151>:
.globl vector151
vector151:
  pushl $0
801063bf:	6a 00                	push   $0x0
  pushl $151
801063c1:	68 97 00 00 00       	push   $0x97
  jmp alltraps
801063c6:	e9 b3 f5 ff ff       	jmp    8010597e <alltraps>

801063cb <vector152>:
.globl vector152
vector152:
  pushl $0
801063cb:	6a 00                	push   $0x0
  pushl $152
801063cd:	68 98 00 00 00       	push   $0x98
  jmp alltraps
801063d2:	e9 a7 f5 ff ff       	jmp    8010597e <alltraps>

801063d7 <vector153>:
.globl vector153
vector153:
  pushl $0
801063d7:	6a 00                	push   $0x0
  pushl $153
801063d9:	68 99 00 00 00       	push   $0x99
  jmp alltraps
801063de:	e9 9b f5 ff ff       	jmp    8010597e <alltraps>

801063e3 <vector154>:
.globl vector154
vector154:
  pushl $0
801063e3:	6a 00                	push   $0x0
  pushl $154
801063e5:	68 9a 00 00 00       	push   $0x9a
  jmp alltraps
801063ea:	e9 8f f5 ff ff       	jmp    8010597e <alltraps>

801063ef <vector155>:
.globl vector155
vector155:
  pushl $0
801063ef:	6a 00                	push   $0x0
  pushl $155
801063f1:	68 9b 00 00 00       	push   $0x9b
  jmp alltraps
801063f6:	e9 83 f5 ff ff       	jmp    8010597e <alltraps>

801063fb <vector156>:
.globl vector156
vector156:
  pushl $0
801063fb:	6a 00                	push   $0x0
  pushl $156
801063fd:	68 9c 00 00 00       	push   $0x9c
  jmp alltraps
80106402:	e9 77 f5 ff ff       	jmp    8010597e <alltraps>

80106407 <vector157>:
.globl vector157
vector157:
  pushl $0
80106407:	6a 00                	push   $0x0
  pushl $157
80106409:	68 9d 00 00 00       	push   $0x9d
  jmp alltraps
8010640e:	e9 6b f5 ff ff       	jmp    8010597e <alltraps>

80106413 <vector158>:
.globl vector158
vector158:
  pushl $0
80106413:	6a 00                	push   $0x0
  pushl $158
80106415:	68 9e 00 00 00       	push   $0x9e
  jmp alltraps
8010641a:	e9 5f f5 ff ff       	jmp    8010597e <alltraps>

8010641f <vector159>:
.globl vector159
vector159:
  pushl $0
8010641f:	6a 00                	push   $0x0
  pushl $159
80106421:	68 9f 00 00 00       	push   $0x9f
  jmp alltraps
80106426:	e9 53 f5 ff ff       	jmp    8010597e <alltraps>

8010642b <vector160>:
.globl vector160
vector160:
  pushl $0
8010642b:	6a 00                	push   $0x0
  pushl $160
8010642d:	68 a0 00 00 00       	push   $0xa0
  jmp alltraps
80106432:	e9 47 f5 ff ff       	jmp    8010597e <alltraps>

80106437 <vector161>:
.globl vector161
vector161:
  pushl $0
80106437:	6a 00                	push   $0x0
  pushl $161
80106439:	68 a1 00 00 00       	push   $0xa1
  jmp alltraps
8010643e:	e9 3b f5 ff ff       	jmp    8010597e <alltraps>

80106443 <vector162>:
.globl vector162
vector162:
  pushl $0
80106443:	6a 00                	push   $0x0
  pushl $162
80106445:	68 a2 00 00 00       	push   $0xa2
  jmp alltraps
8010644a:	e9 2f f5 ff ff       	jmp    8010597e <alltraps>

8010644f <vector163>:
.globl vector163
vector163:
  pushl $0
8010644f:	6a 00                	push   $0x0
  pushl $163
80106451:	68 a3 00 00 00       	push   $0xa3
  jmp alltraps
80106456:	e9 23 f5 ff ff       	jmp    8010597e <alltraps>

8010645b <vector164>:
.globl vector164
vector164:
  pushl $0
8010645b:	6a 00                	push   $0x0
  pushl $164
8010645d:	68 a4 00 00 00       	push   $0xa4
  jmp alltraps
80106462:	e9 17 f5 ff ff       	jmp    8010597e <alltraps>

80106467 <vector165>:
.globl vector165
vector165:
  pushl $0
80106467:	6a 00                	push   $0x0
  pushl $165
80106469:	68 a5 00 00 00       	push   $0xa5
  jmp alltraps
8010646e:	e9 0b f5 ff ff       	jmp    8010597e <alltraps>

80106473 <vector166>:
.globl vector166
vector166:
  pushl $0
80106473:	6a 00                	push   $0x0
  pushl $166
80106475:	68 a6 00 00 00       	push   $0xa6
  jmp alltraps
8010647a:	e9 ff f4 ff ff       	jmp    8010597e <alltraps>

8010647f <vector167>:
.globl vector167
vector167:
  pushl $0
8010647f:	6a 00                	push   $0x0
  pushl $167
80106481:	68 a7 00 00 00       	push   $0xa7
  jmp alltraps
80106486:	e9 f3 f4 ff ff       	jmp    8010597e <alltraps>

8010648b <vector168>:
.globl vector168
vector168:
  pushl $0
8010648b:	6a 00                	push   $0x0
  pushl $168
8010648d:	68 a8 00 00 00       	push   $0xa8
  jmp alltraps
80106492:	e9 e7 f4 ff ff       	jmp    8010597e <alltraps>

80106497 <vector169>:
.globl vector169
vector169:
  pushl $0
80106497:	6a 00                	push   $0x0
  pushl $169
80106499:	68 a9 00 00 00       	push   $0xa9
  jmp alltraps
8010649e:	e9 db f4 ff ff       	jmp    8010597e <alltraps>

801064a3 <vector170>:
.globl vector170
vector170:
  pushl $0
801064a3:	6a 00                	push   $0x0
  pushl $170
801064a5:	68 aa 00 00 00       	push   $0xaa
  jmp alltraps
801064aa:	e9 cf f4 ff ff       	jmp    8010597e <alltraps>

801064af <vector171>:
.globl vector171
vector171:
  pushl $0
801064af:	6a 00                	push   $0x0
  pushl $171
801064b1:	68 ab 00 00 00       	push   $0xab
  jmp alltraps
801064b6:	e9 c3 f4 ff ff       	jmp    8010597e <alltraps>

801064bb <vector172>:
.globl vector172
vector172:
  pushl $0
801064bb:	6a 00                	push   $0x0
  pushl $172
801064bd:	68 ac 00 00 00       	push   $0xac
  jmp alltraps
801064c2:	e9 b7 f4 ff ff       	jmp    8010597e <alltraps>

801064c7 <vector173>:
.globl vector173
vector173:
  pushl $0
801064c7:	6a 00                	push   $0x0
  pushl $173
801064c9:	68 ad 00 00 00       	push   $0xad
  jmp alltraps
801064ce:	e9 ab f4 ff ff       	jmp    8010597e <alltraps>

801064d3 <vector174>:
.globl vector174
vector174:
  pushl $0
801064d3:	6a 00                	push   $0x0
  pushl $174
801064d5:	68 ae 00 00 00       	push   $0xae
  jmp alltraps
801064da:	e9 9f f4 ff ff       	jmp    8010597e <alltraps>

801064df <vector175>:
.globl vector175
vector175:
  pushl $0
801064df:	6a 00                	push   $0x0
  pushl $175
801064e1:	68 af 00 00 00       	push   $0xaf
  jmp alltraps
801064e6:	e9 93 f4 ff ff       	jmp    8010597e <alltraps>

801064eb <vector176>:
.globl vector176
vector176:
  pushl $0
801064eb:	6a 00                	push   $0x0
  pushl $176
801064ed:	68 b0 00 00 00       	push   $0xb0
  jmp alltraps
801064f2:	e9 87 f4 ff ff       	jmp    8010597e <alltraps>

801064f7 <vector177>:
.globl vector177
vector177:
  pushl $0
801064f7:	6a 00                	push   $0x0
  pushl $177
801064f9:	68 b1 00 00 00       	push   $0xb1
  jmp alltraps
801064fe:	e9 7b f4 ff ff       	jmp    8010597e <alltraps>

80106503 <vector178>:
.globl vector178
vector178:
  pushl $0
80106503:	6a 00                	push   $0x0
  pushl $178
80106505:	68 b2 00 00 00       	push   $0xb2
  jmp alltraps
8010650a:	e9 6f f4 ff ff       	jmp    8010597e <alltraps>

8010650f <vector179>:
.globl vector179
vector179:
  pushl $0
8010650f:	6a 00                	push   $0x0
  pushl $179
80106511:	68 b3 00 00 00       	push   $0xb3
  jmp alltraps
80106516:	e9 63 f4 ff ff       	jmp    8010597e <alltraps>

8010651b <vector180>:
.globl vector180
vector180:
  pushl $0
8010651b:	6a 00                	push   $0x0
  pushl $180
8010651d:	68 b4 00 00 00       	push   $0xb4
  jmp alltraps
80106522:	e9 57 f4 ff ff       	jmp    8010597e <alltraps>

80106527 <vector181>:
.globl vector181
vector181:
  pushl $0
80106527:	6a 00                	push   $0x0
  pushl $181
80106529:	68 b5 00 00 00       	push   $0xb5
  jmp alltraps
8010652e:	e9 4b f4 ff ff       	jmp    8010597e <alltraps>

80106533 <vector182>:
.globl vector182
vector182:
  pushl $0
80106533:	6a 00                	push   $0x0
  pushl $182
80106535:	68 b6 00 00 00       	push   $0xb6
  jmp alltraps
8010653a:	e9 3f f4 ff ff       	jmp    8010597e <alltraps>

8010653f <vector183>:
.globl vector183
vector183:
  pushl $0
8010653f:	6a 00                	push   $0x0
  pushl $183
80106541:	68 b7 00 00 00       	push   $0xb7
  jmp alltraps
80106546:	e9 33 f4 ff ff       	jmp    8010597e <alltraps>

8010654b <vector184>:
.globl vector184
vector184:
  pushl $0
8010654b:	6a 00                	push   $0x0
  pushl $184
8010654d:	68 b8 00 00 00       	push   $0xb8
  jmp alltraps
80106552:	e9 27 f4 ff ff       	jmp    8010597e <alltraps>

80106557 <vector185>:
.globl vector185
vector185:
  pushl $0
80106557:	6a 00                	push   $0x0
  pushl $185
80106559:	68 b9 00 00 00       	push   $0xb9
  jmp alltraps
8010655e:	e9 1b f4 ff ff       	jmp    8010597e <alltraps>

80106563 <vector186>:
.globl vector186
vector186:
  pushl $0
80106563:	6a 00                	push   $0x0
  pushl $186
80106565:	68 ba 00 00 00       	push   $0xba
  jmp alltraps
8010656a:	e9 0f f4 ff ff       	jmp    8010597e <alltraps>

8010656f <vector187>:
.globl vector187
vector187:
  pushl $0
8010656f:	6a 00                	push   $0x0
  pushl $187
80106571:	68 bb 00 00 00       	push   $0xbb
  jmp alltraps
80106576:	e9 03 f4 ff ff       	jmp    8010597e <alltraps>

8010657b <vector188>:
.globl vector188
vector188:
  pushl $0
8010657b:	6a 00                	push   $0x0
  pushl $188
8010657d:	68 bc 00 00 00       	push   $0xbc
  jmp alltraps
80106582:	e9 f7 f3 ff ff       	jmp    8010597e <alltraps>

80106587 <vector189>:
.globl vector189
vector189:
  pushl $0
80106587:	6a 00                	push   $0x0
  pushl $189
80106589:	68 bd 00 00 00       	push   $0xbd
  jmp alltraps
8010658e:	e9 eb f3 ff ff       	jmp    8010597e <alltraps>

80106593 <vector190>:
.globl vector190
vector190:
  pushl $0
80106593:	6a 00                	push   $0x0
  pushl $190
80106595:	68 be 00 00 00       	push   $0xbe
  jmp alltraps
8010659a:	e9 df f3 ff ff       	jmp    8010597e <alltraps>

8010659f <vector191>:
.globl vector191
vector191:
  pushl $0
8010659f:	6a 00                	push   $0x0
  pushl $191
801065a1:	68 bf 00 00 00       	push   $0xbf
  jmp alltraps
801065a6:	e9 d3 f3 ff ff       	jmp    8010597e <alltraps>

801065ab <vector192>:
.globl vector192
vector192:
  pushl $0
801065ab:	6a 00                	push   $0x0
  pushl $192
801065ad:	68 c0 00 00 00       	push   $0xc0
  jmp alltraps
801065b2:	e9 c7 f3 ff ff       	jmp    8010597e <alltraps>

801065b7 <vector193>:
.globl vector193
vector193:
  pushl $0
801065b7:	6a 00                	push   $0x0
  pushl $193
801065b9:	68 c1 00 00 00       	push   $0xc1
  jmp alltraps
801065be:	e9 bb f3 ff ff       	jmp    8010597e <alltraps>

801065c3 <vector194>:
.globl vector194
vector194:
  pushl $0
801065c3:	6a 00                	push   $0x0
  pushl $194
801065c5:	68 c2 00 00 00       	push   $0xc2
  jmp alltraps
801065ca:	e9 af f3 ff ff       	jmp    8010597e <alltraps>

801065cf <vector195>:
.globl vector195
vector195:
  pushl $0
801065cf:	6a 00                	push   $0x0
  pushl $195
801065d1:	68 c3 00 00 00       	push   $0xc3
  jmp alltraps
801065d6:	e9 a3 f3 ff ff       	jmp    8010597e <alltraps>

801065db <vector196>:
.globl vector196
vector196:
  pushl $0
801065db:	6a 00                	push   $0x0
  pushl $196
801065dd:	68 c4 00 00 00       	push   $0xc4
  jmp alltraps
801065e2:	e9 97 f3 ff ff       	jmp    8010597e <alltraps>

801065e7 <vector197>:
.globl vector197
vector197:
  pushl $0
801065e7:	6a 00                	push   $0x0
  pushl $197
801065e9:	68 c5 00 00 00       	push   $0xc5
  jmp alltraps
801065ee:	e9 8b f3 ff ff       	jmp    8010597e <alltraps>

801065f3 <vector198>:
.globl vector198
vector198:
  pushl $0
801065f3:	6a 00                	push   $0x0
  pushl $198
801065f5:	68 c6 00 00 00       	push   $0xc6
  jmp alltraps
801065fa:	e9 7f f3 ff ff       	jmp    8010597e <alltraps>

801065ff <vector199>:
.globl vector199
vector199:
  pushl $0
801065ff:	6a 00                	push   $0x0
  pushl $199
80106601:	68 c7 00 00 00       	push   $0xc7
  jmp alltraps
80106606:	e9 73 f3 ff ff       	jmp    8010597e <alltraps>

8010660b <vector200>:
.globl vector200
vector200:
  pushl $0
8010660b:	6a 00                	push   $0x0
  pushl $200
8010660d:	68 c8 00 00 00       	push   $0xc8
  jmp alltraps
80106612:	e9 67 f3 ff ff       	jmp    8010597e <alltraps>

80106617 <vector201>:
.globl vector201
vector201:
  pushl $0
80106617:	6a 00                	push   $0x0
  pushl $201
80106619:	68 c9 00 00 00       	push   $0xc9
  jmp alltraps
8010661e:	e9 5b f3 ff ff       	jmp    8010597e <alltraps>

80106623 <vector202>:
.globl vector202
vector202:
  pushl $0
80106623:	6a 00                	push   $0x0
  pushl $202
80106625:	68 ca 00 00 00       	push   $0xca
  jmp alltraps
8010662a:	e9 4f f3 ff ff       	jmp    8010597e <alltraps>

8010662f <vector203>:
.globl vector203
vector203:
  pushl $0
8010662f:	6a 00                	push   $0x0
  pushl $203
80106631:	68 cb 00 00 00       	push   $0xcb
  jmp alltraps
80106636:	e9 43 f3 ff ff       	jmp    8010597e <alltraps>

8010663b <vector204>:
.globl vector204
vector204:
  pushl $0
8010663b:	6a 00                	push   $0x0
  pushl $204
8010663d:	68 cc 00 00 00       	push   $0xcc
  jmp alltraps
80106642:	e9 37 f3 ff ff       	jmp    8010597e <alltraps>

80106647 <vector205>:
.globl vector205
vector205:
  pushl $0
80106647:	6a 00                	push   $0x0
  pushl $205
80106649:	68 cd 00 00 00       	push   $0xcd
  jmp alltraps
8010664e:	e9 2b f3 ff ff       	jmp    8010597e <alltraps>

80106653 <vector206>:
.globl vector206
vector206:
  pushl $0
80106653:	6a 00                	push   $0x0
  pushl $206
80106655:	68 ce 00 00 00       	push   $0xce
  jmp alltraps
8010665a:	e9 1f f3 ff ff       	jmp    8010597e <alltraps>

8010665f <vector207>:
.globl vector207
vector207:
  pushl $0
8010665f:	6a 00                	push   $0x0
  pushl $207
80106661:	68 cf 00 00 00       	push   $0xcf
  jmp alltraps
80106666:	e9 13 f3 ff ff       	jmp    8010597e <alltraps>

8010666b <vector208>:
.globl vector208
vector208:
  pushl $0
8010666b:	6a 00                	push   $0x0
  pushl $208
8010666d:	68 d0 00 00 00       	push   $0xd0
  jmp alltraps
80106672:	e9 07 f3 ff ff       	jmp    8010597e <alltraps>

80106677 <vector209>:
.globl vector209
vector209:
  pushl $0
80106677:	6a 00                	push   $0x0
  pushl $209
80106679:	68 d1 00 00 00       	push   $0xd1
  jmp alltraps
8010667e:	e9 fb f2 ff ff       	jmp    8010597e <alltraps>

80106683 <vector210>:
.globl vector210
vector210:
  pushl $0
80106683:	6a 00                	push   $0x0
  pushl $210
80106685:	68 d2 00 00 00       	push   $0xd2
  jmp alltraps
8010668a:	e9 ef f2 ff ff       	jmp    8010597e <alltraps>

8010668f <vector211>:
.globl vector211
vector211:
  pushl $0
8010668f:	6a 00                	push   $0x0
  pushl $211
80106691:	68 d3 00 00 00       	push   $0xd3
  jmp alltraps
80106696:	e9 e3 f2 ff ff       	jmp    8010597e <alltraps>

8010669b <vector212>:
.globl vector212
vector212:
  pushl $0
8010669b:	6a 00                	push   $0x0
  pushl $212
8010669d:	68 d4 00 00 00       	push   $0xd4
  jmp alltraps
801066a2:	e9 d7 f2 ff ff       	jmp    8010597e <alltraps>

801066a7 <vector213>:
.globl vector213
vector213:
  pushl $0
801066a7:	6a 00                	push   $0x0
  pushl $213
801066a9:	68 d5 00 00 00       	push   $0xd5
  jmp alltraps
801066ae:	e9 cb f2 ff ff       	jmp    8010597e <alltraps>

801066b3 <vector214>:
.globl vector214
vector214:
  pushl $0
801066b3:	6a 00                	push   $0x0
  pushl $214
801066b5:	68 d6 00 00 00       	push   $0xd6
  jmp alltraps
801066ba:	e9 bf f2 ff ff       	jmp    8010597e <alltraps>

801066bf <vector215>:
.globl vector215
vector215:
  pushl $0
801066bf:	6a 00                	push   $0x0
  pushl $215
801066c1:	68 d7 00 00 00       	push   $0xd7
  jmp alltraps
801066c6:	e9 b3 f2 ff ff       	jmp    8010597e <alltraps>

801066cb <vector216>:
.globl vector216
vector216:
  pushl $0
801066cb:	6a 00                	push   $0x0
  pushl $216
801066cd:	68 d8 00 00 00       	push   $0xd8
  jmp alltraps
801066d2:	e9 a7 f2 ff ff       	jmp    8010597e <alltraps>

801066d7 <vector217>:
.globl vector217
vector217:
  pushl $0
801066d7:	6a 00                	push   $0x0
  pushl $217
801066d9:	68 d9 00 00 00       	push   $0xd9
  jmp alltraps
801066de:	e9 9b f2 ff ff       	jmp    8010597e <alltraps>

801066e3 <vector218>:
.globl vector218
vector218:
  pushl $0
801066e3:	6a 00                	push   $0x0
  pushl $218
801066e5:	68 da 00 00 00       	push   $0xda
  jmp alltraps
801066ea:	e9 8f f2 ff ff       	jmp    8010597e <alltraps>

801066ef <vector219>:
.globl vector219
vector219:
  pushl $0
801066ef:	6a 00                	push   $0x0
  pushl $219
801066f1:	68 db 00 00 00       	push   $0xdb
  jmp alltraps
801066f6:	e9 83 f2 ff ff       	jmp    8010597e <alltraps>

801066fb <vector220>:
.globl vector220
vector220:
  pushl $0
801066fb:	6a 00                	push   $0x0
  pushl $220
801066fd:	68 dc 00 00 00       	push   $0xdc
  jmp alltraps
80106702:	e9 77 f2 ff ff       	jmp    8010597e <alltraps>

80106707 <vector221>:
.globl vector221
vector221:
  pushl $0
80106707:	6a 00                	push   $0x0
  pushl $221
80106709:	68 dd 00 00 00       	push   $0xdd
  jmp alltraps
8010670e:	e9 6b f2 ff ff       	jmp    8010597e <alltraps>

80106713 <vector222>:
.globl vector222
vector222:
  pushl $0
80106713:	6a 00                	push   $0x0
  pushl $222
80106715:	68 de 00 00 00       	push   $0xde
  jmp alltraps
8010671a:	e9 5f f2 ff ff       	jmp    8010597e <alltraps>

8010671f <vector223>:
.globl vector223
vector223:
  pushl $0
8010671f:	6a 00                	push   $0x0
  pushl $223
80106721:	68 df 00 00 00       	push   $0xdf
  jmp alltraps
80106726:	e9 53 f2 ff ff       	jmp    8010597e <alltraps>

8010672b <vector224>:
.globl vector224
vector224:
  pushl $0
8010672b:	6a 00                	push   $0x0
  pushl $224
8010672d:	68 e0 00 00 00       	push   $0xe0
  jmp alltraps
80106732:	e9 47 f2 ff ff       	jmp    8010597e <alltraps>

80106737 <vector225>:
.globl vector225
vector225:
  pushl $0
80106737:	6a 00                	push   $0x0
  pushl $225
80106739:	68 e1 00 00 00       	push   $0xe1
  jmp alltraps
8010673e:	e9 3b f2 ff ff       	jmp    8010597e <alltraps>

80106743 <vector226>:
.globl vector226
vector226:
  pushl $0
80106743:	6a 00                	push   $0x0
  pushl $226
80106745:	68 e2 00 00 00       	push   $0xe2
  jmp alltraps
8010674a:	e9 2f f2 ff ff       	jmp    8010597e <alltraps>

8010674f <vector227>:
.globl vector227
vector227:
  pushl $0
8010674f:	6a 00                	push   $0x0
  pushl $227
80106751:	68 e3 00 00 00       	push   $0xe3
  jmp alltraps
80106756:	e9 23 f2 ff ff       	jmp    8010597e <alltraps>

8010675b <vector228>:
.globl vector228
vector228:
  pushl $0
8010675b:	6a 00                	push   $0x0
  pushl $228
8010675d:	68 e4 00 00 00       	push   $0xe4
  jmp alltraps
80106762:	e9 17 f2 ff ff       	jmp    8010597e <alltraps>

80106767 <vector229>:
.globl vector229
vector229:
  pushl $0
80106767:	6a 00                	push   $0x0
  pushl $229
80106769:	68 e5 00 00 00       	push   $0xe5
  jmp alltraps
8010676e:	e9 0b f2 ff ff       	jmp    8010597e <alltraps>

80106773 <vector230>:
.globl vector230
vector230:
  pushl $0
80106773:	6a 00                	push   $0x0
  pushl $230
80106775:	68 e6 00 00 00       	push   $0xe6
  jmp alltraps
8010677a:	e9 ff f1 ff ff       	jmp    8010597e <alltraps>

8010677f <vector231>:
.globl vector231
vector231:
  pushl $0
8010677f:	6a 00                	push   $0x0
  pushl $231
80106781:	68 e7 00 00 00       	push   $0xe7
  jmp alltraps
80106786:	e9 f3 f1 ff ff       	jmp    8010597e <alltraps>

8010678b <vector232>:
.globl vector232
vector232:
  pushl $0
8010678b:	6a 00                	push   $0x0
  pushl $232
8010678d:	68 e8 00 00 00       	push   $0xe8
  jmp alltraps
80106792:	e9 e7 f1 ff ff       	jmp    8010597e <alltraps>

80106797 <vector233>:
.globl vector233
vector233:
  pushl $0
80106797:	6a 00                	push   $0x0
  pushl $233
80106799:	68 e9 00 00 00       	push   $0xe9
  jmp alltraps
8010679e:	e9 db f1 ff ff       	jmp    8010597e <alltraps>

801067a3 <vector234>:
.globl vector234
vector234:
  pushl $0
801067a3:	6a 00                	push   $0x0
  pushl $234
801067a5:	68 ea 00 00 00       	push   $0xea
  jmp alltraps
801067aa:	e9 cf f1 ff ff       	jmp    8010597e <alltraps>

801067af <vector235>:
.globl vector235
vector235:
  pushl $0
801067af:	6a 00                	push   $0x0
  pushl $235
801067b1:	68 eb 00 00 00       	push   $0xeb
  jmp alltraps
801067b6:	e9 c3 f1 ff ff       	jmp    8010597e <alltraps>

801067bb <vector236>:
.globl vector236
vector236:
  pushl $0
801067bb:	6a 00                	push   $0x0
  pushl $236
801067bd:	68 ec 00 00 00       	push   $0xec
  jmp alltraps
801067c2:	e9 b7 f1 ff ff       	jmp    8010597e <alltraps>

801067c7 <vector237>:
.globl vector237
vector237:
  pushl $0
801067c7:	6a 00                	push   $0x0
  pushl $237
801067c9:	68 ed 00 00 00       	push   $0xed
  jmp alltraps
801067ce:	e9 ab f1 ff ff       	jmp    8010597e <alltraps>

801067d3 <vector238>:
.globl vector238
vector238:
  pushl $0
801067d3:	6a 00                	push   $0x0
  pushl $238
801067d5:	68 ee 00 00 00       	push   $0xee
  jmp alltraps
801067da:	e9 9f f1 ff ff       	jmp    8010597e <alltraps>

801067df <vector239>:
.globl vector239
vector239:
  pushl $0
801067df:	6a 00                	push   $0x0
  pushl $239
801067e1:	68 ef 00 00 00       	push   $0xef
  jmp alltraps
801067e6:	e9 93 f1 ff ff       	jmp    8010597e <alltraps>

801067eb <vector240>:
.globl vector240
vector240:
  pushl $0
801067eb:	6a 00                	push   $0x0
  pushl $240
801067ed:	68 f0 00 00 00       	push   $0xf0
  jmp alltraps
801067f2:	e9 87 f1 ff ff       	jmp    8010597e <alltraps>

801067f7 <vector241>:
.globl vector241
vector241:
  pushl $0
801067f7:	6a 00                	push   $0x0
  pushl $241
801067f9:	68 f1 00 00 00       	push   $0xf1
  jmp alltraps
801067fe:	e9 7b f1 ff ff       	jmp    8010597e <alltraps>

80106803 <vector242>:
.globl vector242
vector242:
  pushl $0
80106803:	6a 00                	push   $0x0
  pushl $242
80106805:	68 f2 00 00 00       	push   $0xf2
  jmp alltraps
8010680a:	e9 6f f1 ff ff       	jmp    8010597e <alltraps>

8010680f <vector243>:
.globl vector243
vector243:
  pushl $0
8010680f:	6a 00                	push   $0x0
  pushl $243
80106811:	68 f3 00 00 00       	push   $0xf3
  jmp alltraps
80106816:	e9 63 f1 ff ff       	jmp    8010597e <alltraps>

8010681b <vector244>:
.globl vector244
vector244:
  pushl $0
8010681b:	6a 00                	push   $0x0
  pushl $244
8010681d:	68 f4 00 00 00       	push   $0xf4
  jmp alltraps
80106822:	e9 57 f1 ff ff       	jmp    8010597e <alltraps>

80106827 <vector245>:
.globl vector245
vector245:
  pushl $0
80106827:	6a 00                	push   $0x0
  pushl $245
80106829:	68 f5 00 00 00       	push   $0xf5
  jmp alltraps
8010682e:	e9 4b f1 ff ff       	jmp    8010597e <alltraps>

80106833 <vector246>:
.globl vector246
vector246:
  pushl $0
80106833:	6a 00                	push   $0x0
  pushl $246
80106835:	68 f6 00 00 00       	push   $0xf6
  jmp alltraps
8010683a:	e9 3f f1 ff ff       	jmp    8010597e <alltraps>

8010683f <vector247>:
.globl vector247
vector247:
  pushl $0
8010683f:	6a 00                	push   $0x0
  pushl $247
80106841:	68 f7 00 00 00       	push   $0xf7
  jmp alltraps
80106846:	e9 33 f1 ff ff       	jmp    8010597e <alltraps>

8010684b <vector248>:
.globl vector248
vector248:
  pushl $0
8010684b:	6a 00                	push   $0x0
  pushl $248
8010684d:	68 f8 00 00 00       	push   $0xf8
  jmp alltraps
80106852:	e9 27 f1 ff ff       	jmp    8010597e <alltraps>

80106857 <vector249>:
.globl vector249
vector249:
  pushl $0
80106857:	6a 00                	push   $0x0
  pushl $249
80106859:	68 f9 00 00 00       	push   $0xf9
  jmp alltraps
8010685e:	e9 1b f1 ff ff       	jmp    8010597e <alltraps>

80106863 <vector250>:
.globl vector250
vector250:
  pushl $0
80106863:	6a 00                	push   $0x0
  pushl $250
80106865:	68 fa 00 00 00       	push   $0xfa
  jmp alltraps
8010686a:	e9 0f f1 ff ff       	jmp    8010597e <alltraps>

8010686f <vector251>:
.globl vector251
vector251:
  pushl $0
8010686f:	6a 00                	push   $0x0
  pushl $251
80106871:	68 fb 00 00 00       	push   $0xfb
  jmp alltraps
80106876:	e9 03 f1 ff ff       	jmp    8010597e <alltraps>

8010687b <vector252>:
.globl vector252
vector252:
  pushl $0
8010687b:	6a 00                	push   $0x0
  pushl $252
8010687d:	68 fc 00 00 00       	push   $0xfc
  jmp alltraps
80106882:	e9 f7 f0 ff ff       	jmp    8010597e <alltraps>

80106887 <vector253>:
.globl vector253
vector253:
  pushl $0
80106887:	6a 00                	push   $0x0
  pushl $253
80106889:	68 fd 00 00 00       	push   $0xfd
  jmp alltraps
8010688e:	e9 eb f0 ff ff       	jmp    8010597e <alltraps>

80106893 <vector254>:
.globl vector254
vector254:
  pushl $0
80106893:	6a 00                	push   $0x0
  pushl $254
80106895:	68 fe 00 00 00       	push   $0xfe
  jmp alltraps
8010689a:	e9 df f0 ff ff       	jmp    8010597e <alltraps>

8010689f <vector255>:
.globl vector255
vector255:
  pushl $0
8010689f:	6a 00                	push   $0x0
  pushl $255
801068a1:	68 ff 00 00 00       	push   $0xff
  jmp alltraps
801068a6:	e9 d3 f0 ff ff       	jmp    8010597e <alltraps>
801068ab:	66 90                	xchg   %ax,%ax
801068ad:	66 90                	xchg   %ax,%ax
801068af:	90                   	nop

801068b0 <walkpgdir>:
// Return the address of the PTE in page table pgdir
// that corresponds to virtual address va.  If alloc!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int alloc)
{
801068b0:	55                   	push   %ebp
801068b1:	89 e5                	mov    %esp,%ebp
801068b3:	57                   	push   %edi
801068b4:	56                   	push   %esi
801068b5:	89 d6                	mov    %edx,%esi
  pde_t *pde;
  pte_t *pgtab;

  pde = &pgdir[PDX(va)];
801068b7:	c1 ea 16             	shr    $0x16,%edx
{
801068ba:	53                   	push   %ebx
  pde = &pgdir[PDX(va)];
801068bb:	8d 3c 90             	lea    (%eax,%edx,4),%edi
{
801068be:	83 ec 0c             	sub    $0xc,%esp
  if(*pde & PTE_P){
801068c1:	8b 1f                	mov    (%edi),%ebx
801068c3:	f6 c3 01             	test   $0x1,%bl
801068c6:	74 28                	je     801068f0 <walkpgdir+0x40>
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
801068c8:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
801068ce:	81 c3 00 00 00 80    	add    $0x80000000,%ebx
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table
    // entries, if necessary.
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
  }
  return &pgtab[PTX(va)];
801068d4:	89 f0                	mov    %esi,%eax
}
801068d6:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return &pgtab[PTX(va)];
801068d9:	c1 e8 0a             	shr    $0xa,%eax
801068dc:	25 fc 0f 00 00       	and    $0xffc,%eax
801068e1:	01 d8                	add    %ebx,%eax
}
801068e3:	5b                   	pop    %ebx
801068e4:	5e                   	pop    %esi
801068e5:	5f                   	pop    %edi
801068e6:	5d                   	pop    %ebp
801068e7:	c3                   	ret    
801068e8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801068ef:	90                   	nop
    if(!alloc || (pgtab = (pte_t*)kalloc()) == 0)
801068f0:	85 c9                	test   %ecx,%ecx
801068f2:	74 2c                	je     80106920 <walkpgdir+0x70>
801068f4:	e8 57 bd ff ff       	call   80102650 <kalloc>
801068f9:	89 c3                	mov    %eax,%ebx
801068fb:	85 c0                	test   %eax,%eax
801068fd:	74 21                	je     80106920 <walkpgdir+0x70>
    memset(pgtab, 0, PGSIZE);
801068ff:	83 ec 04             	sub    $0x4,%esp
80106902:	68 00 10 00 00       	push   $0x1000
80106907:	6a 00                	push   $0x0
80106909:	50                   	push   %eax
8010690a:	e8 71 de ff ff       	call   80104780 <memset>
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
8010690f:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
80106915:	83 c4 10             	add    $0x10,%esp
80106918:	83 c8 07             	or     $0x7,%eax
8010691b:	89 07                	mov    %eax,(%edi)
8010691d:	eb b5                	jmp    801068d4 <walkpgdir+0x24>
8010691f:	90                   	nop
}
80106920:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return 0;
80106923:	31 c0                	xor    %eax,%eax
}
80106925:	5b                   	pop    %ebx
80106926:	5e                   	pop    %esi
80106927:	5f                   	pop    %edi
80106928:	5d                   	pop    %ebp
80106929:	c3                   	ret    
8010692a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80106930 <mappages>:
// Create PTEs for virtual addresses starting at va that refer to
// physical addresses starting at pa. va and size might not
// be page-aligned.
static int
mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm)
{
80106930:	55                   	push   %ebp
80106931:	89 e5                	mov    %esp,%ebp
80106933:	57                   	push   %edi
80106934:	89 c7                	mov    %eax,%edi
  char *a, *last;
  pte_t *pte;

  a = (char*)PGROUNDDOWN((uint)va);
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
80106936:	8d 44 0a ff          	lea    -0x1(%edx,%ecx,1),%eax
{
8010693a:	56                   	push   %esi
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
8010693b:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  a = (char*)PGROUNDDOWN((uint)va);
80106940:	89 d6                	mov    %edx,%esi
{
80106942:	53                   	push   %ebx
  a = (char*)PGROUNDDOWN((uint)va);
80106943:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
{
80106949:	83 ec 1c             	sub    $0x1c,%esp
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
8010694c:	89 45 e0             	mov    %eax,-0x20(%ebp)
8010694f:	8b 45 08             	mov    0x8(%ebp),%eax
80106952:	29 f0                	sub    %esi,%eax
80106954:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80106957:	eb 1f                	jmp    80106978 <mappages+0x48>
80106959:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  for(;;){
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
      return -1;
    if(*pte & PTE_P)
80106960:	f6 00 01             	testb  $0x1,(%eax)
80106963:	75 45                	jne    801069aa <mappages+0x7a>
      panic("remap");
    *pte = pa | perm | PTE_P;
80106965:	0b 5d 0c             	or     0xc(%ebp),%ebx
80106968:	83 cb 01             	or     $0x1,%ebx
8010696b:	89 18                	mov    %ebx,(%eax)
    if(a == last)
8010696d:	3b 75 e0             	cmp    -0x20(%ebp),%esi
80106970:	74 2e                	je     801069a0 <mappages+0x70>
      break;
    a += PGSIZE;
80106972:	81 c6 00 10 00 00    	add    $0x1000,%esi
  for(;;){
80106978:	8b 45 e4             	mov    -0x1c(%ebp),%eax
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
8010697b:	b9 01 00 00 00       	mov    $0x1,%ecx
80106980:	89 f2                	mov    %esi,%edx
80106982:	8d 1c 06             	lea    (%esi,%eax,1),%ebx
80106985:	89 f8                	mov    %edi,%eax
80106987:	e8 24 ff ff ff       	call   801068b0 <walkpgdir>
8010698c:	85 c0                	test   %eax,%eax
8010698e:	75 d0                	jne    80106960 <mappages+0x30>
    pa += PGSIZE;
  }
  return 0;
}
80106990:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return -1;
80106993:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80106998:	5b                   	pop    %ebx
80106999:	5e                   	pop    %esi
8010699a:	5f                   	pop    %edi
8010699b:	5d                   	pop    %ebp
8010699c:	c3                   	ret    
8010699d:	8d 76 00             	lea    0x0(%esi),%esi
801069a0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
801069a3:	31 c0                	xor    %eax,%eax
}
801069a5:	5b                   	pop    %ebx
801069a6:	5e                   	pop    %esi
801069a7:	5f                   	pop    %edi
801069a8:	5d                   	pop    %ebp
801069a9:	c3                   	ret    
      panic("remap");
801069aa:	83 ec 0c             	sub    $0xc,%esp
801069ad:	68 08 7d 10 80       	push   $0x80107d08
801069b2:	e8 d9 99 ff ff       	call   80100390 <panic>
801069b7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801069be:	66 90                	xchg   %ax,%ax

801069c0 <deallocuvm.part.0>:
// Deallocate user pages to bring the process size from oldsz to
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
801069c0:	55                   	push   %ebp
801069c1:	89 e5                	mov    %esp,%ebp
801069c3:	57                   	push   %edi
801069c4:	56                   	push   %esi
801069c5:	89 c6                	mov    %eax,%esi
801069c7:	53                   	push   %ebx
801069c8:	89 d3                	mov    %edx,%ebx
  uint a, pa;

  if(newsz >= oldsz)
    return oldsz;

  a = PGROUNDUP(newsz);
801069ca:	8d 91 ff 0f 00 00    	lea    0xfff(%ecx),%edx
801069d0:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
801069d6:	83 ec 1c             	sub    $0x1c,%esp
801069d9:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  for(; a  < oldsz; a += PGSIZE){
801069dc:	39 da                	cmp    %ebx,%edx
801069de:	73 5b                	jae    80106a3b <deallocuvm.part.0+0x7b>
801069e0:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
801069e3:	89 d7                	mov    %edx,%edi
801069e5:	eb 14                	jmp    801069fb <deallocuvm.part.0+0x3b>
801069e7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801069ee:	66 90                	xchg   %ax,%ax
801069f0:	81 c7 00 10 00 00    	add    $0x1000,%edi
801069f6:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
801069f9:	76 40                	jbe    80106a3b <deallocuvm.part.0+0x7b>
    pte = walkpgdir(pgdir, (char*)a, 0);
801069fb:	31 c9                	xor    %ecx,%ecx
801069fd:	89 fa                	mov    %edi,%edx
801069ff:	89 f0                	mov    %esi,%eax
80106a01:	e8 aa fe ff ff       	call   801068b0 <walkpgdir>
80106a06:	89 c3                	mov    %eax,%ebx
    if(!pte)
80106a08:	85 c0                	test   %eax,%eax
80106a0a:	74 44                	je     80106a50 <deallocuvm.part.0+0x90>
      a = PGADDR(PDX(a) + 1, 0, 0) - PGSIZE;
    else if((*pte & PTE_P) != 0){
80106a0c:	8b 00                	mov    (%eax),%eax
80106a0e:	a8 01                	test   $0x1,%al
80106a10:	74 de                	je     801069f0 <deallocuvm.part.0+0x30>
      pa = PTE_ADDR(*pte);
      if(pa == 0)
80106a12:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80106a17:	74 47                	je     80106a60 <deallocuvm.part.0+0xa0>
        panic("kfree");
      char *v = P2V(pa);
      kfree(v);
80106a19:	83 ec 0c             	sub    $0xc,%esp
      char *v = P2V(pa);
80106a1c:	05 00 00 00 80       	add    $0x80000000,%eax
80106a21:	81 c7 00 10 00 00    	add    $0x1000,%edi
      kfree(v);
80106a27:	50                   	push   %eax
80106a28:	e8 63 ba ff ff       	call   80102490 <kfree>
      *pte = 0;
80106a2d:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
80106a33:	83 c4 10             	add    $0x10,%esp
  for(; a  < oldsz; a += PGSIZE){
80106a36:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
80106a39:	77 c0                	ja     801069fb <deallocuvm.part.0+0x3b>
    }
  }
  return newsz;
}
80106a3b:	8b 45 e0             	mov    -0x20(%ebp),%eax
80106a3e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106a41:	5b                   	pop    %ebx
80106a42:	5e                   	pop    %esi
80106a43:	5f                   	pop    %edi
80106a44:	5d                   	pop    %ebp
80106a45:	c3                   	ret    
80106a46:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106a4d:	8d 76 00             	lea    0x0(%esi),%esi
      a = PGADDR(PDX(a) + 1, 0, 0) - PGSIZE;
80106a50:	89 fa                	mov    %edi,%edx
80106a52:	81 e2 00 00 c0 ff    	and    $0xffc00000,%edx
80106a58:	8d ba 00 00 40 00    	lea    0x400000(%edx),%edi
80106a5e:	eb 96                	jmp    801069f6 <deallocuvm.part.0+0x36>
        panic("kfree");
80106a60:	83 ec 0c             	sub    $0xc,%esp
80106a63:	68 66 74 10 80       	push   $0x80107466
80106a68:	e8 23 99 ff ff       	call   80100390 <panic>
80106a6d:	8d 76 00             	lea    0x0(%esi),%esi

80106a70 <seginit>:
{
80106a70:	f3 0f 1e fb          	endbr32 
80106a74:	55                   	push   %ebp
80106a75:	89 e5                	mov    %esp,%ebp
80106a77:	83 ec 18             	sub    $0x18,%esp
  c = &cpus[cpuid()];
80106a7a:	e8 61 cf ff ff       	call   801039e0 <cpuid>
  pd[0] = size-1;
80106a7f:	ba 2f 00 00 00       	mov    $0x2f,%edx
80106a84:	69 c0 b0 00 00 00    	imul   $0xb0,%eax,%eax
80106a8a:	66 89 55 f2          	mov    %dx,-0xe(%ebp)
  c->gdt[SEG_KCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, 0);
80106a8e:	c7 80 f8 27 11 80 ff 	movl   $0xffff,-0x7feed808(%eax)
80106a95:	ff 00 00 
80106a98:	c7 80 fc 27 11 80 00 	movl   $0xcf9a00,-0x7feed804(%eax)
80106a9f:	9a cf 00 
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
80106aa2:	c7 80 00 28 11 80 ff 	movl   $0xffff,-0x7feed800(%eax)
80106aa9:	ff 00 00 
80106aac:	c7 80 04 28 11 80 00 	movl   $0xcf9200,-0x7feed7fc(%eax)
80106ab3:	92 cf 00 
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
80106ab6:	c7 80 08 28 11 80 ff 	movl   $0xffff,-0x7feed7f8(%eax)
80106abd:	ff 00 00 
80106ac0:	c7 80 0c 28 11 80 00 	movl   $0xcffa00,-0x7feed7f4(%eax)
80106ac7:	fa cf 00 
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
80106aca:	c7 80 10 28 11 80 ff 	movl   $0xffff,-0x7feed7f0(%eax)
80106ad1:	ff 00 00 
80106ad4:	c7 80 14 28 11 80 00 	movl   $0xcff200,-0x7feed7ec(%eax)
80106adb:	f2 cf 00 
  lgdt(c->gdt, sizeof(c->gdt));
80106ade:	05 f0 27 11 80       	add    $0x801127f0,%eax
  pd[1] = (uint)p;
80106ae3:	66 89 45 f4          	mov    %ax,-0xc(%ebp)
  pd[2] = (uint)p >> 16;
80106ae7:	c1 e8 10             	shr    $0x10,%eax
80106aea:	66 89 45 f6          	mov    %ax,-0xa(%ebp)
  asm volatile("lgdt (%0)" : : "r" (pd));
80106aee:	8d 45 f2             	lea    -0xe(%ebp),%eax
80106af1:	0f 01 10             	lgdtl  (%eax)
}
80106af4:	c9                   	leave  
80106af5:	c3                   	ret    
80106af6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106afd:	8d 76 00             	lea    0x0(%esi),%esi

80106b00 <switchkvm>:
{
80106b00:	f3 0f 1e fb          	endbr32 
  lcr3(V2P(kpgdir));   // switch to the kernel page table
80106b04:	a1 a4 54 11 80       	mov    0x801154a4,%eax
80106b09:	05 00 00 00 80       	add    $0x80000000,%eax
}

static inline void
lcr3(uint val)
{
  asm volatile("movl %0,%%cr3" : : "r" (val));
80106b0e:	0f 22 d8             	mov    %eax,%cr3
}
80106b11:	c3                   	ret    
80106b12:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106b19:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80106b20 <switchuvm>:
{
80106b20:	f3 0f 1e fb          	endbr32 
80106b24:	55                   	push   %ebp
80106b25:	89 e5                	mov    %esp,%ebp
80106b27:	57                   	push   %edi
80106b28:	56                   	push   %esi
80106b29:	53                   	push   %ebx
80106b2a:	83 ec 1c             	sub    $0x1c,%esp
80106b2d:	8b 75 08             	mov    0x8(%ebp),%esi
  if(p == 0)
80106b30:	85 f6                	test   %esi,%esi
80106b32:	0f 84 cb 00 00 00    	je     80106c03 <switchuvm+0xe3>
  if(p->kstack == 0)
80106b38:	8b 46 08             	mov    0x8(%esi),%eax
80106b3b:	85 c0                	test   %eax,%eax
80106b3d:	0f 84 da 00 00 00    	je     80106c1d <switchuvm+0xfd>
  if(p->pgdir == 0)
80106b43:	8b 46 04             	mov    0x4(%esi),%eax
80106b46:	85 c0                	test   %eax,%eax
80106b48:	0f 84 c2 00 00 00    	je     80106c10 <switchuvm+0xf0>
  pushcli();
80106b4e:	e8 1d da ff ff       	call   80104570 <pushcli>
  mycpu()->gdt[SEG_TSS] = SEG16(STS_T32A, &mycpu()->ts,
80106b53:	e8 18 ce ff ff       	call   80103970 <mycpu>
80106b58:	89 c3                	mov    %eax,%ebx
80106b5a:	e8 11 ce ff ff       	call   80103970 <mycpu>
80106b5f:	89 c7                	mov    %eax,%edi
80106b61:	e8 0a ce ff ff       	call   80103970 <mycpu>
80106b66:	83 c7 08             	add    $0x8,%edi
80106b69:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80106b6c:	e8 ff cd ff ff       	call   80103970 <mycpu>
80106b71:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80106b74:	ba 67 00 00 00       	mov    $0x67,%edx
80106b79:	66 89 bb 9a 00 00 00 	mov    %di,0x9a(%ebx)
80106b80:	83 c0 08             	add    $0x8,%eax
80106b83:	66 89 93 98 00 00 00 	mov    %dx,0x98(%ebx)
  mycpu()->ts.iomb = (ushort) 0xFFFF;
80106b8a:	bf ff ff ff ff       	mov    $0xffffffff,%edi
  mycpu()->gdt[SEG_TSS] = SEG16(STS_T32A, &mycpu()->ts,
80106b8f:	83 c1 08             	add    $0x8,%ecx
80106b92:	c1 e8 18             	shr    $0x18,%eax
80106b95:	c1 e9 10             	shr    $0x10,%ecx
80106b98:	88 83 9f 00 00 00    	mov    %al,0x9f(%ebx)
80106b9e:	88 8b 9c 00 00 00    	mov    %cl,0x9c(%ebx)
80106ba4:	b9 99 40 00 00       	mov    $0x4099,%ecx
80106ba9:	66 89 8b 9d 00 00 00 	mov    %cx,0x9d(%ebx)
  mycpu()->ts.ss0 = SEG_KDATA << 3;
80106bb0:	bb 10 00 00 00       	mov    $0x10,%ebx
  mycpu()->gdt[SEG_TSS].s = 0;
80106bb5:	e8 b6 cd ff ff       	call   80103970 <mycpu>
80106bba:	80 a0 9d 00 00 00 ef 	andb   $0xef,0x9d(%eax)
  mycpu()->ts.ss0 = SEG_KDATA << 3;
80106bc1:	e8 aa cd ff ff       	call   80103970 <mycpu>
80106bc6:	66 89 58 10          	mov    %bx,0x10(%eax)
  mycpu()->ts.esp0 = (uint)p->kstack + KSTACKSIZE;
80106bca:	8b 5e 08             	mov    0x8(%esi),%ebx
80106bcd:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80106bd3:	e8 98 cd ff ff       	call   80103970 <mycpu>
80106bd8:	89 58 0c             	mov    %ebx,0xc(%eax)
  mycpu()->ts.iomb = (ushort) 0xFFFF;
80106bdb:	e8 90 cd ff ff       	call   80103970 <mycpu>
80106be0:	66 89 78 6e          	mov    %di,0x6e(%eax)
  asm volatile("ltr %0" : : "r" (sel));
80106be4:	b8 28 00 00 00       	mov    $0x28,%eax
80106be9:	0f 00 d8             	ltr    %ax
  lcr3(V2P(p->pgdir));  // switch to process's address space
80106bec:	8b 46 04             	mov    0x4(%esi),%eax
80106bef:	05 00 00 00 80       	add    $0x80000000,%eax
  asm volatile("movl %0,%%cr3" : : "r" (val));
80106bf4:	0f 22 d8             	mov    %eax,%cr3
}
80106bf7:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106bfa:	5b                   	pop    %ebx
80106bfb:	5e                   	pop    %esi
80106bfc:	5f                   	pop    %edi
80106bfd:	5d                   	pop    %ebp
  popcli();
80106bfe:	e9 bd d9 ff ff       	jmp    801045c0 <popcli>
    panic("switchuvm: no process");
80106c03:	83 ec 0c             	sub    $0xc,%esp
80106c06:	68 0e 7d 10 80       	push   $0x80107d0e
80106c0b:	e8 80 97 ff ff       	call   80100390 <panic>
    panic("switchuvm: no pgdir");
80106c10:	83 ec 0c             	sub    $0xc,%esp
80106c13:	68 39 7d 10 80       	push   $0x80107d39
80106c18:	e8 73 97 ff ff       	call   80100390 <panic>
    panic("switchuvm: no kstack");
80106c1d:	83 ec 0c             	sub    $0xc,%esp
80106c20:	68 24 7d 10 80       	push   $0x80107d24
80106c25:	e8 66 97 ff ff       	call   80100390 <panic>
80106c2a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80106c30 <inituvm>:
{
80106c30:	f3 0f 1e fb          	endbr32 
80106c34:	55                   	push   %ebp
80106c35:	89 e5                	mov    %esp,%ebp
80106c37:	57                   	push   %edi
80106c38:	56                   	push   %esi
80106c39:	53                   	push   %ebx
80106c3a:	83 ec 1c             	sub    $0x1c,%esp
80106c3d:	8b 45 0c             	mov    0xc(%ebp),%eax
80106c40:	8b 75 10             	mov    0x10(%ebp),%esi
80106c43:	8b 7d 08             	mov    0x8(%ebp),%edi
80106c46:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if(sz >= PGSIZE)
80106c49:	81 fe ff 0f 00 00    	cmp    $0xfff,%esi
80106c4f:	77 4b                	ja     80106c9c <inituvm+0x6c>
  mem = kalloc();
80106c51:	e8 fa b9 ff ff       	call   80102650 <kalloc>
  memset(mem, 0, PGSIZE);
80106c56:	83 ec 04             	sub    $0x4,%esp
80106c59:	68 00 10 00 00       	push   $0x1000
  mem = kalloc();
80106c5e:	89 c3                	mov    %eax,%ebx
  memset(mem, 0, PGSIZE);
80106c60:	6a 00                	push   $0x0
80106c62:	50                   	push   %eax
80106c63:	e8 18 db ff ff       	call   80104780 <memset>
  mappages(pgdir, 0, PGSIZE, V2P(mem), PTE_W|PTE_U);
80106c68:	58                   	pop    %eax
80106c69:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
80106c6f:	5a                   	pop    %edx
80106c70:	6a 06                	push   $0x6
80106c72:	b9 00 10 00 00       	mov    $0x1000,%ecx
80106c77:	31 d2                	xor    %edx,%edx
80106c79:	50                   	push   %eax
80106c7a:	89 f8                	mov    %edi,%eax
80106c7c:	e8 af fc ff ff       	call   80106930 <mappages>
  memmove(mem, init, sz);
80106c81:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106c84:	89 75 10             	mov    %esi,0x10(%ebp)
80106c87:	83 c4 10             	add    $0x10,%esp
80106c8a:	89 5d 08             	mov    %ebx,0x8(%ebp)
80106c8d:	89 45 0c             	mov    %eax,0xc(%ebp)
}
80106c90:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106c93:	5b                   	pop    %ebx
80106c94:	5e                   	pop    %esi
80106c95:	5f                   	pop    %edi
80106c96:	5d                   	pop    %ebp
  memmove(mem, init, sz);
80106c97:	e9 84 db ff ff       	jmp    80104820 <memmove>
    panic("inituvm: more than a page");
80106c9c:	83 ec 0c             	sub    $0xc,%esp
80106c9f:	68 4d 7d 10 80       	push   $0x80107d4d
80106ca4:	e8 e7 96 ff ff       	call   80100390 <panic>
80106ca9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80106cb0 <loaduvm>:
{
80106cb0:	f3 0f 1e fb          	endbr32 
80106cb4:	55                   	push   %ebp
80106cb5:	89 e5                	mov    %esp,%ebp
80106cb7:	57                   	push   %edi
80106cb8:	56                   	push   %esi
80106cb9:	53                   	push   %ebx
80106cba:	83 ec 1c             	sub    $0x1c,%esp
80106cbd:	8b 45 0c             	mov    0xc(%ebp),%eax
80106cc0:	8b 75 18             	mov    0x18(%ebp),%esi
  if((uint) addr % PGSIZE != 0)
80106cc3:	a9 ff 0f 00 00       	test   $0xfff,%eax
80106cc8:	0f 85 99 00 00 00    	jne    80106d67 <loaduvm+0xb7>
  for(i = 0; i < sz; i += PGSIZE){
80106cce:	01 f0                	add    %esi,%eax
80106cd0:	89 f3                	mov    %esi,%ebx
80106cd2:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(readi(ip, P2V(pa), offset+i, n) != n)
80106cd5:	8b 45 14             	mov    0x14(%ebp),%eax
80106cd8:	01 f0                	add    %esi,%eax
80106cda:	89 45 e0             	mov    %eax,-0x20(%ebp)
  for(i = 0; i < sz; i += PGSIZE){
80106cdd:	85 f6                	test   %esi,%esi
80106cdf:	75 15                	jne    80106cf6 <loaduvm+0x46>
80106ce1:	eb 6d                	jmp    80106d50 <loaduvm+0xa0>
80106ce3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80106ce7:	90                   	nop
80106ce8:	81 eb 00 10 00 00    	sub    $0x1000,%ebx
80106cee:	89 f0                	mov    %esi,%eax
80106cf0:	29 d8                	sub    %ebx,%eax
80106cf2:	39 c6                	cmp    %eax,%esi
80106cf4:	76 5a                	jbe    80106d50 <loaduvm+0xa0>
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
80106cf6:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80106cf9:	8b 45 08             	mov    0x8(%ebp),%eax
80106cfc:	31 c9                	xor    %ecx,%ecx
80106cfe:	29 da                	sub    %ebx,%edx
80106d00:	e8 ab fb ff ff       	call   801068b0 <walkpgdir>
80106d05:	85 c0                	test   %eax,%eax
80106d07:	74 51                	je     80106d5a <loaduvm+0xaa>
    pa = PTE_ADDR(*pte);
80106d09:	8b 00                	mov    (%eax),%eax
    if(readi(ip, P2V(pa), offset+i, n) != n)
80106d0b:	8b 4d e0             	mov    -0x20(%ebp),%ecx
    if(sz - i < PGSIZE)
80106d0e:	bf 00 10 00 00       	mov    $0x1000,%edi
    pa = PTE_ADDR(*pte);
80106d13:	25 00 f0 ff ff       	and    $0xfffff000,%eax
    if(sz - i < PGSIZE)
80106d18:	81 fb ff 0f 00 00    	cmp    $0xfff,%ebx
80106d1e:	0f 46 fb             	cmovbe %ebx,%edi
    if(readi(ip, P2V(pa), offset+i, n) != n)
80106d21:	29 d9                	sub    %ebx,%ecx
80106d23:	05 00 00 00 80       	add    $0x80000000,%eax
80106d28:	57                   	push   %edi
80106d29:	51                   	push   %ecx
80106d2a:	50                   	push   %eax
80106d2b:	ff 75 10             	pushl  0x10(%ebp)
80106d2e:	e8 4d ad ff ff       	call   80101a80 <readi>
80106d33:	83 c4 10             	add    $0x10,%esp
80106d36:	39 f8                	cmp    %edi,%eax
80106d38:	74 ae                	je     80106ce8 <loaduvm+0x38>
}
80106d3a:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return -1;
80106d3d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80106d42:	5b                   	pop    %ebx
80106d43:	5e                   	pop    %esi
80106d44:	5f                   	pop    %edi
80106d45:	5d                   	pop    %ebp
80106d46:	c3                   	ret    
80106d47:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106d4e:	66 90                	xchg   %ax,%ax
80106d50:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
80106d53:	31 c0                	xor    %eax,%eax
}
80106d55:	5b                   	pop    %ebx
80106d56:	5e                   	pop    %esi
80106d57:	5f                   	pop    %edi
80106d58:	5d                   	pop    %ebp
80106d59:	c3                   	ret    
      panic("loaduvm: address should exist");
80106d5a:	83 ec 0c             	sub    $0xc,%esp
80106d5d:	68 67 7d 10 80       	push   $0x80107d67
80106d62:	e8 29 96 ff ff       	call   80100390 <panic>
    panic("loaduvm: addr must be page aligned");
80106d67:	83 ec 0c             	sub    $0xc,%esp
80106d6a:	68 08 7e 10 80       	push   $0x80107e08
80106d6f:	e8 1c 96 ff ff       	call   80100390 <panic>
80106d74:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106d7b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80106d7f:	90                   	nop

80106d80 <allocuvm>:
{
80106d80:	f3 0f 1e fb          	endbr32 
80106d84:	55                   	push   %ebp
80106d85:	89 e5                	mov    %esp,%ebp
80106d87:	57                   	push   %edi
80106d88:	56                   	push   %esi
80106d89:	53                   	push   %ebx
80106d8a:	83 ec 1c             	sub    $0x1c,%esp
  if(newsz >= KERNBASE)
80106d8d:	8b 45 10             	mov    0x10(%ebp),%eax
{
80106d90:	8b 7d 08             	mov    0x8(%ebp),%edi
  if(newsz >= KERNBASE)
80106d93:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80106d96:	85 c0                	test   %eax,%eax
80106d98:	0f 88 b2 00 00 00    	js     80106e50 <allocuvm+0xd0>
  if(newsz < oldsz)
80106d9e:	3b 45 0c             	cmp    0xc(%ebp),%eax
    return oldsz;
80106da1:	8b 45 0c             	mov    0xc(%ebp),%eax
  if(newsz < oldsz)
80106da4:	0f 82 96 00 00 00    	jb     80106e40 <allocuvm+0xc0>
  a = PGROUNDUP(oldsz);
80106daa:	8d b0 ff 0f 00 00    	lea    0xfff(%eax),%esi
80106db0:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
  for(; a < newsz; a += PGSIZE){
80106db6:	39 75 10             	cmp    %esi,0x10(%ebp)
80106db9:	77 40                	ja     80106dfb <allocuvm+0x7b>
80106dbb:	e9 83 00 00 00       	jmp    80106e43 <allocuvm+0xc3>
    memset(mem, 0, PGSIZE);
80106dc0:	83 ec 04             	sub    $0x4,%esp
80106dc3:	68 00 10 00 00       	push   $0x1000
80106dc8:	6a 00                	push   $0x0
80106dca:	50                   	push   %eax
80106dcb:	e8 b0 d9 ff ff       	call   80104780 <memset>
    if(mappages(pgdir, (char*)a, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0){
80106dd0:	58                   	pop    %eax
80106dd1:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
80106dd7:	5a                   	pop    %edx
80106dd8:	6a 06                	push   $0x6
80106dda:	b9 00 10 00 00       	mov    $0x1000,%ecx
80106ddf:	89 f2                	mov    %esi,%edx
80106de1:	50                   	push   %eax
80106de2:	89 f8                	mov    %edi,%eax
80106de4:	e8 47 fb ff ff       	call   80106930 <mappages>
80106de9:	83 c4 10             	add    $0x10,%esp
80106dec:	85 c0                	test   %eax,%eax
80106dee:	78 78                	js     80106e68 <allocuvm+0xe8>
  for(; a < newsz; a += PGSIZE){
80106df0:	81 c6 00 10 00 00    	add    $0x1000,%esi
80106df6:	39 75 10             	cmp    %esi,0x10(%ebp)
80106df9:	76 48                	jbe    80106e43 <allocuvm+0xc3>
    mem = kalloc();
80106dfb:	e8 50 b8 ff ff       	call   80102650 <kalloc>
80106e00:	89 c3                	mov    %eax,%ebx
    if(mem == 0){
80106e02:	85 c0                	test   %eax,%eax
80106e04:	75 ba                	jne    80106dc0 <allocuvm+0x40>
      cprintf("allocuvm out of memory\n");
80106e06:	83 ec 0c             	sub    $0xc,%esp
80106e09:	68 85 7d 10 80       	push   $0x80107d85
80106e0e:	e8 9d 98 ff ff       	call   801006b0 <cprintf>
  if(newsz >= oldsz)
80106e13:	8b 45 0c             	mov    0xc(%ebp),%eax
80106e16:	83 c4 10             	add    $0x10,%esp
80106e19:	39 45 10             	cmp    %eax,0x10(%ebp)
80106e1c:	74 32                	je     80106e50 <allocuvm+0xd0>
80106e1e:	8b 55 10             	mov    0x10(%ebp),%edx
80106e21:	89 c1                	mov    %eax,%ecx
80106e23:	89 f8                	mov    %edi,%eax
80106e25:	e8 96 fb ff ff       	call   801069c0 <deallocuvm.part.0>
      return 0;
80106e2a:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
}
80106e31:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106e34:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106e37:	5b                   	pop    %ebx
80106e38:	5e                   	pop    %esi
80106e39:	5f                   	pop    %edi
80106e3a:	5d                   	pop    %ebp
80106e3b:	c3                   	ret    
80106e3c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return oldsz;
80106e40:	89 45 e4             	mov    %eax,-0x1c(%ebp)
}
80106e43:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106e46:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106e49:	5b                   	pop    %ebx
80106e4a:	5e                   	pop    %esi
80106e4b:	5f                   	pop    %edi
80106e4c:	5d                   	pop    %ebp
80106e4d:	c3                   	ret    
80106e4e:	66 90                	xchg   %ax,%ax
    return 0;
80106e50:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
}
80106e57:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106e5a:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106e5d:	5b                   	pop    %ebx
80106e5e:	5e                   	pop    %esi
80106e5f:	5f                   	pop    %edi
80106e60:	5d                   	pop    %ebp
80106e61:	c3                   	ret    
80106e62:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      cprintf("allocuvm out of memory (2)\n");
80106e68:	83 ec 0c             	sub    $0xc,%esp
80106e6b:	68 9d 7d 10 80       	push   $0x80107d9d
80106e70:	e8 3b 98 ff ff       	call   801006b0 <cprintf>
  if(newsz >= oldsz)
80106e75:	8b 45 0c             	mov    0xc(%ebp),%eax
80106e78:	83 c4 10             	add    $0x10,%esp
80106e7b:	39 45 10             	cmp    %eax,0x10(%ebp)
80106e7e:	74 0c                	je     80106e8c <allocuvm+0x10c>
80106e80:	8b 55 10             	mov    0x10(%ebp),%edx
80106e83:	89 c1                	mov    %eax,%ecx
80106e85:	89 f8                	mov    %edi,%eax
80106e87:	e8 34 fb ff ff       	call   801069c0 <deallocuvm.part.0>
      kfree(mem);
80106e8c:	83 ec 0c             	sub    $0xc,%esp
80106e8f:	53                   	push   %ebx
80106e90:	e8 fb b5 ff ff       	call   80102490 <kfree>
      return 0;
80106e95:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
80106e9c:	83 c4 10             	add    $0x10,%esp
}
80106e9f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106ea2:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106ea5:	5b                   	pop    %ebx
80106ea6:	5e                   	pop    %esi
80106ea7:	5f                   	pop    %edi
80106ea8:	5d                   	pop    %ebp
80106ea9:	c3                   	ret    
80106eaa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80106eb0 <deallocuvm>:
{
80106eb0:	f3 0f 1e fb          	endbr32 
80106eb4:	55                   	push   %ebp
80106eb5:	89 e5                	mov    %esp,%ebp
80106eb7:	8b 55 0c             	mov    0xc(%ebp),%edx
80106eba:	8b 4d 10             	mov    0x10(%ebp),%ecx
80106ebd:	8b 45 08             	mov    0x8(%ebp),%eax
  if(newsz >= oldsz)
80106ec0:	39 d1                	cmp    %edx,%ecx
80106ec2:	73 0c                	jae    80106ed0 <deallocuvm+0x20>
}
80106ec4:	5d                   	pop    %ebp
80106ec5:	e9 f6 fa ff ff       	jmp    801069c0 <deallocuvm.part.0>
80106eca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80106ed0:	89 d0                	mov    %edx,%eax
80106ed2:	5d                   	pop    %ebp
80106ed3:	c3                   	ret    
80106ed4:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106edb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80106edf:	90                   	nop

80106ee0 <freevm>:

// Free a page table and all the physical memory pages
// in the user part.
void
freevm(pde_t *pgdir)
{
80106ee0:	f3 0f 1e fb          	endbr32 
80106ee4:	55                   	push   %ebp
80106ee5:	89 e5                	mov    %esp,%ebp
80106ee7:	57                   	push   %edi
80106ee8:	56                   	push   %esi
80106ee9:	53                   	push   %ebx
80106eea:	83 ec 0c             	sub    $0xc,%esp
80106eed:	8b 75 08             	mov    0x8(%ebp),%esi
  uint i;

  if(pgdir == 0)
80106ef0:	85 f6                	test   %esi,%esi
80106ef2:	74 55                	je     80106f49 <freevm+0x69>
  if(newsz >= oldsz)
80106ef4:	31 c9                	xor    %ecx,%ecx
80106ef6:	ba 00 00 00 80       	mov    $0x80000000,%edx
80106efb:	89 f0                	mov    %esi,%eax
80106efd:	89 f3                	mov    %esi,%ebx
80106eff:	e8 bc fa ff ff       	call   801069c0 <deallocuvm.part.0>
    panic("freevm: no pgdir");
  deallocuvm(pgdir, KERNBASE, 0);
  for(i = 0; i < NPDENTRIES; i++){
80106f04:	8d be 00 10 00 00    	lea    0x1000(%esi),%edi
80106f0a:	eb 0b                	jmp    80106f17 <freevm+0x37>
80106f0c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80106f10:	83 c3 04             	add    $0x4,%ebx
80106f13:	39 df                	cmp    %ebx,%edi
80106f15:	74 23                	je     80106f3a <freevm+0x5a>
    if(pgdir[i] & PTE_P){
80106f17:	8b 03                	mov    (%ebx),%eax
80106f19:	a8 01                	test   $0x1,%al
80106f1b:	74 f3                	je     80106f10 <freevm+0x30>
      char * v = P2V(PTE_ADDR(pgdir[i]));
80106f1d:	25 00 f0 ff ff       	and    $0xfffff000,%eax
      kfree(v);
80106f22:	83 ec 0c             	sub    $0xc,%esp
80106f25:	83 c3 04             	add    $0x4,%ebx
      char * v = P2V(PTE_ADDR(pgdir[i]));
80106f28:	05 00 00 00 80       	add    $0x80000000,%eax
      kfree(v);
80106f2d:	50                   	push   %eax
80106f2e:	e8 5d b5 ff ff       	call   80102490 <kfree>
80106f33:	83 c4 10             	add    $0x10,%esp
  for(i = 0; i < NPDENTRIES; i++){
80106f36:	39 df                	cmp    %ebx,%edi
80106f38:	75 dd                	jne    80106f17 <freevm+0x37>
    }
  }
  kfree((char*)pgdir);
80106f3a:	89 75 08             	mov    %esi,0x8(%ebp)
}
80106f3d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106f40:	5b                   	pop    %ebx
80106f41:	5e                   	pop    %esi
80106f42:	5f                   	pop    %edi
80106f43:	5d                   	pop    %ebp
  kfree((char*)pgdir);
80106f44:	e9 47 b5 ff ff       	jmp    80102490 <kfree>
    panic("freevm: no pgdir");
80106f49:	83 ec 0c             	sub    $0xc,%esp
80106f4c:	68 b9 7d 10 80       	push   $0x80107db9
80106f51:	e8 3a 94 ff ff       	call   80100390 <panic>
80106f56:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106f5d:	8d 76 00             	lea    0x0(%esi),%esi

80106f60 <setupkvm>:
{
80106f60:	f3 0f 1e fb          	endbr32 
80106f64:	55                   	push   %ebp
80106f65:	89 e5                	mov    %esp,%ebp
80106f67:	56                   	push   %esi
80106f68:	53                   	push   %ebx
  if((pgdir = (pde_t*)kalloc()) == 0)
80106f69:	e8 e2 b6 ff ff       	call   80102650 <kalloc>
80106f6e:	89 c6                	mov    %eax,%esi
80106f70:	85 c0                	test   %eax,%eax
80106f72:	74 42                	je     80106fb6 <setupkvm+0x56>
  memset(pgdir, 0, PGSIZE);
80106f74:	83 ec 04             	sub    $0x4,%esp
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
80106f77:	bb 20 a4 10 80       	mov    $0x8010a420,%ebx
  memset(pgdir, 0, PGSIZE);
80106f7c:	68 00 10 00 00       	push   $0x1000
80106f81:	6a 00                	push   $0x0
80106f83:	50                   	push   %eax
80106f84:	e8 f7 d7 ff ff       	call   80104780 <memset>
80106f89:	83 c4 10             	add    $0x10,%esp
                (uint)k->phys_start, k->perm) < 0) {
80106f8c:	8b 43 04             	mov    0x4(%ebx),%eax
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start,
80106f8f:	83 ec 08             	sub    $0x8,%esp
80106f92:	8b 4b 08             	mov    0x8(%ebx),%ecx
80106f95:	ff 73 0c             	pushl  0xc(%ebx)
80106f98:	8b 13                	mov    (%ebx),%edx
80106f9a:	50                   	push   %eax
80106f9b:	29 c1                	sub    %eax,%ecx
80106f9d:	89 f0                	mov    %esi,%eax
80106f9f:	e8 8c f9 ff ff       	call   80106930 <mappages>
80106fa4:	83 c4 10             	add    $0x10,%esp
80106fa7:	85 c0                	test   %eax,%eax
80106fa9:	78 15                	js     80106fc0 <setupkvm+0x60>
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
80106fab:	83 c3 10             	add    $0x10,%ebx
80106fae:	81 fb 60 a4 10 80    	cmp    $0x8010a460,%ebx
80106fb4:	75 d6                	jne    80106f8c <setupkvm+0x2c>
}
80106fb6:	8d 65 f8             	lea    -0x8(%ebp),%esp
80106fb9:	89 f0                	mov    %esi,%eax
80106fbb:	5b                   	pop    %ebx
80106fbc:	5e                   	pop    %esi
80106fbd:	5d                   	pop    %ebp
80106fbe:	c3                   	ret    
80106fbf:	90                   	nop
      freevm(pgdir);
80106fc0:	83 ec 0c             	sub    $0xc,%esp
80106fc3:	56                   	push   %esi
      return 0;
80106fc4:	31 f6                	xor    %esi,%esi
      freevm(pgdir);
80106fc6:	e8 15 ff ff ff       	call   80106ee0 <freevm>
      return 0;
80106fcb:	83 c4 10             	add    $0x10,%esp
}
80106fce:	8d 65 f8             	lea    -0x8(%ebp),%esp
80106fd1:	89 f0                	mov    %esi,%eax
80106fd3:	5b                   	pop    %ebx
80106fd4:	5e                   	pop    %esi
80106fd5:	5d                   	pop    %ebp
80106fd6:	c3                   	ret    
80106fd7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106fde:	66 90                	xchg   %ax,%ax

80106fe0 <kvmalloc>:
{
80106fe0:	f3 0f 1e fb          	endbr32 
80106fe4:	55                   	push   %ebp
80106fe5:	89 e5                	mov    %esp,%ebp
80106fe7:	83 ec 08             	sub    $0x8,%esp
  kpgdir = setupkvm();
80106fea:	e8 71 ff ff ff       	call   80106f60 <setupkvm>
80106fef:	a3 a4 54 11 80       	mov    %eax,0x801154a4
  lcr3(V2P(kpgdir));   // switch to the kernel page table
80106ff4:	05 00 00 00 80       	add    $0x80000000,%eax
80106ff9:	0f 22 d8             	mov    %eax,%cr3
}
80106ffc:	c9                   	leave  
80106ffd:	c3                   	ret    
80106ffe:	66 90                	xchg   %ax,%ax

80107000 <clearpteu>:

// Clear PTE_U on a page. Used to create an inaccessible
// page beneath the user stack.
void
clearpteu(pde_t *pgdir, char *uva)
{
80107000:	f3 0f 1e fb          	endbr32 
80107004:	55                   	push   %ebp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80107005:	31 c9                	xor    %ecx,%ecx
{
80107007:	89 e5                	mov    %esp,%ebp
80107009:	83 ec 08             	sub    $0x8,%esp
  pte = walkpgdir(pgdir, uva, 0);
8010700c:	8b 55 0c             	mov    0xc(%ebp),%edx
8010700f:	8b 45 08             	mov    0x8(%ebp),%eax
80107012:	e8 99 f8 ff ff       	call   801068b0 <walkpgdir>
  if(pte == 0)
80107017:	85 c0                	test   %eax,%eax
80107019:	74 05                	je     80107020 <clearpteu+0x20>
    panic("clearpteu");
  *pte &= ~PTE_U;
8010701b:	83 20 fb             	andl   $0xfffffffb,(%eax)
}
8010701e:	c9                   	leave  
8010701f:	c3                   	ret    
    panic("clearpteu");
80107020:	83 ec 0c             	sub    $0xc,%esp
80107023:	68 ca 7d 10 80       	push   $0x80107dca
80107028:	e8 63 93 ff ff       	call   80100390 <panic>
8010702d:	8d 76 00             	lea    0x0(%esi),%esi

80107030 <copyuvm>:

// Given a parent process's page table, create a copy
// of it for a child.
pde_t*
copyuvm(pde_t *pgdir, uint sz)
{
80107030:	f3 0f 1e fb          	endbr32 
80107034:	55                   	push   %ebp
80107035:	89 e5                	mov    %esp,%ebp
80107037:	57                   	push   %edi
80107038:	56                   	push   %esi
80107039:	53                   	push   %ebx
8010703a:	83 ec 1c             	sub    $0x1c,%esp
  pde_t *d;
  pte_t *pte;
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
8010703d:	e8 1e ff ff ff       	call   80106f60 <setupkvm>
80107042:	89 45 e0             	mov    %eax,-0x20(%ebp)
80107045:	85 c0                	test   %eax,%eax
80107047:	0f 84 9b 00 00 00    	je     801070e8 <copyuvm+0xb8>
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
8010704d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80107050:	85 c9                	test   %ecx,%ecx
80107052:	0f 84 90 00 00 00    	je     801070e8 <copyuvm+0xb8>
80107058:	31 f6                	xor    %esi,%esi
8010705a:	eb 46                	jmp    801070a2 <copyuvm+0x72>
8010705c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      panic("copyuvm: page not present");
    pa = PTE_ADDR(*pte);
    flags = PTE_FLAGS(*pte);
    if((mem = kalloc()) == 0)
      goto bad;
    memmove(mem, (char*)P2V(pa), PGSIZE);
80107060:	83 ec 04             	sub    $0x4,%esp
80107063:	81 c7 00 00 00 80    	add    $0x80000000,%edi
80107069:	68 00 10 00 00       	push   $0x1000
8010706e:	57                   	push   %edi
8010706f:	50                   	push   %eax
80107070:	e8 ab d7 ff ff       	call   80104820 <memmove>
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0) {
80107075:	58                   	pop    %eax
80107076:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
8010707c:	5a                   	pop    %edx
8010707d:	ff 75 e4             	pushl  -0x1c(%ebp)
80107080:	b9 00 10 00 00       	mov    $0x1000,%ecx
80107085:	89 f2                	mov    %esi,%edx
80107087:	50                   	push   %eax
80107088:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010708b:	e8 a0 f8 ff ff       	call   80106930 <mappages>
80107090:	83 c4 10             	add    $0x10,%esp
80107093:	85 c0                	test   %eax,%eax
80107095:	78 61                	js     801070f8 <copyuvm+0xc8>
  for(i = 0; i < sz; i += PGSIZE){
80107097:	81 c6 00 10 00 00    	add    $0x1000,%esi
8010709d:	39 75 0c             	cmp    %esi,0xc(%ebp)
801070a0:	76 46                	jbe    801070e8 <copyuvm+0xb8>
    if((pte = walkpgdir(pgdir, (void *) i, 0)) == 0)
801070a2:	8b 45 08             	mov    0x8(%ebp),%eax
801070a5:	31 c9                	xor    %ecx,%ecx
801070a7:	89 f2                	mov    %esi,%edx
801070a9:	e8 02 f8 ff ff       	call   801068b0 <walkpgdir>
801070ae:	85 c0                	test   %eax,%eax
801070b0:	74 61                	je     80107113 <copyuvm+0xe3>
    if(!(*pte & PTE_P))
801070b2:	8b 00                	mov    (%eax),%eax
801070b4:	a8 01                	test   $0x1,%al
801070b6:	74 4e                	je     80107106 <copyuvm+0xd6>
    pa = PTE_ADDR(*pte);
801070b8:	89 c7                	mov    %eax,%edi
    flags = PTE_FLAGS(*pte);
801070ba:	25 ff 0f 00 00       	and    $0xfff,%eax
801070bf:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    pa = PTE_ADDR(*pte);
801070c2:	81 e7 00 f0 ff ff    	and    $0xfffff000,%edi
    if((mem = kalloc()) == 0)
801070c8:	e8 83 b5 ff ff       	call   80102650 <kalloc>
801070cd:	89 c3                	mov    %eax,%ebx
801070cf:	85 c0                	test   %eax,%eax
801070d1:	75 8d                	jne    80107060 <copyuvm+0x30>
    }
  }
  return d;

bad:
  freevm(d);
801070d3:	83 ec 0c             	sub    $0xc,%esp
801070d6:	ff 75 e0             	pushl  -0x20(%ebp)
801070d9:	e8 02 fe ff ff       	call   80106ee0 <freevm>
  return 0;
801070de:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
801070e5:	83 c4 10             	add    $0x10,%esp
}
801070e8:	8b 45 e0             	mov    -0x20(%ebp),%eax
801070eb:	8d 65 f4             	lea    -0xc(%ebp),%esp
801070ee:	5b                   	pop    %ebx
801070ef:	5e                   	pop    %esi
801070f0:	5f                   	pop    %edi
801070f1:	5d                   	pop    %ebp
801070f2:	c3                   	ret    
801070f3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801070f7:	90                   	nop
      kfree(mem);
801070f8:	83 ec 0c             	sub    $0xc,%esp
801070fb:	53                   	push   %ebx
801070fc:	e8 8f b3 ff ff       	call   80102490 <kfree>
      goto bad;
80107101:	83 c4 10             	add    $0x10,%esp
80107104:	eb cd                	jmp    801070d3 <copyuvm+0xa3>
      panic("copyuvm: page not present");
80107106:	83 ec 0c             	sub    $0xc,%esp
80107109:	68 ee 7d 10 80       	push   $0x80107dee
8010710e:	e8 7d 92 ff ff       	call   80100390 <panic>
      panic("copyuvm: pte should exist");
80107113:	83 ec 0c             	sub    $0xc,%esp
80107116:	68 d4 7d 10 80       	push   $0x80107dd4
8010711b:	e8 70 92 ff ff       	call   80100390 <panic>

80107120 <uva2ka>:

//PAGEBREAK!
// Map user virtual address to kernel address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
80107120:	f3 0f 1e fb          	endbr32 
80107124:	55                   	push   %ebp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80107125:	31 c9                	xor    %ecx,%ecx
{
80107127:	89 e5                	mov    %esp,%ebp
80107129:	83 ec 08             	sub    $0x8,%esp
  pte = walkpgdir(pgdir, uva, 0);
8010712c:	8b 55 0c             	mov    0xc(%ebp),%edx
8010712f:	8b 45 08             	mov    0x8(%ebp),%eax
80107132:	e8 79 f7 ff ff       	call   801068b0 <walkpgdir>
  if((*pte & PTE_P) == 0)
80107137:	8b 00                	mov    (%eax),%eax
    return 0;
  if((*pte & PTE_U) == 0)
    return 0;
  return (char*)P2V(PTE_ADDR(*pte));
}
80107139:	c9                   	leave  
  if((*pte & PTE_U) == 0)
8010713a:	89 c2                	mov    %eax,%edx
  return (char*)P2V(PTE_ADDR(*pte));
8010713c:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  if((*pte & PTE_U) == 0)
80107141:	83 e2 05             	and    $0x5,%edx
  return (char*)P2V(PTE_ADDR(*pte));
80107144:	05 00 00 00 80       	add    $0x80000000,%eax
80107149:	83 fa 05             	cmp    $0x5,%edx
8010714c:	ba 00 00 00 00       	mov    $0x0,%edx
80107151:	0f 45 c2             	cmovne %edx,%eax
}
80107154:	c3                   	ret    
80107155:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010715c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80107160 <copyout>:
// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
80107160:	f3 0f 1e fb          	endbr32 
80107164:	55                   	push   %ebp
80107165:	89 e5                	mov    %esp,%ebp
80107167:	57                   	push   %edi
80107168:	56                   	push   %esi
80107169:	53                   	push   %ebx
8010716a:	83 ec 0c             	sub    $0xc,%esp
8010716d:	8b 75 14             	mov    0x14(%ebp),%esi
80107170:	8b 55 0c             	mov    0xc(%ebp),%edx
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
  while(len > 0){
80107173:	85 f6                	test   %esi,%esi
80107175:	75 3c                	jne    801071b3 <copyout+0x53>
80107177:	eb 67                	jmp    801071e0 <copyout+0x80>
80107179:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    va0 = (uint)PGROUNDDOWN(va);
    pa0 = uva2ka(pgdir, (char*)va0);
    if(pa0 == 0)
      return -1;
    n = PGSIZE - (va - va0);
80107180:	8b 55 0c             	mov    0xc(%ebp),%edx
80107183:	89 fb                	mov    %edi,%ebx
80107185:	29 d3                	sub    %edx,%ebx
80107187:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    if(n > len)
8010718d:	39 f3                	cmp    %esi,%ebx
8010718f:	0f 47 de             	cmova  %esi,%ebx
      n = len;
    memmove(pa0 + (va - va0), buf, n);
80107192:	29 fa                	sub    %edi,%edx
80107194:	83 ec 04             	sub    $0x4,%esp
80107197:	01 c2                	add    %eax,%edx
80107199:	53                   	push   %ebx
8010719a:	ff 75 10             	pushl  0x10(%ebp)
8010719d:	52                   	push   %edx
8010719e:	e8 7d d6 ff ff       	call   80104820 <memmove>
    len -= n;
    buf += n;
801071a3:	01 5d 10             	add    %ebx,0x10(%ebp)
    va = va0 + PGSIZE;
801071a6:	8d 97 00 10 00 00    	lea    0x1000(%edi),%edx
  while(len > 0){
801071ac:	83 c4 10             	add    $0x10,%esp
801071af:	29 de                	sub    %ebx,%esi
801071b1:	74 2d                	je     801071e0 <copyout+0x80>
    va0 = (uint)PGROUNDDOWN(va);
801071b3:	89 d7                	mov    %edx,%edi
    pa0 = uva2ka(pgdir, (char*)va0);
801071b5:	83 ec 08             	sub    $0x8,%esp
    va0 = (uint)PGROUNDDOWN(va);
801071b8:	89 55 0c             	mov    %edx,0xc(%ebp)
801071bb:	81 e7 00 f0 ff ff    	and    $0xfffff000,%edi
    pa0 = uva2ka(pgdir, (char*)va0);
801071c1:	57                   	push   %edi
801071c2:	ff 75 08             	pushl  0x8(%ebp)
801071c5:	e8 56 ff ff ff       	call   80107120 <uva2ka>
    if(pa0 == 0)
801071ca:	83 c4 10             	add    $0x10,%esp
801071cd:	85 c0                	test   %eax,%eax
801071cf:	75 af                	jne    80107180 <copyout+0x20>
  }
  return 0;
}
801071d1:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return -1;
801071d4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801071d9:	5b                   	pop    %ebx
801071da:	5e                   	pop    %esi
801071db:	5f                   	pop    %edi
801071dc:	5d                   	pop    %ebp
801071dd:	c3                   	ret    
801071de:	66 90                	xchg   %ax,%ax
801071e0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
801071e3:	31 c0                	xor    %eax,%eax
}
801071e5:	5b                   	pop    %ebx
801071e6:	5e                   	pop    %esi
801071e7:	5f                   	pop    %edi
801071e8:	5d                   	pop    %ebp
801071e9:	c3                   	ret    
